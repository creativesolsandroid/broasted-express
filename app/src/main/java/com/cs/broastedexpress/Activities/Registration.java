package com.cs.broastedexpress.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.cs.broastedexpress.Activities.SplashActivity.regId;

public class Registration extends AppCompatActivity {
    EditText mFirstName, mFamilyName, mNickName, mPhoneNumber, mEmail, mPassword, mConfirmPassword;
    TextView mGenderMale, mGenderFemale, mGenderMale1, mGenderFemale1, termsText, signUpBtn;
    CheckBox terms;
    private String firstName, familyName, nickName, phoneNumber, email, password, confirmPassword, gender = "Male";
    String response, language;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_registration);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_registration_arabic);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();

        mFirstName = (EditText) findViewById(R.id.name);
        mFamilyName = (EditText) findViewById(R.id.family_name);
        mNickName = (EditText) findViewById(R.id.nick_name);
        mPhoneNumber = (EditText) findViewById(R.id.mobile_number);
        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        mConfirmPassword = (EditText) findViewById(R.id.confirm_password);

        mGenderMale = (TextView) findViewById(R.id.gender_male);
        mGenderFemale = (TextView) findViewById(R.id.gender_female);
        mGenderMale1 = (TextView) findViewById(R.id.gender_male1);
        mGenderFemale1 = (TextView) findViewById(R.id.gender_female1);
        termsText = (TextView) findViewById(R.id.register_terms_text);
        signUpBtn = (TextView) findViewById(R.id.signup_btn);

        terms = (CheckBox) findViewById(R.id.terms);

        termsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(Registration.this, WebViewActivity.class);
                loginIntent.putExtra("webview_toshow", "register_terms");
                startActivity(loginIntent);
            }
        });

        mGenderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender = "Male";
                mGenderMale1.setVisibility(View.VISIBLE);
                mGenderFemale1.setVisibility(View.GONE);
            }
        });
        mGenderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender = "Female";
                mGenderFemale1.setVisibility(View.VISIBLE);
                mGenderMale1.setVisibility(View.GONE);
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject parent = new JSONObject();

                firstName = mFirstName.getText().toString();
                familyName = mFamilyName.getText().toString();
                nickName = mNickName.getText().toString();
                phoneNumber = mPhoneNumber.getText().toString();
                email = mEmail.getText().toString().replaceAll("", "");
                password = mPassword.getText().toString();
                confirmPassword = mConfirmPassword.getText().toString();

                if (firstName.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        mFirstName.setError("Please enter First Name");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mFirstName.setError("من فضلك ارسل الاسم بالكامل");
                    }
                } else if (phoneNumber.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        mPhoneNumber.setError("Please enter Mobile Number");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mPhoneNumber.setError("من فضلك أدخل رقم الجوال");
                    }
                } else if (phoneNumber.length() != 9) {
                    if (language.equalsIgnoreCase("En")) {
                        mPhoneNumber.setError("Please enter valid Mobile Number");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mPhoneNumber.setError("من فضلك ادخل رقم جوال صحيح");
                    }

                } else if (email.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        mEmail.setError("Please enter Email");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mEmail.setError("من فضلك ادخل البريد الالكتروني");
                    }
                } else if (!isValidEmail(email)) {
                    if (language.equalsIgnoreCase("En")) {
                        mEmail.setError("Please use Email format (example - abc@abc.com");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mEmail.setError("من فضلك استخدم صيغة البريد الالكتروني (example - abc@abc.com)");
                    }

                } else if (password.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        mPassword.setError("Please enter Password");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mPassword.setError("من فضلك ادخل كلمة السر");
                    }
                } else if (password.length() < 8) {
                    if (language.equalsIgnoreCase("En")) {
                        mPassword.setError("Password must be at least 8 characters");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mPassword.setError("الرقم السري يجب أن يحتوي على الأقل على 8 حروف");
                    }
                } else if (!confirmPassword.equals(password)) {
                    if (language.equalsIgnoreCase("En")) {
                        mConfirmPassword.setError("Passwords not match, please retype");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mConfirmPassword.setError("كلمة المرور غير صحيحة ، من فضلك أعد الإدخال ");
                    }

                } else if (!terms.isChecked()) {
                    AlertDialog.Builder alertdialogbulder = new AlertDialog.Builder(Registration.this);

                    if (language.equalsIgnoreCase("En")) {

                        alertdialogbulder.setTitle("Boasted Express");
                        alertdialogbulder.setMessage("Please review and accept the terms services");
                        alertdialogbulder.setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertdialogbulder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertdialogbulder
                                .setMessage("من فضلك راجع وأقبل شروط الخدمة ")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                    AlertDialog alertdialog = alertdialogbulder.create();
                    alertdialog.show();
                } else {
                    try {
                        JSONArray mainItem = new JSONArray();


                        JSONObject mainObj = new JSONObject();
                        mainObj.put("FullName", firstName);
                        mainObj.put("FamilyName", familyName);
                        mainObj.put("NickName", nickName);
                        mainObj.put("Gender", gender);
                        mainObj.put("Email", email);
                        mainObj.put("Mobile", "966" + phoneNumber);
                        mainObj.put("Password", password);
                        mainObj.put("DeviceToken", regId);
                        mainObj.put("DeviceType", "Android");
                        mainObj.put("Language", language);
                        mainItem.put(mainObj);


                        parent.put("UserDetails", mainItem);
                        Log.i("Tag", parent.toString());

                    } catch (JSONException e) {
                    }
                    new InsertRegistration().execute(parent.toString());
                }
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (resultCode == RESULT_CANCELED) {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    public class InsertRegistration extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(Registration.this);
            dialog = ProgressDialog.show(Registration.this, "",
                    "Registering...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        HttpClient httpclient = new DefaultHttpClient();

                        HttpPost httpPost = new HttpPost(Projecturl.REGISTRATION_URL);

                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        httpPost.setEntity(se);

                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        inputStream = httpResponse.getEntity().getContent();

                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.e("TAG", "" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {

                return "no internet";
            }
        }


        @Override
        public void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(Registration.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(Registration.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");


                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    if (isVerified) {
                                        userPrefEditor.putString("login_status", "loggedin");
                                        userPrefEditor.commit();
//                                        Intent loginIntent = new Intent();
                                        setResult(RESULT_OK);
                                        finish();
                                    } else {
                                        Intent loginIntent = new Intent(Registration.this, Verify_Random_number.class);
                                        loginIntent.putExtra("phone_number", mobile);
                                        startActivityForResult(loginIntent, 2);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException je) {

                                String msg = jo.getString("Failure");
                                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(new ContextThemeWrapper(Registration.this, android.R.style.Theme_Material_Light_Dialog));

                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("Broasted Express");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(msg)
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle("بروستد إكسبريس");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage(msg)
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }


                                // create alert dialog
                                android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(Registration.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
