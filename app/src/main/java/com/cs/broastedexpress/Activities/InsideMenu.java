package com.cs.broastedexpress.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.Adapter.InsideMenuAdapter;
import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.InsideMenuCategories;
import com.cs.broastedexpress.Model.InsideMenuValues;
import com.cs.broastedexpress.Model.MenuItems;
import com.cs.broastedexpress.Model.Modifiers;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

public class InsideMenu extends AppCompatActivity {

    RelativeLayout mcount;

    DecimalFormat decimalFormat = new DecimalFormat("0.00");
    public static TextView morder_quantity, morder_price, mcount_basket;
    TextView mheadtitle;
    RelativeLayout minside_boasted, minside_boasted1, minside_fish, minside_fish1, minside_nugget, minside_nugget1, minside_kids, minside_kids1, minside_side, minside_side1, minside_drink, minside_drink1, minside_family, minside_family1, minside_combo, minside_combo1, minside_promatoin, minside_pro1;
    LinearLayout mcheck_out;
    RelativeLayout mlayout_broasted, mlayout_fish, mlayout_nugget, mlayout_drink, mlayout_family, mlayout_combo, mlayout_promo, mlayout_kids;
    ArrayList<InsideMenuCategories> insideMenuCategories = new ArrayList<>();
    //    ArrayList<InsideMenuValues> insideMenuValues = new ArrayList<>();
    public static ArrayList<Modifiers> additional_list = new ArrayList<>();

    ListView minside_list_view;
    int catid;
    private InsideMenuAdapter madapter;
    private DataBaseHelper mydatabase;
    public static float itemPrice, finalPrice;
    Context context;
    String response;
    SharedPreferences languagePrefs;
    String language;
    HorizontalScrollView hsv;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_inside_menu);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_inside_menu_arabic);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mydatabase = new DataBaseHelper(InsideMenu.this);

        catid = getIntent().getExtras().getInt("catid");
        Log.e("TAG", "onCreate: " + catid);

//        this.getIntent().getExtras().getInt("catid",1);

        hsv = (HorizontalScrollView) findViewById(R.id.horizontal);

        mlayout_drink = (RelativeLayout) findViewById(R.id.layout_drink);
        mlayout_family = (RelativeLayout) findViewById(R.id.layout_family);
        mlayout_combo = (RelativeLayout) findViewById(R.id.layout_sandwich);
        mlayout_promo = (RelativeLayout) findViewById(R.id.layout_promo);
        mlayout_broasted = (RelativeLayout) findViewById(R.id.layout_broasted);
        mlayout_fish = (RelativeLayout) findViewById(R.id.layout_fish);
        mlayout_nugget = (RelativeLayout) findViewById(R.id.layout_nugget);
        mlayout_kids = (RelativeLayout) findViewById(R.id.layout_kids);
        mcount= (RelativeLayout) findViewById(R.id.count);

        minside_boasted = (RelativeLayout) findViewById(R.id.inside_boasted);
        minside_boasted1 = (RelativeLayout) findViewById(R.id.inside_boasted1);
        minside_fish = (RelativeLayout) findViewById(R.id.inside_fish);
        minside_fish1 = (RelativeLayout) findViewById(R.id.inside_fish1);
        minside_nugget = (RelativeLayout) findViewById(R.id.inside_nugget);
        minside_nugget1 = (RelativeLayout) findViewById(R.id.inside_nugget1);
        minside_kids = (RelativeLayout) findViewById(R.id.inside_kids);
        minside_kids1 = (RelativeLayout) findViewById(R.id.inside_kids1);
        minside_side = (RelativeLayout) findViewById(R.id.inside_side);
        minside_side1 = (RelativeLayout) findViewById(R.id.inside_side1);
        minside_drink = (RelativeLayout) findViewById(R.id.inside_drink);
        minside_drink1 = (RelativeLayout) findViewById(R.id.inside_drink1);
        minside_family = (RelativeLayout) findViewById(R.id.inside_family);
        minside_family1 = (RelativeLayout) findViewById(R.id.inside_family1);
        minside_combo = (RelativeLayout) findViewById(R.id.inside_sandwich);
        minside_combo1 = (RelativeLayout) findViewById(R.id.inside_sandwich1);
        minside_promatoin = (RelativeLayout) findViewById(R.id.inside_promo);
        minside_pro1 = (RelativeLayout) findViewById(R.id.inside_promo1);

        mcheck_out = (LinearLayout) findViewById(R.id.check_out);
//        mregular= (LinearLayout) findViewById(R.id.regular);
//        mspice= (LinearLayout) findViewById(R.id.spice);
//        mregular1= (LinearLayout) findViewById(R.id.regular1);
//        mspice1= (LinearLayout) findViewById(R.id.spice1);
//
//        minside_plus_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                count++;
//                textcount=Integer.toString(count);
//                minside_quantity.setText(textcount);
//            }
//        });
//
//        minside_sub_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (count==0){
//                }
//                else {
//                    count--;
//                    textcount = Integer.toString(count);
//                    minside_quantity.setText(textcount);
//                }
//            }
//        });
//
//        mregular.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                mregular.setBackground(getResources().getDrawable(R.drawable.circle_inside_menu_yellow));
////                mspice.setBackground(getResources().getDrawable(R.drawable.inside_menu_item_price));
//
//                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(InsideMenu.this);
//                // ...Irrelevant code for customizing the buttons and title
//                LayoutInflater inflater = getLayoutInflater();
//
//                View dialogView = inflater.inflate(R.layout.alertbox, null);
//                dialogBuilder.setView(dialogView);
//
//                ImageView mdelete= (ImageView) dialogView.findViewById(R.id.delete);
//
//                TextView madd_to_basket= (TextView) dialogView.findViewById(R.id.add_to_basket);
//                TextView mMore_than_5= (TextView) dialogView.findViewById(R.id.more_than_5);
//                final TextView mitem_count= (TextView) dialogView.findViewById(R.id.item_count);
//
//                madd_to_basket.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        alertDialog.dismiss();
//                    }
//                });
//
////                mitem_count.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        alertDialog.dismiss();
////                    }
////                });
//
//                mMore_than_5.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        alertDialog.dismiss();
//                    }
//                });
//
//                mdelete.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        alertDialog.dismiss();
//                    }
//                });
//
//                SeekBar mseekbar= (SeekBar) dialogView.findViewById(R.id.seekbar);
//                mseekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                    @Override
//                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                        int progresscount=progress;
//                        if (progresscount==0){
//                            mitem_count.setText("1pcs.");
//                        }
//                        else if (progresscount==1){
//                            mitem_count.setText("2pcs.");
//                        }
//                        else if (progresscount==2){
//                            mitem_count.setText("3pcs.");
//                        }
//                        else if (progresscount==3){
//                            mitem_count.setText("4pcs.");
//                        }
//                        else if (progresscount==4){
//                            mitem_count.setText("5pcs.");
//                        }
//
//                    }
//
//                    @Override
//                    public void onStartTrackingTouch(SeekBar seekBar) {
//
//                    }
//
//                    @Override
//                    public void onStopTrackingTouch(SeekBar seekBar) {
//
//                    }
//                });
//
//
////                        .setPositiveButton(R.id.more_than_5, new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                     Intent a=new Intent(InsideMenu.this,InsideMenu.class);
////                        startActivity(a);
////                    }
////                }).setNegativeButton(R.id.add_to_basket, new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                        Intent b=new Intent(InsideMenu.this,Checkout.class);
////                        startActivity(b);
////                    }
////                });
//
//                alertDialog = dialogBuilder.create();
//                alertDialog.show();
//
//                //Grab the window of the dialog, and change the width
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                Window window = alertDialog.getWindow();
//                lp.copyFrom(window.getAttributes());
//                //This makes the dialog take up the full width
//                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                window.setAttributes(lp);
//            }
//        });
//
//        mspice.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mregular.setBackground(getResources().getDrawable(R.drawable.circle_inside_menu_yellow1));
//                mspice.setBackground(getResources().getDrawable(R.drawable.inside_menu_item_price1));
//            }
//        });
//
//        mregular1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mregular1.setBackground(getResources().getDrawable(R.drawable.circle_inside_menu_yellow));
//                mspice1.setBackground(getResources().getDrawable(R.drawable.inside_menu_item_price));
//            }
//        });
//
//        mspice1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mregular1.setBackground(getResources().getDrawable(R.drawable.circle_inside_menu_yellow1));
//                mspice1.setBackground(getResources().getDrawable(R.drawable.inside_menu_item_price1));
//            }
//        });
        minside_list_view = (ListView) findViewById(R.id.insidelistview);

        mcount_basket = (TextView) findViewById(R.id.count_basket);
        morder_quantity = (TextView) findViewById(R.id.order_quantity);
        morder_price = (TextView) findViewById(R.id.order_price);
        mheadtitle = (TextView) findViewById(R.id.header_title);

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mydatabase.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(InsideMenu.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                }else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(InsideMenu.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have "+ mydatabase.getTotalOrderQty() +" item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent a=new Intent(InsideMenu.this,Checkout.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mydatabase.deleteOrderTable();
//                                        Checkout.morder_quantity.setText("0");
//                                        Checkout.morder_price.setText("0SR");
//                                        Checkout.mcount_basket.setText("0");
//                                        Menu.morder_quantity.setText("0");
//                                        Menu.morder_price.setText("0SR");
//                                        Menu.mcount_basket.setText("0");
                                        InsideMenu.morder_quantity.setText("0");
                                        InsideMenu.morder_price.setText("0.00");
                                        InsideMenu.mcount_basket.setText("0");
                                        Intent a=new Intent(InsideMenu.this,Menu.class);
                                        startActivity(a);
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات"+mydatabase.getTotalOrderQty()+ "لديك " )
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        mydatabase.deleteOrderTable();
//                                        Checkout.morder_quantity.setText("0");
//                                        Checkout.morder_price.setText("0ريال");
//                                        Checkout.mcount_basket.setText("0");
//                                        Menu.morder_quantity.setText("0");
//                                        Menu.morder_price.setText("0ريال");
//                                        Menu.mcount_basket.setText("0");
                                        InsideMenu.morder_quantity.setText("0");
                                        InsideMenu.morder_price.setText("0.00");
                                        InsideMenu.mcount_basket.setText("0");
                                        Intent a=new Intent(InsideMenu.this,Menu.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent a=new Intent(InsideMenu.this,Checkout.class);
                                        startActivity(a);
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                }

            }
        });

        mcheck_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mydatabase.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(InsideMenu.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                } else {
                    Intent checkoutIntent = new Intent(InsideMenu.this, Checkout.class);
                    startActivity(checkoutIntent);
                }
            }
        });

//        morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//        morder_quantity.setText("" + mydatabase.getTotalOrderQty());


        minside_boasted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catid = 1;
                minside_boasted.setVisibility(View.GONE);
                minside_fish.setVisibility(View.VISIBLE);
                minside_nugget.setVisibility(View.VISIBLE);
                minside_kids.setVisibility(View.VISIBLE);
                minside_side.setVisibility(View.VISIBLE);
                minside_drink.setVisibility(View.VISIBLE);
                minside_family.setVisibility(View.VISIBLE);
                minside_combo.setVisibility(View.VISIBLE);
                minside_promatoin.setVisibility(View.VISIBLE);
                minside_boasted1.setVisibility(View.VISIBLE);
                minside_fish1.setVisibility(View.GONE);
                minside_nugget1.setVisibility(View.GONE);
                minside_kids1.setVisibility(View.GONE);
                minside_side1.setVisibility(View.GONE);
                minside_drink1.setVisibility(View.GONE);
                minside_family1.setVisibility(View.GONE);
                minside_combo1.setVisibility(View.GONE);
                minside_pro1.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mheadtitle.setText("Broasted Chicken");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mheadtitle.setText("بروستد");
                }

//                insideMenuCategories.clear();
                try {
                    insideMenuCategories = Projecturl.MenuItems.get(0).getMenuCategories();
                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                    minside_list_view.setAdapter(madapter);
                    madapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
                }
//                new GetCategoryItems().execute(Projecturl.CATEGORY_ITEMS_URL + catid);
            }
        });
        minside_fish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catid = 2;
                minside_boasted.setVisibility(View.VISIBLE);
                minside_fish.setVisibility(View.GONE);
                minside_nugget.setVisibility(View.VISIBLE);
                minside_kids.setVisibility(View.VISIBLE);
                minside_side.setVisibility(View.VISIBLE);
                minside_drink.setVisibility(View.VISIBLE);
                minside_family.setVisibility(View.VISIBLE);
                minside_combo.setVisibility(View.VISIBLE);
                minside_promatoin.setVisibility(View.VISIBLE);
                minside_boasted1.setVisibility(View.GONE);
                minside_fish1.setVisibility(View.VISIBLE);
                minside_nugget1.setVisibility(View.GONE);
                minside_kids1.setVisibility(View.GONE);
                minside_side1.setVisibility(View.GONE);
                minside_drink1.setVisibility(View.GONE);
                minside_family1.setVisibility(View.GONE);
                minside_combo1.setVisibility(View.GONE);
                minside_pro1.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mheadtitle.setText("Fish & Shrimp");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mheadtitle.setText(" سمك و جمبري");
                }

//                insideMenuCategories.clear();
                try {
                    insideMenuCategories = Projecturl.MenuItems.get(1).getMenuCategories();
                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                    minside_list_view.setAdapter(madapter);
                    madapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
                }
//                new GetCategoryItems().execute(Projecturl.CATEGORY_ITEMS_URL + catid);
            }
        });
        minside_nugget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catid = 3;
                minside_boasted.setVisibility(View.VISIBLE);
                minside_fish.setVisibility(View.VISIBLE);
                minside_nugget.setVisibility(View.GONE);
                minside_kids.setVisibility(View.VISIBLE);
                minside_side.setVisibility(View.VISIBLE);
                minside_drink.setVisibility(View.VISIBLE);
                minside_family.setVisibility(View.VISIBLE);
                minside_combo.setVisibility(View.VISIBLE);
                minside_promatoin.setVisibility(View.VISIBLE);
                minside_boasted1.setVisibility(View.GONE);
                minside_fish1.setVisibility(View.GONE);
                minside_nugget1.setVisibility(View.VISIBLE);
                minside_kids1.setVisibility(View.GONE);
                minside_side1.setVisibility(View.GONE);
                minside_drink1.setVisibility(View.GONE);
                minside_family1.setVisibility(View.GONE);
                minside_combo1.setVisibility(View.GONE);
                minside_pro1.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mheadtitle.setText("Nuggets & Strip");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mheadtitle.setText(" سندويتش وناجتس و ستربس");
                }

//                insideMenuCategories.clear();
                try {
                    insideMenuCategories = Projecturl.MenuItems.get(2).getMenuCategories();
                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                    minside_list_view.setAdapter(madapter);
                    madapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
                }
//                new GetCategoryItems().execute(Projecturl.CATEGORY_ITEMS_URL + catid);
            }
        });
        minside_kids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catid = 4;
                minside_boasted.setVisibility(View.VISIBLE);
                minside_fish.setVisibility(View.VISIBLE);
                minside_nugget.setVisibility(View.VISIBLE);
                minside_kids.setVisibility(View.GONE);
                minside_side.setVisibility(View.VISIBLE);
                minside_drink.setVisibility(View.VISIBLE);
                minside_family.setVisibility(View.VISIBLE);
                minside_combo.setVisibility(View.VISIBLE);
                minside_promatoin.setVisibility(View.VISIBLE);
                minside_boasted1.setVisibility(View.GONE);
                minside_fish1.setVisibility(View.GONE);
                minside_nugget1.setVisibility(View.GONE);
                minside_kids1.setVisibility(View.VISIBLE);
                minside_side1.setVisibility(View.GONE);
                minside_drink1.setVisibility(View.GONE);
                minside_family1.setVisibility(View.GONE);
                minside_combo1.setVisibility(View.GONE);
                minside_pro1.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mheadtitle.setText("Kids Meal");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mheadtitle.setText("أطفال");
                }

//                insideMenuCategories.clear();
                try {
                    insideMenuCategories = Projecturl.MenuItems.get(3).getMenuCategories();
                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                    minside_list_view.setAdapter(madapter);
                    madapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
                }
//                new GetCategoryItems().execute(Projecturl.CATEGORY_ITEMS_URL + catid);
            }
        });
        minside_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catid = 5;
                minside_boasted.setVisibility(View.VISIBLE);
                minside_fish.setVisibility(View.VISIBLE);
                minside_nugget.setVisibility(View.VISIBLE);
                minside_kids.setVisibility(View.VISIBLE);
                minside_side.setVisibility(View.GONE);
                minside_drink.setVisibility(View.VISIBLE);
                minside_family.setVisibility(View.VISIBLE);
                minside_combo.setVisibility(View.VISIBLE);
                minside_promatoin.setVisibility(View.VISIBLE);
                minside_boasted1.setVisibility(View.GONE);
                minside_fish1.setVisibility(View.GONE);
                minside_nugget1.setVisibility(View.GONE);
                minside_kids1.setVisibility(View.GONE);
                minside_side1.setVisibility(View.VISIBLE);
                minside_drink1.setVisibility(View.GONE);
                minside_family1.setVisibility(View.GONE);
                minside_combo1.setVisibility(View.GONE);
                minside_pro1.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mheadtitle.setText("Side Items");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mheadtitle.setText(" الطلبات الجانبية");
                }

//                insideMenuCategories.clear();
                try {
                    insideMenuCategories = Projecturl.MenuItems.get(4).getMenuCategories();
                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                    minside_list_view.setAdapter(madapter);
                    madapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
                }
//                new GetCategoryItems().execute(Projecturl.CATEGORY_ITEMS_URL + catid);
            }
        });
        minside_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catid = 6;
                minside_boasted.setVisibility(View.VISIBLE);
                minside_fish.setVisibility(View.VISIBLE);
                minside_nugget.setVisibility(View.VISIBLE);
                minside_kids.setVisibility(View.VISIBLE);
                minside_side.setVisibility(View.VISIBLE);
                minside_drink.setVisibility(View.GONE);
                minside_family.setVisibility(View.VISIBLE);
                minside_combo.setVisibility(View.VISIBLE);
                minside_promatoin.setVisibility(View.VISIBLE);
                minside_boasted1.setVisibility(View.GONE);
                minside_fish1.setVisibility(View.GONE);
                minside_nugget1.setVisibility(View.GONE);
                minside_kids1.setVisibility(View.GONE);
                minside_side1.setVisibility(View.GONE);
                minside_drink1.setVisibility(View.VISIBLE);
                minside_family1.setVisibility(View.GONE);
                minside_combo1.setVisibility(View.GONE);
                minside_pro1.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mheadtitle.setText("Dessert & Drinks");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mheadtitle.setText("حلو و مشروب");
                }
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_drink.getLeft(), mlayout_drink.getTop());
                    }
                });

//                insideMenuCategories.clear();
                try {
                    insideMenuCategories = Projecturl.MenuItems.get(5).getMenuCategories();
                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                    minside_list_view.setAdapter(madapter);
                    madapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
                }
//                new GetCategoryItems().execute(Projecturl.CATEGORY_ITEMS_URL + catid);
            }
        });

        minside_family.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catid = 7;
                minside_boasted.setVisibility(View.VISIBLE);
                minside_fish.setVisibility(View.VISIBLE);
                minside_nugget.setVisibility(View.VISIBLE);
                minside_kids.setVisibility(View.VISIBLE);
                minside_side.setVisibility(View.VISIBLE);
                minside_drink.setVisibility(View.VISIBLE);
                minside_family.setVisibility(View.GONE);
                minside_combo.setVisibility(View.VISIBLE);
                minside_promatoin.setVisibility(View.VISIBLE);
                minside_boasted1.setVisibility(View.GONE);
                minside_fish1.setVisibility(View.GONE);
                minside_nugget1.setVisibility(View.GONE);
                minside_kids1.setVisibility(View.GONE);
                minside_side1.setVisibility(View.GONE);
                minside_drink1.setVisibility(View.GONE);
                minside_family1.setVisibility(View.VISIBLE);
                minside_combo1.setVisibility(View.GONE);
                minside_pro1.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mheadtitle.setText("Family Meal");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mheadtitle.setText("وجبة عائلية");
                }
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_family.getLeft(), mlayout_family.getTop());
                    }
                });


//                insideMenuCategories.clear();
                try {
                    insideMenuCategories = Projecturl.MenuItems.get(6).getMenuCategories();
                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                    minside_list_view.setAdapter(madapter);
                    madapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
                }
//                new GetCategoryItems().execute(Projecturl.CATEGORY_ITEMS_URL + catid);
            }
        });
        minside_combo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catid = 10;
                minside_boasted.setVisibility(View.VISIBLE);
                minside_fish.setVisibility(View.VISIBLE);
                minside_nugget.setVisibility(View.VISIBLE);
                minside_kids.setVisibility(View.VISIBLE);
                minside_side.setVisibility(View.VISIBLE);
                minside_drink.setVisibility(View.VISIBLE);
                minside_family.setVisibility(View.VISIBLE);
                minside_combo.setVisibility(View.GONE);
                minside_promatoin.setVisibility(View.VISIBLE);
                minside_boasted1.setVisibility(View.GONE);
                minside_fish1.setVisibility(View.GONE);
                minside_nugget1.setVisibility(View.GONE);
                minside_kids1.setVisibility(View.GONE);
                minside_side1.setVisibility(View.GONE);
                minside_drink1.setVisibility(View.GONE);
                minside_family1.setVisibility(View.GONE);
                minside_combo1.setVisibility(View.VISIBLE);
                minside_pro1.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    mheadtitle.setText("Sandwich");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mheadtitle.setText("وجبة كومبو");
                }
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_combo.getLeft(), mlayout_combo.getTop());
                    }
                });

//                insideMenuCategories.clear();
                try {
                    insideMenuCategories = Projecturl.MenuItems.get(9).getMenuCategories();
                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                    minside_list_view.setAdapter(madapter);
                    madapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
                }
//                new GetCategoryItems().execute(Projecturl.CATEGORY_ITEMS_URL + catid);
            }
        });
        minside_promatoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catid = 9;
                minside_boasted.setVisibility(View.VISIBLE);
                minside_fish.setVisibility(View.VISIBLE);
                minside_nugget.setVisibility(View.VISIBLE);
                minside_kids.setVisibility(View.VISIBLE);
                minside_side.setVisibility(View.VISIBLE);
                minside_drink.setVisibility(View.VISIBLE);
                minside_family.setVisibility(View.VISIBLE);
                minside_combo.setVisibility(View.VISIBLE);
                minside_promatoin.setVisibility(View.GONE);
                minside_boasted1.setVisibility(View.GONE);
                minside_fish1.setVisibility(View.GONE);
                minside_nugget1.setVisibility(View.GONE);
                minside_kids1.setVisibility(View.GONE);
                minside_side1.setVisibility(View.GONE);
                minside_drink1.setVisibility(View.GONE);
                minside_family1.setVisibility(View.GONE);
                minside_combo1.setVisibility(View.GONE);
                minside_pro1.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    mheadtitle.setText("Promotion");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mheadtitle.setText("عروض");
                }
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_promo.getLeft(), mlayout_promo.getTop());
                    }
                });

//                insideMenuCategories.clear();
                try {
                    insideMenuCategories = Projecturl.MenuItems.get(8).getMenuCategories();
                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                    minside_list_view.setAdapter(madapter);
                    madapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
                }
            }
        });


        if (catid == 1) {
            minside_boasted.performClick();
            if (language.equalsIgnoreCase("En")) {
                mheadtitle.setText("Broasted Chicken");
            } else if (language.equalsIgnoreCase("Ar")) {
                mheadtitle.setText("بروستد");
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_broasted.getRight(), mlayout_broasted.getTop());
                    }
                });
            }
        } else if (catid == 2) {
            minside_fish.performClick();
            if (language.equalsIgnoreCase("En")) {
                mheadtitle.setText("Fish & Shrimp");
            } else if (language.equalsIgnoreCase("Ar")) {
                mheadtitle.setText(" سمك و جمبري");
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_fish.getRight(), mlayout_fish.getTop());
                    }
                });
            }
        } else if (catid == 3) {
            minside_nugget.performClick();
            if (language.equalsIgnoreCase("En")) {
                mheadtitle.setText("Nuggets & Strip");
            } else if (language.equalsIgnoreCase("Ar")) {
                mheadtitle.setText("  سندويتش وناجتس و ستربس");
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_nugget.getRight(), mlayout_nugget.getTop());
                    }
                });
            }
        } else if (catid == 4) {
            minside_kids.performClick();
            if (language.equalsIgnoreCase("En")) {
                mheadtitle.setText("Kids Meal");
            } else if (language.equalsIgnoreCase("Ar")) {
                mheadtitle.setText("أطفال");
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_kids.getRight(), mlayout_kids.getTop());
                    }
                });
            }
        } else if (catid == 5) {
            minside_side.performClick();
            if (language.equalsIgnoreCase("En")) {
                mheadtitle.setText("Side Items");
            } else if (language.equalsIgnoreCase("Ar")) {
                mheadtitle.setText(" الطلبات الجانبية");
            }
        } else if (catid == 6) {
            minside_drink.performClick();
            if (language.equalsIgnoreCase("En")) {
                mheadtitle.setText("Dessert & Drinks");
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_drink.getLeft(), mlayout_drink.getTop());
                    }
                });
            } else if (language.equalsIgnoreCase("Ar")) {
                mheadtitle.setText("حلو و مشروب");
            }
        } else if (catid == 7) {
            minside_family.performClick();
            if (language.equalsIgnoreCase("En")) {
                mheadtitle.setText("Family Meal");
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_family.getLeft(), mlayout_family.getTop());
                    }
                });
            } else if (language.equalsIgnoreCase("Ar")) {
                mheadtitle.setText("وجبة عائلية");
            }
        } else if (catid == 10) {
            minside_combo.performClick();
            if (language.equalsIgnoreCase("En")) {
                mheadtitle.setText("Sandwich");
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_combo.getLeft(), mlayout_combo.getTop());
                    }
                });
            } else if (language.equalsIgnoreCase("Ar")) {
                mheadtitle.setText("وجبة كومبو");
            }
        } else if (catid == 9) {
            minside_promatoin.performClick();
            if (language.equalsIgnoreCase("En")) {
                mheadtitle.setText("Promotion");
                hsv.post(new Runnable() {
                    @Override
                    public void run() {
                        hsv.scrollTo(mlayout_promo.getLeft(), mlayout_promo.getTop());
                    }
                });
            } else if (language.equalsIgnoreCase("Ar")) {
                mheadtitle.setText("عروض");
            }
        }

//        try {
//
//            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//            if (language.equalsIgnoreCase("En")) {
//                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//            } else if (language.equalsIgnoreCase("Ar")) {
//                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//            }
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//        InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//        if (language.equalsIgnoreCase("En")) {
//            InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//            InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//        } else if (language.equalsIgnoreCase("Ar")) {
//            InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//            InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//        }
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            insideMenuCategories.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(InsideMenu.this);
            dialog = ProgressDialog.show(InsideMenu.this, "",
                    "Loading items...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(InsideMenu.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(InsideMenu.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONArray ja = new JSONArray(result);

                            for (int i = 0; i < ja.length(); i++) {
                                MenuItems menuItems = new MenuItems();
                                ArrayList<InsideMenuCategories> menuList = new ArrayList<>();
                                JSONObject jobj = ja.getJSONObject(i);
                                menuItems.setCategoryId(jobj.getString("CategoryId"));
                                menuItems.setCategoryName(jobj.getString("CategoryName"));
                                menuItems.setCatDescription(jobj.getString("CatDescription"));
                                menuItems.setCatDescription_Ar(jobj.getString("CatDescription_Ar"));

                                JSONArray jsonArray = jobj.getJSONArray("Items");
                                for (int j = 0; j < jsonArray.length(); j++) {
                                    InsideMenuCategories cat = new InsideMenuCategories();
                                    ArrayList<InsideMenuValues> itemList = new ArrayList<>();
                                    JSONObject jo = jsonArray.getJSONObject(j);
                                    JSONObject jo1 = jo.getJSONObject("Key");
                                    cat.setCatId(jo1.getString("CategoryId"));
                                    cat.setItemId(jo1.getString("ItemId"));
                                    cat.setItemName(jo1.getString("ItemName"));
                                    cat.setModifierId(jo1.getString("ModifierId"));
                                    cat.setItemName_Ar(jo1.getString("ItemName_Ar"));
                                    cat.setDescription(jo1.getString("Description"));
                                    cat.setDescription_Ar(jo1.getString("Description_Ar"));
                                    cat.setAdditionId(jo1.getString("AdditionalsId"));
                                    cat.setImage(jo1.getString("Images"));
                                    cat.setIsDelivery(jo1.getString("IsDelivery"));
                                    JSONObject value = jo.getJSONObject("Value");
                                    Iterator iterator = value.keys();
                                    while (iterator.hasNext()) {
                                        InsideMenuValues items = new InsideMenuValues();
                                        String size = (String) iterator.next();
                                        String price = value.getString(size);
                                        items.setSize(Integer.parseInt(size));
                                        items.setPrices(price);
                                        itemList.add(items);
                                        cat.setInsideMenuchild(itemList);
                                    }
                                    insideMenuCategories.add(cat);
                                    madapter = new InsideMenuAdapter(InsideMenu.this, insideMenuCategories, language);
                                    minside_list_view.setAdapter(madapter);
                                    madapter.notifyDataSetChanged();

                                    menuList.add(cat);
                                    menuItems.setMenuCategories(menuList);
                                }
                                Projecturl.MenuItems.add(menuItems);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    madapter.notifyDataSetChanged();
                }
            } else {
                Toast.makeText(InsideMenu.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        try {
            madapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
        InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
        if (language.equalsIgnoreCase("En")) {
            InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
            InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
        } else if (language.equalsIgnoreCase("Ar")) {
            InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
            InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
        }
    }

}