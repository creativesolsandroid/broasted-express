package com.cs.broastedexpress.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.broastedexpress.Adapter.OrderHistoryAdapter;
import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.OrderHistory;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class History extends AppCompatActivity {

    private DataBaseHelper myDbHelper;
    private ArrayList<OrderHistory> transactionsList = new ArrayList<>();
    private SwipeMenuListView orderHistoryListView;
    TextView emptyView, title;
    private OrderHistoryAdapter mAdapter;
    SharedPreferences userPrefs;
    String userId;
    SharedPreferences languagePrefs;
    String language, response;
    LinearLayout mHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_history);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_history);
        }

        myDbHelper = new DataBaseHelper(History.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


//        mHistory= (LinearLayout) findViewById(R.id.history);

//        mHistory.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent a=new Intent(History.this, TrackOrder.class);
//                startActivity(a);
//            }
//        });
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        orderHistoryListView = (SwipeMenuListView) findViewById(R.id.fav_order_listview);
        emptyView = (TextView) findViewById(R.id.empty_view);
        title = (TextView) findViewById(R.id.header_title);
        mAdapter = new OrderHistoryAdapter(History.this, transactionsList, language);
        orderHistoryListView.setAdapter(mAdapter);
        new GetOrderDetails().execute(Projecturl.ORDER_HISTORY_URL + userId);

        orderHistoryListView.setEmptyView(emptyView);

        if (language.equalsIgnoreCase("En")) {
            title.setText("Order History");
        } else if (language.equalsIgnoreCase("Ar")) {
            title.setText("جميع الطلبات");
            emptyView.setText("لا يوجد منتجات في السلة");
        }
//        if(language.equalsIgnoreCase("En")){
//            title.setText("Order History");
//        }else if(language.equalsIgnoreCase("Ar")){
//            title.setText("جميع الطلبات");
//            emptyView.setText("لا يوجد منتجات في السلة");
//        }

        orderHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(History.this, TrackOrder.class);
                intent.putExtra("orderId", transactionsList.get(position).getOrderId());
                startActivity(intent);
            }
        });

        // step 1. create a MenuCreator
        SwipeMenuCreator creator2 = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                switch (menu.getViewType()) {
                    case 0:
                        // create menu of type 0
                        SwipeMenuItem viewItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        viewItem.setBackground(new ColorDrawable(Color.rgb(0x80,
                                0x80, 0x88)));
                        // set item width
                        viewItem.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            viewItem.setTitle("View");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            viewItem.setTitle("مراجعة");
                        }
                        // set item title fontsize
                        viewItem.setTitleSize(18);
                        // set item title font color
                        viewItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(viewItem);

                        // create "delete" item
                        SwipeMenuItem reorderItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        reorderItem.setBackground(new ColorDrawable(Color.rgb(0x25,
                                0xAE, 0x88)));
                        // set item width
                        reorderItem.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            reorderItem.setTitle("Re-order");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            reorderItem.setTitle("إعادة الطلب");
                        }
                        // set item title fontsize
                        reorderItem.setTitleSize(18);
                        // set item title font color
                        reorderItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(reorderItem);

                        // create "delete" item
                        SwipeMenuItem deleteItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                                0x3F, 0x25)));
                        // set item width
                        deleteItem.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            deleteItem.setTitle("Delete");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            deleteItem.setTitle("حذف");
                        }
                        // set item title fontsize
                        deleteItem.setTitleSize(18);
                        // set item title font color
                        deleteItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(deleteItem);
                        break;
                    case 1:
                        // create menu of type 1
                        SwipeMenuItem viewItem1 = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        viewItem1.setBackground(new ColorDrawable(Color.rgb(0x80,
                                0x80, 0x88)));
                        // set item width
                        viewItem1.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            viewItem1.setTitle("View");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            viewItem1.setTitle("مراجعة");
                        }
                        // set item title fontsize
                        viewItem1.setTitleSize(18);
                        // set item title font color
                        viewItem1.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(viewItem1);

                        // create "delete" item
                        SwipeMenuItem reorderItem1 = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        reorderItem1.setBackground(new ColorDrawable(Color.rgb(0x25,
                                0xAE, 0x88)));
                        // set item width
                        reorderItem1.setWidth(dp2px(90));

                        if(language.equalsIgnoreCase("En")) {
                            reorderItem1.setTitle("Re-order");
                        }
                        else if(language.equalsIgnoreCase("Ar")){
                            reorderItem1.setTitle("إعادة الطلب");
                        }
                        // set item title fontsize
                        reorderItem1.setTitleSize(18);
                        // set item title font color
                        reorderItem1.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(reorderItem1);
                        break;
                }
            }

        };
        orderHistoryListView.setMenuCreator(creator2);

        // step 2. listener item click event
        orderHistoryListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        String orderId1 = transactionsList.get(position).getOrderId();
                        Intent intent = new Intent(History.this, ViewDetailsActivity.class);
                        intent.putExtra("orderid",orderId1);
                        startActivity(intent);
                        break;
                    case 1:
                        Log.i("TAG","user id "+ userId);
                        String orderId = transactionsList.get(position).getOrderId();
                        Log.i("TAG","order id "+ orderId);
                        new GetCheckoutOrderDetails().execute(Projecturl.ORDERD_DETAILS_URL + "?uId=" + userId + "&oId=" + orderId);
                        break;
                    case 2:
                        new DeleteOrder().execute(Projecturl.DELETE_ORDER+transactionsList.get(position).getOrderId());
                        break;

                }
                return false;
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }


    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(History.this);
            transactionsList.clear();
            dialog = ProgressDialog.show(History.this, "",
                    "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(History.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(History.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    OrderHistory oh = new OrderHistory();
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    String orderId = jo1.getString("odrId");

                                    String storeName = jo1.getString("Sname");
                                    String storeName_ar = jo1.getString("Sname_ar");
                                    String userAddress = jo1.getString("UserAddress");
                                    if (userAddress != null) {
                                        if (userAddress.isEmpty()) {
                                            userAddress = " ";
                                        }
                                    }
                                    String orderDate = jo1.getString("Odate");
//                                    boolean isFavourite = jo1.getBoolean("IsFavorite");
                                    String total_Price = jo1.getString("TotPrice");
                                    String orderType = jo1.getString("OrderType");
                                    String orderStatus = jo1.getString("OrderStatus");
                                    String invoiceNo = jo1.getString("InvoiceNo");
                                    String itemDetails = "", itemDetailsAr = "";

                                    JSONArray ja1 = jo1.getJSONArray("items");
                                    for (int j = 0; j < ja1.length(); j++) {
                                        JSONObject jo2 = ja1.getJSONObject(j);
                                        if (itemDetails.equalsIgnoreCase("") && itemDetailsAr.equalsIgnoreCase("")) {
                                            itemDetails = jo2.getString("ItmName");
                                            itemDetailsAr = jo2.getString("ItmName_ar");
                                        } else {
                                            itemDetails = itemDetails + ", " + jo2.getString("ItmName");
                                            itemDetailsAr = itemDetailsAr + ", " + jo2.getString("ItmName_ar");
                                        }
                                    }


                                    oh.setOrderId(orderId);
                                    oh.setUserAddress(userAddress);
                                    oh.setOrderDate(orderDate);
                                    oh.setOrderId(orderId);

                                    oh.setStoreName(storeName);
                                    oh.setStoreName_ar(storeName_ar);
//                                    oh.setIsFavorite(isFavourite);
                                    oh.setTotalPrice(total_Price);
//                                    oh.setStatus(status);
                                    oh.setOrderStatus(orderStatus);
                                    oh.setInvoiceNo(invoiceNo);
                                    oh.setItemDetails(itemDetails);
                                    oh.setItemDetails_ar(itemDetailsAr);
                                    oh.setOrderType(orderType);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    transactionsList.add(oh);

                                }
                            } catch (JSONException je) {
                                je.printStackTrace();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Broasted Express");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(History.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            mAdapter.notifyDataSetChanged();
//            Intent b=new Intent(History.this, TrackOrder.class);
//            b.putExtra("orderId",-1);
//            startActivity(b);
            super.onPostExecute(result);

        }

    }

    public class GetCheckoutOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(History.this);
            dialog = ProgressDialog.show(History.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(History.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(History.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            HashMap<String, String> values = new HashMap<>();

                            JSONObject jo = new JSONObject(result);

                            JSONArray ja = jo.getJSONArray("SubItem");
                            myDbHelper.deleteOrderTable();
                            for (int i = 0; i < ja.length(); i++) {
                                String ids = "0", additionalsStr = "", additionalsStrAr = "", additionalsPrice = "", additionalQty = "";
                                String categoryId = "", subCatId = "", itemId = "", itemName = "", itemNameAr = "", itemImage = "", itemDesc = "", itemDescAr = "", itemType = "", comments = "";
                                String price = "", nonDelivery= "";
                                float additionPrice = 0;
                                float priceAd = 0, priceAd1 = 0;
                                int quantity = 0;
                                float finalPrice = 0;
                                JSONArray ja1 = ja.getJSONArray(i);
                                for (int j = 0; j < ja1.length(); j++) {
                                    if (j == 0) {
                                        JSONObject jo1 = ja1.getJSONObject(0);
                                        itemId = jo1.getString("ItemId");
                                        itemName = jo1.getString("ItemName");
                                        itemNameAr = jo1.getString("ItemName_Ar");
                                        itemDesc = jo1.getString("Description");
                                        itemDescAr = jo1.getString("Description_Ar");
                                        comments = jo1.getString("Comments");
                                        itemImage = jo1.getString("Images");
                                        price = jo1.getString("ItemPrice");
                                        quantity = jo1.getInt("Qty");
                                        categoryId = jo1.getString("CategoryId");
                                        itemType = jo1.getString("Size");
                                        nonDelivery = jo1.getString("IsDelivery");
                                        Log.e("TAG", "");
                                    } else {
                                        JSONArray ja2 = ja1.getJSONArray(j);
                                        for (int k = 0; k < ja2.length(); k++) {
                                            JSONObject jo2 = ja2.getJSONObject(k);
                                            if (!jo2.getString("AdditionalID").equals("0")) {
                                                if (ids.equalsIgnoreCase("0")) {
                                                    ids = jo2.getString("AdditionalID");
                                                    additionalsStr = jo2.getString("AdditionalName");
                                                    additionalsStrAr = jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = jo2.getString("AdditionalPrice");
                                                    additionalQty = jo2.getString("AddQty");
                                                } else {
                                                    ids = ids + "," + jo2.getString("AdditionalID");
                                                    additionalsStr = additionalsStr + "," + jo2.getString("AdditionalName");
                                                    additionalsStrAr = additionalsStrAr + "," + jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = additionalsPrice + "," + jo2.getString("AdditionalPrice");
                                                    additionalQty = additionalQty + "," + jo2.getString("AddQty");
                                                }
                                                additionPrice = additionPrice + ((Float.parseFloat((jo2.getString("AdditionalPrice"))) * Integer.parseInt(jo2.getString("AddQty"))));
                                            }

                                        }

                                        priceAd = Float.parseFloat(price);
                                        priceAd1 = Float.parseFloat(price) + additionPrice;
                                    }
                                }

                                if (priceAd != 0) {
                                    finalPrice = priceAd1 * quantity;
                                } else {
                                    finalPrice = Float.parseFloat(price) * quantity;
                                }


                                values.put("itemId", itemId);
                                values.put("itemTypeId", itemType);
                                values.put("additionals", ids);
                                values.put("qty", Integer.toString(quantity));
                                values.put("price", String.valueOf(priceAd));
                                values.put("additionalsPrice", additionalsPrice);
                                values.put("additionalsTypeId", additionalQty);
                                values.put("totalAmount", Float.toString(finalPrice));
                                values.put("comment", comments);
                                values.put("status", "1");
                                values.put("creationDate", "14/07/2015");
                                values.put("modifiedDate", "14/07/2015");
                                values.put("categoryId", categoryId);
                                values.put("itemName", itemName);
                                values.put("itemNameAr", itemNameAr);
                                values.put("image", itemImage);
                                values.put("desc", itemDesc);
                                values.put("descAr", itemDescAr);
                                values.put("nonDelivery", nonDelivery);
                                values.put("AdditionalName", additionalsStr);
                                values.put("AdditionalNameAr", additionalsStrAr);
                                values.put("Additionalimage", "");
                                myDbHelper.insertOrder(values);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(History.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            Intent intent = new Intent(History.this, Checkout.class);
            startActivity(intent);

            super.onPostExecute(result);

        }

    }

    public class DeleteOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(History.this);
            dialog = ProgressDialog.show(History.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(History.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(History.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            new GetOrderDetails().execute(Projecturl.ORDER_HISTORY_URL + userId);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(History.this, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            }else {
                Toast.makeText(History.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}
