package com.cs.broastedexpress.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class ChangePassword extends AppCompatActivity {
    private String response12 = null;
    ProgressDialog dialog;

    EditText oldPassword, newPassword, confirmPassword;
    Button submit;
    SharedPreferences userPrefs;
    String response, userId;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.content_change_password);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.content_change_password_arabic);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        oldPassword = (EditText) findViewById(R.id.old_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);

        submit = (Button) findViewById(R.id.change_password_button);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String oldPwd = oldPassword.getText().toString();
                String newPwd = newPassword.getText().toString();
                String confirmPwd = confirmPassword.getText().toString();
                if (oldPwd.length() == 0) {
                    oldPassword.setError("Please enter Old Password");
                } else if (newPwd.length() == 0) {
                    newPassword.setError("Please enter New Password");
                } else if (newPwd.length() < 8) {
                    if (language.equalsIgnoreCase("En")) {
                        newPassword.setError("Password must be at least 8 characters");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        newPassword.setError("الرقم السري يجب أن يحتوي على الأقل على 8 حروف");
                    }
                } else if (!newPwd.matches("^(?=.*?\\d)[a-zA-Z0-9]{8,13}$")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePassword.this);

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("*Your password must fulfill the following:\nPassword must contain at least 1 numeric digits\nPassword must be at least 8 characters\nPassword must be at less than 13 characters")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else if (confirmPwd.length() == 0) {
                    confirmPassword.setError("Please retype password");
                } else if (!newPwd.equals(confirmPwd)) {
                    confirmPassword.setError("Passwords not match, please retype");
                } else {
                    new ChangePasswordResponse().execute(userId, oldPwd, newPwd);
                }
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                View view = this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        super.onBackPressed();
    }

    public class ChangePasswordResponse extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String networkStatus;
        InputStream is = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ChangePassword.this);
            dialog = ProgressDialog.show(ChangePassword.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    // 2. make POST request to the given URL
//                        HttpGet httpPost = new HttpGet(Constants.CHANGE_PASSWORD_URL+arg0[0]+"?OldPsw="+arg0[1]+"&NewPsw="+arg0[2]);

                    // Making HTTP request
                    try {
                        // defaultHttpClient
                        DefaultHttpClient httpClient = new DefaultHttpClient();
                        HttpGet httpPost = new HttpGet(Projecturl.CHANGE_PASSWORD_URL + params[0] + "?OldPsw=" + params[1] + "&NewPsw=" + params[2]);

                        httpPost.setHeader("Accept", "application/xml");
                        httpPost.setHeader("Content-type", "application/xml");
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        is = httpEntity.getContent();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                is, "iso-8859-1"), 8);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        while ((line = reader.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        is.close();
                        response12 = sb.toString();
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + response12);
            } else {
                return "no internet";
            }
            return response12;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                if (result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePassword.this);

                    // set title
                    alertDialogBuilder.setTitle("Broasted Express");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    if (response12.contains("false")) {
                        dialog.dismiss();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangePassword.this);

                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Invalid Old Password")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    } else if (response12.contains("true")) {
                        dialog.dismiss();
                        Toast.makeText(ChangePassword.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                        onBackPressed();

                    }
                }
            } else {
                dialog.dismiss();
            }

            super.onPostExecute(result1);
        }

    }
}
