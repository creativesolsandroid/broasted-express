package com.cs.broastedexpress.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.broastedexpress.Adapter.AddressAdapter;
import com.cs.broastedexpress.GPSTracker;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.Address;
import com.cs.broastedexpress.Model.StoreInfo;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class Myaddress extends AppCompatActivity {
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int SAVE_ADDRESS_REQUEST = 2;
    private static final int EDIT_ADDRESS_REQUEST = 3;

//    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
//            new LatLng(24.70321657, 46.68097073), new LatLng(24.80321657, 47.68097073));

    TextView addAddress;
    ArrayList<Address> addressList = new ArrayList<>();
    SwipeMenuListView addressListView;
    AddressAdapter mAdapter;
    Double lat, longi;
    GPSTracker gps;
    String response;
    int mPosition;
    boolean toConfirmOrder;
    SharedPreferences userPrefs;
    String userId;
    TextView title;
    SharedPreferences languagePrefs;
    String language;

    private String timeResponse = null;
    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    String serverTime;
    String storeId,starttime,endtime,storeName,storeAddress,imagesURL,is24x7,storename_ar,storeaddress_ar;
    Double latitude,longitudes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.address_list);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.address_list_arabic);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            toConfirmOrder = getIntent().getExtras().getBoolean("confirm_order", false);
        } catch (NullPointerException npe) {
            toConfirmOrder = false;
        }
        addAddress = (TextView) findViewById(R.id.add_a_new_address);
        title = (TextView) findViewById(R.id.header_title);
//        addressTxt = (TextView) findViewById(R.id.address);
        addressListView = (SwipeMenuListView) findViewById(R.id.address_listView);
        mAdapter = new AddressAdapter(Myaddress.this, addressList, language);
        addressListView.setAdapter(mAdapter);
        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
//                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    Intent intent = intentBuilder.build(Myaddress.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException |
                        GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
        addressListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (toConfirmOrder) {
                    mPosition = position;
                    if(addressList.get(mPosition).getId() == null || addressList.get(mPosition).getId().equals("")){
                        Toast.makeText(Myaddress.this, "Please select another address", Toast.LENGTH_SHORT).show();
                    }else {
                        lat = Double.parseDouble(addressList.get(position).getLatitude());
                        longi = Double.parseDouble(addressList.get(position).getLongitude());
                        new GetCurrentTime().execute();
                    }

                }
            }
        });

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {


                // create "edit" item
                SwipeMenuItem editItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                editItem.setBackground(new ColorDrawable(Color.rgb(0x25,
                        0xAE, 0x88)));
                // set item width
                editItem.setWidth(dp2px(90));

                editItem.setTitle("Edit");
                // set item title fontsize
                editItem.setTitleSize(18);
                // set item title font color
                editItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(editItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                deleteItem.setTitle("Delete");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        addressListView.setMenuCreator(creator);

        // step 2. listener item click event
        addressListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        Intent intent = new Intent(Myaddress.this, EditAddressActivity.class);
                        intent.putExtra("address", addressList.get(position).getAddress());
                        intent.putExtra("latitude", addressList.get(position).getLatitude());
                        intent.putExtra("longitude", addressList.get(position).getLongitude());
                        intent.putExtra("id", addressList.get(position).getId());
                        intent.putExtra("landmark", addressList.get(position).getLandmark());
                        intent.putExtra("address_type", addressList.get(position).getAddressType());
                        intent.putExtra("house_no", addressList.get(position).getHouseNo());
                        intent.putExtra("house_name", addressList.get(position).getAddressName());
                        startActivityForResult(intent, EDIT_ADDRESS_REQUEST);
                        break;
                    case 1:
                        new DeleteAddress().execute(Projecturl.DELETE_ADDRESS_URL + addressList.get(position).getId());
                        break;
                }
                return false;
            }
        });


//        addressListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                    gps = new GPSTracker(Myaddress.this);
//                    if(gps != null){
//                        if (gps.canGetLocation()) {
//                            lat = gps.getLatitude();
//                            longi = gps.getLongitude();
//                            // Create a LatLng object for the current location
//                            LatLng latLng = new LatLng(lat, longi);
////                new GetStoresInfo().execute(Constants.STORES_URL);
////                listView.setAdapter(mAdapter);
//                            Log.i("Location TAG", "outside" + lat + " " + longi);
//                        } else {
//                            // can't get location
//                            // GPS or Network is not enabled
//                            // Ask user to enable GPS/network in settings
//                            gps.showSettingsAlert();
//                        }
//                    }
//
//
//                Location user   = new Location("");
//                Location dest = new Location("");
//
//
//                user.setLatitude(Double.parseDouble(addressList.get(position).getLatitude()));
//                user.setLongitude(Double.parseDouble(addressList.get(position).getLongitude()));
//
//                dest.setLatitude(24.681921);
//                dest.setLongitude(46.700222);
//
//                float dist = (user.distanceTo(dest))/1000;
//
//                if(dist > 50){
//
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(Myaddress.this, android.R.style.Theme_Material_Light_Dialog));
//
//                    if (language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("Broasted Express");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("We're sorry, but no stores were found in your area.")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        // set title
//                        alertDialogBuilder.setTitle("بروستد إكسبريس");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("نعتذر لك ، لا يوجد فروع متوفرة في منطتك")
//                                .setCancelable(false)
//                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                    }
//
//                    // create alert dialog
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
//                    alertDialog.show();
//                }else {
//                    if (toConfirmOrder) {
//                        mPosition = position;
//                        lat = Double.parseDouble(addressList.get(position).getLatitude());
//                        longi = Double.parseDouble(addressList.get(position).getLongitude());
////                    new GetCurrentTime().execute();
//                        Intent intent = new Intent(Myaddress.this, ConfirmationScreen.class);
//                        intent.putExtra("your_address", addressList.get(mPosition).getAddress());
//                        intent.putExtra("address_id", addressList.get(mPosition).getId());
//                        intent.putExtra("user_latitude", addressList.get(mPosition).getLatitude());
//                        intent.putExtra("user_longitude", addressList.get(mPosition).getLongitude());
//                        intent.putExtra("landmark", addressList.get(mPosition).getLandmark());
//                        intent.putExtra("order_type", Projecturl.ORDER_TYPE);
//                        startActivity(intent);
//                    }
//                }
//            }
//        });

        new GetAddressDetails().execute(Projecturl.SAVED_ADDRESS_URL + userId);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {
            final Place place = PlacePicker.getPlace(this, data);
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }

            if ("".equals(place.getAddress())) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Myaddress.this);

                if (language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("Broasted Express");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Sorry! we couldn't detect your location. Please place the pin on your exact location.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                } else if (language.equalsIgnoreCase("Ar")) {
                    // set title
                    alertDialogBuilder.setTitle("بروستد إكسبريس");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("نأسف الفرع مغلق حالياً لا يمكن استقبال طلبك الان")
                            .setCancelable(false)
                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                }

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
//                Toast.makeText(AddressActivity.this, "Please select a address", Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(Myaddress.this, Saveaddress.class);
                intent.putExtra("address", place.getAddress());
                intent.putExtra("latitude", Double.toString(place.getLatLng().latitude));
                intent.putExtra("longitude", Double.toString(place.getLatLng().longitude));
                startActivityForResult(intent, SAVE_ADDRESS_REQUEST);
            }

//            addressTxt.setText(name+", "+ address);
//            mAddress.setText(address);
//            mAttributions.setText(Html.fromHtml(attributions));

        } else if (requestCode == SAVE_ADDRESS_REQUEST
                && resultCode == Activity.RESULT_OK) {
            new GetAddressDetails().execute(Projecturl.SAVED_ADDRESS_URL + userId);
        } else if (requestCode == EDIT_ADDRESS_REQUEST && resultCode == Activity.RESULT_OK) {
            new GetAddressDetails().execute(Projecturl.SAVED_ADDRESS_URL + userId);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public class GetAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(Myaddress.this);
            dialog = ProgressDialog.show(Myaddress.this, "",
                    "Loading address...");
            addressList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(Myaddress.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(Myaddress.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    Address oh = new Address();
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    String Id = jo1.getString("Id");
                                    String Address = jo1.getString("Address");
                                    String AddressType = jo1.getString("AddressType");
                                    String HouseNo = jo1.getString("HouseNo");
                                    String LandMark = jo1.getString("LandMark");
                                    String Longitude = jo1.getString("Longitude");
                                    String Latitude = jo1.getString("Latitude");
                                    String houseName = jo1.getString("HouseName");


                                    oh.setId(Id);
                                    oh.setAddress(Address);
                                    oh.setAddressType(AddressType);
                                    oh.setHouseNo(HouseNo);
                                    oh.setLandmark(LandMark);
                                    oh.setLatitude(Latitude);
                                    oh.setLongitude(Longitude);
                                    oh.setAddressName(houseName);

                                    addressList.add(oh);

                                }
                            } catch (JSONException je) {
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(Myaddress.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

//            if(addressList.size()>0){
//                addAddress.setText("Add a new address");
//            }
            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }


    public class DeleteAddress extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(Myaddress.this);
            dialog = ProgressDialog.show(Myaddress.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    JSONParser jParser = new JSONParser();

                    response = jParser
                            .getJSONFromUrl(params[0]);
                    Log.i("TAG", "user response:" + response);
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(Myaddress.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(Myaddress.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            new GetAddressDetails().execute(Projecturl.SAVED_ADDRESS_URL + userId);
                            Toast.makeText(Myaddress.this, "Address deleted successfully", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Myaddress.this, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            } else {
                Toast.makeText(Myaddress.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(Myaddress.this);
            dialog = ProgressDialog.show(Myaddress.this, "",
                    "Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Projecturl.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            }else{
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(serverTime == null){
                dialog.dismiss();
            } else if(serverTime.equals("no internet")){
                dialog.dismiss();
                Toast.makeText(Myaddress.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {
                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
//                    timeResponse = "07/02/2018 05:30 PM";
                }catch (JSONException je){
                    je.printStackTrace();
                }
                new GetStoresInfo().execute(Projecturl.STORES_URL);
//                if(language.equalsIgnoreCase("En")) {

//                }else if(language.equalsIgnoreCase("Ar")){
//                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
//                    mStoresListView.setAdapter(mStoreListAdapterArabic);
//                }

            }


            super.onPostExecute(result1);
        }
    }

    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String dayOfWeek;
        @Override
        protected void onPreExecute() {
            storesList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(Myaddress.this);
            dialog = ProgressDialog.show(Myaddress.this, "",
                    "Fetching near by store...");
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]+dayOfWeek);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(Myaddress.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(Myaddress.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            JSONArray ja = mainObj.getJSONArray("Success");
                            for (int i = 0; i < ja.length(); i++) {
                                StoreInfo si = new StoreInfo();
                                JSONObject jo = ja.getJSONObject(i);
//                                lat = 24.70321657;
//                                longi = 466.68097073;

                                storeId = (jo.getString("storeId"));
                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
                                starttime = (jo.getString("ST"));
                                endtime = (jo.getString("ET"));
                                storeName = (jo.getString("StoreName"));
                                storeAddress = (jo.getString("StoreAddress"));
                                latitude = (jo.getDouble("Latitude"));
                                longitudes = (jo.getDouble("Longitude"));
                                si.setCountryName(jo.getString("CountryName"));
                                si.setCityName(jo.getString("CityName"));
                                imagesURL = (jo.getString("imageURL"));
                                si.setDeliverydistance(jo.getInt("DeliveryDistance"));
                                si.setFamilySection(jo.getString("FamilySection"));
                                si.setWifi(jo.getString("Wifi"));
                                si.setPatioSitting(jo.getString("PatioSitting"));
                                si.setDriveThru(jo.getString("DriveThru"));
                                si.setMeetingSpace(jo.getString("MeetingSpace"));
                                si.setHospital(jo.getString("Hospital"));
                                si.setUniversity(jo.getString("University"));
                                si.setOffice(jo.getString("Office"));
                                si.setShoppingMall(jo.getString("ShoppingMall"));
                                si.setStoreName(storeName);
                                si.setStoreAddress(storeAddress);
                                si.setStoreId(storeId);
                                si.setStarttime(starttime);
                                si.setEndtime(endtime);
                                si.setLatitude(latitude);
                                si.setLongitude(longitudes);
                                si.setImageURL(imagesURL);


                                try {
                                    si.setAirport(jo.getString("Airport"));
                                } catch (Exception e) {
                                    si.setAirport("false");
                                }
                                try {
                                    si.setDineIn(jo.getString("DineIn"));
                                } catch (Exception e) {
                                    si.setDineIn("false");
                                }
                                try {
                                    si.setLadies(jo.getString("Ladies"));
                                } catch (Exception e) {
                                    si.setLadies("false");
                                }

                                si.setNeighborhood(jo.getString("Neighborhood"));
                                si.setStoreNumber(jo.getString("phone"));
                                is24x7 = (jo.getString("is24x7"));
                                si.setStatus(jo.getString("status"));
                                si.setOgCountry(jo.getString("OGCountry"));
                                si.setOgCity(jo.getString("OGCity"));
                                storename_ar = (jo.getString("StoreName_ar"));
                                storeaddress_ar = (jo.getString("StoreAddress_ar"));
                                si.setStoreNumber(jo.getString("phone"));
                                si.setStoreName_ar(storename_ar);
                                si.setIs24x7(is24x7);
                                si.setStoreAddress_ar(storeaddress_ar);
                                try {
                                    si.setMessage(jo.getString("Message"));
                                } catch (Exception e) {
                                    si.setMessage("");
                                }

                                try {
                                    si.setMessage_ar(jo.getString("Message_ar"));
                                } catch (Exception e) {
                                    si.setMessage_ar("");
                                }

                                Location me = new Location("");
                                Location dest = new Location("");

                                me.setLatitude(lat);
                                me.setLongitude(longi);

                                dest.setLatitude(jo.getDouble("Latitude"));
                                dest.setLongitude(jo.getDouble("Longitude"));

                                float dist = (me.distanceTo(dest)) / 1000;
                                si.setDistance(dist);


                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
                                SimpleDateFormat dateFormat3 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                Calendar c = Calendar.getInstance();
                                System.out.println("Current time => " + c.getTime());
                                serverTime = timeResponse;
                                String startTime = si.getStarttime();
                                String endTime = si.getEndtime();
//                                String startTime = "03:00AM";
//                                String endTime = "11:00PM";
                                if(dist <= si.getDeliverydistance() && (jo.getBoolean("OnlineOrderStatus"))){

                                    if (startTime.equals("null") && endTime.equals("null")) {
                                        si.setOpenFlag(-1);
                                        storesList.add(si);
                                    } else {

                                        if (endTime.equals("00:00AM")) {
                                            si.setOpenFlag(1);
                                            storesList.add(si);

                                            continue;
                                        } else if (endTime.equals("12:00AM")) {
                                            endTime = "11:59PM";
                                        }

                                        Calendar now = Calendar.getInstance();

                                        int hour = now.get(Calendar.HOUR_OF_DAY);
                                        int minute = now.get(Calendar.MINUTE);


                                        Date serverDate = null;
                                        Date end24Date = null;
                                        Date start24Date = null;
                                        Date current24Date = null;
                                        Date dateToday = null;
                                        Calendar dateStoreClose = Calendar.getInstance();
                                        try {
                                            serverDate = dateFormat.parse(serverTime);
                                            dateToday = dateFormat.parse(serverTime);
                                            end24Date = dateFormat3.parse(endTime);
                                            start24Date = dateFormat3.parse(startTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Date startDate = null;
                                        Date endDate = null;

                                        try {
                                            dateStoreClose.setTime(dateToday);
                                            dateStoreClose.add(Calendar.DATE, 1);
                                            String current24 = timeFormat1.format(serverDate);
                                            String end24 = timeFormat1.format(end24Date);
                                            String start24 = timeFormat1.format(start24Date);
                                            String startDateString = dateFormat1.format(dateToday);
                                            String endDateString = dateFormat1.format(dateToday);
                                            String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
                                            dateStoreClose.add(Calendar.DATE, -2);
                                            String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

                                            try {
                                                end24Date = timeFormat1.parse(end24);
                                                start24Date = timeFormat1.parse(start24);
                                                current24Date = timeFormat1.parse(current24);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            String[] parts2 = start24.split(":");
                                            int startHour = Integer.parseInt(parts2[0]);
                                            int startMinute = Integer.parseInt(parts2[1]);

                                            String[] parts = end24.split(":");
                                            int endHour = Integer.parseInt(parts[0]);
                                            int endMinute = Integer.parseInt(parts[1]);

                                            String[] parts1 = current24.split(":");
                                            int currentHour = Integer.parseInt(parts1[0]);
                                            int currentMinute = Integer.parseInt(parts1[1]);


//                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");


                                            if (startTime.contains("AM") && endTime.contains("AM")) {
                                                if (startHour < endHour) {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateString + "  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else if (startHour > endHour) {
                                                    if (serverTime.contains("AM")) {
                                                        if (currentHour > endHour) {
                                                            startDateString = startDateString + " " + startTime;
                                                            endDateString = endDateTomorrow + "  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        } else {
                                                            startDateString = endDateYesterday + " " + startTime;
                                                            endDateString = endDateString + "  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else {
                                                        startDateString = startDateString + " " + startTime;
                                                        endDateString = endDateTomorrow + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            } else if (startTime.contains("AM") && endTime.contains("PM")) {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateString + "  " + endTime;
                                                try {
                                                    startDate = dateFormat2.parse(startDateString);
                                                    endDate = dateFormat2.parse(endDateString);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                            } else if (startTime.contains("PM") && endTime.contains("AM")) {
                                                if (serverTime.contains("AM")) {
                                                    if (currentHour <= endHour) {
                                                        startDateString = endDateYesterday + " " + startTime;
                                                        endDateString = endDateString + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        startDateString = startDateString + " " + startTime;
                                                        endDateString = endDateTomorrow + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateTomorrow + "  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                            } else if (startTime.contains("PM") && endTime.contains("PM")) {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateString + "  " + endTime;
                                            }
                                        } catch (NumberFormatException e) {
                                            e.printStackTrace();
                                        }

                                        try {
                                            startDate = dateFormat3.parse(si.getStarttime());
                                            endDate = dateFormat3.parse(si.getEndtime());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }


                                        String serverDateString = null;
                                        try {
                                            serverDateString = dateFormat.format(serverDate);
                                        } catch (Exception e) {

                                        }

                                        try {
                                            serverDate = dateFormat.parse(serverDateString);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("TAG DATE", "" + startDate);
                                        Log.i("TAG DATE1", "" + endDate);
                                        Log.i("TAG DATE2", "" + serverDate);

                                        if (serverDate.after(startDate) && serverDate.before(endDate)) {
                                            Log.i("TAG Visible", "true");
                                            si.setOpenFlag(1);
                                            storesList.add(si);
                                            carryout();
                                        } else {
                                            si.setOpenFlag(0);
                                            storesList.add(si);


                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(Myaddress.this, android.R.style.Theme_Material_Light_Dialog));

                                            if (language.equalsIgnoreCase("En")) {
                                                // set title
                                                alertDialogBuilder.setTitle("Broasted Express");

                                                // set dialog message
                                                alertDialogBuilder
                                                        .setMessage("Sorry! We couldn't find any open stores in your location")
                                                        .setCancelable(false)
                                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.dismiss();
                                                            }
                                                        });
                                            } else if (language.equalsIgnoreCase("Ar")) {
                                                // set title
                                                alertDialogBuilder.setTitle("بروستد إكسبريس");

                                                // set dialog message
                                                alertDialogBuilder
                                                        .setMessage("نأسف ! لم نتمكن من إيجاد أي فرع في موقعك الحالي ")
                                                        .setCancelable(false)
                                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.dismiss();
                                                            }
                                                        });
                                            }

                                            // create alert dialog
                                            AlertDialog alertDialog = alertDialogBuilder.create();

                                            // show it
                                            alertDialog.show();
                                        }
                                    }
                                }else {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(Myaddress.this, android.R.style.Theme_Material_Light_Dialog));

                                    if (language.equalsIgnoreCase("En")) {
                                        // set title
                                        alertDialogBuilder.setTitle("Broasted Express");

                                        // set dialog message
                                        alertDialogBuilder
                                                .setMessage("Sorry! We couldn't find any open stores in your location")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        // set title
                                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                                        // set dialog message
                                        alertDialogBuilder
                                                .setMessage("نأسف ! لم نتمكن من إيجاد أي فرع في موقعك الحالي ")
                                                .setCancelable(false)
                                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                    }

                                    // create alert dialog
                                    AlertDialog alertDialog = alertDialogBuilder.create();

                                    // show it
                                    alertDialog.show();
                                }
//                                storesList.add(si);
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Collections.sort(storesList, StoreInfo.storeDistance);
                        Collections.sort(storesList, StoreInfo.storeOpenSort);

                    }
                }

            }else {
                Toast.makeText(Myaddress.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
//            mAdapter.notifyDataSetChanged();

            super.onPostExecute(result);

        }

    }

    public void carryout() {

//        new GetCurrentTime().execute();

            Intent intent = new Intent(Myaddress.this, ConfirmationScreen.class);
        intent.putExtra("your_address", addressList.get(mPosition).getAddress());
        intent.putExtra("address_id", addressList.get(mPosition).getId());
        intent.putExtra("user_latitude", addressList.get(mPosition).getLatitude());
        intent.putExtra("user_longitude", addressList.get(mPosition).getLongitude());
        intent.putExtra("landmark", addressList.get(mPosition).getLandmark());
            if (language.equalsIgnoreCase("En")) {
                intent.putExtra("storeName", storeName);
                intent.putExtra("storeAddress", storeAddress);
            } else if (language.equalsIgnoreCase("Ar")) {
                intent.putExtra("storeName", storename_ar);
                intent.putExtra("storeAddress", storeaddress_ar);
            }
            intent.putExtra("storeImage", imagesURL);
            intent.putExtra("storeId", storeId);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitudes);
            intent.putExtra("lat", lat);
            intent.putExtra("longi", longi);
            intent.putExtra("start_time", starttime);
            intent.putExtra("end_time", endtime);
            intent.putExtra("full_hours", is24x7);
            intent.putExtra("order_type", Projecturl.ORDER_TYPE);
            startActivity(intent);
    }
}
