package com.cs.broastedexpress.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.GPSTracker;
import com.cs.broastedexpress.Home;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.Orders;
import com.cs.broastedexpress.Model.StoreInfo;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

public class OrderType extends AppCompatActivity {

    RelativeLayout mcount;

    private DataBaseHelper myDbHelper;
    SharedPreferences userPrefs;
    String mLoginStatus;
    SharedPreferences languagePrefs;
    String language;
    TextView mcount_basket;

    private String timeResponse = null;

    String response;
    int position = 0;
    private ArrayList<StoreInfo> storesList = new ArrayList<>();

    Double lat, longi;
    Double dest;
    String serverTime;
    String latitute, longitute;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;

    String storeId, starttime, endtime, storeName, storeAddress, imagesURL, is24x7, storename_ar, storeaddress_ar;
    Double latitude, longitudes;
    ArrayList<Orders> NonDeliveryArray = new ArrayList<>();
    AlertDialog customDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myDbHelper = new DataBaseHelper(OrderType.this);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.content_order_type);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.content_order_type_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mcount_basket = (TextView) findViewById(R.id.count_basket);

        mcount = (RelativeLayout) findViewById(R.id.count);

        final LinearLayout mPickup = (LinearLayout) findViewById(R.id.pickup);
        final LinearLayout mDelivery = (LinearLayout) findViewById(R.id.delivery);
        final LinearLayout mDinein = (LinearLayout) findViewById(R.id.dine_in);

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.view.ContextThemeWrapper(OrderType.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                } else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.view.ContextThemeWrapper(OrderType.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent a = new Intent(OrderType.this, Checkout.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        myDbHelper.deleteOrderTable();
//                                        Checkout.morder_quantity.setText("0");
//                                        Checkout.morder_price.setText("0SR");
//                                        Checkout.mcount_basket.setText("0");
//                                        Menu.morder_quantity.setText("0");
//                                        Menu.morder_price.setText("0SR");
//                                        Menu.mcount_basket.setText("0");
//                                        InsideMenu.morder_quantity.setText("0");
//                                        InsideMenu.morder_price.setText("0SR");
//                                        InsideMenu.mcount_basket.setText("0");
                                        mcount_basket.setText("0");
                                        Intent a = new Intent(OrderType.this, Menu.class);
                                        startActivity(a);
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات" + myDbHelper.getTotalOrderQty() + "لديك ")
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        myDbHelper.deleteOrderTable();
//                                        Checkout.morder_quantity.setText("0");
//                                        Checkout.morder_price.setText("0ريال");
//                                        Checkout.mcount_basket.setText("0");
//                                        Menu.morder_quantity.setText("0");
//                                        Menu.morder_price.setText("0ريال");
//                                        Menu.mcount_basket.setText("0");
//                                        InsideMenu.morder_quantity.setText("0");
//                                        InsideMenu.morder_price.setText("0ريال");
//                                        InsideMenu.mcount_basket.setText("0");
                                        mcount_basket.setText("0");
                                        Intent a = new Intent(OrderType.this, Menu.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent a = new Intent(OrderType.this, Checkout.class);
                                        startActivity(a);
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                }

            }
        });

        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());


        mPickup.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mLoginStatus = userPrefs.getString("login_status", "");
                        if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                            mPickup.setBackground(getResources().getDrawable(R.drawable.orderscreenselected));
                            mDelivery.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                            mDinein.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));

                            Projecturl.ORDER_TYPE = "Carryout";

//                            carryout();

                            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                            if (currentapiVersion >= Build.VERSION_CODES.M) {
                                if (!canAccessLocation()) {
                                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                                } else {
                                    getGPSCoordinates();
                                }
                            } else {
                                getGPSCoordinates();
                            }
                        } else {
                            Intent intent = new Intent(OrderType.this, Login.class);
                            startActivityForResult(intent, 2);
                        }
                    }
                });

        mDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NonDeliveryArray = myDbHelper.getNonDeliveryItems();
                mPickup.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                mDelivery.setBackground(getResources().getDrawable(R.drawable.orderscreenselected));
                mDinein.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));

                if (NonDeliveryArray != null && NonDeliveryArray.size() > 0) {

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderType.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    TextView title = (TextView) dialogView.findViewById(R.id.alert_title);

                    if (language.equalsIgnoreCase("Ar")) {
                        yes.setText("تغيير نوع الطلب");
                        no.setText("إزالة المواد التي لا يتم توصيلها");
                        title.setText("مواد لا يتم توصيلها");
                    }
                    String NDitems = null;
                    for (int i = 0; i < NonDeliveryArray.size(); i++) {
                        if (i == 0) {
                            if (language.equalsIgnoreCase("Ar")) {
                                NDitems = NonDeliveryArray.get(i).getItemNameAr();
                            } else {
                                NDitems = NonDeliveryArray.get(i).getItemName();
                            }
                        } else {
                            if (language.equalsIgnoreCase("Ar")) {
                                NDitems = NDitems + "\n" + NonDeliveryArray.get(i).getItemNameAr();
                            } else {
                                NDitems = NDitems + "\n" + NonDeliveryArray.get(i).getItemName();
                            }
                        }
                    }

                    desc.setText(NDitems);

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            mPickup.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                            mDelivery.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                            mDinein.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            for (int i = 0; i < NonDeliveryArray.size(); i++) {
                                myDbHelper.deleteItemFromOrder(NonDeliveryArray.get(i).getItemId(), NonDeliveryArray.get(i).getItemTypeId());
                            }
                            customDialog.dismiss();
                            finish();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                } else if (myDbHelper.getTotalOrderPrice() >= 50) {
                    mLoginStatus = userPrefs.getString("login_status", "");
                    Projecturl.ORDER_TYPE = "Delivery";
                    if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                        Intent intent = new Intent(OrderType.this, Myaddress.class);
                        intent.putExtra("confirm_order", true);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(OrderType.this, Login.class);
                        startActivityForResult(intent, 2);
                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderType.this, android.R.style.Theme_Material_Light_Dialog));


                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Minimum delivery order amount 50 SR\n" +
                                        "\n" +
                                        "Add more items to continue for delivery order")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                        Intent b = new Intent(OrderType.this, Home.class);
                                        b.putExtra("startWith", 1);
//                b.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(b);
                                        finish();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
//                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(" ألحد الأدنى للتوصيل بقيمة 50 ريال أضف مزيدا من الطلبات للاستمرار في توصيل طلبك")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }

            }
        });

        mDinein.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mLoginStatus = userPrefs.getString("login_status", "");
                        if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                            Projecturl.ORDER_TYPE = "Dine-In";
                            mPickup.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                            mDelivery.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                            mDinein.setBackground(getResources().getDrawable(R.drawable.orderscreenselected));
                            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                            if (currentapiVersion >= Build.VERSION_CODES.M) {
                                if (!canAccessLocation()) {
                                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                                } else {
                                    getGPSCoordinates();
                                }
                            } else {
                                getGPSCoordinates();
                            }

                        } else {
                            Intent intent = new Intent(OrderType.this, Login.class);
                            startActivityForResult(intent, 2);
                        }

                    }
                });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getGPSCoordinates() {
        gps = new GPSTracker(OrderType.this);
        if (gps != null) {
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderType.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                } else {
                    Toast.makeText(OrderType.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String dayOfWeek;

        @Override
        protected void onPreExecute() {
            storesList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderType.this);
            dialog = ProgressDialog.show(OrderType.this, "",
                    "Fetching stores...");
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0] + dayOfWeek);
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            storesList.clear();
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderType.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(OrderType.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject mainObj = new JSONObject(result);
                            JSONArray ja = mainObj.getJSONArray("Success");
                            for (int i = 0; i < ja.length(); i++) {
                                StoreInfo si = new StoreInfo();
                                JSONObject jo = ja.getJSONObject(i);
//                                lat = 24.70321657;
//                                longi = 46.68097073;

                                storeId = (jo.getString("storeId"));
                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
                                starttime = (jo.getString("ST"));
                                endtime = (jo.getString("ET"));
                                storeName = (jo.getString("StoreName"));
                                storeAddress = (jo.getString("StoreAddress"));
                                latitude = (jo.getDouble("Latitude"));
                                longitudes = (jo.getDouble("Longitude"));
                                si.setCountryName(jo.getString("CountryName"));
                                si.setCityName(jo.getString("CityName"));
                                imagesURL = (jo.getString("imageURL"));
                                si.setDine_distance(jo.getInt("DineInDistance"));
                                si.setFamilySection(jo.getString("FamilySection"));
                                si.setWifi(jo.getString("Wifi"));
                                si.setPatioSitting(jo.getString("PatioSitting"));
                                si.setDriveThru(jo.getString("DriveThru"));
                                si.setMeetingSpace(jo.getString("MeetingSpace"));
                                si.setHospital(jo.getString("Hospital"));
                                si.setUniversity(jo.getString("University"));
                                si.setOffice(jo.getString("Office"));
                                si.setShoppingMall(jo.getString("ShoppingMall"));
                                si.setStoreName(storeName);
                                si.setStoreAddress(storeAddress);
                                si.setStoreId(storeId);
                                si.setStarttime(starttime);
                                si.setEndtime(endtime);
                                si.setLatitude(latitude);
                                si.setLongitude(longitudes);
                                si.setImageURL(imagesURL);
                                try {
                                    si.setAirport(jo.getString("Airport"));
                                } catch (Exception e) {
                                    si.setAirport("false");
                                }
                                try {
                                    si.setDineIn(jo.getString("DineIn"));
                                } catch (Exception e) {
                                    si.setDineIn("false");
                                }
                                try {
                                    si.setLadies(jo.getString("Ladies"));
                                } catch (Exception e) {
                                    si.setLadies("false");
                                }

                                si.setNeighborhood(jo.getString("Neighborhood"));
                                si.setStoreNumber(jo.getString("phone"));
                                is24x7 = (jo.getString("is24x7"));
                                si.setStatus(jo.getString("status"));
                                si.setOgCountry(jo.getString("OGCountry"));
                                si.setOgCity(jo.getString("OGCity"));
                                storename_ar = (jo.getString("StoreName_ar"));
                                storeaddress_ar = (jo.getString("StoreAddress_ar"));
                                si.setStoreNumber(jo.getString("phone"));
                                si.setStoreName_ar(storename_ar);
                                si.setIs24x7(is24x7);
                                si.setStoreAddress_ar(storeaddress_ar);
                                try {
                                    si.setMessage(jo.getString("Message"));
                                } catch (Exception e) {
                                    si.setMessage("");
                                }

                                try {
                                    si.setMessage_ar(jo.getString("Message_ar"));
                                } catch (Exception e) {
                                    si.setMessage_ar("");
                                }

                                Location me = new Location("");
                                Location dest = new Location("");

                                me.setLatitude(lat);
                                me.setLongitude(longi);

                                dest.setLatitude(jo.getDouble("Latitude"));
                                dest.setLongitude(jo.getDouble("Longitude"));

                                float dist = (me.distanceTo(dest)) / 1000;
                                si.setDistance(dist);


                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
                                SimpleDateFormat dateFormat3 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                Calendar c = Calendar.getInstance();
                                System.out.println("Current time => " + c.getTime());
                                serverTime = timeResponse;
                                String startTime = si.getStarttime();
                                String endTime = si.getEndtime();
//                                String startTime = "03:00AM";
//                                String endTime = "11:00PM";
                                if (dist <= si.getDine_distance() && (jo.getBoolean("OnlineOrderStatus"))) {
//                                Boolean orderstatus = jo.getBoolean("OnlineOrderStatus");
//                                orderstatus = true;
//                                if (dist <= 50 && (orderstatus)) {

                                    if (startTime.equals("null") && endTime.equals("null")) {
                                        si.setOpenFlag(-1);
                                        storesList.add(si);
                                    } else {

                                        if (endTime.equals("00:00AM")) {
                                            si.setOpenFlag(1);
                                            storesList.add(si);

                                            continue;
                                        } else if (endTime.equals("12:00AM")) {
                                            endTime = "11:59PM";
                                        }

                                        Calendar now = Calendar.getInstance();

                                        int hour = now.get(Calendar.HOUR_OF_DAY);
                                        int minute = now.get(Calendar.MINUTE);


                                        Date serverDate = null;
                                        Date end24Date = null;
                                        Date start24Date = null;
                                        Date current24Date = null;
                                        Date dateToday = null;
                                        Calendar dateStoreClose = Calendar.getInstance();
                                        try {
                                            serverDate = dateFormat.parse(serverTime);
                                            end24Date = dateFormat3.parse(endTime);
                                            start24Date = dateFormat3.parse(startTime);
                                            dateToday = dateFormat.parse(serverTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Date startDate = null;
                                        Date endDate = null;
                                        try {
                                            dateStoreClose.setTime(dateToday);
                                            dateStoreClose.add(Calendar.DATE, 1);
                                            String current24 = timeFormat1.format(serverDate);
                                            String end24 = timeFormat1.format(end24Date);
                                            String start24 = timeFormat1.format(start24Date);
                                            String startDateString = dateFormat1.format(dateToday);
                                            String endDateString = dateFormat1.format(dateToday);
                                            String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
                                            dateStoreClose.add(Calendar.DATE, -2);
                                            String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());


                                            try {
                                                end24Date = timeFormat1.parse(end24);
                                                start24Date = timeFormat1.parse(start24);
                                                current24Date = timeFormat1.parse(current24);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }

                                            String[] parts2 = start24.split(":");
                                            int startHour = Integer.parseInt(parts2[0]);
                                            int startMinute = Integer.parseInt(parts2[1]);

                                            String[] parts = end24.split(":");
                                            int endHour = Integer.parseInt(parts[0]);
                                            int endMinute = Integer.parseInt(parts[1]);

                                            String[] parts1 = current24.split(":");
                                            int currentHour = Integer.parseInt(parts1[0]);
                                            int currentMinute = Integer.parseInt(parts1[1]);


//                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");


                                            if (startTime.contains("AM") && endTime.contains("AM")) {
                                                if (startHour < endHour) {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateString + "  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else if (startHour > endHour) {
                                                    if (serverTime.contains("AM")) {
                                                        if (currentHour > endHour) {
                                                            startDateString = startDateString + " " + startTime;
                                                            endDateString = endDateTomorrow + "  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        } else {
                                                            startDateString = endDateYesterday + " " + startTime;
                                                            endDateString = endDateString + "  " + endTime;
                                                            try {
                                                                startDate = dateFormat2.parse(startDateString);
                                                                endDate = dateFormat2.parse(endDateString);
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    } else {
                                                        startDateString = startDateString + " " + startTime;
                                                        endDateString = endDateTomorrow + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }
                                            } else if (startTime.contains("AM") && endTime.contains("PM")) {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateString + "  " + endTime;
                                                try {
                                                    startDate = dateFormat2.parse(startDateString);
                                                    endDate = dateFormat2.parse(endDateString);
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                            } else if (startTime.contains("PM") && endTime.contains("AM")) {
                                                if (serverTime.contains("AM")) {
                                                    if (currentHour <= endHour) {
                                                        startDateString = endDateYesterday + " " + startTime;
                                                        endDateString = endDateString + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        startDateString = startDateString + " " + startTime;
                                                        endDateString = endDateTomorrow + "  " + endTime;
                                                        try {
                                                            startDate = dateFormat2.parse(startDateString);
                                                            endDate = dateFormat2.parse(endDateString);
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateTomorrow + "  " + endTime;
                                                    try {
                                                        startDate = dateFormat2.parse(startDateString);
                                                        endDate = dateFormat2.parse(endDateString);
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                            } else if (startTime.contains("PM") && endTime.contains("PM")) {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateString + "  " + endTime;
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        try {
                                            startDate = dateFormat3.parse(si.getStarttime());
                                            endDate = dateFormat3.parse(si.getEndtime());
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        String serverDateString = null;

                                        try {
                                            serverDateString = dateFormat.format(serverDate);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        try {
                                            serverDate = dateFormat.parse(serverDateString);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("TAG DATE", "" + startDate);
                                        Log.i("TAG DATE1", "" + endDate);
                                        Log.i("TAG DATE2", "" + serverDate);

                                        if (serverDate.after(startDate) && serverDate.before(endDate)) {
                                            Log.i("TAG Visible", "true");
                                            si.setOpenFlag(1);
                                            storesList.add(si);
                                            carryout();
                                        } else {
                                            si.setOpenFlag(0);
                                            storesList.add(si);

                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderType.this, android.R.style.Theme_Material_Light_Dialog));

                                            if (language.equalsIgnoreCase("En")) {
                                                // set title
                                                alertDialogBuilder.setTitle("Broasted Express");

                                                // set dialog message
                                                alertDialogBuilder
                                                        .setMessage("Sorry! We couldn't find any open stores in your location")
                                                        .setCancelable(false)
                                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.dismiss();
                                                            }
                                                        });
                                            } else if (language.equalsIgnoreCase("Ar")) {
                                                // set title
                                                alertDialogBuilder.setTitle("بروستد إكسبريس");

                                                // set dialog message
                                                alertDialogBuilder
                                                        .setMessage("نأسف ! لم نتمكن من إيجاد أي فرع في موقعك الحالي ")
                                                        .setCancelable(false)
                                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.dismiss();
                                                            }
                                                        });
                                            }

                                            // create alert dialog
                                            AlertDialog alertDialog = alertDialogBuilder.create();

                                            // show it
                                            alertDialog.show();

                                        }
                                    }
                                }
                            }


                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    Collections.sort(storesList, StoreInfo.storeDistance);
                    Collections.sort(storesList, StoreInfo.storeOpenSort);
                }
            }

        }else

        {
            Toast.makeText(OrderType.this, "cannot reach server", Toast.LENGTH_SHORT).show();
        }
                if(dialog !=null)

        {
            dialog.dismiss();
        }
//            mAdapter.notifyDataSetChanged();
//            loading = false;
//            swipeLayout.setRefreshing(false);

//            dest=24.681921,46.700222;

            if (storesList.size() == 0){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderType.this, android.R.style.Theme_Material_Light_Dialog));

                if (language.equalsIgnoreCase("En")) {
                    // set title
                    alertDialogBuilder.setTitle("Broasted Express");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Sorry! We couldn't find any open stores in your location")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                } else if (language.equalsIgnoreCase("Ar")) {
                    // set title
                    alertDialogBuilder.setTitle("بروستد إكسبريس");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("نأسف ! لم نتمكن من إيجاد أي فرع في موقعك الحالي ")
                            .setCancelable(false)
                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                }

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }

                super.onPostExecute(result);

    }

}


public class GetCurrentTime extends AsyncTask<String, String, String> {
    java.net.URL url = null;
    String cardNumber = null, password = null;
    double lat, longi;
    String networkStatus;
    String serverTime;
    SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
    SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    ProgressDialog dialog;

    @Override
    protected void onPreExecute() {
        networkStatus = NetworkUtil.getConnectivityStatusString(OrderType.this);
        dialog = ProgressDialog.show(OrderType.this, "",
                "Please Wait....");
    }

    @Override
    protected String doInBackground(String... arg0) {
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                JSONParser jParser = new JSONParser();
                serverTime = jParser.getJSONFromUrl(Projecturl.GET_CURRENT_TIME_URL);


            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Responce", "" + serverTime);
        } else {
            serverTime = "no internet";
        }
        return serverTime;
    }

    @Override
    protected void onPostExecute(String result1) {
        if (serverTime == null) {
            dialog.dismiss();
        } else if (serverTime.equals("no internet")) {
            dialog.dismiss();
            Toast.makeText(OrderType.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
        } else {
            dialog.dismiss();
            try {
                JSONObject jo = new JSONObject(result1);
                timeResponse = jo.getString("DateTime");
//                        timeResponse = "15/05/2018 05:30 PM";
                new GetStoresInfo().execute(Projecturl.STORES_URL);

            } catch (JSONException je) {
                je.printStackTrace();
            }
//                new GetStoresInfo().execute(Projecturl.STORES_URL);
//                if(language.equalsIgnoreCase("En")) {

//                mAdapter = new SelectStoresAdapter(SelectStoresActivity.this, storesList, lat, longi, timeResponse);
//                listView.setAdapter(mAdapter);
//                }else if(language.equalsIgnoreCase("Ar")){
//                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
//                    mStoresListView.setAdapter(mStoreListAdapterArabic);
//                }

        }


        super.onPostExecute(result1);
    }

}

    public void dinein() {
//        new GetCurrentTime().execute();

        if (mLoginStatus.equalsIgnoreCase("loggedin")) {

            Intent intent = new Intent(OrderType.this, ConfirmationScreen.class);
            if (language.equalsIgnoreCase("En")) {
                intent.putExtra("storeName", storeName);
                intent.putExtra("storeAddress", storeAddress);
            } else if (language.equalsIgnoreCase("Ar")) {
                intent.putExtra("storeName", storename_ar);
                intent.putExtra("storeAddress", storeaddress_ar);
            }
            intent.putExtra("storeImage", imagesURL);
            intent.putExtra("storeId", storeId);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitudes);
            intent.putExtra("lat", lat);
            intent.putExtra("longi", longi);
            intent.putExtra("start_time", starttime);
            intent.putExtra("end_time", endtime);
            intent.putExtra("full_hours", is24x7);
            intent.putExtra("order_type", Projecturl.ORDER_TYPE);
            startActivity(intent);
        } else {
            Intent intent = new Intent(OrderType.this, Login.class);
            startActivityForResult(intent, 2);
        }

    }

    public void carryout() {

//        new GetCurrentTime().execute();

//        storeName = "Broasted Express";
//        storename_ar = "بروستد إكسبريس";
//        storeAddress = "Dabab Street, Riyadh";
//        storeaddress_ar = "شارع الضباب،الرياض";
//
//        storeId = "11";


        if (mLoginStatus.equalsIgnoreCase("loggedin")) {

//            Intent intent = new Intent(OrderType.this, ConfirmationScreen.class);
//            if (language.equalsIgnoreCase("En")) {
//                intent.putExtra("storeName", storeName);
//                intent.putExtra("storeAddress", storeAddress);
//            } else if (language.equalsIgnoreCase("Ar")) {
//                intent.putExtra("storeName", storename_ar);
//                intent.putExtra("storeAddress", storeaddress_ar);
//            }
//            intent.putExtra("storeImage", imagesURL);
//            intent.putExtra("storeId", storeId);
//            intent.putExtra("latitude", 24.681921005249023);
//            intent.putExtra("longitude", 46.70022201538086);
//            intent.putExtra("lat", 24.681921005249023);
//            intent.putExtra("longi", 46.70022201538086);
//            intent.putExtra("start_time", "10-01-2018 10:30 AM");
//            intent.putExtra("end_time", "11-01-2018 10:30 AM");
//            intent.putExtra("full_hours", true);
//            intent.putExtra("order_type", Projecturl.ORDER_TYPE);
            Intent intent = new Intent(OrderType.this, ConfirmationScreen.class);
            if (language.equalsIgnoreCase("En")) {
                intent.putExtra("storeName", storeName);
                intent.putExtra("storeAddress", storeAddress);
            } else if (language.equalsIgnoreCase("Ar")) {
                intent.putExtra("storeName", storename_ar);
                intent.putExtra("storeAddress", storeaddress_ar);
            }
            intent.putExtra("storeImage", imagesURL);
            intent.putExtra("storeId", storeId);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitudes);
            intent.putExtra("lat", lat);
            intent.putExtra("longi", longi);
            intent.putExtra("start_time", starttime);
            intent.putExtra("end_time", endtime);
            intent.putExtra("full_hours", is24x7);
            intent.putExtra("order_type", Projecturl.ORDER_TYPE);
            startActivity(intent);
        } else {
            Intent intent = new Intent(OrderType.this, Login.class);
            startActivityForResult(intent, 2);
        }
    }

}
