package com.cs.broastedexpress.Activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.cs.broastedexpress.Adapter.PromoAdapter;
import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.GPSTracker;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.Orders;
import com.cs.broastedexpress.Model.Promos;
import com.cs.broastedexpress.Model.StoreInfo;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;
import com.cs.broastedexpress.Widgets.DateTimePicker;
import com.mobile.connect.PWConnect;
import com.mobile.connect.checkout.dialog.PWConnectCheckoutActivity;
import com.mobile.connect.checkout.meta.PWConnectCheckoutCreateToken;
import com.mobile.connect.checkout.meta.PWConnectCheckoutPaymentMethod;
import com.mobile.connect.checkout.meta.PWConnectCheckoutSettings;
import com.mobile.connect.exception.PWException;
import com.mobile.connect.exception.PWProviderNotInitializedException;
import com.mobile.connect.payment.PWAccount;
import com.mobile.connect.payment.PWCurrency;
import com.mobile.connect.payment.PWPaymentParams;
import com.mobile.connect.service.PWConnectService;
import com.mobile.connect.service.PWProviderBinder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

public class ConfirmationScreen extends AppCompatActivity implements DateTimePicker.DateWatcher {

    public static final String PREFS_NAME = "MCOMMERCE_SAMPLE";


    private PWProviderBinder _binder;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            _binder = (PWProviderBinder) service;
            try {
                // replace by custom sandbox access
                _binder.initializeProvider(PWConnect.PWProviderMode.LIVE, "Hyperpay.Broasted Express.mcommerce", "a9ac6927646211e69325035d15b6ff20");
            } catch (PWException ee) {
                ee.printStackTrace();
            }
            Log.i("mainactivity", "bound to remote service...!");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            _binder = null;
        }
    };

    /**
     * A list of the stored accounts
     */
    private List<PWAccount> accounts = new ArrayList<>();

    /**
     * Reference to the preferences where the accounts are stored
     */


    ArrayList<Orders> orderList = new ArrayList<>();
    TextView medit_order, mconfirm_order, mcash_on, monline_payment, mcash_on1, monline_payment1, madd_new_address, estTime, changeEstTime;
    private DataBaseHelper myDbHelper;
    private SharedPreferences sharedSettings;
    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefsEditor;
    private String storeId, storeName, storeAddress, addressId, storeName_ar, storeAddress_ar, your_address, your_name;
    GPSTracker gps;
    private Double latitude, longitude;
    private double lat, longi;

    String expectedtime1;

    private ArrayList<StoreInfo> storesList = new ArrayList<>();
//    String promocodeStr = "No";
    String serverTime;
    ProgressDialog dialog;
    List<Date> listTimes;
    private int hour;
    private int minute;
    Context ctx;
    boolean checkStatus;
    Date oldTime = null;
    int paymentMode = 3;
    int totalItemsCount;
    float remainingBonusInt;
    SharedPreferences userPrefs, languagePrefs;
    String mobileNumberFormat="";

    TextView vatPercent,receipt_close;
    ImageView minvoice;
    TextView netTotal, vatAmount, amount;
    float vat=5;
    RelativeLayout recieptLayout;

    DecimalFormat decim = new DecimalFormat("0.00");

    private String getCardResponse = null;
    private String timeResponse = null;
    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private String distanceResponse = null;
    private int trafficMins;
    String secs;

    String pickerViewOpen, exp_time_to_dumy;
    String endTime, openTimeStr, todayDate, tomorrowDate;
    String changeTimeIsYes = "false", payerTimeIsYes = "false", No_DistanceStr = "true";
    String expTimeTo;
    Button backBtn;
    boolean flag = true;
    int minuts, check_count, changeMints;
    String freeOrderId = "";
    private Timer timer = new Timer();
    private String expChangeStr = "0000";

    String latitute, longin;
    String response, fullHours, language;
    String userId, orderType, mamount, mvatamount;
    TextView musername, mmoblienumber, mconf_total_amount, mconf_total_items, mstore_Address_txt, mstore_name, mstore_address, mcount_basket;
    boolean isChecked;
    
    float tax ;
    
    RelativeLayout mcount;
    TextView promocode, sr;
    RelativeLayout promoLayout;
    ImageView promoCancel, promoArrow;
    private PromoAdapter mPromoAdapter;
    private ArrayList<Promos> promosList = new ArrayList<>();
    String promocodeStr = "No", promoIdStr= "", promoTypeStr = "";
    JSONArray promoArray = new JSONArray();

    int PreparationTime = 0, TravelTime = 0;
    Date kitchenStartDate = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.content_confirmation_screen);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.content_confirmation_screen_arabic);
        }

        sharedSettings = getSharedPreferences(PREFS_NAME, 0);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        response = userPrefs.getString("user_profile", null);

        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor = orderPrefs.edit();

        myDbHelper = new DataBaseHelper(ConfirmationScreen.this);

        orderList = myDbHelper.getOrderInfo();
        orderType = Projecturl.ORDER_TYPE;

        storeId = getIntent().getExtras().getString("storeId");
        storeName = getIntent().getExtras().getString("storeName");
        storeAddress = getIntent().getExtras().getString("storeAddress");
        latitude = getIntent().getExtras().getDouble("latitude");
        longitude = getIntent().getExtras().getDouble("longitude");
        fullHours = getIntent().getExtras().getString("full_hours");

         tax= myDbHelper.getTotalOrderPrice()*(vat/100);


        Log.e("TAG","full "+fullHours);

        storeId = "11";
        fullHours = "true";

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        startService(new Intent(ConfirmationScreen.this, PWConnectService.class));
        bindService(new Intent(ConfirmationScreen.this, PWConnectService.class), serviceConnection, Context.BIND_AUTO_CREATE);

        mcount = (RelativeLayout) findViewById(R.id.count);
        promoLayout = (RelativeLayout) findViewById(R.id.promo_layout);
        promoCancel = (ImageView) findViewById(R.id.promo_cancel);
        promoArrow = (ImageView) findViewById(R.id.promo_arrow);
        promocode = (TextView) findViewById(R.id.promo_code);
        mPromoAdapter = new PromoAdapter(ConfirmationScreen.this ,promosList);

        medit_order = (TextView) findViewById(R.id.conf_edit_order);
        mconfirm_order = (TextView) findViewById(R.id.confirm_order);
        mcash_on = (TextView) findViewById(R.id.cashon);
//        monline_payment = (TextView) findViewById(R.id.onlinepayment);
//        mcash_on1 = (TextView) findViewById(R.id.cashon1);
        monline_payment1 = (TextView) findViewById(R.id.onlinepayment1);
//        madd_new_address = (TextView) findViewById(R.id.add_new_address);
//        musername = (TextView) findViewById(R.id.username);
//        mmoblienumber = (TextView) findViewById(R.id.moblienumber);
        mcount_basket = (TextView) findViewById(R.id.count_basket);
        mconf_total_amount = (TextView) findViewById(R.id.conf_total_amount);
        mconf_total_items = (TextView) findViewById(R.id.conf_total_items);
        mstore_Address_txt = (TextView) findViewById(R.id.store_address_txt);
        mstore_name = (TextView) findViewById(R.id.store_detail_name);
        mstore_address = (TextView) findViewById(R.id.store_detail_address);
        estTime = (TextView) findViewById(R.id.estimatedtime);
        changeEstTime = (TextView) findViewById(R.id.change_est_time);

        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
        receipt_close = (TextView) findViewById(R.id.receipt_close);
        amount = (TextView) findViewById(R.id.amount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        vatPercent = (TextView) findViewById(R.id.vatPercent);
        netTotal = (TextView) findViewById(R.id.net_total);
        minvoice= (ImageView) findViewById(R.id.invoice);

        listTimes = new ArrayList<>();

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.view.ContextThemeWrapper(ConfirmationScreen.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                } else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.view.ContextThemeWrapper(ConfirmationScreen.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have " + myDbHelper.getTotalOrderQty() + " item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent a = new Intent(ConfirmationScreen.this, Checkout.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        myDbHelper.deleteOrderTable();
//                                        Checkout.morder_quantity.setText("0");
//                                        Checkout.morder_price.setText("0SR");
//                                        Checkout.mcount_basket.setText("0");
//                                        Menu.morder_quantity.setText("0");
//                                        Menu.morder_price.setText("0SR");
//                                        Menu.mcount_basket.setText("0");
//                                        InsideMenu.morder_quantity.setText("0");
//                                        InsideMenu.morder_price.setText("0SR");
//                                        InsideMenu.mcount_basket.setText("0");
                                        mcount_basket.setText("0");
                                        Intent a = new Intent(ConfirmationScreen.this, Menu.class);
                                        startActivity(a);
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات" + myDbHelper.getTotalOrderQty() + "لديك ")
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        myDbHelper.deleteOrderTable();
//                                        Checkout.morder_quantity.setText("0");
//                                        Checkout.morder_price.setText("0ريال");
//                                        Checkout.mcount_basket.setText("0");
//                                        Menu.morder_quantity.setText("0");
//                                        Menu.morder_price.setText("0ريال");
//                                        Menu.mcount_basket.setText("0");
//                                        InsideMenu.morder_quantity.setText("0");
//                                        InsideMenu.morder_price.setText("0ريال");
//                                        InsideMenu.mcount_basket.setText("0");
                                        mcount_basket.setText("0");
                                        Intent a = new Intent(ConfirmationScreen.this, Menu.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent a = new Intent(ConfirmationScreen.this, Checkout.class);
                                        startActivity(a);
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                }

            }
        });


        if (language.equalsIgnoreCase("En")) {
            mstore_name.setText(storeName);
            mstore_address.setText(storeAddress);
        } else if (language.equalsIgnoreCase("Ar")) {
            mstore_name.setText(storeName_ar);
            mstore_address.setText(storeAddress_ar);
        }


//        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
//        if (currentapiVersion >= Build.VERSION_CODES.M) {
//            if (!canAccessLocation()) {
//                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
//            } else {
//                getGPSCoordinates();
//            }
//        }else {
//            getGPSCoordinates();
//        }

//        madd_new_address.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                try {
//                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
//                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
//                    Intent intent = intentBuilder.build(ConfirmationScreen.this);
//                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
//
//                } catch (GooglePlayServicesRepairableException |
//                        GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
//                }
//
//            protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//                if (requestCode == PLACE_PICKER_REQUEST) {
//                    final Place place = PlacePicker.getPlace(this, data);
//                    final CharSequence name = place.getName();
//                    final CharSequence address = place.getAddress();
//                    String attributions = (String) place.getAttributions();
//                    if (attributions == null) {
//                        attributions = "";
//                    }
//
//                    if ("".equals(place.getAddress())) {
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ConfirmationScreen.this);
//
////                    if(language.equalsIgnoreCase("En")) {
//                        // set title
//                        alertDialogBuilder.setTitle("Coffee Station");
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("Sorry! we couldn't detect your location. Please place the pin on your exact location.")
//                                .setCancelable(false)
//                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog.dismiss();
//                                    }
//                                });
////                    }else if(language.equalsIgnoreCase("Ar")){
////                        // set title
////                        alertDialogBuilder.setTitle("د. كيف");
////
////                        // set dialog message
////                        alertDialogBuilder
////                                .setMessage("نأسف الفرع مغلق حالياً لا يمكن استقبال طلبك الان")
////                                .setCancelable(false)
////                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                    public void onClick(DialogInterface dialog, int id) {
////                                        dialog.dismiss();
////                                    }
////                                });
////                    }
//
//                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();
////                Toast.makeText(AddressActivity.this, "Please select a address", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Intent intent = new Intent(ConfirmationScreen.this, Saveaddress.class);
//                        intent.putExtra("address", place.getAddress());
//                        intent.putExtra("latitude", Double.toString(place.getLatLng().latitude));
//                        intent.putExtra("longitude", Double.toString(place.getLatLng().longitude));
//                        startActivityForResult(intent, SAVE_ADDRESS_REQUEST);
//                    }
//                }
//            }
//        }
//        });
        if (response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");
                mobileNumberFormat = userObjuect.getString("mobile");

//                musername.setText(userObjuect.getString("fullName"));
//                mmoblienumber.setText("+" + userObjuect.getString("mobile"));
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }

        if (orderType.equalsIgnoreCase("Delivery")) {
            your_name = getIntent().getExtras().getString("your_address");
            your_address = getIntent().getExtras().getString("landmark");
            if (language.equalsIgnoreCase("En")) {
                mstore_Address_txt.setText("Your Address");
            } else if (language.equalsIgnoreCase("Ar")) {
                mstore_Address_txt.setText("عنوانك");
            }
            addressId = getIntent().getExtras().getString("address_id");
            mstore_name.setText(your_name);
            mstore_address.setText(your_address);
            mstore_name.setTextSize(12);
            mstore_address.setTextSize(12);
        }

        totalItemsCount = myDbHelper.getTotalOrderQty();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        lat = 24.681921;
//        longi = 46.700222;
        gps = new GPSTracker(ConfirmationScreen.this);
        if (gps.canGetLocation()) {

            lat = gps.getLatitude();
            longi = gps.getLongitude();
            latitute = String.valueOf(lat);
            longin = String.valueOf(longi);
            // \n is for new line
//            Toast.makeText(getActivity(), "Your Location is - \nLat: " + lat + "\nLong: " + longi, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

        changeEstTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pickerViewOpen = "open";
                exp_time_to_dumy = "";
                changeExpTimeMethod();
//                showDialog(TIME_DIALOG_ID);
//                new TimePickerDialog(ctx, timePickerListener, hour, minute,
//                        false).show();

            }

        });

        if (language.equalsIgnoreCase("En")) {
                mconf_total_items.setText(" " + myDbHelper.getTotalOrderQty() + " items");
            } else if (language.equalsIgnoreCase("Ar")) {
                mconf_total_items.setText("" + myDbHelper.getTotalOrderQty() + " الأصناف");
        }

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.GONE);
            }
        });


        minvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recieptLayout.setVisibility(View.VISIBLE);
            }
        });
        amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
        vatAmount.setText(""+decim.format(tax));
        netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));
        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
        if (language.equalsIgnoreCase("En")) {
            mconf_total_amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
        } else if (language.equalsIgnoreCase("Ar")) {
          mconf_total_amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
        }
        mcash_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (language.equalsIgnoreCase("En")) {
                    mcash_on.setCompoundDrawablesWithIntrinsicBounds(R.drawable.delivered, 0, R.drawable.conf_cash_on_delivary, 0);
                    monline_payment1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.online, 0, R.drawable.conf_online_payment, 0);
                } else if (language.equalsIgnoreCase("Ar")) {
                    mcash_on.setCompoundDrawablesWithIntrinsicBounds(R.drawable.conf_cash_on_delivary, 0, R.drawable.delivered, 0);
                    monline_payment1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.conf_online_payment, 0, R.drawable.online, 0);
                }
                paymentMode = 2;
                mcash_on.setBackground(getResources().getDrawable(R.drawable.shape));
                monline_payment1.setBackground(getResources().getDrawable(R.drawable.shape14));
//                mcash_on.setVisibility(View.VISIBLE);
//                mcash_on1.setVisibility(View.GONE);
//                monline_payment1.setVisibility(View.GONE);
//                monline_payment.setVisibility(View.VISIBLE);
            }
        });

        monline_payment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMode = 3;
                if (language.equalsIgnoreCase("En")) {
                    mcash_on.setCompoundDrawablesWithIntrinsicBounds(R.drawable.online, 0, R.drawable.conf_cash_on_delivary, 0);
                    monline_payment1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.delivered, 0, R.drawable.conf_online_payment, 0);
                } else if (language.equalsIgnoreCase("Ar")) {
                    mcash_on.setCompoundDrawablesWithIntrinsicBounds(R.drawable.conf_cash_on_delivary, 0, R.drawable.online, 0);
                    monline_payment1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.conf_online_payment, 0, R.drawable.delivered, 0);
                }
                monline_payment1.setBackground(getResources().getDrawable(R.drawable.shape));
                mcash_on.setBackground(getResources().getDrawable(R.drawable.shape14));
//                mcash_on.setVisibility(View.GONE);
//                mcash_on1.setVisibility(View.VISIBLE);
//                monline_payment.setVisibility(View.VISIBLE);
//                monline_payment1.setVisibility(View.GONE);
                if (!mconf_total_amount.getText().toString().equalsIgnoreCase("free")) {
//                    if (isChecked)

                } else {
                }

//                Calendar now = Calendar.getInstance();
//                Intent i = new Intent(ConfirmationScreen.this, PW.class);
//                PWConnectCheckoutSettings settings = null;
//                PWPaymentParams genericParams = null;
//
//                try {
//                     configure amount, currency, and subject of the transaction
//                    genericParams = _binder.getPaymentParamsFactory().createGenericPaymentParams(myDbHelper.getTotalOrderPrice(), PWCurrency.SAUDI_ARABIA_RIYAL, "test subject");
//                     configure payment params with customer data
//                        genericParams.setCustomerGivenName("Aliza");
//                        genericParams.setCustomerFamilyName("Foo");
//                        genericParams.setCustomerAddressCity("Sampletown");
//                        genericParams.setCustomerAddressCountryCode("SA");
//                        genericParams.setCustomerAddressState("PA");
//                        genericParams.setCustomerAddressStreet("123 Grande St");
//                        genericParams.setCustomerAddressZip("1234");
//                        genericParams.setCustomerEmail("aliza.foo@foomail.com");
//                        genericParams.setCustomerIP("255.0.255.0");
//                    genericParams.setCustomIdentifier(storeId + now.get(Calendar.YEAR) + now.get(Calendar.MONTH) + now.get(Calendar.DATE) + estTime.getText().toString().replace(" ", "").replace(":", ""));
//
//
//                    // create the settings for the payment screens
//                    settings = new PWConnectCheckoutSettings();
//                    settings.setHeaderDescription("Food & Beverages");
//                    settings.setHeaderIconResource(R.drawable.ic_launcher);
////                        settings.setPaymentVATAmount(4.5);
//                    settings.setSupportedDirectDebitCountries(new String[]{"SA"});
//                    settings.setSupportedPaymentMethods(new PWConnectCheckoutPaymentMethod[]{PWConnectCheckoutPaymentMethod.VISA, PWConnectCheckoutPaymentMethod.MASTERCARD,
//                            PWConnectCheckoutPaymentMethod.DIRECT_DEBIT});
//                    // ask the user if she wants to store the account
//                    settings.setCreateToken(PWConnectCheckoutCreateToken.PROMPT);
//
//                    // retrieve the stored accounts from the settings
////                        accounts = _binder.getAccountFactory().deserializeAccountList(sharedSettings.getString(ACCOUNTS, _binder.getAccountFactory().serializeAccountList(new ArrayList<PWAccount>())));
////                    settings.setStoredAccounts(accounts);
////
//                    i.putExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_SETTINGS, settings);
//                    i.putExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_GENERIC_PAYMENT_PARAMS, genericParams);
//                    startActivityForResult(i, PWConnectCheckoutActivity.CONNECT_CHECKOUT_ACTIVITY);
//                } catch (PWException e) {
//                    Log.e("connect", "error creating the payment page", e);
//                }
//            }else {
//                new InsertOrder().execute();
//            }
            }
        });

        promoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("TAG","url "+Projecturl.GET_PROMOS_URL+userId+"&DToken="+SplashActivity.regId);
                new GetPromocodeResponse().execute(Projecturl.GET_PROMOS_URL+userId+"&DToken="+SplashActivity.regId);
            }
        });

        promoCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promoLayout.setClickable(true);
                promocode.setText("");
//                sr.setVisibility(View.VISIBLE);
                promoArrow.setVisibility(View.VISIBLE);
                promoCancel.setVisibility(View.GONE);
                promocode.setHint("Apply Promotion");
                if (language.equalsIgnoreCase("En")) {
                    mconf_total_items.setText(" " + myDbHelper.getTotalOrderQty() + " items");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mconf_total_items.setText("" + myDbHelper.getTotalOrderQty() + " الأصناف");
                }
                mconf_total_amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
                promoTypeStr = "";
                remainingBonusInt = 0;
                mconf_total_amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));

                if (language.equalsIgnoreCase("En")) {
                    promocode.setHint("Apply Promotion");
                } else if (language.equalsIgnoreCase("Ar")) {
                    promocode.setHint("قدم للعرض");
                }
            }
        });

        medit_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(ConfirmationScreen.this, Menu.class);
                startActivity(a);
            }
        });

        mconfirm_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mconfirm_order.setClickable(false);
                if (paymentMode == 3) {
                    Calendar now = Calendar.getInstance();
                    Intent i = new Intent(ConfirmationScreen.this, PWConnectCheckoutActivity.class);
                    PWConnectCheckoutSettings settings = null;
                    PWPaymentParams genericParams = null;

                    try {
                        if(mobileNumberFormat==null){
                            mobileNumberFormat = "";
                        }
                        else{
                            mobileNumberFormat = mobileNumberFormat+"@broastedexp.com";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        // configure amount, currency, and subject of the transaction
                        genericParams = _binder.getPaymentParamsFactory().createGenericPaymentParams(Double.parseDouble(mconf_total_amount.getText().toString()), PWCurrency.SAUDI_ARABIA_RIYAL, "test subject");
                        // configure payment params with customer data
//                        genericParams.setCustomerGivenName("Aliza");
//                        genericParams.setCustomerFamilyName("Foo");
//                        genericParams.setCustomerAddressCity("Sampletown");
//                        genericParams.setCustomerAddressCountryCode("SA");
//                        genericParams.setCustomerAddressState("PA");
//                        genericParams.setCustomerAddressStreet("123 Grande St");
//                        genericParams.setCustomerAddressZip("1234");
                        genericParams.setCustomerEmail(mobileNumberFormat);
//                        genericParams.setCustomerIP("255.0.255.0");
                        genericParams.setCustomIdentifier(storeId + now.get(Calendar.YEAR) + now.get(Calendar.MONTH) + now.get(Calendar.DATE) + estTime.getText().toString().replace(" ", "").replace(":", ""));


                        // create the settings for the payment screens
                        settings = new PWConnectCheckoutSettings();
                        settings.setHeaderDescription("Food & Beverages");
                        settings.setHeaderIconResource(R.mipmap.icon1);
//                        settings.setPaymentVATAmount(4.5);
                        settings.setSupportedDirectDebitCountries(new String[]{"SA"});
                        settings.setSupportedPaymentMethods(new PWConnectCheckoutPaymentMethod[]{PWConnectCheckoutPaymentMethod.VISA, PWConnectCheckoutPaymentMethod.MASTERCARD,
                                PWConnectCheckoutPaymentMethod.DIRECT_DEBIT});
                        // ask the user if she wants to store the account
                        settings.setCreateToken(PWConnectCheckoutCreateToken.PROMPT);

                        // retrieve the stored accounts from the settings
//                        accounts = _binder.getAccountFactory().deserializeAccountList(sharedSettings.getString(ACCOUNTS, _binder.getAccountFactory().serializeAccountList(new ArrayList<PWAccount>())));
                        settings.setStoredAccounts(accounts);

                        i.putExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_SETTINGS, settings);
                        i.putExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_GENERIC_PAYMENT_PARAMS, genericParams);
                        startActivityForResult(i, PWConnectCheckoutActivity.CONNECT_CHECKOUT_ACTIVITY);
                    } catch (PWException e) {
                        Log.e("connect", "error creating the payment page", e);
                    }
                } else {
                    new InsertOrder().execute();
                }
            }
        });
        new getTrafficTime().execute();

        timer.schedule(new MyTimerTask(), 30000, 30000);


//        new GetStoredCards().execute(Constants.GET_SAVED_CARDS_URL+userId);
    }

//    public void getGPSCoordinates(){
//        gps = new GPSTracker(ConfirmationScreen.this);
//        if(gps != null){
//            if (gps.canGetLocation()) {
//                lat = gps.getLatitude();
//                longi = gps.getLongitude();
//                // Create a LatLng object for the current location
//                LatLng latLng = new LatLng(lat, longi);
////                new GetStoresInfo().execute(Constants.STORES_URL);
////                listView.setAdapter(mAdapter);
//                new GetCurrentTime().execute();
//
//                Log.i("Location TAG", "outside" + lat + " " + longi);
//            } else {
//                // can't get location
//                // GPS or Network is not enabled
//                // Ask user to enable GPS/network in settings
//                gps.showSettingsAlert();
//            }
//        }
//    }
//
//    private boolean canAccessLocation() {
//        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
//    }
//    private boolean hasPermission(String perm) {
//        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ConfirmationScreen.this, perm));
//    }

    public class GetPromocodeResponse extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            promosList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(ConfirmationScreen.this);
            dialog = ProgressDialog.show(ConfirmationScreen.this, "",
                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... params) {

            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
//            promoFlag = false;
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ConfirmationScreen.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(ConfirmationScreen.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    Promos prom = new Promos();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    String PromoID = jo1.getString("PromoID");
                                    String PromoCode = jo1.getString("PromoCode");
                                    String category = jo1.getString("category");
                                    String itemprice = jo1.getString("itemprice");
                                    String Percentage = jo1.getString("Percentage");
                                    String Img = jo1.getString("Img");
                                    String Desc_En = jo1.getString("Desc_En");
                                    String Desc_Ar = jo1.getString("Desc_Ar");
                                    String PromoTitle_Ar = jo1.getString("PromoTitle_Ar");
                                    String PromoTitle_En = jo1.getString("PromoTitle_En");
                                    String ItemId = jo1.getString("ItemId");
                                    String StoreId = jo1.getString("StoreId");
                                    String Additonals = jo1.getString("Additonals");
                                    String Size = jo1.getString("Size");
                                    String PromoType = jo1.getString("PromoType");
                                    String RemainingBonus = jo1.getString("RemainingBonus");

                                    prom.setPromoID(PromoID);
                                    prom.setPromoCode(PromoCode);
                                    prom.setPercentage(Percentage);
                                    prom.setDescription(Desc_En);
                                    prom.setDescriptionAr(Desc_Ar);
                                    prom.setPromoTitle(PromoTitle_En);
                                    prom.setPromoTitleAr(PromoTitle_Ar);
                                    prom.setItemId(ItemId);
                                    prom.setStoreId(StoreId);
                                    prom.setAdditionals(Additonals);
                                    prom.setSize(Size);
                                    prom.setPromoType(PromoType);
                                    prom.setRemainingBonus(RemainingBonus);
                                    prom.setItemPrice(itemprice);
                                    prom.setCategory(category);
                                    prom.setImage(Img);

                                    promosList.add(prom);

                                }
                                dialog();
                            }catch (JSONException je){
                                Toast.makeText(ConfirmationScreen.this, "Sorry! No active promotions", Toast.LENGTH_SHORT).show();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Oregano");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(ConfirmationScreen.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            mPromoAdapter.notifyDataSetChanged();
            super.onPostExecute(result);
        }
    }

    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ConfirmationScreen.this);
            if (flag) {
                dialog = ProgressDialog.show(ConfirmationScreen.this, "",
                        "Loading. Please Wait....");
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => " + c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Projecturl.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            } else {
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (serverTime == null) {
                if (flag) {
                    dialog.dismiss();
                }
            } else if (serverTime.equals("no internet")) {
                if (flag) {
                    dialog.dismiss();
                }
                Toast.makeText(ConfirmationScreen.this, "Please check internet connection", Toast.LENGTH_SHORT).show();

            } else {

                Log.i("TIME IF TAG", "" + flag);
                if (flag) {
                    dialog.dismiss();
                    try {
                        JSONObject jo = new JSONObject(result1);
                        timeResponse = jo.getString("DateTime");
                    } catch (JSONException je) {
                        je.printStackTrace();
                    }
//                    new GetStoresInfo().execute(Projecturl.STORES_URL);
                    Log.i("TIME TAG", "" + flag);
                    Date d1 = null;
                    Date d2 = null;
                    try {
                        d1 = timeFormat.parse(timeResponse);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    todayDate = timeFormat1.format(d1);
                    Calendar prayerEnd = Calendar.getInstance();
                    prayerEnd.setTime(d1);
                    prayerEnd.add(Calendar.DATE, 1);
                    d2 = prayerEnd.getTime();
                    tomorrowDate = timeFormat1.format(d2);
                    Log.i("DATE TAG TTTT", todayDate + "  " + tomorrowDate);

                    new GetTimes().execute();
                    flag = false;
                    Log.i("TIME IFINSIDE TAG", "" + flag);
                } else {
                    getTimeCaluculation();
                }
            }


            super.onPostExecute(result1);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateChanged(Calendar c) {

    }

//    @Override
//    public void onDateChanged(Calendar c) {
//
//    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new GetCurrentTime().execute();
                }
            });
        }
    }

    public void changeExpTimeMethod() {
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH/mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd/MM/yyyy/HH/mm", Locale.US);
        payerTimeIsYes = "false";
        String st_time;
        String ed_time;
        final String st_time1, ed_time1;
        if (fullHours.equalsIgnoreCase("true")) {
            Date current24Date = null, currentServerDate = null, currentServerDate1;
            try {
                current24Date = timeFormat.parse(timeResponse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String currentTime = timeFormat1.format(current24Date);
            try {
                currentServerDate = timeFormat1.parse(currentTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar now = Calendar.getInstance();
            now.setTime(currentServerDate);
            now.add(Calendar.MINUTE, changeMints);
            currentServerDate = now.getTime();

            Calendar now1 = Calendar.getInstance();
            now1.setTime(currentServerDate);
            now1.add(Calendar.MINUTE, changeMints - 1);
            currentServerDate1 = now1.getTime();

            st_time = timeFormat1.format(currentServerDate);
            ed_time = timeFormat1.format(currentServerDate1);
            st_time = todayDate + "/" + st_time;
            ed_time = tomorrowDate + "/" + ed_time;
        } else {
            endTime = getIntent().getExtras().getString("end_time");
            endTime = endTime.replace(" ", "");
            if (endTime.equals("12:00AM")) {
                endTime = "11:59PM";
            }
            Date date21 = null, date1 = null, date2 = null;
            try {
//            date21 = timeFormat3.parse(endTime);
//                date1 = timeFormat3.parse(openTimeStr);
                date2 = timeFormat3.parse(endTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date current24Date = null, currentServerDate = null, currentServerDate1;
            try {
                current24Date = timeFormat.parse(timeResponse);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String currentTime = timeFormat1.format(current24Date);
            try {
                currentServerDate = timeFormat1.parse(currentTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar now = Calendar.getInstance();
            now.setTime(currentServerDate);
            now.add(Calendar.MINUTE, changeMints);
            currentServerDate = now.getTime();

            st_time = timeFormat1.format(currentServerDate);
            ed_time = timeFormat1.format(date2);
            Date st_time2Date = null;
            try {
                st_time2Date = timeFormat1.parse(st_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String ETimeString = timeFormat2.format(date2);
            String[] parts = ETimeString.split(":");
            int endHour = Integer.parseInt(parts[0]);
            int endMinute = Integer.parseInt(parts[1]);

            String st_time2str = timeFormat2.format(st_time2Date);
            String[] parts1 = st_time2str.split(":");
            int startHour = Integer.parseInt(parts1[0]);
            int startMinute = Integer.parseInt(parts1[1]);

            String CTimeString = timeFormat2.format(current24Date);
            String[] parts2 = CTimeString.split(":");
            int currentHour = Integer.parseInt(parts2[0]);
            int currentMinute = Integer.parseInt(parts2[1]);

            if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
                if (startHour < 0 || (startHour > 5 || (startHour == 5 && (startMinute > 59 || 0 > 59)))) {
                    st_time = todayDate + "/" + st_time;
                    ed_time = todayDate + "/" + ed_time;
                } else {
                    st_time = tomorrowDate + "/" + st_time;
                    ed_time = todayDate + "/" + ed_time;
                }
            } else {
                if (currentHour < 0 || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {
                    st_time = todayDate + "/" + st_time;
                    ed_time = tomorrowDate + "/" + ed_time;
                } else {
                    st_time = todayDate + "/" + st_time;
                    ed_time = todayDate + "/" + ed_time;
                }
            }

        }
        st_time1 = st_time;
        ed_time1 = ed_time;

        Date st_time2Date = null;
        try {
            st_time2Date = timeFormat4.parse(st_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String st_time2str = timeFormat2.format(st_time2Date);
        String[] parts1 = st_time2str.split(":");
        int startHour = Integer.parseInt(parts1[0]);
        int startMinute = Integer.parseInt(parts1[1]);
//        new TimePickerDialog(ctx, timePickerListener, startHour, startMinute,
//                false).show();


        final Dialog mDateTimeDialog = new Dialog(ConfirmationScreen.this);
        // Inflate the root layout
        final RelativeLayout mDateTimeDialogView = (RelativeLayout) getLayoutInflater().inflate(R.layout.date_time_picker, null);
        // Grab widget instance
        final DateTimePicker mDateTimePicker = (DateTimePicker) mDateTimeDialogView.findViewById(R.id.DateTimePicker);
        mDateTimePicker.initData(st_time2Date);
        mDateTimePicker.setDateChangedListener(this);

        // Update demo edittext when the "OK" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.SetDateTime)).setOnClickListener(new View.OnClickListener
                () {
            public void onClick(View v) {
                SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MMM/yyyy/HH/mm", Locale.US);
                SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy/HH/mm", Locale.US);
                SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
                SimpleDateFormat timeFormat3 = new SimpleDateFormat("HH:mm", Locale.US);
                SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
                SimpleDateFormat timeFormat5 = new SimpleDateFormat("dd/MM/yyyy hh:mm a",Locale.US);
                SimpleDateFormat df5 = new SimpleDateFormat("dd-MM-yyyy hh:mm a",Locale.US);
                mDateTimePicker.clearFocus();
                String result_string = mDateTimePicker.getDay() + "/" + String.valueOf(mDateTimePicker.getMonth()) + "/" + String.valueOf(mDateTimePicker.getYear())
                        + "/" + String.valueOf(mDateTimePicker.getHour()) + "/" + String.valueOf(mDateTimePicker.getMinute());
//                edit_text.setText(result_string);
                Date pickerDate = null, pickerDate1 = null, stDate = null, edDate = null;
                String storeST, storeET;
                
                try {
                    pickerDate1 = timeFormat5.parse(timeResponse);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                storeST = getIntent().getExtras().getString("start_time");
                storeET = getIntent().getExtras().getString("end_time");
                try {
                    pickerDate = timeFormat.parse(result_string);
                    Log.i("TAG", "st_time " + storeST);
                    Log.i("TAG", "ed_time " + storeET);
                    stDate = df5.parse(storeST);
                    edDate = df5.parse(storeET);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (pickerDate.after(pickerDate1) && pickerDate.before(edDate)) {
                    for (Date d : listTimes) {
                        Log.i("CHANGE TIME", "FOR");
                        Date prayerDate = d;
                        Date prayerEndTime;
                        Calendar prayerEnd = Calendar.getInstance();
                        prayerEnd.setTime(d);
                        String estdate = timeFormat4.format(prayerEnd.getTime());
                        prayerEnd.add(Calendar.MINUTE, 20);
                        prayerEndTime = prayerEnd.getTime();
                        String payerString = timeFormat3.format(prayerDate);
                        String payerEndString = timeFormat3.format(prayerEndTime);
                        String CTimeString = timeFormat3.format(pickerDate);
                        String[] startParts = payerString.split(":");
                        String[] endParts = payerEndString.split(":");
                        String[] currentParts = CTimeString.split(":");
                        int startHourInteger = Integer.parseInt(startParts[0]);
                        int startMintInteger = Integer.parseInt(startParts[1]);
                        int endHourInteger = Integer.parseInt(endParts[0]);
                        int endMintInteger = Integer.parseInt(endParts[1]);
                        int currentHour = Integer.parseInt(currentParts[0]);
                        int currentMinute = Integer.parseInt(currentParts[1]);
                        int c = (int) (currentHour * 60) + (int) currentMinute;
                        int p = (int) (startHourInteger * 60) + (int) startMintInteger - (int) 5;
                        int f = (int) (endHourInteger * 60) + (int) endMintInteger;
                        if (c > p && c < f) {
                            Log.i("CHANGE TIME", "C>P");
                            expTimeTo = timeFormat2.format(prayerEndTime);
                            expectedtime1 = estdate+expTimeTo;
                            estTime.setText(estdate + " \n " + expTimeTo);
                            payerTimeIsYes = "true";
                            expChangeStr = expTimeTo;
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ConfirmationScreen.this, android.R.style.Theme_Material_Light_Dialog));

                            if (language.equalsIgnoreCase("En")) {
                                // set title
                                alertDialogBuilder.setTitle("Broasted Express");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("It's prayer time from " + payerString + " to " + payerEndString + " we are closed. Please select any other time (or) your order will be considered after prayer time")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                            } else if (language.equalsIgnoreCase("Ar")) {
                                alertDialogBuilder.setTitle("بروستد إكسبريس");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("اوقات الصلاة من %@ الى %@ الفرع مغلق الان . من فضلك حدد اي وقت اخر (أو) سيتم تنفيذ طلبك" + payerEndString + " د ا " + payerString + " ء الصلاة مباشرة")
                                        .setCancelable(false)
                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                            }


                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                        }
                    }

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(pickerDate);
                    int prep = PreparationTime + TravelTime;
                    calendar.add(Calendar.MINUTE, -prep);
                    kitchenStartDate = calendar.getTime();
                    Log.i("TAG","PreparationTime "+PreparationTime);
                    Log.i("TAG","TravelTime "+TravelTime);
                    Log.i("TAG","kitchenStartDate "+kitchenStartDate);
//                    Log.i("TAG","PrayingTime " + PrayingTime);

                    if (payerTimeIsYes.equals("false")) {
                        expTimeTo = df5.format(pickerDate);
                        expChangeStr = expTimeTo;
                        SimpleDateFormat estdate = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
                        String date = estdate.format(pickerDate);
                        String time = timeFormat2.format(pickerDate);
                        expChangeStr = expTimeTo;
                        expectedtime1 = df5.format(pickerDate);
                        estTime.setText(date +" \n "+time);
                        No_DistanceStr = "false";
                        payerTimeIsYes = "false";
                        changeTimeIsYes = "true";
                    }

                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ConfirmationScreen.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Broasted Express");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Order can't be processed for selected time, Please select different time.")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }


                mDateTimeDialog.dismiss();
            }
        });

        // Cancel the dialog when the "Cancel" button is clicked
        ((Button) mDateTimeDialogView.findViewById(R.id.CancelDialog)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDateTimeDialog.cancel();
            }
        });

        // Reset Date and Time pickers when the "Reset" button is clicked

//        ((Button) mDateTimeDialogView.findViewById(R.id.ResetDateTime)).setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                mDateTimePicker.reset();
//            }
//        });

        // Setup TimePicker
        // No title on the dialog window
        mDateTimeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Set the dialog content view
        mDateTimeDialog.setContentView(mDateTimeDialogView);
        // Display the dialog
        mDateTimeDialog.show();


    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;
            updateTime(hour, minute, 1);

        }

    };


    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTime(int hours, int mins, int atTime) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        Date parseD = doParse(aTime);
        if (hours < 6 && timeSet.equals("AM") || hours == 12 && timeSet.equals("AM")) {
            Toast.makeText(ctx, "At this time not available estimaiton time and order requests", Toast.LENGTH_SHORT).show();
            return;
        }
        if (atTime == 1)
//            if(oldTime!=null){
////                int diff = getTimeCaluculation();
//                Log.v("srinu", "old time not null: "+diff);
//                if(diff<0){
//                    Toast.makeText(ctx,"Please select future time",Toast.LENGTH_SHORT).show();
//                    return;
//                }
//            }
        /*if(atTime == 1||atTime == 2){
            oldTime = parseD;


            Log.v("srinu", "old time: ");
        }else{
            Log.v("srinu", "old time nnnn: ");
        }*/
            oldTime = parseD;
        checkStatus = false;

        Calendar cal = Calendar.getInstance();
        cal.setTime(parseD);
//        for(Date itemD : listTimes){
//            Log.v("srinu", "item cal: " + getTimeCaluculation());
//            int calC = getTimeCaluculation();
//            if(calC<=20&&calC>0){
//                checkStatus = true;
//                if(atTime == 1||atTime == 2)
//                    cal.add(Calendar.MINUTE,20);
//                //showAlert("Prayer time");
//            }
//        }
        if (atTime != 0) {
            if (!checkStatus) {
                //estTime.setText(getTimeStr(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
                //showAlert("There are no prayers found at this time");
            }
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df5 = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
        String exp_date = df5.format(c.getTime());
        expectedtime1 = exp_date+getTimeStr(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
        estTime.setText(exp_date + " \n " + getTimeStr(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE)));
        /*if(timeSet.equals("AM")){
            Toast.makeText(getApplicationContext(),"Please select time between 12:00 PM to 11:59 PM",Toast.LENGTH_SHORT).show();
        }
        else{
            btnClick.setText(aTime);
        }*/
    }

    public void getTimeCaluculation() {
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat4 = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US);
        SimpleDateFormat df5 = new SimpleDateFormat("dd-MM-yyyy hh:mm a",Locale.US);
        Date current24Date = null, currentServerDate = null;
        try {
            current24Date = timeFormat.parse(timeResponse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String currentTime = timeFormat.format(current24Date);
        try {
            currentServerDate = timeFormat.parse(currentTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (changeTimeIsYes.equals("false")) {
            Log.i("CHANGE TIME", "YES");
            minuts = 0;
            check_count = 0;
            changeMints = 0;
            check_count = myDbHelper.getTotalOrderQty();
            if (No_DistanceStr.equals("true")) {
//                int dist = distanceKilometers + 1;
//                minuts = minuts + dist;

                int pizzaCount = 0;
                for (Orders order : orderList) {
                    if (order.getCategoryId().equals("7") || order.getCategoryId().equals("8") || order.getCategoryId().equals("4") || order.getCategoryId().equals("6")) {
                        pizzaCount = pizzaCount + Integer.parseInt(order.getQty());
                    }
                }

                Log.i("TAG0", "" + pizzaCount);
                if(pizzaCount ==0) {
                    check_count = (check_count * 90 / 60) + 1;
                }
                else{
                    check_count = 60;
                }

                PreparationTime = check_count;
                TravelTime = trafficMins;
                Log.i("TAG","traveltime "+TravelTime);

                if (orderType.equalsIgnoreCase("Delivery")) {
                    minuts = trafficMins;
//                    changeMints = check_count;
                    if (minuts <= 5) {
                        minuts = 5;
                    }
                    minuts = minuts + check_count;
                    changeMints = minuts;
                } else {
                    minuts = trafficMins;
                    changeMints = check_count;
                    if (minuts <= 5) {
                        minuts = 5;
                    }
                    if (minuts < check_count) {
                        minuts = check_count;
                    }

                }

            }

            Calendar now = Calendar.getInstance();
            kitchenStartDate = now.getTime();
            now.setTime(currentServerDate);
            now.add(Calendar.MINUTE, minuts);
            currentServerDate = now.getTime();
            for (Date d : listTimes) {
                Log.i("CHANGE TIME", "FOR");
                Date prayerDate = d;
                Date prayerEndTime;
                Calendar prayerEnd = Calendar.getInstance();
                prayerEnd.setTime(d);
                prayerEnd.add(Calendar.MINUTE, 20);
                prayerEndTime = prayerEnd.getTime();
                String payerString = timeFormat1.format(prayerDate);
                String payerEndString = timeFormat1.format(prayerEndTime);
                String CTimeString = timeFormat1.format(currentServerDate);
                String[] startParts = payerString.split(":");
                String[] endParts = payerEndString.split(":");
                String[] currentParts = CTimeString.split(":");
                int startHourInteger = Integer.parseInt(startParts[0]);
                int startMintInteger = Integer.parseInt(startParts[1]);
                int endHourInteger = Integer.parseInt(endParts[0]);
                int endMintInteger = Integer.parseInt(endParts[1]);
                int CTHourInteger = Integer.parseInt(currentParts[0]);
                int CTMintInteger = Integer.parseInt(currentParts[1]);
                int c = (int) (CTHourInteger * 60) + (int) CTMintInteger;
                int p = (int) (startHourInteger * 60) + (int) startMintInteger - (int) 5;
                int f = (int) (endHourInteger * 60) + (int) endMintInteger;
                if (c > p && c < f) {
                    Log.i("CHANGE TIME", "C>P");
                    expTimeTo = df5.format(prayerEndTime);
                    Calendar e = Calendar.getInstance();
                    SimpleDateFormat estdate = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
                    String exp_date = estdate.format(e.getTime());
                    String date = estdate.format(prayerEndTime);
                    String time = timeFormat2.format(prayerEndTime);
                    expectedtime1 = df5.format(prayerEndTime);
                    estTime.setText(date +" \n "+time);
                    payerTimeIsYes = "true";
                }
            }
            if (payerTimeIsYes.equals("false")) {
                expTimeTo = df5.format(currentServerDate);
                Calendar c = Calendar.getInstance();
                SimpleDateFormat estdate = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
                String exp_date = estdate.format(c.getTime());
                String date = estdate.format(currentServerDate);
                String time = timeFormat2.format(currentServerDate);
                expectedtime1= df5.format(currentServerDate);
                estTime.setText(date +" \n "+time);
            }
        } else {
            Log.i("CHANGE TIME", "NO");
            Date prayerDate = null;
            try {
                prayerDate = timeFormat2.parse(expTimeTo);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                String payerString = timeFormat1.format(prayerDate);
                String CTimeString = timeFormat1.format(currentServerDate);
                String[] prayerParts = payerString.split(":");
                String[] currentParts = CTimeString.split(":");
                int startHourInteger = Integer.parseInt(prayerParts[0]);
                int startMintInteger = Integer.parseInt(prayerParts[1]);
                int CTHourInteger = Integer.parseInt(currentParts[0]);
                int CTMintInteger = Integer.parseInt(currentParts[1]);
                int c = (int) (CTHourInteger * 60) + (int) CTMintInteger + minuts;
                int p = (int) (startHourInteger * 60) + (int) startMintInteger;
                if (c >= p) {
                    changeTimeIsYes = "false";
                    getTimeCaluculation();
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }


        String startTime = expTimeTo.replace(" ", "");
        if (fullHours.equalsIgnoreCase("true")) {
            return;
        }

        openTimeStr = getIntent().getExtras().getString("start_time");
        endTime = getIntent().getExtras().getString("end_time");
        endTime = endTime.replace(" ", "");
        Log.i("start TIME", "" + openTimeStr);
        Log.i("end TIME", "" + endTime);

        if (endTime.equals("12:00AM")) {
            endTime = "11:59PM";
        }
        Date date21 = null, date1 = null, date2 = null;
        try {
//            date21 = timeFormat3.parse(endTime);
            date1 = timeFormat2.parse(expTimeTo);
            date2 = timeFormat3.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String st_time = timeFormat1.format(date1);
        String ed_time = timeFormat1.format(date2);
        String currentServerDate1 = timeFormat1.format(current24Date);

        String[] parts = ed_time.split(":");
        int endHour = Integer.parseInt(parts[0]);
        int endMinute = Integer.parseInt(parts[1]);

        String[] parts1 = st_time.split(":");
        int startHour = Integer.parseInt(parts1[0]);
        int startMinute = Integer.parseInt(parts1[1]);

        String[] parts2 = currentServerDate1.split(":");
        int currentHour = Integer.parseInt(parts2[0]);
        int currentMinute = Integer.parseInt(parts2[1]);

        if (endHour < 0 || (endHour > 5 || (endHour == 5 && (endMinute > 59 || 0 > 59)))) {
            if (startHour < 0 || (startHour > 5 || (startHour == 5 && (startMinute > 59 || 0 > 59)))) {
                st_time = todayDate + " " + st_time;
                ed_time = todayDate + " " + ed_time;
            } else {
                st_time = tomorrowDate + " " + st_time;
                ed_time = todayDate + " " + ed_time;
            }
        } else {
            if (currentHour < 0 || (currentHour > 5 || (currentHour == 5 && (currentMinute > 59 || 0 > 59)))) {
                st_time = todayDate + " " + st_time;
                ed_time = tomorrowDate + " " + ed_time;
            } else {
                st_time = todayDate + " " + st_time;
                ed_time = todayDate + " " + ed_time;
            }
        }

        Log.i("DATE TAG ddd", st_time + "  " + ed_time);

        Date date3 = null, date4 = null;
        try {
//            date21 = timeFormat3.parse(endTime);
            date3 = timeFormat4.parse(st_time);
            date4 = timeFormat4.parse(ed_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date3.before(date4)) {
            return;
        } else {
            onBackPressed();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ConfirmationScreen.this, android.R.style.Theme_Material_Light_Dialog));

            if (language.equalsIgnoreCase("En")) {
                // set title
                alertDialogBuilder.setTitle("Broasted Express");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Sorry Store is closed! You can't make the order from this store.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            } else if (language.equalsIgnoreCase("Ar")) {
                // set title
                alertDialogBuilder.setTitle("بروستد إكسبريس");

                // set dialog message
                alertDialogBuilder
                        .setMessage("نأسف الفرع مغلق ، لا يمكنك إتمام الطلب من ذلك الفرع")
                        .setCancelable(false)
                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
            }
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

        }

//        long difference = d2.getTime() - d1.getTime();
//        int days = (int) (difference / (1000*60*60*24));
//        int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
//        int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);

    }

    public static String getSourceCode(String requestURL) {
        String response = "";
        try {
            URL url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "UTF-8"));
                while ((line = br.readLine()) != null) {
                    response += line;
                }

            } else {
                response = "";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return response;
        }
        return response;
    }

    public class GetTimes extends AsyncTask<Void, String, String> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ConfirmationScreen.this);
            pd.setMessage("Please wait...");
            pd.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            String res = getSourceCode("http://muslimsalat.com/riyadh/daily.json?key=api_key");
            Log.d("Responce", "" + res);
            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s == null) {
                pd.dismiss();
            } else {
                pd.dismiss();
                listTimes.clear();
                try {
                    JSONObject resObj = new JSONObject(s);
                    JSONArray array = resObj.getJSONArray("items");
                    SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd hh:mm a",Locale.US);

                    String date = String.valueOf(array.getJSONObject(0).getString("date_for"));
                    String israq = String.valueOf(array.getJSONObject(0).getString("shurooq"));
                    String zohar = String.valueOf(array.getJSONObject(0).getString("dhuhr"));
                    String asra = String.valueOf(array.getJSONObject(0).getString("asr"));
                    String magrib = String.valueOf(array.getJSONObject(0).getString("maghrib"));
                    String isha = String.valueOf(array.getJSONObject(0).getString("isha"));

                    Log.i("TAG", "date " + date);
                    Log.i("TAG", "isha " + isha);

                    String israq1 = null;
                    String zohar1 = null;
                    String asra1 = null;
                    String magrib1 = null;
                    String isha1 = null;
                    try {
                        israq1 = String.valueOf(datetime.parse(date + israq));
                        zohar1 = String.valueOf(datetime.parse(date + zohar));
                        asra1 = String.valueOf(datetime.parse(date + asra));
                        magrib1 = String.valueOf(datetime.parse(date + magrib));
                        isha1 = String.valueOf(datetime.parse(date + isha));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    listTimes.add(doParse(array.getJSONObject(0).getString(israq1)));
                    listTimes.add(doParse(array.getJSONObject(0).getString(zohar1)));
                    listTimes.add(doParse(array.getJSONObject(0).getString(asra1)));
                    listTimes.add(doParse(array.getJSONObject(0).getString(magrib1)));
                    listTimes.add(doParse(array.getJSONObject(0).getString(isha1)));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                getTimeCaluculation();

                super.onPostExecute(s);
            }
        }
    }

    String getTimeStr(int hours, int mins) {
        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();
        return aTime;
    }

    Date doParse(String timeStr) {
        try {

            SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.US); //if 24 hour format
            return format.parse(timeStr);
        } catch (Exception e) {

            Log.e("Exception is ", e.toString());
        }
        return null;
    }


    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ConfirmationScreen.this);
            dialog = ProgressDialog.show(ConfirmationScreen.this, "",
                    "Calculating time...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                //24.70321657, 46.68097073
//                distanceResponse = jParser
//                        .getJSONFromUrl(URL_DISTANCE + "24.70321657,46.68097073&destinations="+ latitude +","+ longitude+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");

                try {
                    latitude = getIntent().getDoubleExtra("latitude", 0);
                    longitude = getIntent().getDoubleExtra("longitude", 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("TAG", "lat " + getIntent().getDoubleExtra("lat", 0));

                lat = getIntent().getDoubleExtra("lat", 0);
                longi = getIntent().getDoubleExtra("longi", 0);

                if (orderType.equalsIgnoreCase("Delivery")) {
                    distanceResponse = jParser
                            .getJSONFromUrl(URL_DISTANCE + latitude + "," + longitude + "&destinations=" + lat + "," + longi + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");

                } else {
                    distanceResponse = jParser
                            .getJSONFromUrl(URL_DISTANCE + lat + "," + longi + "&destinations=" + latitude + "," + longitude + "&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                }
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ConfirmationScreen.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Broasted Express");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        secs = jo3.getString("value");
                        trafficMins = Integer.parseInt(secs) / 60;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            dialog.dismiss();
            new GetCurrentTime().execute();
            super.onPostExecute(result);
        }
    }

    public class InsertOrder extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;
        String currentTime;
        String kitchenStart;
        String estimatedTime, total_amt;
        JSONObject parent = new JSONObject();
        InputStream inputStream = null;
        String result = "";

        String itemId = "", qty = "", comments = "", total_price = null, item_price = "", sizes = "", additionals = "", additionalsPices = "";
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
        SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
        SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);
        SimpleDateFormat timeFormat5 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ConfirmationScreen.this);
            dialog = ProgressDialog.show(ConfirmationScreen.this, "",
                    "Please wait...");
//            promocodeStr = promocode.getText().toString();
//            if(promocodeStr.length()!=5){
//                promocodeStr = "No";
//            }

            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());


            SimpleDateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);

            kitchenStart = df3.format(kitchenStartDate);

//            String dtc = "2014-04-02T07:59:02.111Z";
//            SimpleDateFormat readDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            df3.setTimeZone(TimeZone.getDefault());
//            df3.setTimeZone(TimeZone.getTimeZone("GMT")); // missing line
            Date date = null;
            try {
                date = df3.parse(String.valueOf(kitchenStart));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat writeDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",Locale.US);
            writeDate.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
            String s = writeDate.format(date);
            String s1 = df3.format(date);

            Log.e("TAG","date1 " +s);

            Log.e("TAG","date  "+s1);
            SimpleDateFormat df4 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            currentTime = df4.format(c.getTime());
            estimatedTime = expectedtime1.replace("a.m", "AM").replace("p.m", "PM").replace("am", "AM").replace("pm", "PM").replace("AM.","AM").replace("PM.","PM");
            Log.e("TAG","est "+expectedtime1);
            total_amt = mconf_total_amount.getText().toString();

            mamount = decim.format(Float.parseFloat(String.valueOf(myDbHelper.getTotalOrderPrice()))).replace("0,00","0.00");

            float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
            mvatamount = decim.format(tax);

            String version = "";
            try {
                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                version = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }


            if (total_amt.equalsIgnoreCase("free")) {
                paymentMode = 4;
            }

            try {
                JSONArray mainItem = new JSONArray();
                JSONArray subItem = new JSONArray();
                JSONArray promoItem = new JSONArray();

                JSONObject mainObj = new JSONObject();
                mainObj.put("UserId", userId);
                mainObj.put("StoreId", storeId);
                mainObj.put("OrderType", Projecturl.ORDER_TYPE);
                mainObj.put("ExpectedTime", estimatedTime);
                mainObj.put("PreparationTime",PreparationTime);
                mainObj.put("TravelTime",TravelTime);
                mainObj.put("KichenStartTime",s);
                mainObj.put("OrderDate", currentTime);
                mainObj.put("IsFavorite", false);
                mainObj.put("FavoriteName", "");
                mainObj.put("comments", "Android v"+version);
                mainObj.put("PaymentMode", paymentMode);
                mainObj.put("Total_Price", Projecturl.convertToArabic(total_amt));
                mainObj.put("OrderStatus", "New");
                mainObj.put("Device_token", SplashActivity.regId);
                mainObj.put("VatPercentage", "5");
                mainObj.put("SubTotal",Projecturl.convertToArabic(mamount));
                mainObj.put("VatCharges",Projecturl.convertToArabic(mvatamount));


                if (orderType.equalsIgnoreCase("Delivery")) {
                    mainObj.put("AddressID", addressId);
                }
                mainItem.put(mainObj);

                JSONObject promoObj = new JSONObject();
                promoObj.put("DeviceToken", SplashActivity.regId);
                promoObj.put("promotionCode", promocodeStr);
                promoObj.put("BonusAmt", remainingBonusInt);
                promoItem.put(promoObj);

                for (Orders orders : orderList) {
                    JSONObject subObj = new JSONObject();

                    JSONArray subItem1 = new JSONArray();
                    JSONArray subItem2 = new JSONArray();
                    subObj.put("ItemPrice", Projecturl.convertToArabic(decim.format(Float.parseFloat(orders.getPrice()))));
                    subObj.put("Qty", orders.getQty());
                    subObj.put("Comments", orders.getComment());
                    subObj.put("ItemId", orders.getItemId());
                    subObj.put("Size", orders.getItemTypeId());

                    if (orders.getAdditionals() != null && !orders.getAdditionals().equals("")&& !orders.getAdditionalsPrice().equals("")) {

                        JSONObject subObj1 = new JSONObject();
                        Log.i("TAG", "add id " + orders.getAdditionals());
                        String ids = orders.getAdditionals();
                        String prices = orders.getAdditionalsPrice();
                        String qty = orders.getAdditionalsTypeId();
                        List<String> idsList = Arrays.asList(ids.split(","));
                        List<String> pricesList = Arrays.asList(prices.split(","));
                        List<String> qtyList = Arrays.asList(qty.split(","));
                        for (int i = 0; i < idsList.size(); i++) {
                            subObj1.put("AdditionalID", idsList.get(i));
                            subObj1.put("AdditionalPrice", pricesList.get(i));
                            subObj1.put("AddQty", qtyList.get(i));
                            subItem2.put(subObj1);
                        }
                    }else {
                        JSONObject subObj1 = new JSONObject();
//                            if(addPriceParts[i] != null) {
                        subObj1.put("AdditionalID", "0");
                        subObj1.put("AdditionalPrice", "0.00");
                        subObj1.put("AddQty", "0");
                        subItem2.put(subObj1);
                    }

                    subItem1.put(subObj);
                    if (subItem2.length() > 0) {
                        subItem1.put(subItem2);
                    }
                    subItem.put(subItem1);
                }
                if(promoTypeStr.equals("6")){
                    if(promoArray!=null && promoArray.length()>0) {
                        subItem.put(promoArray);
                    }
                }
                parent.put("MainItem", mainItem);
                parent.put("SubItem", subItem);
                if (remainingBonusInt != 0) {
                    parent.put("Promotion", promoItem);
                }
                Log.i("TAG", parent.toString());
            } catch (JSONException je) {

            }


        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Projecturl.INSERT_ORDER_URL);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(parent.toString());

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            result = convertInputStreamToString(inputStream);
                            Log.e("Response", "" + result);
                            return result;
                        }

                    } catch (Exception e) {
                        Log.e("InputStream", e.getLocalizedMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                return "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                if (result1.equalsIgnoreCase("no internet")) {
                    dialog.dismiss();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ConfirmationScreen.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Broasted Express");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
//                    Constants.orderNow = false;

                    String order_number = "";
                    try {
                        JSONObject jo = new JSONObject(result);
                        order_number = jo.getString("Success");
                        Log.e("TAG",""+result);
                        String[] orderNo = order_number.split("-");
                        String[] parts = order_number.split(",");
                        final String orderId = parts[0];

                        myDbHelper.deleteOrderTable();
                        try {
                            Checkout.amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
                            float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                            Checkout.vatAmount.setText(""+decim.format(tax));
                            Checkout.netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));
                            Checkout.morder_price.setText("" + decim.format( myDbHelper.getTotalOrderPrice()+tax));
                            Checkout.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
                            Checkout.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                        OrderFragment.orderPrice.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                        OrderFragment.orderQuantity.setText("" + myDbHelper.getTotalOrderQty());
                            InsideMenu.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            InsideMenu.morder_price.setText("" + decim.format(myDbHelper.getTotalOrderPrice()));
                            InsideMenu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());


                            Menu.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                            Menu.morder_price.setText("" + decim.format(myDbHelper.getTotalOrderPrice() ));
                            Menu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        Date current24Date = null, currentServerDate = null;
                        Date expectedTimeDate = null, expectedTime24 = null;
                        try {
                            current24Date = timeFormat.parse(timeResponse);
                            expectedTime24 = timeFormat5.parse(estimatedTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String currentTime = timeFormat1.format(current24Date);
                        String expTimeStr = timeFormat5.format(expectedTime24);
                        try {
                            currentServerDate = timeFormat1.parse(currentTime);
                            expectedTimeDate = timeFormat5.parse(expTimeStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long diff = expectedTimeDate.getTime() - currentServerDate.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        int expMins = (int) diffMinutes * 60 * 1000;
                        Log.i("TAG", "mins response: " + expMins);
                        orderPrefsEditor.putString("order_id", orderId);
                        orderPrefsEditor.putString("order_status", "open");
                        orderPrefsEditor.commit();
                        try {
                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    orderPrefsEditor.putString("order_id", orderId);
                                    orderPrefsEditor.putString("order_status", "close");
                                    orderPrefsEditor.commit();
                                }
                            }, expMins);
                        } catch (Exception e) {

                        }

                        Intent intent = new Intent(ConfirmationScreen.this, OrderCompleted.class);
                        intent.putExtra("storeId", storeId);
                        intent.putExtra("storeName", storeName);
                        intent.putExtra("storeAddress", storeAddress);
                        intent.putExtra("storeName_ar", storeName_ar);
                        intent.putExtra("storeAddress_ar", storeAddress_ar);
                        intent.putExtra("your_name", your_name);
                        intent.putExtra("your_address", your_address);
                        intent.putExtra("latitude", latitude);
                        intent.putExtra("longitude", longitude);
                        intent.putExtra("amount",Projecturl.convertToArabic(amount.getText().toString()));
                        intent.putExtra("vatamount",Projecturl.convertToArabic(vatAmount.getText().toString()));
                        intent.putExtra("netamount",Projecturl.convertToArabic(netTotal.getText().toString()));
                        intent.putExtra("total_amt", Projecturl.convertToArabic(mconf_total_amount.getText().toString()));
                        intent.putExtra("total_items", mconf_total_items.getText().toString());
                        intent.putExtra("expected_time", estimatedTime);
                        intent.putExtra("payment_mode", Integer.toString(paymentMode));
                        intent.putExtra("order_type", orderType);
                        intent.putExtra("order_number", order_number);
                        startActivity(intent);

                    } catch (JSONException je) {
                        je.printStackTrace();
                        try {
                            JSONObject jo = new JSONObject(result);
                            String msg = jo.getString("Failure");
                            if(msg.equalsIgnoreCase("Promotion Already Used")){
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.view.ContextThemeWrapper(ConfirmationScreen.this, android.R.style.Theme_Material_Light_Dialog));

                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("Broasted Express");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Promotion Already Used")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    promoLayout.setClickable(true);
                                                    promocode.setText("");
//                                                    sr.setVisibility(View.VISIBLE);
                                                    promoArrow.setVisibility(View.VISIBLE);
                                                    promoCancel.setVisibility(View.GONE);
                                                    promocode.setHint("Apply Promotion");
                                                    mcash_on.setCompoundDrawablesWithIntrinsicBounds(R.drawable.delivered, 0, R.drawable.conf_cash_on_delivary, 0);
                                                    monline_payment1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.online, 0, R.drawable.conf_online_payment, 0);
                                                  mconf_total_amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
                                                    if (language.equalsIgnoreCase("En")) {
                                                        promocode.setHint("Apply Promotion");
                                                    } else if (language.equalsIgnoreCase("Ar")) {
                                                        promocode.setHint("قدم للعرض");
                                                    }
                                                    dialog.dismiss();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle("بروستد إكسبريس");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("")
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    promoLayout.setClickable(true);
                                                    promocode.setText("");
//                                                    sr.setVisibility(View.VISIBLE);
                                                    promoArrow.setVisibility(View.VISIBLE);
                                                    promoCancel.setVisibility(View.GONE);
                                                    promocode.setHint("Apply Promotion");
                                                  mconf_total_amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
                                                    if (language.equalsIgnoreCase("En")) {
                                                        promocode.setHint("Apply Promotion");
                                                    } else if (language.equalsIgnoreCase("Ar")) {
                                                        promocode.setHint("قدم للعرض");
                                                    }
                                                    dialog.dismiss();
                                                }
                                            });
                                }

//
                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                                alertDialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }
            }
            dialog.dismiss();
            super.onPostExecute(result1);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }


    public class GetStoredCards extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        double lat, longi;
        String networkStatus;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(ConfirmationScreen.this);
//            dialog = ProgressDialog.show(getActivity(), "",
//                    "Loading. Please Wait....");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    JSONParser jParser = new JSONParser();

                    getCardResponse = jParser
                            .getJSONFromUrl(arg0[0]);
                    Log.i("TAG", "user response:" + getCardResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                getCardResponse = "no internet";
            }
            return getCardResponse;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (getCardResponse == null) {

            } else if (getCardResponse.equals("no internet")) {
                Toast.makeText(ConfirmationScreen.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            } else {

                try {
                    JSONObject jo = new JSONObject(getCardResponse);
                    JSONArray ja = jo.getJSONArray("Success");
                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jo1 = ja.getJSONObject(i);
                        String sTokenStr = jo1.getString("SToken");


                        List<PWAccount> sToken = _binder.getAccountFactory().deserializeAccountList(sTokenStr);
                        accounts.addAll(sToken);
                    }
                } catch (Exception e) {
                    Log.d("", "Error while parsing the results!");
                    e.printStackTrace();
                }


                super.onPostExecute(result1);
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {
            Log.i("mobile.connect.checkout", "user canceled the checkout process/error");
//            Toast.makeText(OrderConfirmation.this, "Checkout cancelled or an error occurred.", Toast.LENGTH_SHORT).show();
            mconfirm_order.setClickable(true);
        } else if (resultCode == RESULT_OK) {
            Toast.makeText(ConfirmationScreen.this, "Thank you for shopping!", Toast.LENGTH_SHORT).show();
            new InsertOrder().execute();
            // if the user added a new account, store it
            if (data.hasExtra(PWConnectCheckoutActivity.CONNECT_CHECKOUT_RESULT_ACCOUNT)) {
                Log.i("mainactivity", "checkout went through, callback has an account");
                accounts.clear();
                ArrayList<PWAccount> newAccounts = data.getExtras().getParcelableArrayList(PWConnectCheckoutActivity.CONNECT_CHECKOUT_RESULT_ACCOUNT);
                accounts.addAll(newAccounts);

                try {
                    Log.i("Card", "  " + _binder.getAccountFactory().serializeAccountList(accounts).toString().replace(" ", ""));
//                    new InsertCardDetails().execute(Constants.SAVE_CARD_DETAILS_URL+userId+"&sToken="+_binder.getAccountFactory().serializeAccountList(newAccounts).replace(" ",""));
//                    sharedSettings.edit().putString(ACCOUNTS, _binder.getAccountFactory().serializeAccountList(accounts)).commit();
                } catch (PWProviderNotInitializedException e) {
                    e.printStackTrace();
                }
            } else {
                Log.i("mainactivity", "checkout went through, callback has transaction result");
            }
        }
    }

//    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String networkStatus;
//        ProgressDialog dialog;
//        String dayOfWeek;
//
//        @Override
//        protected void onPreExecute() {
//            storesList.clear();
//            networkStatus = NetworkUtil.getConnectivityStatusString(ConfirmationScreen.this);
//            dialog = ProgressDialog.show(ConfirmationScreen.this, "",
//                    "Fetching stores...");
//            Calendar calendar = Calendar.getInstance();
//            Date date = calendar.getTime();
//            // full name form of the day
//            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0] + dayOfWeek);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            } else {
//                return "no internet";
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//            storesList.clear();
//            if (result != null) {
//                if (result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(ConfirmationScreen.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                } else {
//                    if (result.equals("")) {
//                        Toast.makeText(ConfirmationScreen.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//                    } else {
//
//                        try {
//                            JSONArray ja = new JSONArray(result);
//                            for (int i = 0; i < ja.length(); i++) {
//                                StoreInfo si = new StoreInfo();
//                                JSONObject jo = ja.getJSONObject(i);
////                                lat = 24.70321657;
////                                longi = 46.68097073;
//                                si.setStoreId(jo.getString("storeId"));
//                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
//                                si.setStarttime(jo.getString("ST"));
//                                si.setEndtime(jo.getString("ET"));
//                                storeName=(jo.getString("StoreName"));
//                                storeAddress=(jo.getString("StoreAddress"));
//                                latitude=(jo.getDouble("24.682588"));
//                                longi=(jo.getDouble("46.688150"));
//                                si.setCountryName(jo.getString("CountryName"));
//                                si.setCityName(jo.getString("CityName"));
//                                si.setImageURL(jo.getString("imageURL"));
//                                si.setFamilySection(jo.getString("FamilySection"));
//                                si.setWifi(jo.getString("Wifi"));
//                                si.setPatioSitting(jo.getString("PatioSitting"));
//                                si.setDriveThru(jo.getString("DriveThru"));
//                                si.setMeetingSpace(jo.getString("MeetingSpace"));
//                                si.setHospital(jo.getString("Hospital"));
//                                si.setUniversity(jo.getString("University"));
//                                si.setOffice(jo.getString("Office"));
//                                si.setShoppingMall(jo.getString("ShoppingMall"));
//                                si.setStoreName(storeName);
//                                si.setStoreAddress(storeAddress);
//                                si.setLatitude(latitude);
//                                si.setLongitude(longi);
//
//                                Log.e("TAG", String.valueOf(+latitude)+longi);
//
//
//                                try {
//                                    si.setAirport(jo.getString("Airport"));
//                                } catch (Exception e) {
//                                    si.setAirport("false");
//                                }
//                                try {
//                                    si.setDineIn(jo.getString("DineIn"));
//                                } catch (Exception e) {
//                                    si.setDineIn("false");
//                                }
//                                try {
//                                    si.setLadies(jo.getString("Ladies"));
//                                } catch (Exception e) {
//                                    si.setLadies("false");
//                                }
//
//                                si.setNeighborhood(jo.getString("Neighborhood"));
//                                si.setStoreNumber(jo.getString("phone"));
//                                si.setIs24x7(jo.getString("is24x7"));
//                                si.setStatus(jo.getString("status"));
//                                si.setOgCountry(jo.getString("OGCountry"));
//                                si.setOgCity(jo.getString("OGCity"));
//                                storeName_ar=(jo.getString("StoreName_ar"));
//                                storeAddress_ar=(jo.getString("StoreAddress_ar"));
//                                si.setStoreNumber(jo.getString("phone"));
//                                si.setStoreName_ar(storeName_ar);
//                                si.setStoreAddress_ar(storeAddress_ar);
//
//                                if (language.equalsIgnoreCase("En")) {
//                                    mstore_name.setText(WordUtils.capitalizeFully(storeName));
//                                    mstore_address.setText(WordUtils.capitalizeFully(storeAddress));
//                                }else if (language.equalsIgnoreCase("Ar")) {
//                                    mstore_name.setText(WordUtils.capitalizeFully(storeName_ar));
//                                    mstore_address.setText(WordUtils.capitalizeFully(storeAddress_ar));
//                                }
//                                try {
//                                    si.setMessage(jo.getString("Message"));
//                                } catch (Exception e) {
//                                    si.setMessage("");
//                                }
//
//                                try {
//                                    si.setMessage_ar(jo.getString("Message_ar"));
//                                } catch (Exception e) {
//                                    si.setMessage_ar("");
//                                }
//
//                                Location me = new Location("");
//                                Location dest = new Location("");
//
//                                me.setLatitude(lat);
//                                me.setLongitude(longi);
//
//                                dest.setLatitude(jo.getDouble("Latitude"));
//                                dest.setLongitude(jo.getDouble("Longitude"));
//
//                                float dist = (me.distanceTo(dest)) / 1000;
//                                si.setDistance(dist);
//
//
//                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
//                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
//                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
//                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
//
//                                Calendar c = Calendar.getInstance();
//                                System.out.println("Current time => " + c.getTime());
//                                serverTime = timeResponse;
//                                String startTime = si.getStarttime().replace(" ", "");
//                                String endTime = si.getEndtime().replace(" ", "");
//
//
//                                if (dist <= 50 && (jo.getBoolean("OnlineOrderStatus"))) {
//
//
//                                    if (startTime.equals("null") && endTime.equals("null")) {
//                                        si.setOpenFlag(-1);
//                                        storesList.add(si);
//                                    } else {
//
//                                        if (endTime.equals("00:00AM")) {
//                                            si.setOpenFlag(1);
//                                            storesList.add(si);
//
//                                            continue;
//                                        } else if (endTime.equals("12:00AM")) {
//                                            endTime = "11:59PM";
//                                        }
//
//                                        Calendar now = Calendar.getInstance();
//
//                                        int hour = now.get(Calendar.HOUR_OF_DAY);
//                                        int minute = now.get(Calendar.MINUTE);
//
//
//                                        Date serverDate = null;
//                                        Date end24Date = null;
//                                        Date start24Date = null;
//                                        Date current24Date = null;
//                                        Date dateToday = null;
//                                        Calendar dateStoreClose = Calendar.getInstance();
//                                        try {
//                                            end24Date = timeFormat.parse(endTime);
//                                            start24Date = timeFormat.parse(startTime);
//                                            serverDate = dateFormat.parse(serverTime);
//                                            dateToday = dateFormat1.parse(serverTime);
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//                                        dateStoreClose.setTime(dateToday);
//                                        dateStoreClose.add(Calendar.DATE, 1);
//                                        String current24 = timeFormat1.format(serverDate);
//                                        String end24 = timeFormat1.format(end24Date);
//                                        String start24 = timeFormat1.format(start24Date);
//                                        String startDateString = dateFormat1.format(dateToday);
//                                        String endDateString = dateFormat1.format(dateToday);
//                                        String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
//                                        dateStoreClose.add(Calendar.DATE, -2);
//                                        String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());
//
//                                        Date startDate = null;
//                                        Date endDate = null;
//
//                                        try {
//                                            end24Date = timeFormat1.parse(end24);
//                                            start24Date = timeFormat1.parse(start24);
//                                            current24Date = timeFormat1.parse(current24);
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        String[] parts2 = start24.split(":");
//                                        int startHour = Integer.parseInt(parts2[0]);
//                                        int startMinute = Integer.parseInt(parts2[1]);
//
//                                        String[] parts = end24.split(":");
//                                        int endHour = Integer.parseInt(parts[0]);
//                                        int endMinute = Integer.parseInt(parts[1]);
//
//                                        String[] parts1 = current24.split(":");
//                                        int currentHour = Integer.parseInt(parts1[0]);
//                                        int currentMinute = Integer.parseInt(parts1[1]);
//
//
////                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");
//
//
//                                        if (startTime.contains("AM") && endTime.contains("AM")) {
//                                            if (startHour < endHour) {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateString + "  " + endTime;
//                                                try {
//                                                    startDate = dateFormat2.parse(startDateString);
//                                                    endDate = dateFormat2.parse(endDateString);
//                                                } catch (ParseException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            } else if (startHour > endHour) {
//                                                if (serverTime.contains("AM")) {
//                                                    if (currentHour > endHour) {
//                                                        startDateString = startDateString + " " + startTime;
//                                                        endDateString = endDateTomorrow + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    } else {
//                                                        startDateString = endDateYesterday + " " + startTime;
//                                                        endDateString = endDateString + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    }
//                                                } else {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateTomorrow + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                }
//                                            }
//                                        } else if (startTime.contains("AM") && endTime.contains("PM")) {
//                                            startDateString = startDateString + " " + startTime;
//                                            endDateString = endDateString + "  " + endTime;
//                                            try {
//                                                startDate = dateFormat2.parse(startDateString);
//                                                endDate = dateFormat2.parse(endDateString);
//                                            } catch (ParseException e) {
//                                                e.printStackTrace();
//                                            }
//                                        } else if (startTime.contains("PM") && endTime.contains("AM")) {
//                                            if (serverTime.contains("AM")) {
//                                                if (currentHour <= endHour) {
//                                                    startDateString = endDateYesterday + " " + startTime;
//                                                    endDateString = endDateString + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                } else {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateTomorrow + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                }
//                                            } else {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateTomorrow + "  " + endTime;
//                                                try {
//                                                    startDate = dateFormat2.parse(startDateString);
//                                                    endDate = dateFormat2.parse(endDateString);
//                                                } catch (ParseException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
//
//                                        } else if (startTime.contains("PM") && endTime.contains("PM")) {
//                                            startDateString = startDateString + " " + startTime;
//                                            endDateString = endDateString + "  " + endTime;
//                                            try {
//                                                startDate = dateFormat2.parse(startDateString);
//                                                endDate = dateFormat2.parse(endDateString);
//                                            } catch (ParseException e) {
//                                                e.printStackTrace();
//                                            }
//                                        }
//
//
//                                        String serverDateString = dateFormat2.format(serverDate);
//
//                                        try {
//                                            serverDate = dateFormat2.parse(serverDateString);
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        Log.i("TAG DATE", "" + startDate);
//                                        Log.i("TAG DATE1", "" + endDate);
//                                        Log.i("TAG DATE2", "" + serverDate);
//
//                                        if (serverDate.after(startDate) && serverDate.before(endDate)) {
//                                            Log.i("TAG Visible", "true");
//                                            si.setOpenFlag(1);
//                                            storesList.add(si);
//                                        } else {
//                                            si.setOpenFlag(0);
//                                            storesList.add(si);
//                                        }
//                                    }
//                                }
////                                storesList.add(si);
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        Collections.sort(storesList, StoreInfo.storeDistance);
//                        Collections.sort(storesList, StoreInfo.storeOpenSort);
//                    }
//                }
//
//            } else {
//                Toast.makeText(ConfirmationScreen.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if (dialog != null) {
//                dialog.dismiss();
//            }
////            mAdapter.notifyDataSetChanged();
////            loading = false;
////            swipeLayout.setRefreshing(false);
//
//            if (storesList.size() == 0) {
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ConfirmationScreen.this, android.R.style.Theme_Material_Light_Dialog));
//
//                if (language.equalsIgnoreCase("En")) {
//                    // set title
//                    alertDialogBuilder.setTitle("Broasted Express");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("We're sorry, but no stores were found in your area.")
//                            .setCancelable(false)
//                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    Intent a = new Intent(ConfirmationScreen.this, OrderType.class);
//                                    startActivity(a);
//                                    dialog.dismiss();
//                                }
//                            });
//                } else if (language.equalsIgnoreCase("Ar")) {
//                    // set title
//                    alertDialogBuilder.setTitle("بروستد إكسبريس");
//
//                    // set dialog message
//                    alertDialogBuilder
//                            .setMessage("نعتذر لك ، لا يوجد فروع متوفرة في منطتك")
//                            .setCancelable(false)
//                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    Intent a = new Intent(ConfirmationScreen.this, OrderType.class);
//                                    startActivity(a);
//                                    dialog.dismiss();
//                                }
//                            });
//                }
//
//                // create alert dialog
//                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                // show it
//                alertDialog.show();
//            }
//
//            super.onPostExecute(result);
//
//        }
//
//    }

    public void dialog() {

        final Dialog dialog2 = new Dialog(ConfirmationScreen.this);

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if(language.equalsIgnoreCase("En")){
            dialog2.setContentView(R.layout.promo_list);
        }else if(language.equalsIgnoreCase("Ar")){
            dialog2.setContentView(R.layout.promo_list_arabic);
        }

        dialog2.setCanceledOnTouchOutside(true);
        ListView lv = (ListView) dialog2.findViewById(R.id.promos_list);
        ImageView cancel = (ImageView) dialog2.findViewById(R.id.cancel_button);
        lv.setAdapter(mPromoAdapter);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                dialog2.dismiss();
                if(promosList.get(position).getStoreId().equals(storeId) ||  promosList.get(position).getStoreId().equals("0")){
                    remainingBonusInt=0;
                    int tprice1 = 0;
                    // full free drink
                    if(promosList.get(position).getPromoType().equals("1")){
                        if(promosList.get(position).getCategory().equals("0") &&promosList.get(position).getItemId().equals("0") && promosList.get(position).getSize().equals("0")) {
                            int highestPrice = 0;
                            for (Orders order : orderList) {
//                                if ((order.getCategoryId().equals("1") && order.getItemId().equals("1"))) {
                                    int qty = Integer.parseInt(order.getQty());
                                    int tprice = Integer.parseInt(order.getTotalAmount());
                                    tprice = tprice / qty;
                                    tprice1 = Integer.parseInt(order.getTotalAmount()) / qty;
                                    if (highestPrice < tprice) {
                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;

                                        freeOrderId = order.getOrderId();
                                    }
                                }
//                            }

                            if (highestPrice != 0) {
                                HighestPriceYes(position,highestPrice,tprice1);

                            } else {
                               HighestPriceNo();
                            }
                        }

                        else if(promosList.get(position).getCategory().equals("0") &&promosList.get(position).getItemId().equals("0") && !promosList.get(position).getSize().equals("0")) {
                            int highestPrice = 0;
                            for (Orders order : orderList) {
                                if (order.getItemTypeId().equals(promosList.get(position).getSize())) {
                                int qty = Integer.parseInt(order.getQty());
                                int tprice = Integer.parseInt(order.getTotalAmount());
                                tprice = tprice / qty;
                                tprice1 = Integer.parseInt(order.getTotalAmount()) / qty;
                                if (highestPrice < tprice) {
                                    highestPrice = (highestPrice > tprice) ? highestPrice : tprice;

                                    freeOrderId = order.getOrderId();
                                }
                            }
                            }

                            if (highestPrice != 0) {
                                HighestPriceYes(position,highestPrice,tprice1);

                            } else {
                                HighestPriceNo();
                            }
                        }

                        else if(!promosList.get(position).getCategory().equals("0") && !promosList.get(position).getItemId().equals("0") && !promosList.get(position).getSize().equals("0")) {
                            int highestPrice = 0;
                            for (Orders order : orderList) {
                                Log.i("TAG","cat "+order.getCategoryId()+","+promosList.get(position).getCategory());
                                Log.i("TAG","item "+order.getItemId()+","+promosList.get(position).getItemId());
                                Log.i("TAG","cat "+order.getItemTypeId()+","+promosList.get(position).getSize());
                                if (order.getCategoryId().equals(promosList.get(position).getCategory()) && order.getItemId().equals(promosList.get(position).getItemId()) && order.getItemTypeId().equals(promosList.get(position).getSize())) {
                                    Log.i("TAG","Highest Price");
                                    int qty = Integer.parseInt(order.getQty());
                                    int tprice = Integer.parseInt(order.getTotalAmount());
                                    tprice = tprice / qty;
                                    tprice1 = Integer.parseInt(order.getTotalAmount()) / qty;
                                    if (highestPrice < tprice) {
                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;

                                        freeOrderId = order.getOrderId();
                                    }
                                }
                            }

                            if (highestPrice != 0) {
                                HighestPriceYes(position,highestPrice,tprice1);

                            } else {
                                HighestPriceNo();
                            }
                        }

                        else if(!promosList.get(position).getCategory().equals("0") && !promosList.get(position).getItemId().equals("0") && promosList.get(position).getSize().equals("0")) {
                            int highestPrice = 0;
                            for (Orders order : orderList) {
                                if (order.getCategoryId().equals(promosList.get(position).getCategory()) && order.getItemId().equals(promosList.get(position).getItemId())) {
                                    int qty = Integer.parseInt(order.getQty());
                                    int tprice = Integer.parseInt(order.getTotalAmount());
                                    tprice = tprice / qty;
                                    tprice1 = Integer.parseInt(order.getTotalAmount()) / qty;
                                    if (highestPrice < tprice) {
                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;

                                        freeOrderId = order.getOrderId();
                                    }
                                }
                            }

                            if (highestPrice != 0) {
                                HighestPriceYes(position,highestPrice,tprice1);

                            } else {
                                HighestPriceNo();
                            }
                        }

                        else if(!promosList.get(position).getCategory().equals("0") && promosList.get(position).getItemId().equals("0") && !promosList.get(position).getSize().equals("0")) {
                            int highestPrice = 0;
                            for (Orders order : orderList) {
                                if (order.getCategoryId().equals(promosList.get(position).getCategory()) && order.getItemTypeId().equals(promosList.get(position).getSize())) {
                                    int qty = Integer.parseInt(order.getQty());
                                    int tprice = Integer.parseInt(order.getTotalAmount());
                                    tprice = tprice / qty;
                                    tprice1 = Integer.parseInt(order.getTotalAmount()) / qty;
                                    if (highestPrice < tprice) {
                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;

                                        freeOrderId = order.getOrderId();
                                    }
                                }
                            }

                            if (highestPrice != 0) {
                                HighestPriceYes(position,highestPrice,tprice1);

                            } else {
                                HighestPriceNo();
                            }
                        }

                        else if(!promosList.get(position).getCategory().equals("0") && promosList.get(position).getItemId().equals("0") && promosList.get(position).getSize().equals("0")) {
                            int highestPrice = 0;
                            for (Orders order : orderList) {
                                if (order.getCategoryId().equals(promosList.get(position).getCategory())) {
                                    int qty = Integer.parseInt(order.getQty());
                                    int tprice = Integer.parseInt(order.getTotalAmount());
                                    tprice = tprice / qty;
                                    tprice1 = Integer.parseInt(order.getTotalAmount()) / qty;
                                    if (highestPrice < tprice) {
                                        highestPrice = (highestPrice > tprice) ? highestPrice : tprice;

                                        freeOrderId = order.getOrderId();
                                    }
                                }
                            }

                            if (highestPrice != 0) {
                                HighestPriceYes(position,highestPrice,tprice1);

                            } else {
                                HighestPriceNo();
                            }
                        }
                    }
                    else if(promosList.get(position).getPromoType().equals("6")){
                        remainingBonusInt = Integer.parseInt(promosList.get(position).getItemPrice());
                        float totalPriceInt = Float.parseFloat(decim.format(myDbHelper.getTotalOrderPrice()));
                        if (language.equalsIgnoreCase("En")) {
                            mconf_total_items.setText(" " + (myDbHelper.getTotalOrderQty()+1) + " items");
                        } else if (language.equalsIgnoreCase("Ar")) {
                            mconf_total_items.setText("" + (myDbHelper.getTotalOrderQty()+1) + " الأصناف");
                        }

                        if(remainingBonusInt <= totalPriceInt){
//                            remainingBonusInt = 0;
//                           PromoType2 promo = new /PromoType2();
//                           promo.setItemId(promosList.get(position).getItemId());
//                           promo.setItemPrice("0");
//                           promo.setQty("1");
//                           promo.setSize(promosList.get(position).getSize());
//                           promo.setComments("Free");
//                           promo.setAdditionalID(promosList.get(position).getAdditionals());
//                           promo.setAdditionalPrice("0");

//                           promoType2ArrayList.add(promo);

                            try {
                                Log.i("TAG","promo array lenght "+promoArray.length());
                                if(promoArray!=null && promoArray.length()==0) {

                                    JSONObject subObj = new JSONObject();

                                    JSONArray subItem2 = new JSONArray();

                                    subObj.put("ItemPrice", "0");
                                    subObj.put("Qty", "1");
                                    subObj.put("Comments", "Free");
                                    subObj.put("ItemId", promosList.get(position).getItemId());
                                    subObj.put("Size", promosList.get(position).getSize());

//                                    JSONObject subObj1 = new JSONObject();
//                                    subObj1.put("AdditionalID", promosList.get(position).getAdditionals());
//                                    subObj1.put("AdditionalPrice", "0");
//                                    subItem2.put(subObj1);

                                    promoArray.put(subObj);
                                    if (subItem2.length() > 0) {
                                        promoArray.put(subItem2);
                                    }
                                }
                                Log.i("TAG","promo array "+promoArray.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            promocodeStr = promosList.get(position).getPromoCode();
                            promoIdStr = promosList.get(position).getPromoID();
                            promoTypeStr = promosList.get(position).getPromoType();

                            if(language.equalsIgnoreCase("En")){
                                promocode.setText(promosList.get(position).getPromoTitle());
                            }else if(language.equalsIgnoreCase("Ar")){
                                promocode.setText(promosList.get(position).getPromoTitleAr());
                            }

                            promoLayout.setClickable(false);
                            promoArrow.setVisibility(View.GONE);
                            promoCancel.setVisibility(View.VISIBLE);
                        }
                        else{
                            promocodeStr="";
                            if(language.equalsIgnoreCase("En")){
                                promocode.setHint("Apply Promotion");
                                Toast.makeText(ConfirmationScreen.this, "Specified item not available in cart", Toast.LENGTH_SHORT).show();
                            }else if(language.equalsIgnoreCase("Ar")){
                                promocode.setHint("قدم للعرض");
                                Toast.makeText(ConfirmationScreen.this, "المنتجات المحددة غير موجودة في سلة الشراء", Toast.LENGTH_SHORT).show();
                            }

                            promoIdStr="";
                            promoTypeStr="";

                            float totalPriceInt1 = Float.parseFloat((decim.format(myDbHelper.getTotalOrderPrice())+tax));

                            mconf_total_amount.setText(""+totalPriceInt1+" SR");
//                                paymentMode = 3;
                        }
                    }
                }

            }
        });


        dialog2.show();

    }

    public void HighestPriceYes(int position , float highestPrice, float tprice1){
        if (tprice1 > Float.parseFloat(promosList.get(position).getItemPrice())) {
            remainingBonusInt = Float.parseFloat(promosList.get(position).getItemPrice());
        } else {
            remainingBonusInt = tprice1;
        }

        promocodeStr = promosList.get(position).getPromoCode();
        if (language.equalsIgnoreCase("En")) {
            promocode.setText(promosList.get(position).getPromoTitle() + " " + remainingBonusInt + " SR Redeem");
        } else if (language.equalsIgnoreCase("Ar")) {
            promocode.setText(promosList.get(position).getPromoTitleAr() + " " + remainingBonusInt + " ريال مجانا ");
        }

        promoIdStr = promosList.get(position).getPromoID();
        promoTypeStr = promosList.get(position).getPromoType();
        float totalPriceInt = Float.parseFloat(decim.format(myDbHelper.getTotalOrderPrice()));
        highestPrice = Float.parseFloat(totalPriceInt - remainingBonusInt + mvatamount);
//                            total_amt = highestPrice;
        if (highestPrice == 0) {
            mconf_total_amount.setText("Free");
            paymentMode = 4;
//            sr.setVisibility(View.INVISIBLE);
        } else {
            mconf_total_amount.setText("" + highestPrice);
        }
        promoLayout.setClickable(false);
        promoArrow.setVisibility(View.GONE);
        promoCancel.setVisibility(View.VISIBLE);
    }

    public void HighestPriceNo(){
        promocodeStr = "";
        if (language.equalsIgnoreCase("En")) {
            promocode.setHint("Apply Promotion");
            Toast.makeText(ConfirmationScreen.this, "Specified item not available in cart", Toast.LENGTH_SHORT).show();
        } else if (language.equalsIgnoreCase("Ar")) {
            promocode.setHint("قدم للعرض");
            Toast.makeText(ConfirmationScreen.this, "المنتجات المحددة غير موجودة في سلة الشراء", Toast.LENGTH_SHORT).show();
        }

        promoIdStr = "";
        promoTypeStr = "";

        float totalPriceInt = Float.parseFloat(decim.format(myDbHelper.getTotalOrderPrice()));

      mconf_total_amount.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
    }

}

