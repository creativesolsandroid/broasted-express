package com.cs.broastedexpress.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;
import com.squareup.picasso.Picasso;

public class Comment extends AppCompatActivity {
    Toolbar toolbar;
    EditText commentBox;
    TextView addButton, itemName;
    ImageView itemIcon;
    String itemId, title, itemtype;
    View rootView;
    private DataBaseHelper myDbHelper;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.comment);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.comment_arabic);
        }
        myDbHelper = new DataBaseHelper(Comment.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        itemId = getIntent().getExtras().getString("itemId");
        title = getIntent().getExtras().getString("title");
        itemtype = getIntent().getExtras().getString("itemtype");
        commentBox = (EditText) findViewById(R.id.comment_box);
        addButton = (TextView) findViewById(R.id.add_comment_button);
        itemName = (TextView) findViewById(R.id.item_name);
        itemIcon = (ImageView) findViewById(R.id.item_icon);

        if (getIntent().getExtras().getString("screen").equals("food")) {
            commentBox.setText(getIntent().getExtras().getString("comment"));
        } else {
            if(Projecturl.COMMENTS.length()>1) {
                commentBox.setText(Projecturl.COMMENTS);
            }
            else{
                commentBox.setText(getIntent().getExtras().getString("comment"));
            }
        }
        itemName.setText(title);
        Picasso.with(Comment.this).load("http://www.alrajhiksa.com/images/" + getIntent().getExtras().getString("itemImage")).into(itemIcon);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = commentBox.getText().toString();
                if (getIntent().getExtras().getString("screen").equals("food")) {
                    myDbHelper.updateComment(getIntent().getExtras().getString("orderId"), text, itemtype);
                } else if (getIntent().getExtras().getString("screen").equals("item")) {
                    Projecturl.COMMENTS = text;
                }

                onBackPressed();
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                View view = this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        super.onBackPressed();
    }
}

