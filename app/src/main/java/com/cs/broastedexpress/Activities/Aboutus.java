package com.cs.broastedexpress.Activities;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.cs.broastedexpress.R;
import com.cs.broastedexpress.Widgets.JustifiedTextView;

public class Aboutus extends AppCompatActivity {
    JustifiedTextView mJTv;
    TextView mheader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
//        WebView view = new WebView(this);
//        view.setVerticalScrollBarEnabled(false);
//
//        ((LinearLayout)findViewById(R.id.inset_web_view)).addView(view);
//
//        view.loadData(getString(R.string.title_activity_aboutus), "text/html; charset=utf-8", "utf-8");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mheader = (TextView) findViewById(R.id.header_title);
        mJTv = (JustifiedTextView) findViewById(R.id.text);
//        mJTv.setText("Bakery & Company began baking confectionary produc ts in low volume bakery facilities. The original handmade pastries, cakes and sandwiches have always used the finest quality. 100% natural healthy ingredients: Highest quality flour, Fresh whole grade A eggs, butter creams and dairy products, pure cane sugar, dark and white chocolates, fresh fruit purees and fillings, the world's finest vanilla and essences.\n" +
//                "\n" +
//                "Today, B&C is a high volume bakery that turns out thousands of products daily. At one point or another, B&C food products are served at the best restaurants and establishments, just waiting to be enjoyed by food enthusiasts and connoisseurs.\n" +
//                "\n" +
//                "We've come a long way, but there are some things that haven't changed. We will always use the finest quality ingredients and maintain consistency, dedication, and passion to our bakery products for all valued clients.");
//        mJTv.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
//        mJTv.setLineSpacing(1);
        mJTv.setTextColor(Color.parseColor("#FFFFFF"));
        mJTv.setAlignment(Paint.Align.LEFT);

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
