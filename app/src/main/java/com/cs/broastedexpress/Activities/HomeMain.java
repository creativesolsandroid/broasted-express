package com.cs.broastedexpress.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.cs.broastedexpress.R;

/**
 * Created by CS on 10/13/2016.
 */

public class HomeMain extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_home);
        ImageView btn = (ImageView) findViewById(R.id.home_slice);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getApplication(), ChangePassword.class);
                startActivity(a);
            }
        });
    }
}
