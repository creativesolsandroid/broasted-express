package com.cs.broastedexpress.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.broastedexpress.R;

import org.json.JSONException;
import org.json.JSONObject;

public class Profile extends AppCompatActivity {

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String response;
    String mLoginStatus;
    SharedPreferences languagePrefs;
    String language;
    EditText name, email, mobile;
    LinearLayout mchange_pass, mManage_add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.content_profile);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.content_profile_arabic);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        mLoginStatus = userPrefs.getString("login_status", "");
//        userId = userPrefs.getString("userId", null);
        response = userPrefs.getString("user_profile", null);

        final Button mlog_out = (Button) findViewById(R.id.log_out);

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.phone);

        TextView medit = (TextView) findViewById(R.id.edit);
        TextView mChangePassword = (TextView) findViewById(R.id.changepassword);
        TextView mManage_address = (TextView) findViewById(R.id.manageaddress);

        mchange_pass = (LinearLayout) findViewById(R.id.change_pass);
        mManage_add = (LinearLayout) findViewById(R.id.manage_add);

        if (response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userobject = property.getJSONObject("profile");

                name.setText(userobject.getString("fullName") + " " + userobject.getString("family_name") + " " + userobject.getString("nick_name"));
                email.setText(userobject.getString("email"));
                mobile.setText("+" + userobject.getString("mobile"));
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }


        mChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Profile.this, ChangePassword.class);
                startActivity(a);
            }
        });

        mManage_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent b = new Intent(Profile.this, Myaddress.class);
                startActivity(b);
            }
        });

        mchange_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Profile.this, ChangePassword.class);
                startActivity(a);
            }
        });

        mManage_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent b = new Intent(Profile.this, Myaddress.class);
                startActivity(b);
            }
        });

        medit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent c = new Intent(Profile.this, EditProfile.class);
                startActivity(c);
            }
        });

        mlog_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPrefEditor.clear();
                userPrefEditor.commit();
                mlog_out.setText("Login");
                onBackPressed();
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        response = userPrefs.getString("user_profile", null);
        if (response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userobject = property.getJSONObject("profile");

                name.setText(userobject.getString("fullName") + " " + userobject.getString("family_name") + " " + userobject.getString("nick_name"));
                email.setText(userobject.getString("email"));
                mobile.setText("+" + userobject.getString("mobile"));
            } catch (JSONException e) {
                Log.d("", "Error while parsing the results!");
                e.printStackTrace();
            }
        }
    }
}