package com.cs.broastedexpress.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {
    private static final int REGISTRATION_REQUEST = 1;
    private static final int VERIFICATION_REQUEST = 2;
    EditText mMobileNumber, mPassword;
    TextView mForgetPassword, mLoginbtn;
    LinearLayout mSignUp;
    String response, language;
    SharedPreferences userprefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String password;
    String moblieNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_login);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_login_arabic);
        }

        userprefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userprefs.edit();

        mMobileNumber = (EditText) findViewById(R.id.mobile_number);
        mPassword = (EditText) findViewById(R.id.password);

        mForgetPassword = (TextView) findViewById(R.id.forgot_pwd);
        mLoginbtn = (TextView) findViewById(R.id.login_btn);

        mSignUp = (LinearLayout) findViewById(R.id.signup_btn);
//        new CheckLoginDetails().execute(Projecturl.LOGIN_URL + "966593004198?psw=designer&dtoken=" + SplashActivity.regId + "&lan=en");

        mLoginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moblieNumber = mMobileNumber.getText().toString();
                password = mPassword.getText().toString();

                if (moblieNumber.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        mMobileNumber.setError("Please Enter Mobile Number");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mMobileNumber.setError("من فضلك أدخل رقم الجوال");
                    }
                } else if (moblieNumber.length() != 9) {
                    if (language.equalsIgnoreCase("En")) {
                        mMobileNumber.setError("Please Enter Valid Mobile Number");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mMobileNumber.setError("من فضلك ادخل رقم جوال صحيح");
                    }
                } else if (password.length() == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        mPassword.setError("Please enter Password");
                    } else if (language.equalsIgnoreCase("Ar")) {
                        mPassword.setError("من فضلك ادخل كلمة السر");
                    }
                    mPassword.requestFocus();
                } else {
                    new CheckLoginDetails().execute(Projecturl.LOGIN_URL + "966" + moblieNumber + "?psw=" + password + "&dtoken=" + SplashActivity.regId + "&lan=en");
                }
            }
        });
        mForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Login.this, ForgetPassword.class);
                a.putExtra("pass",password);
                a.putExtra("mobile",moblieNumber);
                startActivity(a);
            }
        });
        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent b = new Intent(Login.this, Registration.class);
                startActivityForResult(b, REGISTRATION_REQUEST);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REGISTRATION_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == VERIFICATION_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    public class CheckLoginDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(Login.this);
            dialog = ProgressDialog.show(Login.this, "",
                    "Checking login...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }

        @Override
        public void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(Login.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(Login.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                boolean isVerified = jo1.getBoolean("IsVerified");
                                String familyName = jo1.getString("FamilyName");
                                String nickName = jo1.getString("NickName");
                                String gender = jo1.getString("Gender");


                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("family_name", familyName);
                                    jsonObject.put("nick_name", nickName);
                                    jsonObject.put("gender", gender);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    if (isVerified) {
                                        userPrefEditor.putString("login_status", "loggedin");
                                        userPrefEditor.commit();
//                                        Intent loginIntent = new Intent();
                                        setResult(RESULT_OK);
                                        finish();
                                    } else {
                                        Intent loginIntent = new Intent(Login.this, Verify_Random_number.class);
                                        loginIntent.putExtra("phone_number", mobile);
                                        startActivityForResult(loginIntent, VERIFICATION_REQUEST);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException je) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(Login.this, android.R.style.Theme_Material_Light_Dialog));

                                if (language.equalsIgnoreCase("En")) {
                                    // set title
                                    alertDialogBuilder.setTitle("Broasted Express");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("Invalid Mobile/Password")
                                            .setCancelable(false)
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                } else if (language.equalsIgnoreCase("Ar")) {
                                    // set title
                                    alertDialogBuilder.setTitle("بروستد إكسبريس");

                                    // set dialog message
                                    alertDialogBuilder
                                            .setMessage("الرقم السري غير صحيح")
                                            .setCancelable(false)
                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                }
                                            });
                                }


                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(Login.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
}
