package com.cs.broastedexpress.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.Adapter.CheckoutAdapter;
import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.GPSTracker;
import com.cs.broastedexpress.Model.Orders;
import com.cs.broastedexpress.R;
import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Checkout extends AppCompatActivity {

    ArrayList<Orders> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    Context mContext;

    private String timeResponse = null;

    DecimalFormat decim = new DecimalFormat("0.00");

    String storeId,starttime,endtime,storeName,storeAddress,imagesURL,is24x7,storename_ar,storeaddress_ar;
    Double latitude,longitudes;

    String response;
    int position=0;
//    private ArrayList<StoreInfo> storesList = new ArrayList<>();

    Double lat, longi;
    Double dest;
    String serverTime;
    String latitute,longitute;

    public static TextView morder_quantity, morder_price, mcount_basket, netTotal, vatAmount, amount;

    RelativeLayout recieptLayout;
    LinearLayout  vatLayout;
    TextView vatPercent,receipt_close;
    ImageView minvoice;
    float vat=5;

    ListView listView;
    LinearLayout mOrder_now;
    GPSTracker gps;
    //    ImageView mcheck_plus_icon,mcheck_sub_icon;
//    TextView mcheck_quantity,mcheck_total_amount;
//    int count,price=15,final_price=0;
//    String textcount,final_prices;
    CheckoutAdapter mAdapter;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    SharedPreferences languagePrefs;
    String language;

    RelativeLayout mcount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_checkout);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_checkout_arabic);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        myDbHelper = new DataBaseHelper(Checkout.this);
        gps = new GPSTracker(Checkout.this);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

//        mcheck_plus_icon= (ImageView) findViewById(R.id.check_out_plus);
//        mcheck_sub_icon= (ImageView) findViewById(R.id.check_out_sub);
//
//        mcheck_quantity= (TextView) findViewById(R.id.check_out_quantity);
//        mcheck_total_amount= (TextView) findViewById(R.id.check_out_total_amount);
        morder_quantity = (TextView) findViewById(R.id.order_quantity);
        morder_price = (TextView) findViewById(R.id.order_price);
        mcount_basket = (TextView) findViewById(R.id.count_basket);

        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
        receipt_close = (TextView) findViewById(R.id.receipt_close);
        amount = (TextView) findViewById(R.id.amount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        vatPercent = (TextView) findViewById(R.id.vatPercent);
        netTotal = (TextView) findViewById(R.id.net_total);
        minvoice= (ImageView) findViewById(R.id.invoice);

        mcount= (RelativeLayout) findViewById(R.id.count);

        listView = (ListView) findViewById(R.id.check_out_list);

        mOrder_now = (LinearLayout) findViewById(R.id.order_now);

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(Checkout.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                }else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(Checkout.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have "+ myDbHelper.getTotalOrderQty() +" item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        myDbHelper.deleteOrderTable();
                                        Checkout.morder_quantity.setText("0");
                                        Checkout.morder_price.setText("0.00");
                                        Checkout.mcount_basket.setText("0");
//                                        Menu.morder_quantity.setText("0");
//                                        Menu.morder_price.setText("0SR");
//                                        Menu.mcount_basket.setText("0");
//                                        InsideMenu.morder_quantity.setText("0");
//                                        InsideMenu.morder_price.setText("0SR");
//                                        InsideMenu.mcount_basket.setText("0");
                                        Intent a=new Intent(Checkout.this,Menu.class);
                                        startActivity(a);
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات"+myDbHelper.getTotalOrderQty()+ "لديك " )
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        myDbHelper.deleteOrderTable();
                                        Checkout.morder_quantity.setText("0");
                                        Checkout.morder_price.setText("0.00");
                                        Checkout.mcount_basket.setText("0");
//                                        Menu.morder_quantity.setText("0");
//                                        Menu.morder_price.setText("0ريال");
//                                        Menu.mcount_basket.setText("0");
//                                        InsideMenu.morder_quantity.setText("0");
//                                        InsideMenu.morder_price.setText("0ريال");
//                                        InsideMenu.mcount_basket.setText("0");
                                        Intent a=new Intent(Checkout.this,Menu.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("لا", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                }

            }
        });

        mOrder_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    Intent a = new Intent(Checkout.this, OrderType.class);
                    startActivity(a);
            }

        });

        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.GONE);
            }
        });


        minvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recieptLayout.setVisibility(View.VISIBLE);
            }
        });
//        double number;
//        number=myDbHelper.getTotalOrderPrice();


//        mcheck_plus_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                    final_price=final_price + price;
//                    final_prices = Integer.toString(final_price);
//                    mcheck_total_amount.setText(final_prices + "SR");
//                    count++;
//                    textcount=Integer.toString(count);
//                    mcheck_quantity.setText(textcount);
//            }
//        });
//
//
//        mcheck_sub_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (count==0){
////                    final_price=0;
////                    price=0;
//                }
//                else {
//                    final_price=final_price-price;
//                    final_prices=Integer.toString(final_price);
//                    mcheck_total_amount.setText(final_prices+"SR");
//                    count--;
//                    textcount = Integer.toString(count);
//                    mcheck_quantity.setText(textcount);
//                }
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getGPSCoordinates(){
        gps = new GPSTracker(Checkout.this);
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
//                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(Checkout.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(Checkout.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }



//    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
//        ProgressDialog pDialog;
//        String  networkStatus;
//        ProgressDialog dialog;
//        String dayOfWeek;
//        @Override
//        protected void onPreExecute() {
//            storesList.clear();
//            networkStatus = NetworkUtil.getConnectivityStatusString(Checkout.this);
////            dialog = ProgressDialog.show(OrderType.this, "",
////                    "Fetching stores...");
//            Calendar calendar = Calendar.getInstance();
//            Date date = calendar.getTime();
//            // full name form of the day
//            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                JSONParser jParser = new JSONParser();
//
//                response = jParser
//                        .getJSONFromUrl(params[0]+dayOfWeek);
//                Log.i("TAG", "user response:" + response);
//                return response;
//            }else {
//                return "no internet";
//            }
//
//        }
//
//
//        @Override
//        protected void onPostExecute(String result) {
//            storesList.clear();
//            if (result != null) {
//                if(result.equalsIgnoreCase("no internet")) {
//                    Toast.makeText(Checkout.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
//
//                }else{
//                    if(result.equals("")){
//                        Toast.makeText(Checkout.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//                    }else {
//
//                        try {
//                            JSONArray ja = new JSONArray(result);
//                            for (int i = 0; i < ja.length(); i++) {
//                                StoreInfo si = new StoreInfo();
//                                JSONObject jo = ja.getJSONObject(i);
////                                lat = 24.70321657;
////                                longi = 46.68097073;
//
//                                storeId=(jo.getString("storeId"));
//                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
//                                starttime=(jo.getString("ST"));
//                                endtime=(jo.getString("ET"));
//                                storeName=(jo.getString("StoreName"));
//                                storeAddress=(jo.getString("StoreAddress"));
//                                latitude=(jo.getDouble("Latitude"));
//                                longitudes=(jo.getDouble("Longitude"));
//                                si.setCountryName(jo.getString("CountryName"));
//                                si.setCityName(jo.getString("CityName"));
//                                imagesURL=(jo.getString("imageURL"));
//                                si.setFamilySection(jo.getString("FamilySection"));
//                                si.setWifi(jo.getString("Wifi"));
//                                si.setPatioSitting(jo.getString("PatioSitting"));
//                                si.setDriveThru(jo.getString("DriveThru"));
//                                si.setMeetingSpace(jo.getString("MeetingSpace"));
//                                si.setHospital(jo.getString("Hospital"));
//                                si.setUniversity(jo.getString("University"));
//                                si.setOffice(jo.getString("Office"));
//                                si.setShoppingMall(jo.getString("ShoppingMall"));
//                                si.setStoreName(storeName);
//                                si.setStoreAddress(storeAddress);
//                                si.setStoreId(storeId);
//                                si.setStarttime(starttime);
//                                si.setEndtime(endtime);
//                                si.setLatitude(latitude);
//                                si.setLongitude(longitudes);
//                                si.setImageURL(imagesURL);
//                                try{
//                                    si.setAirport(jo.getString("Airport"));
//                                }catch (Exception e){
//                                    si.setAirport("false");
//                                }
//                                try{
//                                    si.setDineIn(jo.getString("DineIn"));
//                                }catch (Exception e){
//                                    si.setDineIn("false");
//                                }
//                                try{
//                                    si.setLadies(jo.getString("Ladies"));
//                                }catch (Exception e){
//                                    si.setLadies("false");
//                                }
//
//                                si.setNeighborhood(jo.getString("Neighborhood"));
//                                si.setStoreNumber(jo.getString("phone"));
//                                is24x7=(jo.getString("is24x7"));
//                                si.setStatus(jo.getString("status"));
//                                si.setOgCountry(jo.getString("OGCountry"));
//                                si.setOgCity(jo.getString("OGCity"));
//                                storename_ar=(jo.getString("StoreName_ar"));
//                                storeaddress_ar=(jo.getString("StoreAddress_ar"));
//                                si.setStoreNumber(jo.getString("phone"));
//                                si.setStoreName_ar(storename_ar);
//                                si.setIs24x7(is24x7);
//                                si.setStoreAddress_ar(storeaddress_ar);
//                                try{
//                                    si.setMessage(jo.getString("Message"));
//                                }catch (Exception e){
//                                    si.setMessage("");
//                                }
//
//                                try{
//                                    si.setMessage_ar(jo.getString("Message_ar"));
//                                }catch (Exception e){
//                                    si.setMessage_ar("");
//                                }
//
//                                Location me   = new Location("");
//                                Location dest = new Location("");
//
//                                me.setLatitude(lat);
//                                me.setLongitude(longi);
//
//                                dest.setLatitude(jo.getDouble("Latitude"));
//                                dest.setLongitude(jo.getDouble("Longitude"));
//
//                                float dist = (me.distanceTo(dest))/1000;
//                                si.setDistance(dist);
//
//
//
//                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
//                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy hh:mma", Locale.US);
//                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
//                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
//
//                                Calendar c = Calendar.getInstance();
//                                System.out.println("Current time => " + c.getTime());
//                                serverTime = timeResponse;
//                                String startTime = si.getStarttime().replace(" ", "");
//                                String endTime = si.getEndtime().replace(" ", "");
//
//
//                                if(dist <= 50 && (jo.getBoolean("OnlineOrderStatus"))) {
//
//
//                                    if (startTime.equals("null") && endTime.equals("null")) {
//                                        si.setOpenFlag(-1);
//                                        storesList.add(si);
//                                    } else {
//
//                                        if (endTime.equals("00:00AM")) {
//                                            si.setOpenFlag(1);
//                                            storesList.add(si);
//
//                                            continue;
//                                        } else if (endTime.equals("12:00AM")) {
//                                            endTime = "11:59PM";
//                                        }
//
//                                        Calendar now = Calendar.getInstance();
//
//                                        int hour = now.get(Calendar.HOUR_OF_DAY);
//                                        int minute = now.get(Calendar.MINUTE);
//
//
//                                        Date serverDate = null;
//                                        Date end24Date = null;
//                                        Date start24Date = null;
//                                        Date current24Date = null;
//                                        Date dateToday = null;
//                                        Calendar dateStoreClose = Calendar.getInstance();
//                                        try {
//                                            end24Date = timeFormat.parse(endTime);
//                                            start24Date = timeFormat.parse(startTime);
//                                            serverDate = dateFormat.parse(serverTime);
//                                            dateToday = dateFormat1.parse(serverTime);
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//                                        dateStoreClose.setTime(dateToday);
//                                        dateStoreClose.add(Calendar.DATE, 1);
//                                        String current24 = timeFormat1.format(serverDate);
//                                        String end24 = timeFormat1.format(end24Date);
//                                        String start24 = timeFormat1.format(start24Date);
//                                        String startDateString = dateFormat1.format(dateToday);
//                                        String endDateString = dateFormat1.format(dateToday);
//                                        String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
//                                        dateStoreClose.add(Calendar.DATE, -2);
//                                        String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());
//
//                                        Date startDate = null;
//                                        Date endDate = null;
//
//                                        try {
//                                            end24Date = timeFormat1.parse(end24);
//                                            start24Date = timeFormat1.parse(start24);
//                                            current24Date = timeFormat1.parse(current24);
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        String[] parts2 = start24.split(":");
//                                        int startHour = Integer.parseInt(parts2[0]);
//                                        int startMinute = Integer.parseInt(parts2[1]);
//
//                                        String[] parts = end24.split(":");
//                                        int endHour = Integer.parseInt(parts[0]);
//                                        int endMinute = Integer.parseInt(parts[1]);
//
//                                        String[] parts1 = current24.split(":");
//                                        int currentHour = Integer.parseInt(parts1[0]);
//                                        int currentMinute = Integer.parseInt(parts1[1]);
//
//
////                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");
//
//
//                                        if (startTime.contains("AM") && endTime.contains("AM")) {
//                                            if (startHour < endHour) {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateString + "  " + endTime;
//                                                try {
//                                                    startDate = dateFormat2.parse(startDateString);
//                                                    endDate = dateFormat2.parse(endDateString);
//                                                } catch (ParseException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            } else if (startHour > endHour) {
//                                                if (serverTime.contains("AM")) {
//                                                    if (currentHour > endHour) {
//                                                        startDateString = startDateString + " " + startTime;
//                                                        endDateString = endDateTomorrow + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    } else {
//                                                        startDateString = endDateYesterday + " " + startTime;
//                                                        endDateString = endDateString + "  " + endTime;
//                                                        try {
//                                                            startDate = dateFormat2.parse(startDateString);
//                                                            endDate = dateFormat2.parse(endDateString);
//                                                        } catch (ParseException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    }
//                                                } else {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateTomorrow + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                }
//                                            }
//                                        } else if (startTime.contains("AM") && endTime.contains("PM")) {
//                                            startDateString = startDateString + " " + startTime;
//                                            endDateString = endDateString + "  " + endTime;
//                                            try {
//                                                startDate = dateFormat2.parse(startDateString);
//                                                endDate = dateFormat2.parse(endDateString);
//                                            } catch (ParseException e) {
//                                                e.printStackTrace();
//                                            }
//                                        } else if (startTime.contains("PM") && endTime.contains("AM")) {
//                                            if (serverTime.contains("AM")) {
//                                                if (currentHour <= endHour) {
//                                                    startDateString = endDateYesterday + " " + startTime;
//                                                    endDateString = endDateString + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                } else {
//                                                    startDateString = startDateString + " " + startTime;
//                                                    endDateString = endDateTomorrow + "  " + endTime;
//                                                    try {
//                                                        startDate = dateFormat2.parse(startDateString);
//                                                        endDate = dateFormat2.parse(endDateString);
//                                                    } catch (ParseException e) {
//                                                        e.printStackTrace();
//                                                    }
//                                                }
//                                            } else {
//                                                startDateString = startDateString + " " + startTime;
//                                                endDateString = endDateTomorrow + "  " + endTime;
//                                                try {
//                                                    startDate = dateFormat2.parse(startDateString);
//                                                    endDate = dateFormat2.parse(endDateString);
//                                                } catch (ParseException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
//
//                                        } else if (startTime.contains("PM") && endTime.contains("PM")) {
//                                            startDateString = startDateString + " " + startTime;
//                                            endDateString = endDateString + "  " + endTime;
//                                            try {
//                                                startDate = dateFormat2.parse(startDateString);
//                                                endDate = dateFormat2.parse(endDateString);
//                                            } catch (ParseException e) {
//                                                e.printStackTrace();
//                                            }
//                                        }
//
//
//                                        String serverDateString = dateFormat2.format(serverDate);
//
//                                        try {
//                                            serverDate = dateFormat2.parse(serverDateString);
//                                        } catch (ParseException e) {
//                                            e.printStackTrace();
//                                        }
//
//                                        Log.i("TAG DATE", "" + startDate);
//                                        Log.i("TAG DATE1", "" + endDate);
//                                        Log.i("TAG DATE2", "" + serverDate);
//
//                                        if (serverDate.after(startDate) && serverDate.before(endDate)) {
//                                            Log.i("TAG Visible", "true");
//                                            si.setOpenFlag(1);
//                                            storesList.add(si);
//                                        } else {
//                                            si.setOpenFlag(0);
//                                            storesList.add(si);
//                                        }
//                                    }
//                                }
////                                storesList.add(si);
//                            }
//
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        Collections.sort(storesList, StoreInfo.storeDistance);
//                        Collections.sort(storesList, StoreInfo.storeOpenSort);
//                    }
//                }
//
//            }else {
//                Toast.makeText(Checkout.this, "cannot reach server", Toast.LENGTH_SHORT).show();
//            }
//            if(dialog != null) {
//                dialog.dismiss();
//            }
////            mAdapter.notifyDataSetChanged();
////            loading = false;
////            swipeLayout.setRefreshing(false);
//
////            dest=24.681921,46.700222;
//            super.onPostExecute(result);
//
//        }
//
//    }
//
//
//
//
//    public class GetCurrentTime extends AsyncTask<String, String, String> {
//        java.net.URL url = null;
//        String cardNumber = null, password = null;
//        double lat, longi;
//        String networkStatus;
//        String serverTime;
//        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
//        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
//        ProgressDialog dialog;
//        @Override
//        protected void onPreExecute() {
//            networkStatus = NetworkUtil.getConnectivityStatusString(Checkout.this);
//            dialog = ProgressDialog.show(Checkout.this, "",
//                    "Please Wait....");
//        }
//
//        @Override
//        protected String doInBackground(String... arg0) {
//            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                try {
////                    Calendar c = Calendar.getInstance();
////                    System.out.println("Current time => "+c.getTime());
//
////                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
////                    timeResponse = timeFormat.format(c.getTime());
//                    JSONParser jParser = new JSONParser();
//                    serverTime = jParser.getJSONFromUrl(Projecturl.GET_CURRENT_TIME_URL);
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                Log.d("Responce", "" + serverTime);
//            }else{
//                serverTime = "no internet";
//            }
//            return serverTime;
//        }
//
//        @Override
//        protected void onPostExecute(String result1) {
//            if(serverTime == null){
//                dialog.dismiss();
//            } else if(serverTime.equals("no internet")){
//                dialog.dismiss();
//                Toast.makeText(Checkout.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
//            } else {
//                dialog.dismiss();
//                try {
//                    JSONObject jo = new JSONObject(result1);
//                    timeResponse = jo.getString("DateTime");
//                }catch (JSONException je){
//                    je.printStackTrace();
//                }
//                new GetStoresInfo().execute(Projecturl.STORES_URL);
////                if(language.equalsIgnoreCase("En")) {
//
////                mAdapter = new SelectStoresAdapter(SelectStoresActivity.this, storesList, lat, longi, timeResponse);
////                listView.setAdapter(mAdapter);
////                }else if(language.equalsIgnoreCase("Ar")){
////                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
////                    mStoresListView.setAdapter(mStoreListAdapterArabic);
////                }
//
//            }
//
//
//            super.onPostExecute(result1);
//        }
//    }


    @Override
    protected void onResume() {
        super.onResume();
        orderList = myDbHelper.getOrderInfo();
        try {
            if(orderList.size() == 0){
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAdapter = new CheckoutAdapter(Checkout.this, orderList, language);
        listView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
        vatAmount.setText(""+decim.format(tax));
        netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));

        mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
        if (language.equalsIgnoreCase("En")) {

            morder_price.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
            morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
        } else if (language.equalsIgnoreCase("Ar")) {
            morder_price.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
            morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
        }
    }
}
