package com.cs.broastedexpress.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.InsideMenuCategories;
import com.cs.broastedexpress.Model.InsideMenuValues;
import com.cs.broastedexpress.Model.MenuItems;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;

public class Menu extends AppCompatActivity {

    DecimalFormat decimalFormat =new DecimalFormat("0.00");

    public static TextView morder_quantity, morder_price, mcount_basket;
    LinearLayout mcheck_out;
    ImageView mBoasted, mmenu_fish, mmenu_nuggets, mmenu_sandwich, mmenu_side, mmenu_drink, mmenu_family_meal, mmenu_kids, mmenu_promation;
    SharedPreferences languagePrefs;
    String language;
    RelativeLayout mcount;
    private DataBaseHelper mydatabase;
    String response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_menu);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_menu_arabic);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mydatabase = new DataBaseHelper(Menu.this);

        mcount= (RelativeLayout) findViewById(R.id.count);

        mBoasted = (ImageView) findViewById(R.id.boasted);
        mmenu_fish = (ImageView) findViewById(R.id.menu_fish);
        mmenu_nuggets = (ImageView) findViewById(R.id.menu_nugget);
        mmenu_sandwich = (ImageView) findViewById(R.id.menu_sandwich);
        mmenu_side = (ImageView) findViewById(R.id.menu_side);
        mmenu_drink = (ImageView) findViewById(R.id.menu_drink);
        mmenu_kids = (ImageView) findViewById(R.id.menu_kids);
        mmenu_family_meal = (ImageView) findViewById(R.id.menu_family_meal);
        mmenu_promation = (ImageView) findViewById(R.id.menu_promotion);

        mcount_basket = (TextView) findViewById(R.id.count_basket);
        morder_quantity = (TextView) findViewById(R.id.order_quantity);
        morder_price = (TextView) findViewById(R.id.order_price);

        mcheck_out = (LinearLayout) findViewById(R.id.check_out);

//        try {
//
//            Menu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//            if (language.equalsIgnoreCase("En")) {
//                Menu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//                Menu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//            } else if (language.equalsIgnoreCase("Ar")) {
//                Menu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//                Menu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//            }
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//        Menu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//        if (language.equalsIgnoreCase("En")) {
//            Menu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//            Menu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//        } else if (language.equalsIgnoreCase("Ar")) {
//            Menu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//            Menu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//        }

        if(Projecturl.MenuItems.size()==0) {
            new GetCategoryItems().execute(Projecturl.GET_ALL_ITEMS);
        }

        mcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mydatabase.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(Menu.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                }else {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(Menu.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("You have "+ mydatabase.getTotalOrderQty() +" item(s) in bag, by this action all items get clear.")
                                .setCancelable(false)
                                .setPositiveButton("Checkout", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent a=new Intent(Menu.this,Checkout.class);
                                        startActivity(a);
                                    }
                                });
                        alertDialogBuilder
                        .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mydatabase.deleteOrderTable();
//                                Checkout.morder_quantity.setText("0");
//                                Checkout.morder_price.setText("0SR");
//                                Checkout.mcount_basket.setText("0");
                                Menu.morder_quantity.setText("0");
                                Menu.morder_price.setText("0.00");
                                Menu.mcount_basket.setText("0");

                                dialog.dismiss();
                            }
                        });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات"+mydatabase.getTotalOrderQty()+ "لديك " )
                                .setCancelable(false)
                                .setPositiveButton("واضح", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        mydatabase.deleteOrderTable();
//                                        Checkout.morder_quantity.setText("0");
//                                        Checkout.morder_price.setText("0ريال");
//                                        Checkout.mcount_basket.setText("0");
                                        Menu.morder_quantity.setText("0");
                                        Menu.morder_price.setText("0.00");
                                        Menu.mcount_basket.setText("0");
                                        InsideMenu.morder_quantity.setText("0");
                                        InsideMenu.morder_price.setText("0.00");
                                        InsideMenu.mcount_basket.setText("0");
                                        dialog.dismiss();
                                    }
                                });
                        alertDialogBuilder
                                .setNegativeButton("تأكيد الطلب", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent a=new Intent(Menu.this,Checkout.class);
                                        startActivity(a);
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                }

            }
        });

        mcheck_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mydatabase.getTotalOrderQty() == 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(Menu.this, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle("Broasted Express");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("There is no items in your order? To proceed checkout please add the items")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle("بروستد إكسبريس");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("لا يوجد منتجات في طلبك ؟ لإتمام المراجعة نأمل إضافة منتجات")
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

//
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // show it
                    alertDialog.show();
                } else {
                    Intent checkoutIntent = new Intent(Menu.this, Checkout.class);
                    startActivity(checkoutIntent);
                }
            }
        });

        mBoasted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Menu.this, InsideMenu.class);
                a.putExtra("catid", 1);
                startActivity(a);
            }
        });

        mmenu_fish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent b = new Intent(Menu.this, InsideMenu.class);
                b.putExtra("catid", 2);
                startActivity(b);
            }
        });

        mmenu_nuggets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent c = new Intent(Menu.this, InsideMenu.class);
                c.putExtra("catid", 3);
                startActivity(c);
            }
        });

        mmenu_sandwich.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent d = new Intent(Menu.this, InsideMenu.class);
                d.putExtra("catid", 10);
                startActivity(d);
            }
        });

        mmenu_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent e = new Intent(Menu.this, InsideMenu.class);
                e.putExtra("catid", 5);
                startActivity(e);
            }
        });

        mmenu_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent f = new Intent(Menu.this, InsideMenu.class);
                f.putExtra("catid", 6);
                startActivity(f);
            }
        });

        mmenu_family_meal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Menu.this, InsideMenu.class);
                a.putExtra("catid", 7);
                startActivity(a);
            }
        });

        mmenu_kids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent b = new Intent(Menu.this, InsideMenu.class);
                b.putExtra("catid", 4);
                startActivity(b);
            }
        });

        mmenu_promation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent c = new Intent(Menu.this, InsideMenu.class);
                c.putExtra("catid", 9);
                startActivity(c);
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            Projecturl.MenuItems.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(Menu.this);
            dialog = ProgressDialog.show(Menu.this, "",
                    "Loading items...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(Menu.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(Menu.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONArray ja = new JSONArray(result);

                            for (int i = 0; i < ja.length(); i++) {
                                MenuItems menuItems = new MenuItems();
                                ArrayList<InsideMenuCategories> menuList = new ArrayList<>();
                                JSONObject jobj = ja.getJSONObject(i);
                                menuItems.setCategoryId(jobj.getString("CategoryId"));
                                menuItems.setCategoryName(jobj.getString("CategoryName"));
                                menuItems.setCatDescription(jobj.getString("CatDescription"));
                                menuItems.setCatDescription_Ar(jobj.getString("CatDescription_Ar"));

                                JSONArray jsonArray = jobj.getJSONArray("Items");
                                for (int j = 0; j < jsonArray.length(); j++) {
                                    InsideMenuCategories cat = new InsideMenuCategories();
                                    ArrayList<InsideMenuValues> itemList = new ArrayList<>();
                                    JSONObject jo = jsonArray.getJSONObject(j);
                                    JSONObject jo1 = jo.getJSONObject("Key");
                                    cat.setCatId(jo1.getString("CategoryId"));
                                    cat.setItemId(jo1.getString("ItemId"));
                                    cat.setItemName(jo1.getString("ItemName"));
                                    cat.setModifierId(jo1.getString("ModifierId"));
                                    cat.setItemName_Ar(jo1.getString("ItemName_Ar"));
                                    cat.setDescription(jo1.getString("Description"));
                                    cat.setDescription_Ar(jo1.getString("Description_Ar"));
                                    cat.setAdditionId(jo1.getString("AdditionalsId"));
                                    cat.setImage(jo1.getString("Images"));
                                    cat.setIsDelivery(jo1.getString("IsDelivery"));
                                    JSONObject value = jo.getJSONObject("Value");
                                    Iterator iterator = value.keys();
                                    while (iterator.hasNext()) {
                                        InsideMenuValues items = new InsideMenuValues();
                                        String size = (String) iterator.next();
                                        String price = value.getString(size);
                                        items.setSize(Integer.parseInt(size));
                                        items.setPrices(price);
                                        itemList.add(items);
                                        cat.setInsideMenuchild(itemList);
                                    }
                                    menuList.add(cat);
                                    menuItems.setMenuCategories(menuList);
                                }
                                Projecturl.MenuItems.add(menuItems);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(Menu.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Menu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
        if (language.equalsIgnoreCase("En")) {
            Menu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
            Menu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
        } else if (language.equalsIgnoreCase("Ar")) {
            Menu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
            Menu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
        }
    }

    @Override
    public void onBackPressed() {
        if(mydatabase.getTotalOrderQty()>0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new android.support.v7.view.ContextThemeWrapper(Menu.this, android.R.style.Theme_Material_Light_Dialog));
            String title= "", msg= "", posBtn = "", negBtn = "",price="";
            if(language.equalsIgnoreCase("En")){
                posBtn = "Yes";
                negBtn = "No";
//                title = "dr.CAFE";
                price="0.00";
                msg = "You have " + mydatabase.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear. Do you want to continue?";
            }else if(language.equalsIgnoreCase("Ar")){
                posBtn = "نعم";
                negBtn = "لا";
//                title = "د. كيف";
                price="0.00";
                msg = "لديك " + mydatabase.getTotalOrderQty()+ "  منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات . هل تود الاستمرار";
            }
            // set title
            alertDialogBuilder.setTitle(title);

            // set dialog message
            final String finalPrice1 = price;
            alertDialogBuilder
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            mydatabase.deleteOrderTable();
//                            Checkout.morder_quantity.setText("0");
//                            Checkout.morder_price.setText(finalPrice1);
//                            Checkout.mcount_basket.setText("0");
                            Menu.morder_quantity.setText("0");
                            Menu.morder_price.setText(finalPrice1);
                            Menu.mcount_basket.setText("0");
//                            InsideMenu.morder_quantity.setText("0");
//                            InsideMenu.morder_price.setText(finalPrice1);
//                            InsideMenu.mcount_basket.setText("0");
//                            Intent b = new Intent(Menu.this, Home.class);
//                            b.putExtra("startWith", 1);
//                          b.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(b);
                            finish();
                        }
                    }).setNegativeButton(negBtn, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();



        }else {
            super.onBackPressed();
        }
    }
}
