package com.cs.broastedexpress.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import org.json.JSONException;
import org.json.JSONObject;


public class Saveaddress extends AppCompatActivity {

    String addressType, response;
    String address, latitude, longitude;
    SharedPreferences userPrefs;
    String userId;
    SharedPreferences languagePrefs;
    String language;
    String flatNumber, landMark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.content_saveaddress);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.content_saveaddress_arabic);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        address = getIntent().getExtras().getString("address");
        latitude = getIntent().getExtras().getString("latitude");
        longitude = getIntent().getExtras().getString("longitude");


        final ImageView Homebtn = (ImageView) findViewById(R.id.home);
        final ImageView Workbtn = (ImageView) findViewById(R.id.work);
        final ImageView Otherbtn = (ImageView) findViewById(R.id.other);

        final ImageView Homebtn1 = (ImageView) findViewById(R.id.home1);
        final ImageView Workbtn1 = (ImageView) findViewById(R.id.work1);
        final ImageView Otherbtn1 = (ImageView) findViewById(R.id.other1);

        RelativeLayout addresshome = (RelativeLayout) findViewById(R.id.address_home);
        RelativeLayout addresswork = (RelativeLayout) findViewById(R.id.address_work);
        RelativeLayout addressother = (RelativeLayout) findViewById(R.id.address_other);

        final EditText otherAddress = (EditText) findViewById(R.id.friendshome);
        final EditText mflate_no = (EditText) findViewById(R.id.flat_no);
        final EditText mland_mark = (EditText) findViewById(R.id.land_mark);
        final EditText selected_address = (EditText) findViewById(R.id.address);

        TextView save = (TextView) findViewById(R.id.save_continue);

        selected_address.setText(address);

        addresshome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "1";
                Homebtn1.setVisibility(View.VISIBLE);
                Workbtn1.setVisibility(View.GONE);
                Otherbtn1.setVisibility(View.GONE);
                otherAddress.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    otherAddress.setHint("Eg. My Home");
                } else if (language.equalsIgnoreCase("Ar")) {
                    otherAddress.setHint("منزلي");
                }
            }
        });

        addresswork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "2";
                Homebtn1.setVisibility(View.GONE);
                Workbtn1.setVisibility(View.VISIBLE);
                Otherbtn1.setVisibility(View.GONE);
                otherAddress.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    otherAddress.setHint("Eg. My Office");
                } else if (language.equalsIgnoreCase("Ar")) {
                    otherAddress.setHint("مكتبي");
                }
            }
        });

        addressother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addressType = "3";
                Homebtn1.setVisibility(View.GONE);
                Workbtn1.setVisibility(View.GONE);
                Otherbtn1.setVisibility(View.VISIBLE);
                otherAddress.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    otherAddress.setHint("Eg. Friend Home");
                } else if (language.equalsIgnoreCase("Ar")) {
                    otherAddress.setHint("منزل صديق");
                }
            }
        });

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mflate_no.getText().toString().isEmpty()) {
                    flatNumber = "....";
                } else {
                    flatNumber = mflate_no.getText().toString().replace(" ", "%20");
                }
                if (mland_mark.getText().toString().isEmpty()) {
                    landMark = "....";
                } else {
                    landMark = mland_mark.getText().toString().replace(" ", "%20");
                }
                String address = selected_address.getText().toString().replace(" ", "%20");
                String addressOther = otherAddress.getText().toString().replace(" ", "%20");
                if (addressOther.length() == 0) {
                    otherAddress.setError("");
                } else if (addressType == null) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(Saveaddress.this, "Please select address type", Toast.LENGTH_SHORT).show();
                    } else if (language.equalsIgnoreCase("Ar")) {
                        Toast.makeText(Saveaddress.this, "من فضلك اختر نوع العنوان", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    new SaveAddressDetails().execute(Projecturl.SAVE_ADDRESS_URL + userId + "?UsrAddress=" + address + "&HouseNo=" + flatNumber + "&LandMark=" + landMark + "&AddressType=" + addressType + "&Latitude=" + latitude + "&Longitude=" + longitude + "&HouseName=" + addressOther);
                }

            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public class SaveAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(Saveaddress.this);
            dialog = ProgressDialog.show(Saveaddress.this, "",
                    "Saving address...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(Saveaddress.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(Saveaddress.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            if (s.equals("1")) {
                                setResult(RESULT_OK);
                                finish();
                            }
                        } catch (JSONException je) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }

                    }
                }

            } else {
                Toast.makeText(Saveaddress.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}
