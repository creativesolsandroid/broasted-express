package com.cs.broastedexpress.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.GPSTracker;
import com.cs.broastedexpress.Home;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.cs.broastedexpress.R.id.order_number;
import static com.cs.broastedexpress.R.id.order_type;
import static com.cs.broastedexpress.R.id.payment_mode;
import static com.cs.broastedexpress.R.id.total_items;

public class OrderCompleted extends AppCompatActivity {

    TextView storeAddressTextView, totalItems, totalAmount, estTime, orderNumber, paymentMode, orderType, netTotal, vatAmount, amount;
    TextView storeAddressTxt, mget_direction;
    Button mtrack_order, mcreate_new_order;
    TextView vatPercent,receipt_close;
    RelativeLayout recieptLayout;
    ImageView minvoice;
    ImageView favOrder;
    private String storeId, storeName, storeAddress,storeName_ar, storeAddress_ar, total_amt, total_item, expected_time, payment_modes, order_types, order_numbers, orderId,your_name,your_address, mamount, mvatamount, mnetamount;
    private Double latitude, longitude;
    SharedPreferences userPrefs;
    String response, userResponse, userId;
    SharedPreferences orderPrefs;
    AlertDialog alertDialog;
    SharedPreferences.Editor orderPrefsEditor;
    SharedPreferences languagePrefs;
    String language;
    Double lat, longi;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_order_completed);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_order_completed_arabic);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mtrack_order = (Button) findViewById(R.id.track_order);
        mcreate_new_order = (Button) findViewById(R.id.create_new_order);

        storeId = getIntent().getExtras().getString("storeId");
        storeName = getIntent().getExtras().getString("storeName");
        storeAddress = getIntent().getExtras().getString("storeAddress");
        your_name = getIntent().getExtras().getString("your_name");
        your_address = getIntent().getExtras().getString("your_address");
        storeName_ar = getIntent().getExtras().getString("storeName_ar");
        storeAddress_ar = getIntent().getExtras().getString("storeAddress_ar");
        latitude = getIntent().getExtras().getDouble("latitude");
        longitude = getIntent().getExtras().getDouble("longitude");
        total_amt = getIntent().getExtras().getString("total_amt");
        total_item = getIntent().getExtras().getString("total_items");
        expected_time = getIntent().getExtras().getString("expected_time");
        payment_modes = getIntent().getExtras().getString("payment_mode");
        order_types = getIntent().getExtras().getString("order_type");
        order_numbers = getIntent().getExtras().getString("order_number");
        mamount = getIntent().getExtras().getString("amount");
        mvatamount = getIntent().getExtras().getString("vatamount");
        mnetamount = getIntent().getExtras().getString("netamount");

        Log.e("TAG","order type"+order_types);

        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor = orderPrefs.edit();

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userResponse = userPrefs.getString("user_profile", null);

        userId = userPrefs.getString("userId", null);
        storeAddressTextView = (TextView) findViewById(R.id.store_detail_address);
        totalItems = (TextView) findViewById(total_items);
        totalAmount = (TextView) findViewById(R.id.total_amount);
        estTime = (TextView) findViewById(R.id.expected_time);
        orderNumber = (TextView) findViewById(order_number);
        paymentMode = (TextView) findViewById(payment_mode);
        orderType = (TextView) findViewById(order_type);
        favOrder = (ImageView) findViewById(R.id.fav_order);
        storeAddressTxt = (TextView) findViewById(R.id.store_address_txt);
        mget_direction = (TextView) findViewById(R.id.get_directions);

        recieptLayout = (RelativeLayout) findViewById(R.id.receipt_layout);
        receipt_close = (TextView) findViewById(R.id.receipt_close);
        amount = (TextView) findViewById(R.id.amount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        vatPercent = (TextView) findViewById(R.id.vatPercent);
        netTotal = (TextView) findViewById(R.id.net_total);
        minvoice= (ImageView) findViewById(R.id.invoice);

        String[] orderNo = order_numbers.split("-");
        String[] parts = order_numbers.split(",");
        orderId = parts[0];

        orderPrefsEditor.putString("order_id", orderId);
        orderPrefsEditor.putString("order_status", "open");
        orderPrefsEditor.commit();


        receipt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recieptLayout.setVisibility(View.GONE);
            }
        });


        minvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recieptLayout.setVisibility(View.VISIBLE);
            }
        });
//        DecimalFormat decim = new DecimalFormat("0.00");
        amount.setText(mamount);
//        float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
        vatAmount.setText(mvatamount);
        netTotal.setText(mnetamount);

        if (language.equalsIgnoreCase("En")) {
            storeAddressTextView.setText(storeName+"\n"+storeAddress);
        }else if (language.equalsIgnoreCase("Ar")) {
            storeAddressTextView.setText(storeName_ar +"\n"+storeAddress_ar);
        }
        totalItems.setText(total_item);
        totalAmount.setText(total_amt);
        SimpleDateFormat datetime = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
        SimpleDateFormat time = new SimpleDateFormat("hh:mm a",Locale.US);
        Date date2 = null, time2 = null;
        try {
            time2 = datetime.parse(expected_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String date1 = date.format(time2);
        String time1 = time.format(time2);
        estTime.setText(date1+"\n"+time1);
        if (order_types.equalsIgnoreCase("Delivery")) {
            if (language.equalsIgnoreCase("En")) {
                storeAddressTxt.setText("Your Delivery Address");
            } else if (language.equalsIgnoreCase("Ar")) {
                storeAddressTxt.setText("عنوانك للتسليم");
            }
            storeAddressTextView.setText(your_name+"\n"+your_address);
            storeAddressTextView.setTextSize(12);
        }

        if (language.equalsIgnoreCase("En")) {
            if (payment_modes.equals("2")) {
                paymentMode.setText("Cash Payment");
            } else if (payment_modes.equals("3")) {
                paymentMode.setText("Online Payment");
            } else if (payment_modes.equals("4")) {
                paymentMode.setText("Free");
            }
            orderType.setText(order_types);
        } else if (language.equalsIgnoreCase("Ar")) {
            if (payment_modes.equals("2")) {
                paymentMode.setText("نقدي عند استلام الطلب");
            } else if (payment_modes.equals("3")) {
                paymentMode.setText("الدفع أونلاين");
            } else if (payment_modes.equals("4")) {
                paymentMode.setText("");
            }
            if (order_types.equalsIgnoreCase("Dine-In")) {
                orderType.setText("داخل الفرع");
            } else if (order_types.equalsIgnoreCase("Carryout")) {
                orderType.setText("خارج الفرع");
            } else if (order_types.equalsIgnoreCase("Delivery")) {
                orderType.setText("توصيل");
                mget_direction.setVisibility(View.GONE);
            }
        }

        mget_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessLocation()) {
                        requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                    } else {
                        getGPSCoordinates();
                    }
                } else {
                    getGPSCoordinates();
                }
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+lat+","+longi+"&daddr=" + latitude + "," + longitude));
                startActivity(intent);
            }
        });

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent intent = new Intent(OrderCompleted.this, TrackOrder.class);
                intent.putExtra("orderId", orderId);
                startActivity(intent);
            }
        }, 30000);

        if (order_types.equalsIgnoreCase("Delivery")) {
            mget_direction.setVisibility(View.GONE);
        }
        orderNumber.setText(orderNo[1]);
//        getDirection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr=24.70321657,46.68097073&daddr=" + latitude + "," + longitude));
//                startActivity(intent);
//            }
//        });

//        trackOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(OrderDetails.this, TrackOrderActivity.class);
//                intent.putExtra("orderId", orderId);
//                startActivity(intent);
//
////                Fragment merchandiseFragment = new TrackOrderFragment();
////                Bundle mBundle7 = new Bundle();
////                mBundle7.putString("orderId", orderId);
////                mBundle7.putInt("flag", 0);
////                merchandiseFragment.setArguments(mBundle7);
//
//            }
//        });

//        createOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(OrderDetails.this, MainActivity.class);
//                intent.putExtra("startWith",1);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//                finish();
//            }
//        });
        favOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderCompleted.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.insert_fav_order_dialog;
                if (language.equalsIgnoreCase("En")) {
                    layout = R.layout.insert_fav_order_dialog;
                } else if (language.equalsIgnoreCase("Ar")) {
                    layout = R.layout.insert_fav_order_dialog_arabic;
                }
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);

                final EditText favName = (EditText) dialogView.findViewById(R.id.fav_name);
                TextView cancelBtn = (TextView) dialogView.findViewById(R.id.no_cancel);
                final TextView saveOrder = (TextView) dialogView.findViewById(R.id.save_order);
                final TextView saveOrder1 = (TextView) dialogView.findViewById(R.id.save_order1);
                favName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (favName.getText().toString().length() > 0) {
                            saveOrder1.setEnabled(true);
                            saveOrder1.setVisibility(View.VISIBLE);

                        } else {
                            saveOrder.setEnabled(false);
                            saveOrder1.setEnabled(false);
                            saveOrder1.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {


                    }
                });


                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });


                saveOrder1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new InsertFavOrder().execute(Projecturl.INSERT_FAVORITE_ORDER_URL + orderId + "&FName=" + favName.getText().toString().replace(" ", "%20"));
                        alertDialog.dismiss();
                    }
                });


                alertDialog = dialogBuilder.create();
                alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });


        mtrack_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(OrderCompleted.this, TrackOrder.class);
                a.putExtra("orderId", orderId);
                a.putExtra("ordernumber", order_number);
                startActivity(a);
            }
        });

        mcreate_new_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent b = new Intent(OrderCompleted.this, Home.class);
                b.putExtra("startWith", 1);
//                b.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(b);
                finish();
            }
        });
    }

    public void getGPSCoordinates(){
        gps = new GPSTracker(OrderCompleted.this);
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
//                new OrderType.GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderCompleted.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(OrderCompleted.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }



    //    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//        }
//        return super.onOptionsItemSelected(item);
//    }
    public class InsertFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderCompleted.this);
            dialog = ProgressDialog.show(OrderCompleted.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OrderCompleted.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(OrderCompleted.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            favOrder.setImageResource(R.drawable.favourite_select);
                            favOrder.setEnabled(false);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(OrderCompleted.this, "Can not reach server", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            } else {
                Toast.makeText(OrderCompleted.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

}
