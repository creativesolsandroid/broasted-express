package com.cs.broastedexpress.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.broastedexpress.Adapter.FavoriteAdapter;
import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.FavoriteOrders;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class FavoriteOrder extends AppCompatActivity {
    private DataBaseHelper myDbHelper;
    String response;
    private ArrayList<FavoriteOrders> favoritesList = new ArrayList<>();
    private SwipeMenuListView FavoriteorderListView;
    TextView emptyView;
    private FavoriteAdapter mAdapter;
    SharedPreferences userPrefs;
    String userId;
    Toolbar toolbar;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.content_favorite_order);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.content_favorite_order_arabic);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        myDbHelper = new DataBaseHelper(FavoriteOrder.this);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        FavoriteorderListView = (SwipeMenuListView) findViewById(R.id.fav_order_listview);
        emptyView = (TextView) findViewById(R.id.empty_view);
        mAdapter = new FavoriteAdapter(FavoriteOrder.this, favoritesList, language);
        FavoriteorderListView.setAdapter(mAdapter);
        new GetFavoriteOrderDetails().execute(Projecturl.GET_FAVORITE_ORDERS_URL + userId);

        FavoriteorderListView.setEmptyView(emptyView);

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                deleteItem.setTitle("Delete");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        FavoriteorderListView.setMenuCreator(creator);

        // step 2. listener item click event
        FavoriteorderListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        new DeleteFavOrder().execute(Projecturl.DELETE_FAVORITE_ORDERS_URL + favoritesList.get(position).getOrderId());
                        break;
                }
                return false;
            }
        });


        FavoriteorderListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                myDbHelper.deleteOrderTable();
                String orderId = favoritesList.get(position).getOrderId();
                new GetOrderDetails().execute(Projecturl.ORDERD_DETAILS_URL + "?uId=" + userId + "&oId=" + orderId);
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }


    public class GetFavoriteOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteOrder.this);
            dialog = ProgressDialog.show(FavoriteOrder.this, "",
                    "Loading...");
            favoritesList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    JSONParser jParser = new JSONParser();

                    response = jParser
                            .getJSONFromUrl(params[0]);
                    Log.e("TAG", "user response:" + response);
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(FavoriteOrder.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(FavoriteOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    FavoriteOrders fo = new FavoriteOrders();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    String orderId = jo1.getString("odrId");
                                    String favoriteName = jo1.getString("Fname");
                                    String orderDate = jo1.getString("Odate");
                                    String storeName = jo1.getString("Sname");
                                    String storeName_ar = jo1.getString("Sname_ar");
                                    String total_Price = jo1.getString("TotPrice");
                                    String itemDetails = "", itemDetailsAr = "";

                                    JSONArray ja1 = jo1.getJSONArray("items");
                                    for (int j = 0; j < ja1.length(); j++) {
                                        JSONObject jo2 = ja1.getJSONObject(j);
                                        if (itemDetails.equalsIgnoreCase("") && itemDetailsAr.equalsIgnoreCase("")) {
                                            itemDetails = jo2.getString("ItmName");
                                            itemDetailsAr = jo2.getString("ItmName_ar");
                                        } else {
                                            itemDetails = itemDetails + ", " + jo2.getString("ItmName");
                                            itemDetailsAr = itemDetailsAr + ", " + jo2.getString("ItmName_ar");
                                        }
                                    }


                                    fo.setOrderId(orderId);
                                    fo.setFavoriteName(favoriteName);
                                    fo.setOrderDate(orderDate);
                                    fo.setStoreName(storeName);
                                    fo.setStoreName_ar(storeName_ar);
                                    fo.setTotalPrice(total_Price);
                                    fo.setItemDetails(itemDetails);
                                    fo.setItemDetails_ar(itemDetailsAr);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    favoritesList.add(fo);

                                }
                            } catch (JSONException je) {
                                je.printStackTrace();
//                                Toast.makeText(FavoriteOrdersActivity.this, "Sorry You Don't Have any Favorite Orders", Toast.LENGTH_SHORT).show();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("BAKERY & Co.");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(FavoriteOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }


    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteOrder.this);
            dialog = ProgressDialog.show(FavoriteOrder.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("PUT");
                    InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    is.close();
                    response = sb.toString();
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(FavoriteOrder.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(FavoriteOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

//                        try {
//                            JSONObject jo= new JSONObject(result);
//                            String s = jo.getString("Success");
                        new GetFavoriteOrderDetails().execute(Projecturl.GET_FAVORITE_ORDERS_URL + userId);
//                            Toast.makeText(FavoriteOrdersActivity.this, "Order deleted successfully", Toast.LENGTH_SHORT).show();

//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Toast.makeText(FavoriteOrdersActivity.this, "Can not reach server", Toast.LENGTH_SHORT).show();
//                        }

                    }
                }

            } else {
                Toast.makeText(FavoriteOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(FavoriteOrder.this);
            dialog = ProgressDialog.show(FavoriteOrder.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(FavoriteOrder.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(FavoriteOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            HashMap<String, String> values = new HashMap<>();

                            JSONObject jo = new JSONObject(result);

                            JSONArray ja = jo.getJSONArray("SubItem");
                            myDbHelper.deleteOrderTable();
                            for (int i = 0; i < ja.length(); i++) {
                                String ids = "0", additionalsStr = "", additionalsStrAr = "", additionalsPrice = "", additionalQty = "";
                                String categoryId = "", itemId = "", itemName = "", itemNameAr = "", itemImage = "", itemDesc = "", itemDescAr = "", itemType = "", comments = "";
                                String price = "",nonDelivery= "";
                                float additionPrice = 0;
                                float priceAd = 0, priceAd1 = 0;
                                int quantity = 0;
                                float finalPrice = 0;
                                JSONArray ja1 = ja.getJSONArray(i);
                                for (int j = 0; j < ja1.length(); j++) {
                                    if (j == 0) {
                                        JSONObject jo1 = ja1.getJSONObject(0);
                                        itemId = jo1.getString("ItemId");
                                        itemName = jo1.getString("ItemName");
                                        itemNameAr = jo1.getString("ItemName_Ar");
                                        itemDesc = jo1.getString("Description");
                                        itemDescAr = jo1.getString("Description_Ar");
                                        comments = jo1.getString("Comments");
                                        itemImage = jo1.getString("Images");
                                        price = jo1.getString("ItemPrice");
                                        quantity = jo1.getInt("Qty");
                                        categoryId = jo1.getString("CategoryId");
                                        itemType = jo1.getString("Size");
                                        nonDelivery = jo1.getString("IsDelivery");
                                    }else {
                                        JSONArray ja2 = ja1.getJSONArray(j);
                                        for (int k = 0; k < ja2.length(); k++) {
                                            JSONObject jo2 = ja2.getJSONObject(k);
                                            if (!jo2.getString("AdditionalID").equals("0")) {
                                                if (ids.equalsIgnoreCase("0")) {
                                                    ids = jo2.getString("AdditionalID");
                                                    additionalsStr = jo2.getString("AdditionalName");
                                                    additionalsStrAr = jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = jo2.getString("AdditionalPrice");
                                                    additionalQty = jo2.getString("AddQty");
                                                } else {
                                                    ids = ids + "," + jo2.getString("AdditionalID");
                                                    additionalsStr = additionalsStr + "," + jo2.getString("AdditionalName");
                                                    additionalsStrAr = additionalsStrAr + "," + jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = additionalsPrice + "," + jo2.getString("AdditionalPrice");
                                                    additionalQty = additionalQty + "," + jo2.getString("AddQty");
                                                }
                                                additionPrice = additionPrice + ((Float.parseFloat((jo2.getString("AdditionalPrice"))) * Integer.parseInt(jo2.getString("AddQty"))));
                                            }

                                        }

                                        priceAd = Float.parseFloat(price);
                                        priceAd1 = Float.parseFloat(price) + additionPrice;
                                    }
                                }

                                if (priceAd != 0) {
                                    finalPrice = priceAd1 * quantity;
                                } else {
                                    finalPrice = Float.parseFloat(price) * quantity;
                                }


                                values.put("itemId", itemId);
                                values.put("itemTypeId", itemType);
                                values.put("additionals", ids);
                                values.put("qty", Integer.toString(quantity));
                                values.put("price", Float.toString(priceAd));
                                values.put("additionalsPrice", additionalsPrice);
                                values.put("additionalsTypeId", additionalQty);
                                values.put("totalAmount", Float.toString(finalPrice));
                                values.put("comment", comments);
                                values.put("status", "1");
                                values.put("creationDate", "14/07/2015");
                                values.put("modifiedDate", "14/07/2015");
                                values.put("categoryId", categoryId);
                                values.put("itemName", itemName);
                                values.put("itemNameAr", itemNameAr);
                                values.put("image", itemImage);
                                values.put("desc", itemDesc);
                                values.put("descAr", itemDescAr);
                                values.put("nonDelivery", nonDelivery);
                                values.put("AdditionalName", additionalsStr);
                                values.put("AdditionalNameAr", additionalsStrAr);
                                values.put("Additionalimage", "");
                                myDbHelper.insertOrder(values);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("TAG", "" + e.toString());
                        }

                    }
                }

            } else {
                Toast.makeText(FavoriteOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            Intent intent = new Intent(FavoriteOrder.this, Checkout.class);
            startActivity(intent);

            super.onPostExecute(result);

        }

    }

}
