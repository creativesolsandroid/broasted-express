package com.cs.broastedexpress.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.Adapter.TrackItemsAdapter;
import com.cs.broastedexpress.GPSTracker;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.OrderHistory;
import com.cs.broastedexpress.Model.Rating;
import com.cs.broastedexpress.Model.RatingDetails;
import com.cs.broastedexpress.Model.TrackItems;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.cs.broastedexpress.R.id.ontheway_image;

public class TrackOrder extends AppCompatActivity implements Animation.AnimationListener {

    Animation animZoomIn,animZoomOut;
    TextView emptyView;
    ImageView share, cancelOrder;
    LinearLayout acceptImage;
    View viewcancel;
    ImageView orderImage, acceptImagecancel, readyImage, onthewayImage, deliveryImage;
    TextView orderTime, acceptTime, readyTime, onthewayTime, deliveryTime, expectedTime, accepted;
    TextView trackDelivery, trackDeliveryText, mready_text, morder_no_text, mexpected_time_text, montheway_text;
    private Timer timer = new Timer();

    public static String driverName, driverNumber, driverId;

    LinearLayout acceptLayout, readyLayout, onthewayLayout, deliveryLayout;
    ScrollView scrollView;

    DecimalFormat decim =new DecimalFormat("0.00");

    ArrayList<OrderHistory> orderHistory;
    Double lat, longi;
    String latitude, longitude, phone, totalPrice, subTotal, vatCharges, vatPercentage;
    String orderNumber, OrderStatus, StoreName, StoreNameAr, OrderType = "", StoreAddress, TrackingTime, ExpectedTime, address;
    String orderId, ordernumber;
    SharedPreferences userPrefs;
    FrameLayout mframe_layout;
    String userId;
    SharedPreferences languagePrefs;
    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefsEditor;
    String language;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    int ratingFromService;
    RelativeLayout rate_layout;
    MaterialRatingBar ratingBar;
    TextView cancel;
    final ArrayList<RatingDetails> rateArray = new ArrayList<>();

    private ArrayList<TrackItems> ItemsList = new ArrayList<>();

    public static TextView totalQty;
    ListView orderListView;
    TextView orderId_tv, totalAmount, promoAmount, netAmount, vatAmount, mamount;
    RelativeLayout summary_layout,list_expand;
    ImageView plus;
    Boolean show=true;
    TrackItemsAdapter mOrderAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_track_order);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.activity_track_order_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor = orderPrefs.edit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        orderId = getIntent().getStringExtra("orderId");

        emptyView = (TextView) findViewById(R.id.empty_view);
        share = (ImageView) findViewById(R.id.track_share);
        cancelOrder = (ImageView) findViewById(R.id.cancel_order);

        mframe_layout = (FrameLayout) findViewById(R.id.frame_layout);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        acceptLayout = (LinearLayout) findViewById(R.id.accept_layout_track);
        readyLayout = (LinearLayout) findViewById(R.id.ready_layout_track);
        onthewayLayout = (LinearLayout) findViewById(R.id.ontheway_layout_track);
        deliveryLayout = (LinearLayout) findViewById(R.id.delivery_layout_track);

        mamount = (TextView) findViewById(R.id.subAmount);
        netAmount = (TextView) findViewById(R.id.netAmount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        orderTime = (TextView) findViewById(R.id.order_time);
        mready_text = (TextView) findViewById(R.id.ready_text);
        acceptTime = (TextView) findViewById(R.id.accepted_time);
        accepted = (TextView) findViewById(R.id.accept);
        readyTime = (TextView) findViewById(R.id.ready_time);
        onthewayTime = (TextView) findViewById(R.id.ontheway_time);
        deliveryTime = (TextView) findViewById(R.id.delivered_time);
        expectedTime = (TextView) findViewById(R.id.expected_time);
        morder_no_text = (TextView) findViewById(R.id.order_number_text);
        mexpected_time_text = (TextView) findViewById(R.id.expected_time_text);
        trackDelivery = (TextView) findViewById(R.id.track_delivery);
        trackDeliveryText = (TextView) findViewById(R.id.track_delivery_desc);
        montheway_text = (TextView) findViewById(R.id.ontheway_text);
        orderImage = (ImageView) findViewById(R.id.order_image);

        ratingBar = (MaterialRatingBar) findViewById(R.id.ratingBar1);
        cancel = (TextView) findViewById(R.id.skip_rating);
        rate_layout = (RelativeLayout) findViewById(R.id.rate_layout);
        rate_layout.setVisibility(View.GONE);

        viewcancel = (View) findViewById(R.id.view_cancel);

        acceptImagecancel = (ImageView) findViewById(R.id.accept_image_cancel);
        readyImage = (ImageView) findViewById(R.id.ready_image);
        onthewayImage = (ImageView) findViewById(ontheway_image);
        deliveryImage = (ImageView) findViewById(R.id.delivery_image);

        acceptImage = (LinearLayout) findViewById(R.id.accept_image);

        orderListView = (ListView) findViewById(R.id.items_list);
        orderId_tv = (TextView) findViewById(R.id.orderId);
        totalQty = (TextView) findViewById(R.id.totalQty);
        totalAmount = (TextView) findViewById(R.id.totalAmount);
        promoAmount = (TextView) findViewById(R.id.promoAmount);
        plus = (ImageView) findViewById(R.id.list_plus);
        summary_layout = (RelativeLayout) findViewById(R.id.summary_layout);
        list_expand = (RelativeLayout) findViewById(R.id.list_expand);
        list_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(show){
                    orderListView.setVisibility(View.VISIBLE);
                    summary_layout.setVisibility(View.VISIBLE);
                    plus.setImageResource(R.drawable.track_minus);
                    show=false;
                }
                else {
                    orderListView.setVisibility(View.GONE);
                    summary_layout.setVisibility(View.GONE);
                    plus.setImageResource(R.drawable.track_plus);
                    show=true;
                }
            }
        });

        summary_layout.setVisibility(View.GONE);
        orderListView.setVisibility(View.GONE);

        mOrderAdapter = new TrackItemsAdapter(TrackOrder.this, ItemsList, language);
        orderListView.setAdapter(mOrderAdapter);

//        Animation zoomin= AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_in);
//        Animation zoomout=AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_out);
//
//        orderImage.setAnimation(zoomin);
//        orderImage.setAnimation(zoomout);

//        orderImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                orderImage.startAnimation(animZoomIn);
//            }
//        });

        driverName = "";
        driverNumber = "";
        driverId = "";
        new GetOrderTrackDetails().execute(Projecturl.TRACK_ORDER_STEPS_URL + orderId + "&uId=" + userId);

        Log.e("TAG","order id"+orderId);
        Log.e("TAG","userid"+userId);

        timer.schedule(new MyTimerTask(), 30000, 30000);

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog();
            }
        });

        cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(TrackOrder.this, android.R.style.Theme_Material_Light_Dialog));
                String title = "", msg = "", posBtn = "", negBtn = "";
                if (language.equalsIgnoreCase("En")) {
                    posBtn = "Yes";
                    negBtn = "No";
                    title = "Broasted Express";
                    msg = "Do you want to cancel the order?";
                } else if (language.equalsIgnoreCase("Ar")) {
                    posBtn = "نعم";
                    negBtn = "لا";
                    title = "بروستد إكسبريس";
                    msg = "هل ترغب في إلغاء الطلب ؟";
                }
                // set title
                alertDialogBuilder.setTitle(title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(posBtn, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id){
                                dialog.dismiss();
                                JSONObject parent = new JSONObject();
                                try {

                                    JSONArray mainItem = new JSONArray();

                                    JSONObject mainObj = new JSONObject();
                                    mainObj.put("OrderId", orderId);
                                    mainObj.put("UserId", userId);
                                    mainObj.put("Device_token", SplashActivity.regId);

                                    mainItem.put(mainObj);

                                    parent.put("CancelOrder", mainItem);
                                } catch (JSONException je) {
                                    je.printStackTrace();
                                }
                                new CancelOrder().execute(Projecturl.CANCEL_ORDER_URL+orderId);
                            }
                        }).setNegativeButton(negBtn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });
    }

    public void onAnimationEnd(Animation animation) {
        // Take any action after completing the animation

        // check for zoom in animation
        if (animation == animZoomIn) {
        }

    }

    public void onAnimationRepeat(Animation animation) {
        // TODO Auto-generated method stub

    }

    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub

    }


    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new GetOrderTrackDetails().execute(Projecturl.TRACK_ORDER_STEPS_URL + orderId + "&uId=" + userId);
                    Log.e("orderid", orderId);
                    Log.e("userid", userId);

                }
            });
        }
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        timer.cancel();
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }
        return super.onOptionsItemSelected(item);
    }

    public class GetOrderTrackDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        ProgressDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrder.this);
            dialog = ProgressDialog.show(TrackOrder.this, "",
                    "Reloading...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(TrackOrder.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    if (result.equals("")) {
                        scrollView.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        cancelOrder.setVisibility(View.GONE);
                        share.setVisibility(View.GONE);
                        Toast.makeText(TrackOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String failure = jo.getString("Failure");
                                scrollView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                                cancelOrder.setVisibility(View.GONE);
                                share.setVisibility(View.GONE);
                                list_expand.setVisibility(View.GONE);
                            } catch (JSONException jse) {

                                try {
                                    JSONArray ja = jo.getJSONArray("Received");
                                    cancelOrder.setEnabled(true);
                                    cancelOrder.setClickable(true);
                                    cancelOrder.setAlpha(1f);
                                    JSONObject jo1 = ja.getJSONObject(0);
                                    orderNumber = jo1.getString("InvoiceNo");
                                    orderId = jo1.getString("OrderId");
                                    String[] orderNum = orderNumber.split("-");
                                    orderId_tv.setText("#"+orderNum[1]);
                                    OrderStatus = "Order!";
                                    StoreName = jo1.getString("StoreName");
                                    StoreNameAr = jo1.getString("StoreName_ar");
                                    StoreAddress = jo1.getString("StoreAddress");
                                    OrderType = jo1.getString("OrderType");
                                    TrackingTime = jo1.getString("TrackingTime");
                                    ExpectedTime = jo1.getString("ExpectedTime");
                                    latitude = jo1.getString("Latitude");
                                    longitude = jo1.getString("Longitude");
                                    phone = jo1.getString("phone");
                                    address = jo1.getString("Address");
                                    totalPrice = jo1.getString("Total_Price");
                                    subTotal = jo1.getString("SubTotal");
                                    vatCharges = jo1.getString("VatCharges");
                                    vatPercentage = jo1.getString("VatPercentage");
                                    float promoAmt = jo1.getInt("PromoAmt");
                                    float total = Float.parseFloat(Projecturl.convertToArabic(subTotal)) - Float.parseFloat(String.valueOf(promoAmt));
                                    promoAmount.setText(""+decim.format(Float.parseFloat(String.valueOf(promoAmt))));
                                    mamount.setText(""+decim.format(Float.parseFloat(String.valueOf(total))));
                                    vatAmount.setText(""+decim.format(Float.parseFloat(vatCharges)));
                                    netAmount.setText(""+decim.format(Float.parseFloat(String.valueOf(totalPrice))));
                                    totalAmount.setText(""+decim.format(Float.parseFloat(subTotal)));
                                    totalQty.setText(jo1.getString("QuantityCount"));

                                    ratingFromService = jo1.getInt("Rating");

                                    RatingDetails rd = new RatingDetails();
                                    JSONArray jA = jo1.getJSONArray("Ratings");
                                    JSONObject obj = jA.getJSONObject(0);
                                    rd.setGivenrating("5");
                                    JSONArray ratingArray = obj.getJSONArray("5");
                                    ArrayList<Rating> ratingsArrayList = new ArrayList<>();
                                    for (int i = 0; i < ratingArray.length(); i++) {
                                        JSONObject object1 = ratingArray.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);

                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray1 = obj.getJSONArray("4");
                                    rd.setGivenrating("4");
                                    for (int i = 0; i < ratingArray1.length(); i++) {
                                        JSONObject object1 = ratingArray1.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray2 = obj.getJSONArray("3");
                                    rd.setGivenrating("3");
                                    for (int i = 0; i < ratingArray2.length(); i++) {
                                        JSONObject object1 = ratingArray2.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    JSONArray ratingArray3 = obj.getJSONArray("2");
                                    rd.setGivenrating("2");
                                    for (int i = 0; i < ratingArray3.length(); i++) {
                                        JSONObject object1 = ratingArray3.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);
                                    JSONArray ratingArray4 = obj.getJSONArray("1");
                                    rd.setGivenrating("1");
                                    for (int i = 0; i < ratingArray4.length(); i++) {
                                        JSONObject object1 = ratingArray4.getJSONObject(i);
                                        Rating rating = new Rating();
                                        rating.setRatingId(object1.getString("RatingId"));
                                        rating.setComment(object1.getString("Comment_En"));
                                        rating.setComment_ar(object1.getString("Comment_Ar"));
                                        rating.setRating(object1.getString("Rating"));
                                        ratingsArrayList.add(rating);
                                    }
                                    rd.setArrayList(ratingsArrayList);

                                    rateArray.add(rd);


                                    ItemsList.clear();
                                    JSONArray itemsArray = jo1.getJSONArray("items");
                                    for(int i=0; i<itemsArray.length(); i++){
                                        TrackItems ti = new TrackItems();

                                        JSONObject jo2 = itemsArray.getJSONObject(i);
                                        ti.setItem_name(jo2.getString("ItmName"));
                                        ti.setItem_name_ar(jo2.getString("ItmName_ar"));
                                        ti.setItemQty(jo2.getString("ItemQty"));
                                        ti.setPrice(jo2.getString("price"));

                                        ItemsList.add(ti);
                                    }

                                    String[] order_no = orderNumber.split("-");
                                    orderNumber = order_no[1];
                                    morder_no_text.setText(orderNumber);
                                    SimpleDateFormat datetime = new SimpleDateFormat("dd-MM-yyyy hh:mm a",Locale.US);
                                    SimpleDateFormat date2 = new SimpleDateFormat("dd-MM-yyyy",Locale.US);
                                    SimpleDateFormat time2 = new SimpleDateFormat("hh:mm a",Locale.US);
                                    Date date3 = null, time3 = null;
                                    try {

                                        time3 = datetime.parse(ExpectedTime);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    String date1 = date2.format(time3);
                                    String time1 = time2.format(time3);

                                    mexpected_time_text.setText(date1+"\n"+time1);
                                    SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                    SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    String date;
                                    Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                    try {
                                        dateObj = curFormater.parse(TrackingTime);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        try {
                                            dateObj = curFormater1.parse(TrackingTime);
                                        } catch (ParseException pe) {

                                        }

                                    }
                                    SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                    date = postFormater.format(dateObj);
                                    orderTime.setText(date);
                                } catch (JSONException je) {
                                    scrollView.setVisibility(View.GONE);
                                    emptyView.setVisibility(View.VISIBLE);
                                    cancelOrder.setVisibility(View.GONE);
                                    share.setVisibility(View.GONE);
                                }


                                try {
                                    JSONArray ja1 = jo.getJSONArray("Cancel");
                                    JSONObject jo2 = ja1.getJSONObject(0);
                                    cancelOrder.setVisibility(View.GONE);

                                    if (language.equalsIgnoreCase("En")) {
                                        accepted.setText("Cancel");
                                        expectedTime.setText("Your Order has been cancelled");
                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        accepted.setText("الغاء");
                                        expectedTime.setText("طلبك تم الغاءه");
                                    }
                                    acceptImagecancel.setVisibility(View.VISIBLE);
                                    readyLayout.setVisibility(View.GONE);
                                    onthewayLayout.setVisibility(View.GONE);
                                    deliveryLayout.setVisibility(View.GONE);
                                    viewcancel.setVisibility(View.GONE);

                                    TrackingTime = jo2.getString("TrackingTime");


//                                }
                                } catch (JSONException jsone) {

                                    try {
                                        JSONArray ja = jo.getJSONArray("Confirmed");
                                        JSONObject jo1 = ja.getJSONObject(0);
                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setClickable(true);
                                        cancelOrder.setAlpha(1f);
                                        TrackingTime = jo1.getString("TrackingTime");
                                        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                        String date;
                                        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                        try {
                                            dateObj = curFormater.parse(TrackingTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            try {
                                                dateObj = curFormater1.parse(TrackingTime);
                                            } catch (ParseException pe) {

                                            }

                                        }
                                        SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                        date = postFormater.format(dateObj);
//                                        if(OrderType.equalsIgnoreCase("Delivery")){
//                                            expectedTime.setText("Your order is accepted, will be delivered by "+ExpectedTime);
//                                        }else {
//                                            trackDelivery.setText("SERVED");
//                                            trackDeliveryText.setText("Your order is served");
//                                            expectedTime.setText("Your order is accepted, will be served by "+ExpectedTime);
//                                        }

                                        acceptTime.setText(date);
                                        acceptLayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    } catch (JSONException je) {

                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setClickable(true);
                                        cancelOrder.setAlpha(1f);
                                        acceptLayout.setAlpha(0.15f);
                                        readyLayout.setAlpha(0.15f);
                                        if (language.equalsIgnoreCase("En")) {
                                            if (OrderType.equalsIgnoreCase("Delivery")) {
//                                            expectedTime.setText("Your order is accepted, will be delivered by ");
                                                onthewayLayout.setAlpha(0.15f);
                                                deliveryLayout.setAlpha(0.15f);
                                            } else {
                                                trackDelivery.setText("SERVED");
                                                trackDeliveryText.setText("Your food has been Served");
//                                            expectedTime.setText("Your order is accepted, will be served by ");
                                                onthewayLayout.setVisibility(View.GONE);
                                                deliveryLayout.setAlpha(0.15f);
                                            }
                                        } else if (language.equalsIgnoreCase("Ar")) {
                                            if (OrderType.equalsIgnoreCase("Delivery")) {
//                                            expectedTime.setText("Your order is accepted, will be delivered by ");
                                                onthewayLayout.setAlpha(0.15f);
                                                deliveryLayout.setAlpha(0.15f);
                                            } else {
                                                trackDelivery.setText("تم استلامه");
                                                trackDeliveryText.setText("تم تقديم طلبك");
//                                            expectedTime.setText("Your order is accepted, will be served by ");
                                                onthewayLayout.setVisibility(View.GONE);
                                                deliveryLayout.setAlpha(0.15f);
                                            }
                                        }
                                    }

                                    try {
                                        JSONArray ja = jo.getJSONArray("Ready");
                                        JSONObject jo1 = ja.getJSONObject(0);
                                        cancelOrder.setEnabled(false);
                                        cancelOrder.setClickable(false);
                                        cancelOrder.setAlpha(0.5f);
                                        TrackingTime = jo1.getString("TrackingTime");
                                        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                        String date;
                                        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                        try {
                                            dateObj = curFormater.parse(TrackingTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            try {
                                                dateObj = curFormater1.parse(TrackingTime);
                                            } catch (ParseException pe) {

                                            }

                                        }
                                        SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                        date = postFormater.format(dateObj);
                                        readyTime.setText(date);
                                        readyLayout.setAlpha(1f);
                                    } catch (JSONException je) {
                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setAlpha(1f);
                                        cancelOrder.setClickable(true);
                                        readyLayout.setAlpha(0.15f);
                                        if (language.equalsIgnoreCase("En")) {
                                            if (OrderType.equalsIgnoreCase("Delivery")) {
                                                mready_text.setText("Your food is ready at" + StoreAddress);
                                                onthewayLayout.setAlpha(0.15f);
                                                deliveryLayout.setAlpha(0.15f);
                                            } else {
                                                mready_text.setText("Your Order is Ready");
                                                onthewayLayout.setVisibility(View.GONE);
                                                deliveryLayout.setAlpha(0.15f);
                                            }
                                        } else if (language.equalsIgnoreCase("Ar")) {
                                            if (OrderType.equalsIgnoreCase("Delivery")) {
                                                mready_text.setText("طلبك جاهز في" + StoreNameAr);
                                                onthewayLayout.setAlpha(0.15f);
                                                deliveryLayout.setAlpha(0.15f);
                                            } else {
                                                mready_text.setText("طلبك جاهز في");
                                                onthewayLayout.setVisibility(View.GONE);
                                                deliveryLayout.setAlpha(0.15f);
                                            }
                                        }
                                    }

                                    if (OrderType.equalsIgnoreCase("Delivery")) {
                                        onthewayLayout.setVisibility(View.VISIBLE);
                                        try {
                                            JSONArray ja = jo.getJSONArray("OnTheWay");
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
                                            JSONArray driverArray = jo1.getJSONArray("DriverName");
                                            JSONObject driverObj = driverArray.getJSONObject(0);
                                            driverName = driverObj.getString("FullName");
                                            driverNumber = driverObj.getString("Mobile");
                                            driverId = jo1.getString("UserId");
                                            TrackingTime = jo1.getString("TrackingTime");
                                            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                            SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                            String date;
                                            Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                            if (language.equalsIgnoreCase("En")) {
                                                montheway_text.setText("Your food is on the way to \n" + address);
                                            } else if (language.equalsIgnoreCase("Ar")) {
                                                montheway_text.setText("\n طلبك الآن في الطريق إلى" + address);
                                            }
                                            try {
                                                dateObj = curFormater.parse(TrackingTime);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                try {
                                                    dateObj = curFormater1.parse(TrackingTime);
                                                } catch (ParseException pe) {

                                                }

                                            }
                                            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                            date = postFormater.format(dateObj);
                                            onthewayTime.setText(date);
                                            onthewayLayout.setAlpha(1f);

                                            animZoomIn= AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_in);
                                            animZoomOut=AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_out);

                                            onthewayImage.setAnimation(animZoomIn);
                                            onthewayImage.setAnimation(animZoomOut);

                                            animZoomIn.setAnimationListener(new Animation.AnimationListener() {
                                                @Override
                                                public void onAnimationStart(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationRepeat(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationEnd(Animation animation) {

                                                    onthewayImage.startAnimation(animZoomOut);
                                                }
                                            });

                                            animZoomOut.setAnimationListener(new Animation.AnimationListener() {
                                                @Override
                                                public void onAnimationStart(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationRepeat(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationEnd(Animation animation) {

                                                    onthewayImage.startAnimation(animZoomIn);
                                                }
                                            });



                                            onthewayLayout.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent i = new Intent(TrackOrder.this, Live_Tracking.class);
                                                    i.putExtra("driver_name", driverName);
                                                    i.putExtra("driver_number", driverNumber);
                                                    i.putExtra("driver_id", driverId);
                                                    i.putExtra("order_id", orderId);
                                                    i.putExtra("exp_time", ExpectedTime);
                                                    startActivity(i);
                                                }
                                            });

                                        } catch (JSONException je) {

                                            onthewayLayout.setAlpha(0.15f);
                                            deliveryLayout.setAlpha(0.15f);
                                        }


                                        try {
                                            JSONArray ja = jo.getJSONArray("Delivered");
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
                                            TrackingTime = jo1.getString("TrackingTime");
                                            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                            SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                            String date;
                                            Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                            OrderStatus = "Delivery";
                                            if (language.equalsIgnoreCase("En")) {
                                                trackDelivery.setText("DELIVERED");
                                                trackDeliveryText.setText("Your food has been delivered");
                                            } else if (language.equalsIgnoreCase("Ar")) {
                                                trackDelivery.setText("تم التوصيل");
                                                trackDeliveryText.setText("تم توصيل طلبك");
                                            }

                                            try {
                                                dateObj = curFormater.parse(TrackingTime);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                try {
                                                    dateObj = curFormater1.parse(TrackingTime);
                                                } catch (ParseException pe) {

                                                }

                                            }
                                            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                            date = postFormater.format(dateObj);
                                            deliveryTime.setText(date);
                                            deliveryLayout.setAlpha(1f);
                                            animZoomIn= AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_in);
                                            animZoomOut=AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_out);

                                            onthewayImage.setAnimation(animZoomIn);
                                            onthewayImage.setAnimation(animZoomOut);

                                            animZoomIn.setAnimationListener(new Animation.AnimationListener() {
                                                @Override
                                                public void onAnimationStart(Animation animation) {

                                                    onthewayImage.clearAnimation();

                                                }

                                                @Override
                                                public void onAnimationRepeat(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationEnd(Animation animation) {

                                                    onthewayImage.clearAnimation();
                                                }
                                            });

                                            animZoomOut.setAnimationListener(new Animation.AnimationListener() {
                                                @Override
                                                public void onAnimationStart(Animation animation) {
                                                    onthewayImage.clearAnimation();
                                                }

                                                @Override
                                                public void onAnimationRepeat(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationEnd(Animation animation) {

                                                    onthewayImage.clearAnimation();
                                                }
                                            });
                                            onthewayLayout.setClickable(false);

                                            if(ratingFromService == 0) {
                                                rate_layout.setVisibility(View.VISIBLE);
                                                cancel.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        rate_layout.setVisibility(View.GONE);
                                                    }
                                                });

                                                ratingBar.setRating(0);
                                                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                                    @Override
                                                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                        if (fromUser) {
                                                            Intent intent = new Intent(TrackOrder.this, RatingActivity.class);
                                                            intent.putExtra("rating", rating);
                                                            intent.putExtra("array", rateArray);
                                                            intent.putExtra("userid", userId);
                                                            intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                            startActivity(intent);
                                                            rate_layout.setVisibility(View.GONE);
                                                        }
                                                    }
                                                });
                                            }

                                        } catch (JSONException je) {
                                            deliveryLayout.setAlpha(0.15f);
                                        }

                                    } else {
                                        onthewayLayout.setVisibility(View.GONE);
                                        try {
                                            JSONArray ja = jo.getJSONArray("Delivered");
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
                                            TrackingTime = jo1.getString("TrackingTime");
                                            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                            SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                            String date;
                                            Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                            OrderStatus = "Served";
                                            if (language.equalsIgnoreCase("En")) {
                                                trackDelivery.setText("SERVED");
                                                trackDeliveryText.setText("Your food has been Served");
                                            } else if (language.equalsIgnoreCase("Ar")) {
                                                trackDelivery.setText("تم استلامه");
                                                trackDeliveryText.setText("تم تقديم طلبك ");
                                            }
                                            try {
                                                dateObj = curFormater.parse(TrackingTime);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                try {
                                                    dateObj = curFormater1.parse(TrackingTime);
                                                } catch (ParseException pe) {

                                                }

                                            }
                                            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                            date = postFormater.format(dateObj);
                                            deliveryTime.setText(date);
                                            deliveryLayout.setAlpha(1f);

                                            if(ratingFromService == 0) {
                                                rate_layout.setVisibility(View.VISIBLE);
                                                cancel.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        rate_layout.setVisibility(View.GONE);
                                                    }
                                                });

                                                ratingBar.setRating(0);
                                                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                                    @Override
                                                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                        if (fromUser) {
                                                            Intent intent = new Intent(TrackOrder.this, RatingActivity.class);
                                                            intent.putExtra("rating", rating);
                                                            intent.putExtra("array", rateArray);
                                                            intent.putExtra("userid", userId);
                                                            intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                            startActivity(intent);
                                                            rate_layout.setVisibility(View.GONE);
                                                        }
                                                    }
                                                });
                                            }
                                        } catch (JSONException je) {
                                            deliveryLayout.setAlpha(0.15f);
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(TrackOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            try {
                if (dialog != null) {
                    dialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (orderPrefs.getString("order_id", "").equals(orderId)) {
                if (orderPrefs.getString("order_status", "close").equalsIgnoreCase("close")) {
                    cancelOrder.setEnabled(false);
                    cancelOrder.setAlpha(0.5f);
                    cancelOrder.setClickable(false);
                } else {
                    cancelOrder.setEnabled(true);
                    cancelOrder.setAlpha(1f);
                    cancelOrder.setClickable(true);
                }
            } else {
                cancelOrder.setEnabled(false);
                cancelOrder.setAlpha(0.5f);
                cancelOrder.setClickable(false);
            }
            super.onPostExecute(result);

        }

    }


    public void dialog() {

        final Dialog dialog2 = new Dialog(TrackOrder.this);

        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (language.equalsIgnoreCase("En")) {
            dialog2.setContentView(R.layout.track_order_share_dialog);
        } else if (language.equalsIgnoreCase("Ar")) {
            dialog2.setContentView(R.layout.track_order_share_dialog_arabic);
        }
        dialog2.setCanceledOnTouchOutside(true);
        TextView share = (TextView) dialog2.findViewById(R.id.share_order);
        TextView getDirection = (TextView) dialog2.findViewById(R.id.get_direction);
        TextView cancel = (TextView) dialog2.findViewById(R.id.cancel_button);
        TextView call= (TextView) dialog2.findViewById(R.id.get_direction1);

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + phone));
                        if (ActivityCompat.checkSelfPermission(TrackOrder.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + phone));
                    startActivity(intent);
                }
            }
        });

        if (OrderType.equalsIgnoreCase("Delivery")) {
            getDirection.setVisibility(View.GONE);
        }

        share.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessLocation()) {
                        requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                    } else {
                        getGPSCoordinates();
                    }
                } else {
                    getGPSCoordinates();
                }

            }
        });

        getDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });


        dialog2.show();

    }


    public void getGPSCoordinates() {
        gps = new GPSTracker(TrackOrder.this);
        if (gps != null) {
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "Broasted Express Order Details \n Order No: " + orderNumber + "\n Expected Time: " + ExpectedTime + "\n Store: " + StoreName + "\n Amount: " + totalPrice + "\nhttp://maps.google.com/maps?saddr=" + lat + "," + longi + "&daddr=" + latitude + "," + longitude);

                startActivity(Intent.createChooser(intent, "Share"));
                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }


    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(TrackOrder.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + phone));
                    if (ActivityCompat.checkSelfPermission(TrackOrder.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(TrackOrder.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

//    private boolean hasPermission(String perm) {
//        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(TrackOrder.this, perm));
//    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//
//        switch (requestCode) {
//
//
//            case LOCATION_REQUEST:
//                if (canAccessLocation()) {
//                    getGPSCoordinates();
//                } else {
//                    Toast.makeText(TrackOrder.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
//                }
//                break;
//        }
//    }

    public class CancelOrder extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String networkStatus;
        JSONObject parent = new JSONObject();
        InputStream inputStream = null;
        String result = "";
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrder.this);
            dialog = ProgressDialog.show(TrackOrder.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Projecturl.CANCEL_ORDER_URL+orderId);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(arg0[0]);

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            result = convertInputStreamToString(inputStream);
                            return result;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("Responce", "" + result);
            } else {
                return "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                Log.i("TAG","user resposne: "+result1);
                if (result1.equalsIgnoreCase("no internet")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(TrackOrder.this, android.R.style.Theme_Material_Light_Dialog));

                    // set title
                    alertDialogBuilder.setTitle("Broasted Express");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Communication Error! Please check the internet connection?")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    try {
                        JSONObject jo = new JSONObject(result);

//                        JSONObject ja = jo.getJSONObject("Success");

                        new GetOrderTrackDetails().execute(Projecturl.TRACK_ORDER_STEPS_URL + orderId + "&uId=" + userId);
                        Toast.makeText(TrackOrder.this, "Order cancelled successfully", Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result1);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
