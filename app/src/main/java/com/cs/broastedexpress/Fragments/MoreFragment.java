package com.cs.broastedexpress.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.Activities.Aboutus;
import com.cs.broastedexpress.Activities.Login;
import com.cs.broastedexpress.Activities.MoreWebView;
import com.cs.broastedexpress.Activities.Myaddress;
import com.cs.broastedexpress.Activities.Profile;
import com.cs.broastedexpress.R;

import java.util.List;

/**
 * Created by CS on 10/11/2016.
 */

public class MoreFragment extends Fragment implements View.OnClickListener {

    private static final int PROFILE_REQUEST = 1;
    private static final int ADDRESS_REQUEST = 2;
    String mLoginStatus;
    TextView langEnglish, langArabic, langEnglish1, langArabic1;
    TextView aboutUs, contactUs;
    TextView moreFb, moreTwitter, moreYoutube, moreKeek, moreInstagram;
    ImageView moreRate;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    View rootView;
    LinearLayout mProfile, mManage_address;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.more_fragment, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.more_fragment_arabic, container, false);
        }

        mProfile = (LinearLayout) rootView.findViewById(R.id.my_profile);
        mManage_address = (LinearLayout) rootView.findViewById(R.id.manage_address);

        moreFb = (TextView) rootView.findViewById(R.id.more_fb);
        moreTwitter = (TextView) rootView.findViewById(R.id.more_twitter);
        moreYoutube = (TextView) rootView.findViewById(R.id.more_youtube);
        moreInstagram = (TextView) rootView.findViewById(R.id.more_instagram);
        moreKeek = (TextView) rootView.findViewById(R.id.more_keek);
        aboutUs = (TextView) rootView.findViewById(R.id.more_aboutus);
        contactUs = (TextView) rootView.findViewById(R.id.more_contactus);
        langEnglish = (TextView) rootView.findViewById(R.id.lang_english);
        langArabic = (TextView) rootView.findViewById(R.id.lang_arabic);
        langEnglish1 = (TextView) rootView.findViewById(R.id.lang_english1);
        langArabic1 = (TextView) rootView.findViewById(R.id.lang_arabic1);

        moreRate = (ImageView) rootView.findViewById(R.id.more_rate);

        mProfile.setOnClickListener(this);
        mManage_address.setOnClickListener(this);
        moreFb.setOnClickListener(this);
        moreTwitter.setOnClickListener(this);
        moreYoutube.setOnClickListener(this);
        moreInstagram.setOnClickListener(this);
        moreKeek.setOnClickListener(this);
        moreRate.setOnClickListener(this);
        aboutUs.setOnClickListener(this);
        contactUs.setOnClickListener(this);
        langEnglish.setOnClickListener(this);
        langArabic.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.my_profile:
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(getActivity(), Profile.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), Login.class);
                    startActivityForResult(intent, PROFILE_REQUEST);
                }
                break;

            case R.id.manage_address:
                mLoginStatus = userPrefs.getString("login_status", "");
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent i = new Intent(getActivity(), Myaddress.class);
                    startActivity(i);
                } else {
                    Intent intent = new Intent(getActivity(), Login.class);
                    startActivityForResult(intent, ADDRESS_REQUEST);
                }

                break;

            case R.id.more_fb:
                Intent fbIntent = new Intent(getActivity(), MoreWebView.class);
                fbIntent.putExtra("title", "Facebook");
                fbIntent.putExtra("url", "http://www.facebook.com/broastedexpressksa");
                startActivity(fbIntent);
                break;

            case R.id.more_twitter:
                Intent twitterIntent = new Intent(getActivity(), MoreWebView.class);
                twitterIntent.putExtra("title", "Twitter");
                twitterIntent.putExtra("url", "http://www.twitter.com/broastedexpress");
                startActivity(twitterIntent);
                break;

            case R.id.more_keek:
                Intent keekIntent = new Intent(getActivity(), MoreWebView.class);
                keekIntent.putExtra("title", "KeeK");
                keekIntent.putExtra("url", "http://www.keek.com/broastedexpress");
                startActivity(keekIntent);
                break;

            case R.id.more_youtube:
                Intent youtubeIntent = new Intent(getActivity(), MoreWebView.class);
                youtubeIntent.putExtra("title", "Youtube");
                youtubeIntent.putExtra("url", "http://www.youtube.com/user/broastedexpress");
                startActivity(youtubeIntent);
                break;

            case R.id.more_instagram:
                Intent instagramIntent = new Intent(getActivity(), MoreWebView.class);
                instagramIntent.putExtra("title", "Instagram");
                instagramIntent.putExtra("url", "http://www.instagram.com/broastedexpress");
                startActivity(instagramIntent);
                break;

            case R.id.more_rate:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.boastedexpress")));
                break;

            case R.id.more_aboutus:
                Intent intent1 = new Intent(getActivity(), Aboutus.class);
                startActivity(intent1);
                break;
            case R.id.more_contactus:
//                String email = "info@BroastedExpress.com";
//                Intent intent = new Intent(Intent.ACTION_SEND);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.setType("plain/text");
//                intent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
//                intent.putExtra(Intent.EXTRA_SUBJECT, "Broasted Express Experiance");
//                intent.putExtra(Intent.EXTRA_TITLE, "Broasted Express Experiance");
//                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
//                getActivity().startActivity(intent);

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("plain/text");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@BroastedExpress.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Broasted Express Experience");
                i.putExtra(Intent.EXTRA_TITLE  , "Broasted Express Experience");
                final PackageManager pm = getActivity().getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                String className = null;
                for (final ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                        className = info.activityInfo.name;

                        if(className != null && !className.isEmpty()){
                            break;
                        }
                    }
                }
                i.setClassName("com.google.android.gm", className);
                try {
                    getActivity().startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "There are no email apps installed.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lang_english:
                languagePrefsEditor.putString("language", "En");
                languagePrefsEditor.commit();
//                langEnglish.setVisibility(View.VISIBLE);
//                langEnglish1.setVisibility(View.GONE);
//                langArabic.setVisibility(View.VISIBLE);
//                langArabic1.setVisibility(View.GONE);
                langEnglish.setBackground(getResources().getDrawable(R.drawable.semicircle));
                langArabic.setBackground(getResources().getDrawable(R.drawable.semicircle1));
                langEnglish.setTextColor(Color.parseColor("#000000"));
                langArabic.setTextColor(Color.parseColor("#FFFFFF"));
                getActivity().recreate();
                break;
            case R.id.lang_arabic:
                languagePrefsEditor.putString("language", "Ar");
                languagePrefsEditor.commit();
//                langEnglish.setVisibility(View.GONE);
//                langEnglish1.setVisibility(View.VISIBLE);
//                langArabic.setVisibility(View.GONE);
//                langArabic1.setVisibility(View.VISIBLE);
                langArabic.setBackground(getResources().getDrawable(R.drawable.semicircle_ar));
                langEnglish.setBackground(getResources().getDrawable(R.drawable.semicircle_ar1));
                langArabic.setTextColor(Color.parseColor("#000000"));
                langEnglish.setTextColor(Color.parseColor("#FFFFFF"));
                getActivity().recreate();
                break;
        }
    }
}
