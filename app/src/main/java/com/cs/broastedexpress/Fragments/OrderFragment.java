package com.cs.broastedexpress.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cs.broastedexpress.Activities.FavoriteOrder;
import com.cs.broastedexpress.Activities.History;
import com.cs.broastedexpress.Activities.Login;
import com.cs.broastedexpress.Activities.Menu;
import com.cs.broastedexpress.R;

/**
 * Created by CS on 10/11/2016.
 */

public class OrderFragment extends Fragment {
    private static final int ORDER_HISTORY_REQUEST = 1;
    private static final int FAVORITE_ORDER_REQUEST = 2;
    View rootView;
    String mLoginStatus;
    SharedPreferences userPrefs;
    SharedPreferences languagePrefs;
    String language;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.order_fragment, container, false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.order_fragment_arabic, container, false);
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");

        final LinearLayout New_order = (LinearLayout) rootView.findViewById(R.id.new_order);
        final LinearLayout mFavrioute_order = (LinearLayout) rootView.findViewById(R.id.favorite_order);
        final LinearLayout mHistory_order = (LinearLayout) rootView.findViewById(R.id.order_history);
        New_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                New_order.setBackground(getResources().getDrawable(R.drawable.orderscreenselected));
                mFavrioute_order.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                mHistory_order.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                Intent a = new Intent(getActivity(), Menu.class);
                startActivity(a);
            }
        });
        mFavrioute_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                New_order.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                mFavrioute_order.setBackground(getResources().getDrawable(R.drawable.orderscreenselected));
                mHistory_order.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent i = new Intent(getActivity(), FavoriteOrder.class);
                    startActivity(i);
                } else {
                    Intent intent = new Intent(getActivity(), Login.class);
                    startActivityForResult(intent, FAVORITE_ORDER_REQUEST);
                }

            }
        });
        mHistory_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                New_order.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                mFavrioute_order.setBackground(getResources().getDrawable(R.drawable.orderscreenunselected));
                mHistory_order.setBackground(getResources().getDrawable(R.drawable.orderscreenselected));
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent i = new Intent(getActivity(), History.class);
                    startActivity(i);
                } else {
                    Intent intent = new Intent(getActivity(), Login.class);
                    startActivityForResult(intent, ORDER_HISTORY_REQUEST);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLoginStatus = userPrefs.getString("login_status", "");
        if (requestCode == ORDER_HISTORY_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), History.class);
            startActivity(intent);
        } else if (requestCode == FAVORITE_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), FavoriteOrder.class);
            startActivity(intent);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
