package com.cs.broastedexpress.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.Activities.FavoriteOrder;
import com.cs.broastedexpress.Activities.History;
import com.cs.broastedexpress.Activities.InsideMenu;
import com.cs.broastedexpress.Activities.Login;
import com.cs.broastedexpress.Activities.Menu;
import com.cs.broastedexpress.Activities.RatingActivity;
import com.cs.broastedexpress.Activities.TrackOrder;
import com.cs.broastedexpress.Adapter.BannersAdapter;
import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.Home;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.Rating;
import com.cs.broastedexpress.Model.RatingDetails;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;
import com.github.demono.AutoScrollViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by CS on 10/8/2016.
 */
public class HomeFragment extends Fragment {
    private static final int ORDER_HISTORY_REQUEST = 1;
    private static final int FAVORITE_ORDER_REQUEST = 2;
    private static final int TRACK_ORDER_REQUEST = 3;
    View rootView;
    SharedPreferences.Editor orderPrefsEditor;
    SharedPreferences orderPrefs;
    String orderId;
    SharedPreferences languagePrefs;
    String language;
    String mLoginStatus;
    SharedPreferences userPrefs;
    int ratingFromService;
    RelativeLayout rate_layout;
    MaterialRatingBar ratingBar;
    TextView cancel;
    final ArrayList<RatingDetails> rateArray = new ArrayList<>();
    String userId;
    DataBaseHelper myDbHelper;
    ImageView promoImage, home_slice;
    public static String BannerImage;
    public static ArrayList<String> PromoBanners = new ArrayList<>();
    CircleIndicator defaultIndicator;
    AutoScrollViewPager defaultViewpager;
    BannersAdapter mAdapter;
    Boolean isActivityVisible = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        languagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        myDbHelper=new DataBaseHelper(getActivity());

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.home_fragment, container,
                    false);
        } else if (language.equalsIgnoreCase("Ar")) {
            rootView = inflater.inflate(R.layout.home_fragment_arabic, container,
                    false);
        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");
        userId = userPrefs.getString("userId", "");

        defaultViewpager = (AutoScrollViewPager) rootView.findViewById(R.id.viewPager);
        defaultIndicator = (CircleIndicator) rootView.findViewById(R.id.indicator);

        TextView txt = (TextView) rootView.findViewById(R.id.order);
        TextView mfav = (TextView) rootView.findViewById(R.id.homefav);
        TextView mtrack = (TextView) rootView.findViewById(R.id.hometrack);
        TextView mhistory = (TextView) rootView.findViewById(R.id.homehistory);
        TextView mpromotion = (TextView) rootView.findViewById(R.id.promotion);

        promoImage = (ImageView) rootView.findViewById(R.id.promo_image);
        home_slice = (ImageView) rootView.findViewById(R.id.home_slice);

        ratingBar = (MaterialRatingBar) rootView.findViewById(R.id.ratingBar1);
        cancel = (TextView) rootView.findViewById(R.id.skip_rating);
        rate_layout = (RelativeLayout) rootView.findViewById(R.id.rate_layout);
        rate_layout.setVisibility(View.GONE);

        ImageView morder_now = (ImageView) rootView.findViewById(R.id.order_now);
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myDbHelper.getTotalOrderQty()==0) {
                    ((Home) getActivity()).selectItem(1);
                }else {
                    Intent intent=new Intent(getActivity(),Menu.class);
                    startActivity(intent);
                }
            }
        });

        mfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent i = new Intent(getActivity(), FavoriteOrder.class);
                    startActivity(i);
                } else {
                    Intent intent = new Intent(getActivity(), Login.class);
                    startActivityForResult(intent, FAVORITE_ORDER_REQUEST);
                }
            }
        });

        mtrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent i = new Intent(getActivity(), TrackOrder.class);
                    i.putExtra("orderId", "-1");
                    startActivity(i);
                } else {
                    Intent intent = new Intent(getActivity(), Login.class);
                    startActivityForResult(intent, TRACK_ORDER_REQUEST);
                }
            }
        });

        mhistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent i = new Intent(getActivity(), History.class);
                    startActivity(i);
                } else {
                    Intent intent = new Intent(getActivity(), Login.class);
                    startActivityForResult(intent, ORDER_HISTORY_REQUEST);
                }
            }
        });

        mpromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getActivity(), InsideMenu.class);
                a.putExtra("catid", 9);
                startActivity(a);
            }
        });

        morder_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(getActivity(), InsideMenu.class);
                a.putExtra("catid", 7);
                startActivity(a);
            }
        });

        return rootView;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLoginStatus = userPrefs.getString("login_status", "");
        if (requestCode == ORDER_HISTORY_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), History.class);
            startActivity(intent);
        } else if (requestCode == FAVORITE_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), FavoriteOrder.class);
            startActivity(intent);
        } else if (requestCode == TRACK_ORDER_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(getActivity(), TrackOrder.class);
            intent.putExtra("orderId", "-1");
            startActivity(intent);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public class GetOrderTrackDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            try {
                PromoBanners.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }
        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
                } else {
                    if (result.equals("")) {
                    } else {
                        try {
                            JSONObject jo = new JSONObject(result);
                            int userRating = jo.getInt("UserRating");
                            String OrderStatus = jo.getString("OrderStatus");

                            JSONArray jsonArray = jo.getJSONArray("Banner");
                            for(int i =0; i<jsonArray.length(); i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                BannerImage = jsonObject.getString("BannerName");
//                            BannerImage = jsonObject.getString("BannerImage");
                                Boolean status = jsonObject.getBoolean("Status");
                                if(status) {
                                    PromoBanners.add(BannerImage);
                                }
                            }

                            if(PromoBanners!=null && PromoBanners.size()>0 && isActivityVisible) {
                                mAdapter = new BannersAdapter(getContext(), PromoBanners);
                                defaultViewpager.setAdapter(mAdapter);
                                defaultIndicator.setViewPager(defaultViewpager);
                                defaultViewpager.startAutoScroll();
                                defaultViewpager.setCycle(true);
                                promoImage.setVisibility(View.INVISIBLE);
                            }

                            if(userRating==0 && OrderStatus.equalsIgnoreCase("Delivered")) {
                                RatingDetails rd = new RatingDetails();
                                JSONArray jA = jo.getJSONArray("Ratings");
                                JSONObject obj = jA.getJSONObject(0);
                                rd.setGivenrating("5");
                                JSONArray ratingArray = obj.getJSONArray("5");
                                ArrayList<Rating> ratingsArrayList = new ArrayList<>();
                                for (int i = 0; i < ratingArray.length(); i++) {
                                    JSONObject object1 = ratingArray.getJSONObject(i);
                                    Rating rating = new Rating();
                                    rating.setRatingId(object1.getString("RatingId"));
                                    rating.setComment(object1.getString("Comment_En"));
                                    rating.setComment_ar(object1.getString("Comment_Ar"));
                                    rating.setRating(object1.getString("Rating"));
                                    ratingsArrayList.add(rating);

                                }
                                rd.setArrayList(ratingsArrayList);
                                JSONArray ratingArray1 = obj.getJSONArray("4");
                                rd.setGivenrating("4");
                                for (int i = 0; i < ratingArray1.length(); i++) {
                                    JSONObject object1 = ratingArray1.getJSONObject(i);
                                    Rating rating = new Rating();
                                    rating.setRatingId(object1.getString("RatingId"));
                                    rating.setComment(object1.getString("Comment_En"));
                                    rating.setComment_ar(object1.getString("Comment_Ar"));
                                    rating.setRating(object1.getString("Rating"));
                                    ratingsArrayList.add(rating);
                                }
                                rd.setArrayList(ratingsArrayList);
                                JSONArray ratingArray2 = obj.getJSONArray("3");
                                rd.setGivenrating("3");
                                for (int i = 0; i < ratingArray2.length(); i++) {
                                    JSONObject object1 = ratingArray2.getJSONObject(i);
                                    Rating rating = new Rating();
                                    rating.setRatingId(object1.getString("RatingId"));
                                    rating.setComment(object1.getString("Comment_En"));
                                    rating.setComment_ar(object1.getString("Comment_Ar"));
                                    rating.setRating(object1.getString("Rating"));
                                    ratingsArrayList.add(rating);
                                }
                                rd.setArrayList(ratingsArrayList);

                                JSONArray ratingArray3 = obj.getJSONArray("2");
                                rd.setGivenrating("2");
                                for (int i = 0; i < ratingArray3.length(); i++) {
                                    JSONObject object1 = ratingArray3.getJSONObject(i);
                                    Rating rating = new Rating();
                                    rating.setRatingId(object1.getString("RatingId"));
                                    rating.setComment(object1.getString("Comment_En"));
                                    rating.setComment_ar(object1.getString("Comment_Ar"));
                                    rating.setRating(object1.getString("Rating"));
                                    ratingsArrayList.add(rating);
                                }
                                rd.setArrayList(ratingsArrayList);
                                JSONArray ratingArray4 = obj.getJSONArray("1");
                                rd.setGivenrating("1");
                                for (int i = 0; i < ratingArray4.length(); i++) {
                                    JSONObject object1 = ratingArray4.getJSONObject(i);
                                    Rating rating = new Rating();
                                    rating.setRatingId(object1.getString("RatingId"));
                                    rating.setComment(object1.getString("Comment_En"));
                                    rating.setComment_ar(object1.getString("Comment_Ar"));
                                    rating.setRating(object1.getString("Rating"));
                                    ratingsArrayList.add(rating);
                                }
                                rd.setArrayList(ratingsArrayList);

                                rateArray.add(rd);

                                rate_layout.setVisibility(View.VISIBLE);
                                cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        rate_layout.setVisibility(View.GONE);
                                    }
                                });

                                ratingBar.setRating(0);
                                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                    @Override
                                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                        if (fromUser) {
                                            Intent intent = new Intent(getActivity(), RatingActivity.class);
                                            intent.putExtra("rating", rating);
                                            intent.putExtra("array", rateArray);
                                            intent.putExtra("userid", userId);
                                            intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                            startActivity(intent);
                                            rate_layout.setVisibility(View.GONE);
                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
            }
            super.onPostExecute(result);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isActivityVisible = true;
        if(PromoBanners!=null && PromoBanners.size()>0 && isActivityVisible) {
            mAdapter = new BannersAdapter(getContext(), PromoBanners);
            defaultViewpager.setAdapter(mAdapter);
            defaultIndicator.setViewPager(defaultViewpager);
            defaultViewpager.startAutoScroll();
            defaultViewpager.setCycle(true);
            promoImage.setVisibility(View.INVISIBLE);
        }
        else{
            if (userPrefs.getString("userId", "no").equals("no")) {
                new GetOrderTrackDetails().execute(Projecturl.PROMO_URL + "0&oId=0");
            } else {
                new GetOrderTrackDetails().execute(Projecturl.PROMO_URL + userPrefs.getString("userId", "") + "&oId=-1");
            }
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        isActivityVisible = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isActivityVisible = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isActivityVisible = false;
    }
}