package com.cs.broastedexpress;

import com.cs.broastedexpress.Model.MenuItems;

import java.util.ArrayList;

/**
 * Created by CS on 10/20/2016.
 */

public class Projecturl {
//    static String local_url = "http://85.194.94.241/BXServices";
//
//    public static String REGISTRATION_URL = local_url + "/api/RegistrationApi/RegisterUser";
//    public static String LOGIN_URL = local_url + "/api/RegistrationApi/Signin/";
//    public static String CHANGE_PASSWORD_URL = local_url + "/api/RegistrationApi/ChangePassword/";
//    public static String VERIFY_RANDOM_NUMBER_URL = local_url + "/api/RegistrationApi/VerifyMobileNo?MobileNo=";
//    public static String FORGOT_PASSWORD_URL = local_url + "/api/RegistrationApi/ForgetPassword?Username=";
//    public static String SAVE_ADDRESS_URL = local_url + "/api/RegistrationApi/SaveUserAdderss/";
//    public static String SAVED_ADDRESS_URL = local_url + "/api/RegistrationApi/GetAddressDetails?userid=";
//    public static String DELETE_ADDRESS_URL = local_url + "/api/RegistrationApi/DeleteUserAddress?addressid=";
//    public static String EDIT_ADDRESS_URL = local_url + "/api/RegistrationApi/UpdateUserAddress";
//    public static String EDIT_USER_PROFILE_URL = local_url + "/api/RegistrationApi/UpdateUserProfile";
//    public static String CATEGORY_ITEMS_URL = local_url + "/api/OrderItems/GetSubCategory?Mid=";
//    public static String INSERT_ORDER_URL = local_url + "/api/OrderItems/InsertOrder";
//    public static String GET_FAVORITE_ORDERS_URL = local_url + "/api/OrderItems/FaviorateOrder?uId=";
//    public static String DELETE_FAVORITE_ORDERS_URL = local_url + "/api/OrderItems/DeleteFavOrder?oId=";
//    public static String ORDERD_DETAILS_URL = local_url + "/api/OrderItems/OrderDetails";
//    public static String INSERT_FAVORITE_ORDER_URL = local_url + "/api/OrderItems/IFavOrder?OId=";
//    public static String ORDER_HISTORY_URL = local_url + "/api/OrderItems/OrderHistory?uId=";
//    public static String TRACK_ORDER_STEPS_URL = local_url + "/api/OrderItems/TrackOrder?oId=";
//    public static String CANCEL_ORDER_URL = local_url + "/api/OrderItems/CancelOrder?oId=";
//    public static String LIVE_TRACKING_URL = local_url + "/api/OrderItems/OrderLocation?oId=";
//    public static String GET_CURRENT_TIME_URL = local_url + "/api/RegistrationApi/GetCurrentTime";
//    public static String GET_MESSAGES_URL = local_url + "/api/RegistrationApi/GetPushMSG?userid=";
//    public static String UPDATE_MESSAGE_URL = local_url + "/api/RegistrationApi/UpdatePushMsg";
//    public static String STORES_URL = local_url + "/api/RegistrationApi/GetStoreDetails?Day=";

    static String local_url = "http://www.alrajhiksa.com";
//    static String local_url = "http://csadms.com/BXServices";

    public static String REGISTRATION_URL = local_url + "/api/RegistrationApi/RegisterUser";
    public static String LOGIN_URL = local_url + "/api/RegistrationApi/Signin/";
    public static String CHANGE_PASSWORD_URL = local_url + "/api/RegistrationApi/ChangePassword/";
    public static String VERIFY_RANDOM_NUMBER_URL = local_url + "/api/RegistrationApi/VerifyMobileNo?MobileNo=";
    public static String FORGOT_PASSWORD_URL = local_url + "/api/RegistrationApi/ForgetPassword?Username=";
    public static String SEND_OTP = local_url + "/api/RegistrationApi/SendOTP?userid=";
    public static String SAVE_ADDRESS_URL = local_url + "/api/RegistrationApi/SaveUserAdderss/";
    public static String SAVED_ADDRESS_URL = local_url + "/api/RegistrationApi/GetAddressDetails?userid=";
    public static String DELETE_ADDRESS_URL = local_url + "/api/RegistrationApi/DeleteUserAddress?addressid=";
    public static String EDIT_ADDRESS_URL = local_url + "/api/RegistrationApi/UpdateUserAddress";
    public static String EDIT_USER_PROFILE_URL = local_url + "/api/RegistrationApi/UpdateUserProfile";
    public static String CATEGORY_ITEMS_URL = local_url + "/api/OrderItems/GetSubCategory?Mid=";
    public static String GET_ALL_ITEMS = local_url + "/api/OrderItems/GetAllItems";
    public static String ADDITIONALS_URL = local_url+"/api/OrderItems/GetAdditions?additionalId=";
    public static String INSERT_ORDER_URL = local_url + "/api/OrderItems/InsertOrderVAT";
    public static String GET_FAVORITE_ORDERS_URL = local_url + "/api/OrderItems/FaviorateOrder?uId=";
    public static String DELETE_FAVORITE_ORDERS_URL = local_url + "/api/OrderItems/DeleteFavOrder?oId=";
    public static String ORDERD_DETAILS_URL = local_url + "/api/OrderItems/OrderDetails";
    public static String INSERT_FAVORITE_ORDER_URL = local_url + "/api/OrderItems/IFavOrder?OId=";
    public static String ORDER_HISTORY_URL = local_url + "/api/OrderItems/OrderHistory?uId=";
    public static String TRACK_ORDER_STEPS_URL = local_url + "/api/OrderItems/TrackOrder?oId=";
    public static String CANCEL_ORDER_URL = local_url + "/api/OrderItems/CancelOrder?oId=";
    public static String LIVE_TRACKING_URL = local_url + "/api/OrderItems/OrderLocation?oId=";
    public static String GET_CURRENT_TIME_URL = local_url + "/api/RegistrationApi/GetCurrentTime";
    public static String GET_MESSAGES_URL = local_url + "/api/RegistrationApi/GetPushMSG?userid=";
    public static String UPDATE_MESSAGE_URL = local_url + "/api/RegistrationApi/UpdatePushMsg";
    public static String STORES_URL = local_url + "/api/RegistrationApi/GetStoreDetails?Day=";
    public static String GET_PROMOS_URL = local_url + "/api/BXPromotions/GetPromotions?userId=";
    public static String RATING_URL = local_url + "/api/OrderItems/PostInsertRating";
    public static String DELETE_ORDER = local_url + "/api/OrderItems/DeleteOrder?oId=";

    public static String PROMO_URL = local_url + "/api/OrderItems/OrderDetailsRatings?uId=";

    public static String IMAGE_URL = "http://www.alrajhiksa.com/images/";

    public static String ORDER_TYPE = "";
    public static String COMMENTS = "";
    public static int messageCount=0;
    public static ArrayList<MenuItems> MenuItems = new ArrayList<>();

    public static String convertToArabic(String value)
    {
        String newValue = (value
                .replaceAll("١","1" ).replaceAll("٢","2" )
                .replaceAll("٣","3" ).replaceAll("٤","4" )
                .replaceAll("٥","5" ).replaceAll("٦","6" )
                .replaceAll("٧","7" ).replaceAll("٨","8")
                .replaceAll("٩","9" ).replaceAll("٠","0" )
                .replaceAll("٫","."));
        return newValue;
    }

}
