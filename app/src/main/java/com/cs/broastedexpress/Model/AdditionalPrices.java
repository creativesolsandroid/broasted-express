package com.cs.broastedexpress.Model;

/**
 * Created by CS on 5/8/2018.
 */

public class AdditionalPrices {

    String itemType, price;

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
