package com.cs.broastedexpress.Model;

/**
 * Created by CS on 11/10/2016.
 */

public class Orders {
    String orderId, itemId, itemTypeId, subCatId, additionals, qty, price, additionalsPrice, additionalsTypeId, totalAmount, comment, status, creationDate, modifiedDate, categoryId, itemName, itemNameAr, image, nonDelivery, additionalName, additionalNameAr, additionalimage;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(String itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getAdditionals() {
        return additionals;
    }

    public void setAdditionals(String additionals) {
        this.additionals = additionals;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdditionalsPrice() {
        return additionalsPrice;
    }

    public void setAdditionalsPrice(String additionalsPrice) {
        this.additionalsPrice = additionalsPrice;
    }

    public String getAdditionalsTypeId() {
        return additionalsTypeId;
    }

    public void setAdditionalsTypeId(String additionalsTypeId) {
        this.additionalsTypeId = additionalsTypeId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNameAr() {
        return itemNameAr;
    }

    public void setItemNameAr(String itemNameAr) {
        this.itemNameAr = itemNameAr;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNonDelivery() {
        return nonDelivery;
    }

    public void setNonDelivery(String nonDelivery) {
        this.nonDelivery = nonDelivery;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public String getAdditionalNameAr() {
        return additionalNameAr;
    }

    public void setAdditionalNameAr(String additionalNameAr) {
        this.additionalNameAr = additionalNameAr;
    }

    public String getAdditionalimage() {
        return additionalimage;
    }

    public void setAdditionalimage(String additionalimage) {
        this.additionalimage = additionalimage;
    }
}
