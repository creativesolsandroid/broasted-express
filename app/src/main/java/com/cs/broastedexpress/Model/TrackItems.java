package com.cs.broastedexpress.Model;

import java.io.Serializable;

/**
 * Created by cs android on 14-02-2017.
 */

public class TrackItems implements Serializable {

    String item_name;
    String price;
    String item_name_ar;
    String ItemQty;

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getItem_name_ar() {
        return item_name_ar;
    }

    public void setItem_name_ar(String item_name_ar) {
        this.item_name_ar = item_name_ar;
    }

    public String getItemQty() {
        return ItemQty;
    }

    public void setItemQty(String itemQty) {
        ItemQty = itemQty;
    }
}