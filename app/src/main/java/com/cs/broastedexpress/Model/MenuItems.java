package com.cs.broastedexpress.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by CS on 09-05-2017.
 */

public class MenuItems implements Serializable {

    String CategoryId,CategoryName,CatDescription,CatDescription_Ar;
    ArrayList<InsideMenuCategories> menuCategories;

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getCatDescription() {
        return CatDescription;
    }

    public void setCatDescription(String catDescription) {
        CatDescription = catDescription;
    }

    public String getCatDescription_Ar() {
        return CatDescription_Ar;
    }

    public void setCatDescription_Ar(String catDescription_Ar) {
        CatDescription_Ar = catDescription_Ar;
    }

    public ArrayList<InsideMenuCategories> getMenuCategories() {
        return menuCategories;
    }

    public void setMenuCategories(ArrayList<InsideMenuCategories> menuCategories) {
        this.menuCategories = menuCategories;
    }
}
