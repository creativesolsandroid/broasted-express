package com.cs.broastedexpress.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by CS on 11/9/2016.
 */

public class InsideMenuCategories implements Serializable{
    String catId, itemId, itemName, modifierId, itemName_Ar, description, description_Ar, additionId, image, isDelivery;

    ArrayList<InsideMenuValues> insideMenuchild;

    public ArrayList<InsideMenuValues> getInsideMenuchild() {
        return insideMenuchild;
    }

    public void setInsideMenuchild(ArrayList<InsideMenuValues> insideMenuchild) {
        this.insideMenuchild = insideMenuchild;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public String getItemName_Ar() {
        return itemName_Ar;
    }

    public void setItemName_Ar(String itemName_Ar) {
        this.itemName_Ar = itemName_Ar;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription_Ar() {
        return description_Ar;
    }

    public void setDescription_Ar(String description_Ar) {
        this.description_Ar = description_Ar;
    }

    public String getAdditionId() {
        return additionId;
    }

    public void setAdditionId(String additionId) {
        this.additionId = additionId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(String isDelivery) {
        this.isDelivery = isDelivery;
    }
}
