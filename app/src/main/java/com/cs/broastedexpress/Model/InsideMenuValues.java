package com.cs.broastedexpress.Model;

/**
 * Created by CS on 11/9/2016.
 */

public class InsideMenuValues {
    String prices;
    int size;

    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.prices = prices;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
