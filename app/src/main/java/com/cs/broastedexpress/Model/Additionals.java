package com.cs.broastedexpress.Model;

import java.util.ArrayList;

/**
 * Created by CS on 5/8/2018.
 */

public class Additionals {
    String additionalsId, modifierId, additionalName, additionalPrice, additionalNameAr, images;
    ArrayList<AdditionalPrices> additionalPriceList;

    public ArrayList<AdditionalPrices> getAdditionalPriceList() {
        return additionalPriceList;
    }

    public void setAdditionalPriceList(ArrayList<AdditionalPrices> additionalPriceList) {
        this.additionalPriceList = additionalPriceList;
    }

    public String getAdditionalsId() {
        return additionalsId;
    }

    public void setAdditionalsId(String additionalsId) {
        this.additionalsId = additionalsId;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public String getAdditionalPrice() {
        return additionalPrice;
    }

    public void setAdditionalPrice(String additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public String getAdditionalNameAr() {
        return additionalNameAr;
    }

    public void setAdditionalNameAr(String additionalNameAr) {
        this.additionalNameAr = additionalNameAr;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
