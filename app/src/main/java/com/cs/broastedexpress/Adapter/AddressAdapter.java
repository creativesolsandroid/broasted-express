package com.cs.broastedexpress.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.broastedexpress.Model.Address;
import com.cs.broastedexpress.R;

import java.util.ArrayList;

/**
 * Created by CS on 10/24/2016.
 */

public class AddressAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Address> addressList = new ArrayList<>();
    int pos;
    String id;
    String language;

    public AddressAdapter(Context context, ArrayList<Address> addressList, String language) {
        this.context = context;
        this.addressList = addressList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
    }

    @Override
    public int getCount() {
        return addressList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView flatNo, landmark, address, addressName;
        ImageView addressType;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.content_myaddress, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.content_myaddress_arabic, null);
            }


            holder.flatNo = (TextView) convertView
                    .findViewById(R.id.flat_no);
//            holder.storeName = (TextView) convertView
//                    .findViewById(R.id.store_name);
            holder.landmark = (TextView) convertView
                    .findViewById(R.id.landmark);
            holder.address = (TextView) convertView
                    .findViewById(R.id.address);
            holder.addressName = (TextView) convertView.findViewById(R.id.address_name);
            holder.addressType = (ImageView) convertView.findViewById(R.id.address_type);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("En")) {
            if (addressList.get(position).getHouseNo() == null) {
                holder.flatNo.setText("Flat No : ....");
            } else {
                holder.flatNo.setText("Flat No : " + addressList.get(position).getHouseNo());
            }
            holder.landmark.setText("LandMark : " + addressList.get(position).getLandmark());
            holder.address.setText("" + addressList.get(position).getAddress());
            holder.addressName.setText("" + addressList.get(position).getAddressName());
        } else if (language.equalsIgnoreCase("Ar")) {

            if (addressList.get(position).getHouseNo() == null) {
                holder.flatNo.setText("شقة رقم : ...");
            } else {
                holder.flatNo.setText("شقة رقم : " + addressList.get(position).getHouseNo());
            }
            holder.landmark.setText("معلم أو مكان معروف : " + addressList.get(position).getLandmark());
            holder.address.setText("" + addressList.get(position).getAddress());
            holder.addressName.setText("" + addressList.get(position).getAddressName());
        }

        if (addressList.get(position).getAddressType().equalsIgnoreCase("1")) {
            holder.addressType.setImageResource(R.drawable.home1);
        } else if (addressList.get(position).getAddressType().equalsIgnoreCase("2")) {
            holder.addressType.setImageResource(R.drawable.work1);
        } else {
            holder.addressType.setImageResource(R.drawable.other1);
        }

        return convertView;
    }
}

