package com.cs.broastedexpress.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.broastedexpress.Activities.Checkout;
import com.cs.broastedexpress.Activities.Comment;
import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.Model.Orders;
import com.cs.broastedexpress.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 11/14/2016.
 */

public class CheckoutAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Orders> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    int qty;
    float price;
    float vat=5;
    String language;

    DecimalFormat dec =new DecimalFormat("0.00");

    public CheckoutAdapter(Context context, ArrayList<Orders> orderList, String language) {

        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

        myDbHelper = new DataBaseHelper(context);


    }

    @Override
    public int getCount() {
        return orderList.size();
    }


    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class VeiwHolder {
        ImageView mcheck_plus_icon, mcheck_sub_icon, mboasted_img, mComment, non_delivery;
        TextView mcheck_quantity, mcheck_total_amount, mcheck_out_text, mcheck_out_price;
//        int count,price=15,final_price=0;
//        String textcount,final_prices;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final VeiwHolder holder;
        if (convertView == null) {
            holder = new VeiwHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.activity_checkout_row, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.activity_checkout_row_arabic, null);
            }

            holder.mcheck_plus_icon = (ImageView) convertView.findViewById(R.id.check_out_plus);
            holder.mcheck_sub_icon = (ImageView) convertView.findViewById(R.id.check_out_sub);
            holder.mboasted_img = (ImageView) convertView.findViewById(R.id.boasted_img);
            holder.mComment = (ImageView) convertView.findViewById(R.id.comment);
            holder.non_delivery = (ImageView) convertView.findViewById(R.id.non_delivery);

            holder.mcheck_quantity = (TextView) convertView.findViewById(R.id.check_out_quantity);
            holder.mcheck_total_amount = (TextView) convertView.findViewById(R.id.check_out_total_amount);
            holder.mcheck_out_price = (TextView) convertView.findViewById(R.id.check_out_price);
            holder.mcheck_out_text = (TextView) convertView.findViewById(R.id.check_out_text);

            convertView.setTag(holder);
        } else {
            holder = (VeiwHolder) convertView.getTag();
        }


        holder.mComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderList = myDbHelper.getOrderInfo();
                Intent intent = new Intent(context, Comment.class);
                if (language.equalsIgnoreCase("En")) {
                    intent.putExtra("title", orderList.get(position).getItemName());
                } else if (language.equalsIgnoreCase("Ar")) {
                    intent.putExtra("title", orderList.get(position).getItemNameAr());
                }
                intent.putExtra("itemImage", orderList.get(position).getImage());
                intent.putExtra("itemId", orderList.get(position).getItemId());
                intent.putExtra("itemtype", orderList.get(position).getItemTypeId());
                intent.putExtra("orderId", orderList.get(position).getOrderId());
                intent.putExtra("comment", orderList.get(position).getComment());
                intent.putExtra("screen", "food");

                context.startActivity(intent);
            }
        });

        if(orderList.get(position).getNonDelivery().equals("true")){
            holder.non_delivery.setVisibility(View.INVISIBLE);
        }
        else{
            holder.non_delivery.setVisibility(View.VISIBLE);
        }

        if (language.equalsIgnoreCase("En")) {
            if (orderList.get(position).getItemTypeId().equals("1")) {
                holder.mcheck_out_text.setText(orderList.get(position).getItemName() + " (Regular)");
            } else if (orderList.get(position).getItemTypeId().equals("2")) {
                holder.mcheck_out_text.setText(orderList.get(position).getItemName() + " (Spice)");
            }
        } else if (language.equalsIgnoreCase("Ar")) {
            if (orderList.get(position).getItemTypeId().equals("1")) {
                holder.mcheck_out_text.setText(orderList.get(position).getItemNameAr() + " (عادي)");
            } else if (orderList.get(position).getItemTypeId().equals("2")) {
                holder.mcheck_out_text.setText(orderList.get(position).getItemNameAr() + " (حار )");
            }
        }

        holder.mcheck_quantity.setText(orderList.get(position).getQty());
        float priceTxt = Float.parseFloat(orderList.get(position).getPrice()) * Integer.parseInt(orderList.get(position).getQty());
        if (language.equalsIgnoreCase("En")) {
            holder.mcheck_out_price.setText("" + dec.format(Float.parseFloat(orderList.get(position).getPrice())));
            holder.mcheck_total_amount.setText("" + dec.format(priceTxt));
        }
        else if (language.equalsIgnoreCase("Ar")) {
            holder.mcheck_out_price.setText("" + dec.format(Float.parseFloat(orderList.get(position).getPrice())));
            holder.mcheck_total_amount.setText("" + dec.format(priceTxt));
        }
        Picasso.with(context).load("http://www.alrajhiksa.com/images/" + orderList.get(position).getImage()).into(holder.mboasted_img);


//        try {
//            // get input stream
//            InputStream ims = context.getAssets().open(orderList.get(position).getImage());
//            // load image as Drawable
//            Drawable d = Drawable.createFromStream(ims, null);
//            // set image to ImageView
//            holder.mboasted_img.setImageDrawable(d);
//        }
//        catch(IOException ex) {
//            holder.mboasted_img.setImageDrawable(context.getResources().getDrawable(R.drawable.inside_menu_boasted));
//        }

//        holder.mcheck_plus_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                holder.final_price= (int) (holder.final_price + price);
//                holder.mcheck_total_amount.setText(holder.final_prices + "SR");
//                holder.final_prices = Integer.toString(holder.final_price);
//                holder.count++;
//                holder.textcount=Integer.toString(holder.count);
//                holder.mcheck_quantity.setText(holder.textcount);
//            }
//        });
//
//
//        holder.mcheck_sub_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (holder.count==0){
////                    final_price=0;
////                    price=0;
//                }
//                else {
//                    holder.final_price= (int) (holder.final_price-price);
//                    holder.final_prices=Integer.toString(holder.final_price);
//                    holder.mcheck_total_amount.setText(holder.final_prices+"SR");
//                    holder.count--;
//                    holder.textcount = Integer.toString(holder.count);
//                    holder.mcheck_quantity.setText(holder.textcount);
//                }
//            }
//        });
        holder.mcheck_plus_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(orderList.get(position).getCategoryId().equals("4")){
//                    qty = Integer.parseInt(orderList.get(position).getQty()) + 5;
//                    price = (Float.parseFloat(orderList.get(position).getPrice()) * 5) + (Float.parseFloat(orderList.get(position).getTotalAmount()));
//                }
//            else {
                qty = Integer.parseInt(orderList.get(position).getQty()) + 1;
                price = (Float.parseFloat(orderList.get(position).getPrice()) + (Float.parseFloat(orderList.get(position).getTotalAmount())));
//                }
                myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getItemId(), orderList.get(position).getItemTypeId());
                orderList = myDbHelper.getOrderInfo();
                notifyDataSetChanged();
                try {

//                    InsideMenu.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    if (language.equalsIgnoreCase("En")) {
//                        InsideMenu.morder_price.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                        InsideMenu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        InsideMenu.morder_price.setText("" + myDbHelper.getTotalOrderPrice() + " ريال");
//                        InsideMenu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
//                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                try {

//                    Menu.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    if (language.equalsIgnoreCase("En")) {
//                        Menu.morder_price.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                        Menu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        Menu.morder_price.setText("" + myDbHelper.getTotalOrderPrice() + " ريال");
//                        Menu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
//                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                try {


                    DecimalFormat decim = new DecimalFormat("0.00");
                    Checkout.amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
                    float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                    Checkout.vatAmount.setText(""+decim.format(tax));
                    Checkout.netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));
                    Checkout.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    if (language.equalsIgnoreCase("En")) {
                        Checkout.morder_price.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
                        Checkout.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
                    } else if (language.equalsIgnoreCase("Ar")) {
                        Checkout.morder_price.setText("" + decim.format(myDbHelper.getTotalOrderPrice()+tax));
                        Checkout.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


            }
        });


        holder.mcheck_sub_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(orderList.get(position).getCategoryId().equals("4")){
//                    qty = Integer.parseInt(orderList.get(position).getQty()) - 5;
//                }else {
                qty = Integer.parseInt(orderList.get(position).getQty()) - 1;
//                }
                if (qty == 0) {
                    myDbHelper.deleteItemFromOrder(orderList.get(position).getItemId(), orderList.get(position).getItemTypeId());


                } else {
//                    if(orderList.get(position).getCategoryId().equals("4")){
//                        price = (Float.parseFloat(orderList.get(position).getTotalAmount())) - (Float.parseFloat(orderList.get(position).getPrice()) * 5);
//                    }else {
                    price = (Float.parseFloat(orderList.get(position).getTotalAmount())) - Float.parseFloat(orderList.get(position).getPrice());
//                    }
                    myDbHelper.updateOrder(String.valueOf(qty), String.valueOf(price), orderList.get(position).getItemId(), orderList.get(position).getItemTypeId());
                }


                orderList = myDbHelper.getOrderInfo();

//                try {
//
//                    InsideMenu.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    if (language.equalsIgnoreCase("En")) {
//                        InsideMenu.morder_price.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                        InsideMenu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        InsideMenu.morder_price.setText("" + myDbHelper.getTotalOrderPrice() + " ريال");
//                        InsideMenu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
//                    }
//                } catch (NullPointerException e) {
//                    e.printStackTrace();
//                }

//                try {
//
//                    Menu.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
//                    if (language.equalsIgnoreCase("En")) {
//                        Menu.morder_price.setText("" + myDbHelper.getTotalOrderPrice() + " SR");
//                        Menu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
//                    } else if (language.equalsIgnoreCase("Ar")) {
//                        Menu.morder_price.setText("" + myDbHelper.getTotalOrderPrice() + " ريال");
//                        Menu.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
//                    }
//                } catch (NullPointerException e) {
//                    e.printStackTrace();
//                }

                try {

                    DecimalFormat decim = new DecimalFormat("0.00");
                    Checkout.amount.setText(""+decim.format(myDbHelper.getTotalOrderPrice()));
                    float tax = myDbHelper.getTotalOrderPrice()*(vat/100);
                    Checkout.vatAmount.setText(""+decim.format(tax));
                    Checkout.netTotal.setText(""+decim.format(myDbHelper.getTotalOrderPrice()+tax));
                    Checkout.mcount_basket.setText("" + myDbHelper.getTotalOrderQty());
                    if (language.equalsIgnoreCase("En")) {
                        Checkout.morder_price.setText("" +decim.format( myDbHelper.getTotalOrderPrice()+tax));
                        Checkout.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
                    } else if (language.equalsIgnoreCase("Ar")) {
                        Checkout.morder_price.setText("" + decim.format( myDbHelper.getTotalOrderPrice()+tax));
                        Checkout.morder_quantity.setText("" + myDbHelper.getTotalOrderQty());
                    }
                    notifyDataSetChanged();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (orderList.size() == 0) {
//                    Intent a = new Intent(context, Menu.class);
//                    context.startActivity(a);
                    ((Activity) context).finish();
                }
                notifyDataSetChanged();
            }
        });


        return convertView;
    }
}
