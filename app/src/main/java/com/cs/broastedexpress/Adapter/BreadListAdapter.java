package com.cs.broastedexpress.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.broastedexpress.Model.Modifiers;
import com.cs.broastedexpress.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.broastedexpress.Adapter.InsideMenuAdapter.breadpos;
import static com.cs.broastedexpress.Adapter.InsideMenuAdapter.itemQty1;
import static com.cs.broastedexpress.Adapter.InsideMenuAdapter.itemfinalQty;
import static com.cs.broastedexpress.Adapter.InsideMenuAdapter.itemfinalQty1;
import static com.cs.broastedexpress.Adapter.InsideMenuAdapter.itemfinalprice;
import static com.cs.broastedexpress.Adapter.InsideMenuAdapter.itemfinalprice1;
import static com.cs.broastedexpress.Adapter.InsideMenuAdapter.itemprice;
import static com.cs.broastedexpress.Adapter.InsideMenuAdapter.orderprice;
import static com.cs.broastedexpress.Adapter.InsideMenuAdapter.qty;

/**
 * Created by CS on 4/30/2018.
 */

public class BreadListAdapter extends BaseAdapter {

    Context context;
    public LayoutInflater inflater;
    int pos;
    String id;
    String language;
    int itemLayout;
    Adapter parentActivity;
    ArrayList<Modifiers> breadlist = new ArrayList<>();
    public static int Qty1 = 0;
    public static float orderPrice1 = 0;

    public BreadListAdapter(Context context, ArrayList<Modifiers> breadlist, String language, InsideMenuAdapter parentActivity) {
        this.context = context;
//        this.addressList = addressList
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.parentActivity = parentActivity;
        this.breadlist = breadlist;

    }

    @Override
    public int getCount() {
        return breadlist.get(0).getChildItems().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        TextView Additional_name, Additional_price, alert_quantity;
        ImageView additional_img, alert_plue, alert_mins;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.additional_row, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.additional_row, null);
            }

            holder.Additional_name = (TextView) convertView.findViewById(R.id.sauce_additional_name);
            holder.Additional_price = (TextView) convertView.findViewById(R.id.sauce_additional_price);
            holder.alert_quantity = (TextView) convertView.findViewById(R.id.alert_quantity);
            holder.alert_plue = (ImageView) convertView.findViewById(R.id.alert_plus_icon);
            holder.alert_mins = (ImageView) convertView.findViewById(R.id.alert_sub_icon);

            holder.additional_img = (ImageView) convertView.findViewById(R.id.sauce_additional_img);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (breadlist.get(0).getModifierId().equals("2")) {
            holder.Additional_name.setText(breadlist.get(0).getChildItems().get(position).getAdditionalName());
            holder.Additional_price.setText(breadlist.get(0).getChildItems().get(position).getAdditionalPriceList().get(0).getPrice());
            Picasso.with(context).load("http://www.alrajhiksa.com/images/" + breadlist.get(0).getChildItems().get(position).getImages()).into(holder.additional_img);

            if(itemQty1!=null){
                int count = 0;
                for(int i=0; i<itemQty1.size(); i++){
                    if(itemQty1.get(i).equals(breadlist.get(0).getChildItems().get(position).getAdditionalName())){
                        count = count + 1;
                    }
                }
                if(count>0) {
                    String pos = Integer.toString(position);
                    if (!breadpos.contains(pos)) {
                        breadpos.add(pos);
                    }
                    holder.alert_quantity.setText("" + count);
//                holder.qty.setAlpha(1);
                }
                else{
                    String pos = Integer.toString(position);
                    if (breadpos.contains(pos)) {
                        breadpos.remove(pos);
                    }
                    holder.alert_quantity.setText("0");
//                holder.qty.setAlpha(0.5f);
                }
            }
            else{
                holder.alert_quantity.setText("0");
//            holder.qty.setAlpha(0.5f);
            }

            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            InsideMenuAdapter.malert_total_price.setText(decimalFormat.format((itemprice + orderprice)*qty));

            Log.i("TAG","additional price "+orderprice);

            Log.i("TAG","item price additional "+itemprice);

            holder.alert_plue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (itemfinalQty1.size() == 0) {

                        orderprice = 0;
                        Qty1 = 0;


                        Qty1 = Qty1 + 1;
                        orderprice = orderprice + Float.parseFloat(breadlist.get(0).getChildItems().get(position).getAdditionalPriceList().get(0).getPrice());

                        itemQty1.add(breadlist.get(0).getChildItems().get(position).getAdditionalName());
                        notifyDataSetChanged();

                        itemfinalQty1.add(String.valueOf(Qty1));
                        itemfinalprice1.add(String.valueOf(orderprice));

                        Log.i("TAG", "qty " + itemfinalQty);
                        Log.i("TAG", "price " + itemfinalprice);

                    } else {

                        Qty1 = Qty1 + 1;
                        orderprice = orderprice + Float.parseFloat(breadlist.get(0).getChildItems().get(position).getAdditionalPriceList().get(0).getPrice());

                        itemQty1.add(breadlist.get(0).getChildItems().get(position).getAdditionalName());
                        notifyDataSetChanged();

                        itemfinalQty1.add(String.valueOf(Qty1));
                        itemfinalprice1.add(String.valueOf(orderprice));
                    }
                }
            });

            holder.alert_mins.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (itemfinalQty1.size() == 0) {
                        if (Integer.parseInt(holder.alert_quantity.getText().toString()) > 0) {
                            orderprice = 0;
                            Qty1 = 0;

                            Qty1 = Qty1 - 1;
                            orderprice = orderprice - Float.parseFloat(breadlist.get(0).getChildItems().get(position).getAdditionalPriceList().get(0).getPrice());

                            itemQty1.remove(breadlist.get(0).getChildItems().get(position).getAdditionalName());
                            notifyDataSetChanged();

                            itemfinalQty1.add(String.valueOf(Qty1));
                            itemfinalprice1.add(String.valueOf(orderprice));

                        }
                    } else {

                        if (Integer.parseInt(holder.alert_quantity.getText().toString()) > 0) {

                            Qty1 = Qty1 - 1;
                            orderprice = orderprice - Float.parseFloat(breadlist.get(0).getChildItems().get(position).getAdditionalPriceList().get(0).getPrice());

                            itemQty1.remove(breadlist.get(0).getChildItems().get(position).getAdditionalName());
                            notifyDataSetChanged();

                            itemfinalQty1.add(String.valueOf(Qty1));
                            itemfinalprice1.add(String.valueOf(orderprice));
                        }

                    }

                }
            });

        }

        return convertView;
    }
}

//public class BreadListAdapter extends RecyclerView.Adapter<BreadListAdapter.ViewHolder> {
//
//    Context context;
//    public LayoutInflater inflater;
//    int pos;
//    String id;
//    String language;
//    int itemLayout;
//    Adapter parentActivity;
//    ArrayList<Modifiers> breadlist = new ArrayList<>();
//
//    public BreadListAdapter(Context context, ArrayList<Modifiers> breadlist, String language, InsideMenuAdapter parentActivity) {
//        this.context = context;
////        this.addressList = addressList
//        this.inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.language = language;
//        this.parentActivity = parentActivity;
//        this.breadlist = breadlist;
//
//    }
//
//    @Override
//    public BreadListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = inflater.inflate(R.layout.additional_row, parent, false);
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//
//        if (breadlist.get(0).getModifierId().equals("2")) {
//            holder.Additional_name.setText(breadlist.get(0).getChildItems().get(position).getAdditionalName());
//            holder.Additional_price.setText(breadlist.get(0).getChildItems().get(position).getAdditionalPriceList().get(0).getPrice());
//
//        }
//
//    }
//
//
//    @Override
//    public int getItemCount() {
//        return breadlist.get(0).getChildItems().size();
//    }
//
//    //    @Override
////    public int getCount() {
////        return 6;
////    }
////
////    @Override
////    public Object getItem(int position) {
////        return null;
////    }
////
////    @Override
////    public long getItemId(int position) {
////        return 0;
////    }
////
//    public static class ViewHolder extends RecyclerView.ViewHolder {
//
//        TextView Additional_name, Additional_price, alert_quantity;
//        ImageView additional_img;
//
//
//        ViewHolder(View itemView) {
//            super(itemView);
//
//            Additional_name = (TextView) itemView.findViewById(R.id.sauce_additional_name);
//            Additional_price = (TextView) itemView.findViewById(R.id.sauce_additional_price);
//            alert_quantity = (TextView) itemView.findViewById(R.id.alert_quantity);
//
//            additional_img = (ImageView) itemView.findViewById(R.id.sauce_additional_img);
//        }
//
//    }
////
////    @Override
////    public View getView(int position, View convertView, ViewGroup parent) {
////
////        final ViewHolder holder;
////        pos = position;
////        if (convertView == null) {
////            holder = new ViewHolder();
////            if (language.equalsIgnoreCase("En")) {
////                convertView = inflater.inflate(R.layout.additional_row, null);
////            } else if (language.equalsIgnoreCase("Ar")) {
////                convertView = inflater.inflate(R.layout.additional_row, null);
////            }
////
////
////            convertView.setTag(holder);
////        } else {
////            holder = (ViewHolder) convertView.getTag();
////        }
////        return convertView;
////    }
//}
