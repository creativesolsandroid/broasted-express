package com.cs.broastedexpress.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.broastedexpress.Model.OrderHistory;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 11/16/2016.
 */

public class OrderHistoryAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<OrderHistory> transactionsList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public OrderHistoryAdapter(Context context, ArrayList<OrderHistory> transactionsList, String language) {
        this.context = context;
        this.transactionsList = transactionsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
    }

    @Override
    public int getCount() {
        return transactionsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        // menu type count
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if(transactionsList.get(position).getOrderStatus().equals("Rejected") ||transactionsList.get(position).getOrderStatus().equals("Cancel") ||transactionsList.get(position).getOrderStatus().equals("Close")){
            type = 0;
        }
        else{
            type = 1;
        }
        // current menu type
        return type;
    }

    public static class ViewHolder {
        TextView orderNumber, orderDate, totalPrice, itemDetails, storeName;
        ImageView progress;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.order_history_list, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.order_history_list_arabic, null);
            }


            holder.orderNumber = (TextView) convertView
                    .findViewById(R.id.order_number);
            holder.orderDate = (TextView) convertView
                    .findViewById(R.id.order_date);
            holder.totalPrice = (TextView) convertView
                    .findViewById(R.id.favorite_price);
            holder.storeName = (TextView) convertView
                    .findViewById(R.id.store_name);

            holder.itemDetails = (TextView) convertView.findViewById(R.id.item_details);
            holder.progress = (ImageView) convertView.findViewById(R.id.order_step);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("En")) {
            holder.orderNumber.setText("Order No : " + transactionsList.get(position).getInvoiceNo());
        } else if (language.equalsIgnoreCase("Ar")) {
            holder.orderNumber.setText("رقم الطلب : " + transactionsList.get(position).getInvoiceNo());
        }
//        if (language.equalsIgnoreCase("En")) {
//            String total1 = transactionsList.get(position).getTotalPrice();
//            String total = String.valueOf(transactionsList.get(position).getTotalPrice().replace("?????","00.00"));
//            String total2 = transactionsList.get(1).getTotalPrice();
//            String total3 = transactionsList.get(2).getTotalPrice();
//            String total4 = transactionsList.get(3).getTotalPrice();
//            if (total2 == "?????") {
//                holder.totalPrice.setText("" +String.valueOf(transactionsList.get(1).getTotalPrice().replace("?????","00.00")));
//                Log.e("TAG","history"+String.valueOf(transactionsList.get(1).getTotalPrice().replace("?????","00.00")));
//            }else if (total3=="?????"){
//                holder.totalPrice.setText("" +String.valueOf(transactionsList.get(2).getTotalPrice().replace("?????","00.00")));
//            }else if (total4=="?????"){
//                holder.totalPrice.setText("" +String.valueOf(transactionsList.get(3).getTotalPrice().replace("?????","00.00")));
//            }else {
                holder.totalPrice.setText("" + decimalFormat.format(Float.parseFloat(Projecturl.convertToArabic(transactionsList.get(position).getTotalPrice()))));
//            }
//        } else if (language.equalsIgnoreCase("Ar")) {
//            holder.totalPrice.setText(decimalFormat.format(Float.parseFloat(Projecturl.convertToArabic(transactionsList.get(position).getTotalPrice()))));
//        }

        if (language.equalsIgnoreCase("En")) {
            holder.storeName.setText(transactionsList.get(position).getStoreName());
            holder.itemDetails.setText(transactionsList.get(position).getItemDetails());
        } else if (language.equalsIgnoreCase("Ar")) {
            holder.storeName.setText(transactionsList.get(position).getStoreName_ar());
            holder.itemDetails.setText(transactionsList.get(position).getItemDetails_ar());
        }

        Log.i("DATE TAG", "" + transactionsList.get(position).getOrderDate());
        SimpleDateFormat curFormater = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
        try {
            dateObj = curFormater.parse(transactionsList.get(position).getOrderDate());
        } catch (ParseException e) {
            e.printStackTrace();
            try {
                dateObj = curFormater1.parse(transactionsList.get(position).getOrderDate());
            } catch (ParseException pe) {

            }

        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy  hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.orderDate.setText(date);

        if (language.equalsIgnoreCase("En")) {
            if (!transactionsList.get(position).getOrderType().equalsIgnoreCase("Delivery")) {
                if (transactionsList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.order_history4);
                } else if (transactionsList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.order_history2);
                } else if (transactionsList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.order_history3);
                } else if (transactionsList.get(position).getOrderStatus().equals("Rejected") || transactionsList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.order_history5);
                } else {
                    holder.progress.setImageResource(R.drawable.order_history1);
                }
            } else {
                holder.storeName.setText(transactionsList.get(position).getUserAddress());
                if (transactionsList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery5);
                } else if (transactionsList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery2);
                } else if (transactionsList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery3);
                } else if (transactionsList.get(position).getOrderStatus().equals("OnTheWay")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery4);
                } else if (transactionsList.get(position).getOrderStatus().equals("Rejected") || transactionsList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.order_history5);
                } else {
                    holder.progress.setImageResource(R.drawable.order_history_delivery1);
                }
            }
        } else if (language.equalsIgnoreCase("Ar")) {
            if (!transactionsList.get(position).getOrderType().equalsIgnoreCase("Delivery")) {
                if (transactionsList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.order_history4_arabic);
                } else if (transactionsList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.order_history2_arabic);
                } else if (transactionsList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.order_history3_arabic);
                } else if (transactionsList.get(position).getOrderStatus().equals("Rejected") || transactionsList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.order_history5_arabic);
                } else {
                    holder.progress.setImageResource(R.drawable.order_history1_arabic);
                }
            } else {
                holder.storeName.setText(transactionsList.get(position).getUserAddress());
                if (transactionsList.get(position).getOrderStatus().equals("Close")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery5_arabic);
                } else if (transactionsList.get(position).getOrderStatus().equals("Pending")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery2_arabic);
                } else if (transactionsList.get(position).getOrderStatus().equals("Ready")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery3_arabic);
                } else if (transactionsList.get(position).getOrderStatus().equals("OnTheWay")) {
                    holder.progress.setImageResource(R.drawable.order_history_delivery4_arabic);
                } else if (transactionsList.get(position).getOrderStatus().equals("Rejected") || transactionsList.get(position).getOrderStatus().equals("Cancel")) {
                    holder.progress.setImageResource(R.drawable.order_history5_arabic);
                } else {
                    holder.progress.setImageResource(R.drawable.order_history_delivery1_arabic);
                }
            }
        }
        return convertView;
    }
}
