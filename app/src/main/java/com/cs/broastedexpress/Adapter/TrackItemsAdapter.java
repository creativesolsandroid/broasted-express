package com.cs.broastedexpress.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cs.broastedexpress.Model.TrackItems;
import com.cs.broastedexpress.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by cs android on 14-02-2017.
 */

public class TrackItemsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
//    ArrayList<TrackItems> orderList = new ArrayList<>();
    ArrayList<TrackItems> items = new ArrayList<>();
    int qty, price;
    String language, size;
    int pos=0;

    public TrackItemsAdapter(Context context, ArrayList<TrackItems> Track_itemtypes, String language) {
        this.context = context;
        this.items = Track_itemtypes;
        this.language = language;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }


    public int getCount() {
        return items.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView itemName, itemPrice, itemNo;
        LinearLayout additionalsLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.track_items_list, null);
            }
            else if(language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.track_items_list_arabic, null);
            }

            holder.itemName = (TextView) convertView.findViewById(R.id.item_name);
            holder.itemPrice = (TextView) convertView.findViewById(R.id.total_amount);
            holder.itemNo = (TextView) convertView.findViewById(R.id.qty);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
//            for(int i=0; i<items.size();i++)
        if(language.equalsIgnoreCase("En")) {
            holder.itemName.setText(items.get(position).getItem_name());
        }
        else if(language.equalsIgnoreCase("Ar")) {
            holder.itemName.setText(items.get(position).getItem_name_ar());
        }
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        holder.itemPrice.setText(""+decimalFormat.format(Float.parseFloat(items.get(position).getPrice())*Integer.parseInt(items.get(position).getItemQty())));
        holder.itemNo.setText(items.get(position).getItemQty());

        return convertView;
    }

}
