package com.cs.broastedexpress.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;

import java.util.ArrayList;

/**
 * Created by cs android on 23-02-2017.
 */

public class BannersAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ImageView Banner;
    Context context;

    ArrayList<String> bannerList=new ArrayList<String>();


    public BannersAdapter(Context context, ArrayList<String> bannerList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.offerBanner = false;
        this.context = context;
        this.bannerList = bannerList;
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.banners_list, container, false);

        Banner=(ImageView)itemView.findViewById(R.id.banner);

        Glide.with(context).load(Projecturl.IMAGE_URL+bannerList.get(position)).into(Banner);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
