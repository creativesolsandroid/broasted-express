package com.cs.broastedexpress.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.Activities.Comment;
import com.cs.broastedexpress.Activities.InsideMenu;
import com.cs.broastedexpress.DataBaseHelper;
import com.cs.broastedexpress.JSONParser;
import com.cs.broastedexpress.Model.AdditionalPrices;
import com.cs.broastedexpress.Model.Additionals;
import com.cs.broastedexpress.Model.InsideMenuCategories;
import com.cs.broastedexpress.Model.Modifiers;
import com.cs.broastedexpress.NetworkUtil;
import com.cs.broastedexpress.Projecturl;
import com.cs.broastedexpress.R;
import com.cs.broastedexpress.Widgets.CustomGridView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.cs.broastedexpress.Activities.InsideMenu.finalPrice;
import static com.cs.broastedexpress.R.id.inside_regular_count;

/**
 * Created by CS on 11/9/2016.
 */

public class InsideMenuAdapter extends android.widget.BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<InsideMenuCategories> insideMenuCategorieslist;
    ArrayList<Modifiers> SauceList = new ArrayList<>();
    ArrayList<Modifiers> Breadlist = new ArrayList<>();
    ArrayList<Modifiers> Drinklist = new ArrayList<>();
    int pos;
    float totalAmt, totalAmt1, price, price1;
    public static int qty = 1, qty1 = 1;
    String id, size;
    String language;
    int count, count1, progresscount;
    float orderPrice;
    DataBaseHelper mydatabase;
    SeekBar mseekbar, mseekbar1;
    AdditionallistAdpter madapter;
    BreadListAdapter mbreadadapter;
    DrinkListAdapter mdrinkadapter;
    CustomGridView msauce_gridview, mbread_gridview, mdrink_gridview;
    public static TextView malert_total_price;
    LinearLayout madditional_layout, msauce_layout, mbread_layout, mdrink_layout;

    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public static Float totalprice, itemprice;
    public static float orderprice = 0;

    public static ArrayList<String> saucepos = new ArrayList<>();
    public static ArrayList<String> itemQty = new ArrayList<>();
    public static ArrayList<String> itemfinalQty = new ArrayList<>();
    public static ArrayList<String> itemfinalprice = new ArrayList<>();

    public static ArrayList<String> breadpos = new ArrayList<>();
    public static ArrayList<String> itemQty1 = new ArrayList<>();
    public static ArrayList<String> itemfinalQty1 = new ArrayList<>();
    public static ArrayList<String> itemfinalprice1 = new ArrayList<>();

    public static ArrayList<String> drinkpos = new ArrayList<>();
    public static ArrayList<String> itemQty2 = new ArrayList<>();
    public static ArrayList<String> itemfinalQty2 = new ArrayList<>();
    public static ArrayList<String> itemfinalprice2 = new ArrayList<>();

    public InsideMenuAdapter(Context context, ArrayList<InsideMenuCategories> insideMenuCategorieslist, String language) {
        this.context = context;
        this.insideMenuCategorieslist = insideMenuCategorieslist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        mydatabase = new DataBaseHelper(context);
    }

    @Override
    public int getCount() {
        return insideMenuCategorieslist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolders {
        ImageView minside_image, non_delivery;
        LinearLayout mregular1, mspice;
        AlertDialog alertDialog, alertDialog1;
        TextView malert_quantity, malert_quantity1, minside_item_name, minside_description, minside_price_regular, minside_price_spices, malert_itemname, malert_itemdescription, malert_itemname1, malert_itemdescription1, minside_spice_count, minside_regular_count;
        LinearLayout madd_to_basket, madd_to_basket1;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolders holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolders();

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.inside_menu_child, null);
            } else if (language.equalsIgnoreCase("Ar")) {
                convertView = inflater.inflate(R.layout.inside_menu_child_arabic, null);
            }

            holder.minside_item_name = (TextView) convertView.findViewById(R.id.inside_item_name);
            holder.minside_description = (TextView) convertView.findViewById(R.id.inside_description);
            holder.minside_price_regular = (TextView) convertView.findViewById(R.id.inside_price_regular);
            holder.minside_price_spices = (TextView) convertView.findViewById(R.id.inside_price_spices);
            holder.minside_regular_count = (TextView) convertView.findViewById(inside_regular_count);
            holder.minside_spice_count = (TextView) convertView.findViewById(R.id.inside_spice_count);

            holder.mregular1 = (LinearLayout) convertView.findViewById(R.id.regular1);
            holder.mspice = (LinearLayout) convertView.findViewById(R.id.spice);

            holder.minside_image = (ImageView) convertView.findViewById(R.id.inside_image);
            holder.non_delivery = (ImageView) convertView.findViewById(R.id.non_delivery);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolders) convertView.getTag();
        }

        if (mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), "1") == 0) {
            holder.minside_regular_count.setVisibility(GONE);
            holder.mregular1.setBackground(context.getResources().getDrawable(R.drawable.circle_inside_menu_yellow1));
            notifyDataSetChanged();
        } else {
            holder.minside_regular_count.setText("" + mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), "1"));
            holder.minside_regular_count.setVisibility(VISIBLE);
            holder.mregular1.setBackground(context.getResources().getDrawable(R.drawable.circle_inside_menu_yellow));
            notifyDataSetChanged();
        }

        if (mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), "2") == 0) {
            holder.minside_spice_count.setVisibility(GONE);
            holder.mspice.setBackground(context.getResources().getDrawable(R.drawable.inside_menu_item_price));
            notifyDataSetChanged();
        } else {
            holder.minside_spice_count.setText("" + mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), "2"));
            holder.minside_spice_count.setVisibility(VISIBLE);
            holder.mspice.setBackground(context.getResources().getDrawable(R.drawable.inside_menu_item_price1));
            notifyDataSetChanged();
        }

        if (insideMenuCategorieslist.get(position).getIsDelivery().equals("true")) {
            holder.non_delivery.setVisibility(View.INVISIBLE);
        } else {
            holder.non_delivery.setVisibility(View.VISIBLE);
        }


        orderprice = 0;
        itemQty.clear();
        itemQty1.clear();
        itemQty2.clear();
        itemfinalQty.clear();
        itemfinalQty1.clear();
        itemfinalQty2.clear();
        itemfinalprice.clear();
        itemfinalprice1.clear();
        itemfinalprice2.clear();
        saucepos.clear();
        breadpos.clear();
        drinkpos.clear();
        holder.mregular1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("TAG", "price " + totalprice);

                Log.i("TAG", "Sauce size" + SauceList.size());
                Log.i("TAG", "Bread size" + Breadlist.size());
                Log.i("TAG", "Drink size" + Drinklist.size());
                Log.i("TAG", "Additionalid" + insideMenuCategorieslist.get(position).getAdditionId());
                qty = 1;

                Projecturl.COMMENTS = "";

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                // ...Irrelevant code for customizing the buttons and title
                final LayoutInflater inflater = ((AppCompatActivity) context).getLayoutInflater();

                if (language.equalsIgnoreCase("En")) {
                    View dialogView = inflater.inflate(R.layout.additional_alert_dialog, null);
                    dialogBuilder.setView(dialogView);


                    ImageView mdelete = (ImageView) dialogView.findViewById(R.id.delete);
                    ImageView mbroasted_express = (ImageView) dialogView.findViewById(R.id.broasted_express);
                    final ImageView malert_sub_icon = (ImageView) dialogView.findViewById(R.id.alert_sub_icon);
                    ImageView malert_plus_icon = (ImageView) dialogView.findViewById(R.id.alert_plus_icon);
                    ImageView mComment = (ImageView) dialogView.findViewById(R.id.comment);
                    msauce_gridview = (CustomGridView) dialogView.findViewById(R.id.grid_sauce);
                    mbread_gridview = (CustomGridView) dialogView.findViewById(R.id.grid_bread);
                    mdrink_gridview = (CustomGridView) dialogView.findViewById(R.id.grid_drink);
                    madditional_layout = (LinearLayout) dialogView.findViewById(R.id.additionals_layout);
                    msauce_layout = (LinearLayout) dialogView.findViewById(R.id.sauce_layout);
                    mbread_layout = (LinearLayout) dialogView.findViewById(R.id.bread_layout);
                    mdrink_layout = (LinearLayout) dialogView.findViewById(R.id.drink_layout);

                    holder.madd_to_basket = (LinearLayout) dialogView.findViewById(R.id.add_to_basket);
                    holder.malert_quantity = (TextView) dialogView.findViewById(R.id.alert_quantity);
//                final TextView mitem_count = (TextView) dialogView.findViewById(R.id.item_count);
                    holder.malert_itemname = (TextView) dialogView.findViewById(R.id.alert_itemname);
                    holder.malert_itemdescription = (TextView) dialogView.findViewById(R.id.alert_itemdescription);
                    malert_total_price = (TextView) dialogView.findViewById(R.id.total_amount);
                    RelativeLayout mainLayout = (RelativeLayout) dialogView.findViewById(R.id.mainlayout);

                    TextView malert_price = (TextView) dialogView.findViewById(R.id.alert_price);

                    if (insideMenuCategorieslist.get(position).getAdditionId().equals("null") || insideMenuCategorieslist.get(position).getAdditionId().equals("")) {

                        madditional_layout.setVisibility(GONE);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT, 450);
                        mainLayout.setLayoutParams(layoutParams);
                        for (int j = 0; j < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); j++) {
                            if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getSize() == 1) {
                                malert_total_price.setText("" + decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices())));
                            }
                        }
                    } else {
                        madditional_layout.setVisibility(VISIBLE);
                        new GetAdditionals().execute(Projecturl.ADDITIONALS_URL + insideMenuCategorieslist.get(position).getAdditionId());
                    }

                    Log.i("TAG", "S size" + SauceList.size());
                    Log.i("TAG", "B size" + Breadlist.size());
                    Log.i("TAG", "D size" + Drinklist.size());
                    Log.i("TAG", "Additionalid" + insideMenuCategorieslist.get(position).getAdditionId());

                    mbroasted_express.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });

                    Picasso.with(context).load("http://www.alrajhiksa.com/images/" + insideMenuCategorieslist.get(position).getImage()).into(mbroasted_express);

                    holder.malert_itemname.setText("" + insideMenuCategorieslist.get(position).getItemName());
                    holder.malert_itemdescription.setText("" + insideMenuCategorieslist.get(position).getDescription());
                    holder.malert_quantity.setText("1");

                    for (int j = 0; j < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); j++) {
                        if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getSize() == 1) {
                            malert_price.setText("" + decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices())));
                            itemprice = Float.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices());
                            Log.i("TAG", "item price " + itemprice);
                        }
                    }

                    mComment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, Comment.class);
                            if (language.equalsIgnoreCase("En")) {
                                intent.putExtra("title", insideMenuCategorieslist.get(position).getItemName());
                            } else if (language.equalsIgnoreCase("Ar")) {
                                intent.putExtra("title", insideMenuCategorieslist.get(position).getItemName_Ar());
                            }
                            intent.putExtra("itemImage", insideMenuCategorieslist.get(position).getImage());
                            intent.putExtra("itemId", insideMenuCategorieslist.get(position).getItemId());
                            intent.putExtra("itemtype", insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize());
                            intent.putExtra("comment", mydatabase.getComment(insideMenuCategorieslist.get(position).getItemId(),
                                    Integer.toString(1)));
                            intent.putExtra("screen", "item");
                            context.startActivity(intent);
                        }
                    });

                    malert_sub_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (qty == 1) {
//                                mydatabase.deleteItemFromOrder(insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));
//                                malert_sub_icon.setEnabled(false);
                            } else {
                                qty = qty - 1;
//                                mseekbar.setProgress(qty - 1);
                                holder.malert_quantity.setText(String.valueOf(qty));
                                malert_total_price.setText(decimalFormat.format((itemprice + orderprice) * qty));
                                Log.i("TAG", "item price sub " + itemprice);

//                                holder.minside_regular_count.setText(String.valueOf(qty));
//                                for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
//                                    if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 1) {
//                                        price = Integer.parseInt(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices()) * qty;
//                                    }
//                                }

//                                mydatabase.updateOrder(String.valueOf(qty), String.valueOf(price), insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));
//                                notifyDataSetChanged();
                            }

//                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//                            if (language.equalsIgnoreCase("En")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalorderprice() + " SR");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalorderprice() + " ريال");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            }
                            notifyDataSetChanged();

                        }
                    });

                    malert_plus_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            qty = qty + 1;
//                            mseekbar.setProgress(qty - 1);
                            holder.malert_quantity.setText(String.valueOf(qty));
                            malert_total_price.setText(decimalFormat.format((itemprice + orderprice) * qty));
                            Log.i("TAG", "item price plus " + itemprice);

//                            holder.minside_regular_count.setText(String.valueOf(qty));
//
//                            for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
//                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 1) {
//                                    price = Integer.parseInt(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices()) * qty;
//                                    notifyDataSetChanged();
//                                }
//                            }
//                            mydatabase.updateOrder(String.valueOf(qty), String.valueOf(price), insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));

//                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//                            if (language.equalsIgnoreCase("En")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            }
                            notifyDataSetChanged();
                        }
                    });

                    mdelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.alertDialog.dismiss();
                        }
                    });

//                    mseekbar = (SeekBar) dialogView.findViewById(R.id.seekbar);
//                    mseekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                        @Override
//                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                            progresscount = progress;
//                            if (progresscount == 0) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            } else if (progresscount == 1) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            } else if (progresscount == 2) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            } else if (progresscount == 3) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            } else if (progresscount == 4) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            }
//                        }
//
//                        @Override
//                        public void onStartTrackingTouch(SeekBar seekBar) {
//
//                        }
//
//                        @Override
//                        public void onStopTrackingTouch(SeekBar seekBar) {
//
//                        }
//                    });

                    holder.madd_to_basket.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.minside_regular_count.setText(String.valueOf(qty));

                            for (int i = 0; i < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); i++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getSize() == 1) {
                                    orderPrice = Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getPrices());

                                }
                            }

                            for (int k = 0; k < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); k++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(k).getSize() == 1) {
                                    totalAmt = (itemprice + orderprice) * qty;
                                    notifyDataSetChanged();
                                }
                            }
                            for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 1) {
                                    price = Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices());
                                    notifyDataSetChanged();
                                }
                            }
                            for (int m = 0; m < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); m++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(m).getSize() == 1) {
                                    count = qty;
                                    notifyDataSetChanged();
                                }
                            }

                            String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "0";
                            if (saucepos != null && saucepos.size() > 0) {
                                for (int i = 0; i < saucepos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getImages();
                                        addl_price = addl_price + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty.size(); j++) {
                                            if (itemQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalsId();
                                        addl_name = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName();
                                        addl_name_ar = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalNameAr();
                                        addl_image = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getImages();
                                        addl_price = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty.size(); j++) {
                                            if (itemQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }


                            if (breadpos != null && breadpos.size() > 0) {
                                for (int i = 0; i < breadpos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getImages();
                                        addl_price = addl_price + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty1.size(); j++) {
                                            if (itemQty1.get(j).equals(Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalsId();
                                        addl_name = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName();
                                        addl_name_ar = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalNameAr();
                                        addl_image = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getImages();
                                        addl_price = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty1.size(); j++) {
                                            if (itemQty1.get(j).equals(Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }

                            if (drinkpos != null && drinkpos.size() > 0) {
                                for (int i = 0; i < drinkpos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getImages();
                                        addl_price = addl_price + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty2.size(); j++) {
                                            if (itemQty2.get(j).equals(Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalsId();
                                        addl_name = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName();
                                        addl_name_ar = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalNameAr();
                                        addl_image = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getImages();
                                        addl_price = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty2.size(); j++) {
                                            if (itemQty2.get(j).equals(Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }

                            if (mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), Integer.toString(1)) == 0) {

                                HashMap<String, String> values = new HashMap<>();
                                values.put("itemId", insideMenuCategorieslist.get(position).getItemId());
                                values.put("itemTypeId", Integer.toString(1));
                                values.put("subCategoryId", "");
                                values.put("additionals", addl_ids);
                                values.put("qty", String.valueOf(count));
                                values.put("price", Float.toString(price));
                                values.put("additionalsPrice", addl_price);
                                values.put("additionalsTypeId", addl_qty);
                                values.put("totalAmount", Float.toString(totalAmt));
                                values.put("comment", Projecturl.COMMENTS);
                                values.put("status", "");
                                values.put("creationDate", "10/11/2016");
                                values.put("modifiedDate", "14/11/2016");
                                values.put("categoryId", insideMenuCategorieslist.get(position).getCatId());
                                values.put("itemName", insideMenuCategorieslist.get(position).getItemName());
                                values.put("itemNameAr", insideMenuCategorieslist.get(position).getItemName_Ar());
                                values.put("image", insideMenuCategorieslist.get(position).getImage());
                                values.put("nonDelivery", insideMenuCategorieslist.get(position).getIsDelivery());
                                values.put("AdditionalName", addl_name);
                                values.put("AdditionalNameAr", addl_name_ar);
                                values.put("Additionalimage", addl_image);
                                mydatabase.insertOrder(values);
//                                notifyDataSetChanged();
                            } else {
                                int cnt = mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), Integer.toString(1));
                                count = qty + cnt;
                                finalPrice = count * price;

                                mydatabase.updateOrderwithComment(String.valueOf(count), String.valueOf(finalPrice), insideMenuCategorieslist.get(position).getItemId(), Integer.toString(1), Projecturl.COMMENTS);
//                                notifyDataSetChanged();
                            }

                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
                            if (language.equalsIgnoreCase("En")) {
                                InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
                            } else if (language.equalsIgnoreCase("Ar")) {
                                InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
                            }
                            notifyDataSetChanged();

//                        alert(position, 1);
                            holder.alertDialog.dismiss();
                        }

                    });

                } else if (language.equalsIgnoreCase("Ar")) {


                    Projecturl.COMMENTS = "";
                    View dialogView1 = inflater.inflate(R.layout.additional_alert_dialog_arabic, null);
                    dialogBuilder.setView(dialogView1);

                    ImageView mdelete = (ImageView) dialogView1.findViewById(R.id.delete);
                    ImageView mbroasted_express = (ImageView) dialogView1.findViewById(R.id.broasted_express);
                    final ImageView malert_sub_icon = (ImageView) dialogView1.findViewById(R.id.alert_sub_icon);
                    ImageView malert_plus_icon = (ImageView) dialogView1.findViewById(R.id.alert_plus_icon);
                    ImageView mComment = (ImageView) dialogView1.findViewById(R.id.comment);

                    holder.madd_to_basket = (LinearLayout) dialogView1.findViewById(R.id.add_to_basket);
                    holder.malert_quantity = (TextView) dialogView1.findViewById(R.id.alert_quantity);
//                final TextView mitem_count = (TextView) dialogView.findViewById(R.id.item_count);
                    holder.malert_itemname = (TextView) dialogView1.findViewById(R.id.alert_itemname);
                    holder.malert_itemdescription = (TextView) dialogView1.findViewById(R.id.alert_itemdescription);

                    TextView malert_price = (TextView) dialogView1.findViewById(R.id.alert_price);

                    msauce_gridview = (CustomGridView) dialogView1.findViewById(R.id.grid_sauce);
                    mbread_gridview = (CustomGridView) dialogView1.findViewById(R.id.grid_bread);
                    mdrink_gridview = (CustomGridView) dialogView1.findViewById(R.id.grid_drink);
                    madditional_layout = (LinearLayout) dialogView1.findViewById(R.id.additionals_layout);
                    msauce_layout = (LinearLayout) dialogView1.findViewById(R.id.sauce_layout);
                    mbread_layout = (LinearLayout) dialogView1.findViewById(R.id.bread_layout);
                    mdrink_layout = (LinearLayout) dialogView1.findViewById(R.id.drink_layout);
                    holder.malert_itemdescription = (TextView) dialogView1.findViewById(R.id.alert_itemdescription);
                    malert_total_price = (TextView) dialogView1.findViewById(R.id.total_amount);
                    RelativeLayout mainLayout = (RelativeLayout) dialogView1.findViewById(R.id.mainlayout);

                    if (insideMenuCategorieslist.get(position).getAdditionId().equals("null") || insideMenuCategorieslist.get(position).getAdditionId().equals("")) {

                        madditional_layout.setVisibility(GONE);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT, 450);
                        mainLayout.setLayoutParams(layoutParams);
                        for (int j = 0; j < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); j++) {
                            if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getSize() == 1) {
                                malert_total_price.setText("" + decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices())));
                            }
                        }
                    } else {
                        madditional_layout.setVisibility(VISIBLE);
                        new GetAdditionals().execute(Projecturl.ADDITIONALS_URL + insideMenuCategorieslist.get(position).getAdditionId());
                    }

                    Log.i("TAG", "S size" + SauceList.size());
                    Log.i("TAG", "B size" + Breadlist.size());
                    Log.i("TAG", "D size" + Drinklist.size());
                    Log.i("TAG", "Additionalid" + insideMenuCategorieslist.get(position).getAdditionId());


                    mbroasted_express.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//
                        }
                    });

                    Picasso.with(context).load("http://www.alrajhiksa.com/images/" + insideMenuCategorieslist.get(position).getImage()).into(mbroasted_express);

                    holder.malert_itemname.setText("" + insideMenuCategorieslist.get(position).getItemName_Ar());
                    holder.malert_itemdescription.setText("" + insideMenuCategorieslist.get(position).getDescription_Ar());
                    holder.malert_quantity.setText("" + mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), "1"));

                    for (int j = 0; j < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); j++) {
                        if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getSize() == 1) {
                            malert_price.setText("" + decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices())));
                            itemprice = Float.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices());
                        }
                    }

                    mComment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, Comment.class);
                            if (language.equalsIgnoreCase("En")) {
                                intent.putExtra("title", insideMenuCategorieslist.get(position).getItemName());
                            } else if (language.equalsIgnoreCase("Ar")) {
                                intent.putExtra("title", insideMenuCategorieslist.get(position).getItemName_Ar());
                            }
                            intent.putExtra("itemImage", insideMenuCategorieslist.get(position).getImage());
                            intent.putExtra("itemId", insideMenuCategorieslist.get(position).getItemId());
                            intent.putExtra("itemtype", insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize());
                            intent.putExtra("comment", mydatabase.getComment(insideMenuCategorieslist.get(position).getItemId(),
                                    Integer.toString(1)));
                            intent.putExtra("screen", "item");
                            context.startActivity(intent);
                        }
                    });

                    malert_sub_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (qty == 1) {
//                                mydatabase.deleteItemFromOrder(insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));
                                malert_sub_icon.setEnabled(false);
                            } else {
                                qty = qty - 1;
//                                mseekbar.setProgress(qty - 1);
                                holder.malert_quantity.setText(String.valueOf(qty));

                                malert_total_price.setText(decimalFormat.format((itemprice + orderprice) * qty));
                                Log.i("TAG", "item price sub " + itemprice);
//                                holder.minside_regular_count.setText(String.valueOf(qty));
//                                for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
//                                    if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 1) {
//                                        price = Integer.parseInt(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices()) * qty;
//                                    }
//                                }
//
//                                mydatabase.updateOrder(String.valueOf(qty), String.valueOf(price), insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));

//                                notifyDataSetChanged();

                            }

//                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//                            if (language.equalsIgnoreCase("En")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            }
                            notifyDataSetChanged();

                        }
                    });

                    malert_plus_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            qty = qty + 1;
//                            mseekbar.setProgress(qty - 1);
                            holder.malert_quantity.setText(String.valueOf(qty));

                            malert_total_price.setText(decimalFormat.format((itemprice + orderprice) * qty));
                            Log.i("TAG", "item price plus " + itemprice);
//                            holder.minside_regular_count.setText(String.valueOf(qty));
//
//                            for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
//                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 1) {
//                                    price = Integer.parseInt(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices()) * qty;
//                                    notifyDataSetChanged();
//                                }
//                            }
//                            mydatabase.updateOrder(String.valueOf(qty), String.valueOf(price), insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));

//                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//                            if (language.equalsIgnoreCase("En")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            }

                            notifyDataSetChanged();
                        }
                    });

                    mdelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.alertDialog.dismiss();
                        }
                    });

//                    mseekbar = (SeekBar) dialogView1.findViewById(R.id.seekbar);
//                    mseekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                        @Override
//                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                            progresscount = progress;
//                            if (progresscount == 0) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            } else if (progresscount == 1) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            } else if (progresscount == 2) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            } else if (progresscount == 3) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            } else if (progresscount == 4) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity.setText(String.valueOf(qty));
//                            }
//                        }
//
//                        @Override
//                        public void onStartTrackingTouch(SeekBar seekBar) {
//
//                        }
//
//                        @Override
//                        public void onStopTrackingTouch(SeekBar seekBar) {
//
//                        }
//                    });
                    holder.madd_to_basket.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.minside_regular_count.setText(String.valueOf(qty));
                            Log.e("QTY", String.valueOf(qty));

                            for (int i = 0; i < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); i++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getSize() == 1) {
                                    orderPrice = Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getPrices());

                                }
                            }

                            for (int k = 0; k < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); k++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(k).getSize() == 1) {
                                    totalAmt = (itemprice + orderprice) * qty;
                                    notifyDataSetChanged();
                                }
                            }
                            for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 1) {
                                    price = Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices());
                                    notifyDataSetChanged();
                                }
                            }
                            for (int m = 0; m < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); m++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(m).getSize() == 1) {
                                    count = qty;
                                    notifyDataSetChanged();

                                }
                            }

                            String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "0";
                            if (saucepos != null && saucepos.size() > 0) {
                                for (int i = 0; i < saucepos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getImages();
                                        addl_price = addl_price + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty.size(); j++) {
                                            if (itemQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalsId();
                                        addl_name = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName();
                                        addl_name_ar = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalNameAr();
                                        addl_image = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getImages();
                                        addl_price = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty.size(); j++) {
                                            if (itemQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }


                            if (breadpos != null && breadpos.size() > 0) {
                                for (int i = 0; i < breadpos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getImages();
                                        addl_price = addl_price + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty1.size(); j++) {
                                            if (itemQty1.get(j).equals(Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalsId();
                                        addl_name = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName();
                                        addl_name_ar = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalNameAr();
                                        addl_image = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getImages();
                                        addl_price = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty1.size(); j++) {
                                            if (itemQty1.get(j).equals(Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }

                            if (drinkpos != null && drinkpos.size() > 0) {
                                for (int i = 0; i < drinkpos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getImages();
                                        addl_price = addl_price + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty2.size(); j++) {
                                            if (itemQty2.get(j).equals(Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalsId();
                                        addl_name = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName();
                                        addl_name_ar = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalNameAr();
                                        addl_image = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getImages();
                                        addl_price = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty2.size(); j++) {
                                            if (itemQty2.get(j).equals(Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }

                            if (mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), Integer.toString(1)) == 0) {

                                HashMap<String, String> values = new HashMap<>();
                                values.put("itemId", insideMenuCategorieslist.get(position).getItemId());
                                values.put("itemTypeId", Integer.toString(1));
                                values.put("subCategoryId", "");
                                values.put("additionals", addl_ids);
                                values.put("qty", String.valueOf(count));
                                values.put("price", Float.toString(price));
                                values.put("additionalsPrice", addl_price);
                                values.put("additionalsTypeId", addl_qty);
                                values.put("totalAmount", Float.toString(totalAmt));
                                values.put("comment", Projecturl.COMMENTS);
                                values.put("status", "");
                                values.put("creationDate", "10/11/2016");
                                values.put("modifiedDate", "14/11/2016");
                                values.put("categoryId", insideMenuCategorieslist.get(position).getCatId());
                                values.put("itemName", insideMenuCategorieslist.get(position).getItemName());
                                values.put("itemNameAr", insideMenuCategorieslist.get(position).getItemName_Ar());
                                values.put("image", insideMenuCategorieslist.get(position).getImage());
                                values.put("nonDelivery", insideMenuCategorieslist.get(position).getIsDelivery());
                                values.put("AdditionalName", addl_name);
                                values.put("AdditionalNameAr", addl_name_ar);
                                values.put("Additionalimage", addl_image);
                                mydatabase.insertOrder(values);
                                notifyDataSetChanged();
                            } else {
                                int cnt = mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), Integer.toString(1));
                                count = qty + cnt;
                                finalPrice = count * price;

                                mydatabase.updateOrderwithComment(String.valueOf(count), String.valueOf(finalPrice), insideMenuCategorieslist.get(position).getItemId(), Integer.toString(1), Projecturl.COMMENTS);
                                notifyDataSetChanged();
                            }

                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
                            if (language.equalsIgnoreCase("En")) {
                                InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
                            } else if (language.equalsIgnoreCase("Ar")) {
                                InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
                            }

                            holder.alertDialog.dismiss();
                        }

                    });

                }
                holder.alertDialog = dialogBuilder.create();
                holder.alertDialog.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = holder.alertDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });

        orderprice = 0;
        itemQty.clear();
        itemQty1.clear();
        itemQty2.clear();
        itemfinalQty.clear();
        itemfinalQty1.clear();
        itemfinalQty2.clear();
        itemfinalprice.clear();
        itemfinalprice1.clear();
        itemfinalprice2.clear();
        saucepos.clear();
        breadpos.clear();
        drinkpos.clear();
        holder.mspice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qty = 1;

                Projecturl.COMMENTS = "";

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = ((AppCompatActivity) context).getLayoutInflater();

                if (language.equalsIgnoreCase("En")) {
                    View dialogView = inflater.inflate(R.layout.additional_alert_dialog_spice, null);
                    dialogBuilder.setView(dialogView);

                    ImageView mdelete1 = (ImageView) dialogView.findViewById(R.id.delete1);
                    ImageView mbroasted_express1 = (ImageView) dialogView.findViewById(R.id.broasted_express1);
                    final ImageView malert_sub_icon1 = (ImageView) dialogView.findViewById(R.id.alert_sub_icon1);
                    ImageView malert_plus_icon1 = (ImageView) dialogView.findViewById(R.id.alert_plus_icon1);
                    ImageView mComment1 = (ImageView) dialogView.findViewById(R.id.comment1);
                    msauce_gridview = (CustomGridView) dialogView.findViewById(R.id.grid_sauce);
                    mbread_gridview = (CustomGridView) dialogView.findViewById(R.id.grid_bread);
                    mdrink_gridview = (CustomGridView) dialogView.findViewById(R.id.grid_drink);
                    madditional_layout = (LinearLayout) dialogView.findViewById(R.id.additionals_layout);
                    msauce_layout = (LinearLayout) dialogView.findViewById(R.id.sauce_layout);
                    mbread_layout = (LinearLayout) dialogView.findViewById(R.id.bread_layout);
                    mdrink_layout = (LinearLayout) dialogView.findViewById(R.id.drink_layout);
                    holder.malert_itemdescription = (TextView) dialogView.findViewById(R.id.alert_itemdescription);
                    malert_total_price = (TextView) dialogView.findViewById(R.id.total_amount);
                    RelativeLayout mainLayout = (RelativeLayout) dialogView.findViewById(R.id.mainlayout);

                    holder.madd_to_basket1 = (LinearLayout) dialogView.findViewById(R.id.add_to_basket1);
                    holder.malert_quantity1 = (TextView) dialogView.findViewById(R.id.alert_quantity1);
                    TextView malert_choices1 = (TextView) dialogView.findViewById(R.id.alert_choice1);
//                final TextView mitem_count = (TextView) dialogView.findViewById(R.id.item_count);
                    holder.malert_itemname1 = (TextView) dialogView.findViewById(R.id.alert_itemname1);
                    holder.malert_itemdescription1 = (TextView) dialogView.findViewById(R.id.alert_itemdescription1);
                    TextView malert_price1 = (TextView) dialogView.findViewById(R.id.alert_price1);

                    LinearLayout maler_spice1 = (LinearLayout) dialogView.findViewById(R.id.alert_regular1);

                    if (insideMenuCategorieslist.get(position).getAdditionId().equals("null") || insideMenuCategorieslist.get(position).getAdditionId().equals("")) {

                        madditional_layout.setVisibility(GONE);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT, 450);
                        mainLayout.setLayoutParams(layoutParams);
                        for (int j = 0; j < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); j++) {
                            if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getSize() == 2) {
                                malert_total_price.setText("" + decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices())));
                            }
                        }
                    } else {
                        madditional_layout.setVisibility(VISIBLE);
                        new GetAdditionals().execute(Projecturl.ADDITIONALS_URL + insideMenuCategorieslist.get(position).getAdditionId());
                    }

                    Log.i("TAG", "S size" + SauceList.size());
                    Log.i("TAG", "B size" + Breadlist.size());
                    Log.i("TAG", "D size" + Drinklist.size());
                    Log.i("TAG", "Additionalid" + insideMenuCategorieslist.get(position).getAdditionId());


                    mbroasted_express1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });

                    Picasso.with(context).load("http://www.alrajhiksa.com/images/" + insideMenuCategorieslist.get(position).getImage()).into(mbroasted_express1);

                    holder.malert_itemname1.setText("" + insideMenuCategorieslist.get(position).getItemName());
                    holder.malert_itemdescription1.setText("" + insideMenuCategorieslist.get(position).getDescription());
                    holder.malert_quantity1.setText("1");

                    for (int j = 0; j < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); j++) {
                        if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getSize() == 2) {
                            malert_price1.setText("" + decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices())));
                            itemprice = Float.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices());
                        }
                    }

                    mComment1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, Comment.class);
                            if (language.equalsIgnoreCase("En")) {
                                intent.putExtra("title", insideMenuCategorieslist.get(position).getItemName());
                            } else if (language.equalsIgnoreCase("Ar")) {
                                intent.putExtra("title", insideMenuCategorieslist.get(position).getItemName_Ar());
                            }
                            intent.putExtra("itemImage", insideMenuCategorieslist.get(position).getImage());
                            intent.putExtra("itemId", insideMenuCategorieslist.get(position).getItemId());
                            intent.putExtra("itemtype", insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize());
                            intent.putExtra("comment", mydatabase.getComment(insideMenuCategorieslist.get(position).getItemId(),
                                    Integer.toString(2)));
                            intent.putExtra("screen", "item");
                            context.startActivity(intent);
                        }
                    });

                    maler_spice1.setBackground(context.getResources().getDrawable(R.drawable.inside_menu_item_price1));
                    malert_choices1.setText(" Spice ");

                    malert_sub_icon1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (qty == 1) {
//                                mydatabase.deleteItemFromOrder(insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));
                                malert_sub_icon1.setEnabled(false);
                            } else {
                                qty = qty - 1;
//                                mseekbar1.setProgress(qty - 1);
                                holder.malert_quantity1.setText(String.valueOf(qty));
                                malert_total_price.setText(decimalFormat.format((itemprice + orderprice) * qty));
                                Log.i("TAG", "item price sub " + itemprice);
//                                holder.minside_spice_count.setText(String.valueOf(qty));
//                                for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
//                                    if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 2) {
//                                        price1 = Integer.parseInt(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices()) * qty;
//                                    }
//                                }
//
//                                mydatabase.updateOrder(String.valueOf(qty), String.valueOf(price1), insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));

//                                InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//                                if (language.equalsIgnoreCase("En")) {
//                                    InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//                                    InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                                } else if (language.equalsIgnoreCase("Ar")) {
//                                    InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//                                    InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                                }

                                notifyDataSetChanged();
                            }
                        }
                    });

                    malert_plus_icon1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            qty = qty + 1;
//                            mseekbar1.setProgress(qty - 1);
                            holder.malert_quantity1.setText(String.valueOf(qty));
                            malert_total_price.setText(decimalFormat.format((itemprice + orderprice) * qty));
                            Log.i("TAG", "item price plus " + itemprice);
//                            holder.minside_spice_count.setText(String.valueOf(qty));
//                            for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
//                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 2) {
//                                    price1 = Integer.parseInt(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices()) * qty;
//                                    notifyDataSetChanged();
//                                }
//                            }
//                            mydatabase.updateOrder(String.valueOf(qty), String.valueOf(price1), insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));

//                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//                            if (language.equalsIgnoreCase("En")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            }

                            notifyDataSetChanged();
                        }
                    });

                    mdelete1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.alertDialog1.dismiss();
                        }
                    });

//                    mseekbar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                        @Override
//                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                            int progresscount1 = progress;
//                            if (progresscount1 == 0) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            } else if (progresscount1 == 1) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            } else if (progresscount1 == 2) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            } else if (progresscount1 == 3) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            } else if (progresscount1 == 4) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            }
//
//                        }
//
//                        @Override
//                        public void onStartTrackingTouch(SeekBar seekBar) {
//
//                        }
//
//                        @Override
//                        public void onStopTrackingTouch(SeekBar seekBar) {
//
//                        }
//                    });

                    holder.madd_to_basket1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.minside_spice_count.setText(String.valueOf(qty));
                            Log.e("QTY", String.valueOf(qty));

                            for (int i = 0; i < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); i++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getSize() == 2) {
                                    orderPrice = Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getPrices());

                                }
                            }

                            for (int k = 0; k < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); k++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(k).getSize() == 2) {
                                    totalAmt1 = (itemprice + orderprice) * qty;
                                    notifyDataSetChanged();
                                }
                            }
                            for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 2) {
                                    price1 = Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices());
                                    notifyDataSetChanged();
                                }
                            }
                            for (int m = 0; m < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); m++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(m).getSize() == 2) {
                                    count1 = qty;
                                    notifyDataSetChanged();

                                }
                            }

                            String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "0";
                            if (saucepos != null && saucepos.size() > 0) {
                                for (int i = 0; i < saucepos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getImages();
                                        addl_price = addl_price + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty.size(); j++) {
                                            if (itemQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalsId();
                                        addl_name = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName();
                                        addl_name_ar = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalNameAr();
                                        addl_image = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getImages();
                                        addl_price = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty.size(); j++) {
                                            if (itemQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }


                            if (breadpos != null && breadpos.size() > 0) {
                                for (int i = 0; i < breadpos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getImages();
                                        addl_price = addl_price + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty1.size(); j++) {
                                            if (itemQty1.get(j).equals(Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalsId();
                                        addl_name = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName();
                                        addl_name_ar = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalNameAr();
                                        addl_image = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getImages();
                                        addl_price = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty1.size(); j++) {
                                            if (itemQty1.get(j).equals(Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }

                            if (drinkpos != null && drinkpos.size() > 0) {
                                for (int i = 0; i < drinkpos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getImages();
                                        addl_price = addl_price + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty2.size(); j++) {
                                            if (itemQty2.get(j).equals(Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalsId();
                                        addl_name = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName();
                                        addl_name_ar = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalNameAr();
                                        addl_image = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getImages();
                                        addl_price = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty2.size(); j++) {
                                            if (itemQty2.get(j).equals(Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }

                            Log.i("TAG", "add id " + addl_ids);
                            if (mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), Integer.toString(2)) == 0) {

                                HashMap<String, String> values = new HashMap<>();
                                values.put("itemId", insideMenuCategorieslist.get(position).getItemId());
                                values.put("itemTypeId", Integer.toString(2));
                                values.put("subCategoryId", "");
                                values.put("additionals", addl_ids);
                                Log.i("TAG", "add id1 " + addl_ids);
                                values.put("qty", String.valueOf(count1));
                                values.put("price", Float.toString(price1));
                                values.put("additionalsPrice", addl_price);
                                values.put("additionalsTypeId", addl_qty);
                                values.put("totalAmount", Float.toString(totalAmt1));
                                values.put("comment", Projecturl.COMMENTS);
                                values.put("status", "");
                                values.put("creationDate", "10/11/2016");
                                values.put("modifiedDate", "14/11/2016");
                                values.put("categoryId", insideMenuCategorieslist.get(position).getCatId());
                                values.put("itemName", insideMenuCategorieslist.get(position).getItemName());
                                values.put("itemNameAr", insideMenuCategorieslist.get(position).getItemName_Ar());
                                values.put("image", insideMenuCategorieslist.get(position).getImage());
                                values.put("nonDelivery", insideMenuCategorieslist.get(position).getIsDelivery());
                                values.put("AdditionalName", addl_name);
                                values.put("AdditionalNameAr", addl_name_ar);
                                values.put("Additionalimage", addl_image);
                                mydatabase.insertOrder(values);
                                notifyDataSetChanged();
                            } else {
                                int cnt = mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), Integer.toString(2));
                                count1 = qty + cnt;
                                finalPrice = count1 * price1;

                                mydatabase.updateOrderwithComment(String.valueOf(count1), String.valueOf(finalPrice), insideMenuCategorieslist.get(position).getItemId(), Integer.toString(2), Projecturl.COMMENTS);
                                notifyDataSetChanged();
                            }

                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
                            if (language.equalsIgnoreCase("En")) {
                                InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
                            } else if (language.equalsIgnoreCase("Ar")) {
                                InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
                            }
                            holder.alertDialog1.dismiss();
                        }
                    });

                } else if (language.equalsIgnoreCase("Ar")) {
                    Projecturl.COMMENTS = "";
                    View dialogView1 = inflater.inflate(R.layout.additional_alert_dialog_spice_arabic, null);
                    dialogBuilder.setView(dialogView1);

                    ImageView mdelete1 = (ImageView) dialogView1.findViewById(R.id.delete1);
                    ImageView mbroasted_express1 = (ImageView) dialogView1.findViewById(R.id.broasted_express1);
                    final ImageView malert_sub_icon1 = (ImageView) dialogView1.findViewById(R.id.alert_sub_icon1);
                    ImageView malert_plus_icon1 = (ImageView) dialogView1.findViewById(R.id.alert_plus_icon1);
                    ImageView mComment1 = (ImageView) dialogView1.findViewById(R.id.comment1);

                    holder.madd_to_basket1 = (LinearLayout) dialogView1.findViewById(R.id.add_to_basket1);
                    holder.malert_quantity1 = (TextView) dialogView1.findViewById(R.id.alert_quantity1);
                    TextView malert_choices1 = (TextView) dialogView1.findViewById(R.id.alert_choice1);
                    holder.malert_itemname1 = (TextView) dialogView1.findViewById(R.id.alert_itemname1);
                    holder.malert_itemdescription1 = (TextView) dialogView1.findViewById(R.id.alert_itemdescription1);
                    TextView malert_price1 = (TextView) dialogView1.findViewById(R.id.alert_price1);
                    mseekbar1 = (SeekBar) dialogView1.findViewById(R.id.seekbar1);

                    LinearLayout maler_spice1 = (LinearLayout) dialogView1.findViewById(R.id.alert_regular1);

                    msauce_gridview = (CustomGridView) dialogView1.findViewById(R.id.grid_sauce);
                    mbread_gridview = (CustomGridView) dialogView1.findViewById(R.id.grid_bread);
                    mdrink_gridview = (CustomGridView) dialogView1.findViewById(R.id.grid_drink);
                    madditional_layout = (LinearLayout) dialogView1.findViewById(R.id.additionals_layout);
                    msauce_layout = (LinearLayout) dialogView1.findViewById(R.id.sauce_layout);
                    mbread_layout = (LinearLayout) dialogView1.findViewById(R.id.bread_layout);
                    mdrink_layout = (LinearLayout) dialogView1.findViewById(R.id.drink_layout);
                    holder.malert_itemdescription = (TextView) dialogView1.findViewById(R.id.alert_itemdescription);
                    malert_total_price = (TextView) dialogView1.findViewById(R.id.total_amount);
                    RelativeLayout mainLayout = (RelativeLayout) dialogView1.findViewById(R.id.mainlayout);

                    if (insideMenuCategorieslist.get(position).getAdditionId().equals("null") || insideMenuCategorieslist.get(position).getAdditionId().equals("")) {

                        madditional_layout.setVisibility(GONE);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT, 450);
                        mainLayout.setLayoutParams(layoutParams);
                        for (int j = 0; j < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); j++) {
                            if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getSize() == 2) {
                                malert_total_price.setText("" + decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices())));
                            }
                        }
                    } else {
                        madditional_layout.setVisibility(VISIBLE);
                        new GetAdditionals().execute(Projecturl.ADDITIONALS_URL + insideMenuCategorieslist.get(position).getAdditionId());
                    }

                    Log.i("TAG", "S size" + SauceList.size());
                    Log.i("TAG", "B size" + Breadlist.size());
                    Log.i("TAG", "D size" + Drinklist.size());
                    Log.i("TAG", "Additionalid" + insideMenuCategorieslist.get(position).getAdditionId());

                    mbroasted_express1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });

                    Picasso.with(context).load("http://www.alrajhiksa.com/images/" + insideMenuCategorieslist.get(position).getImage()).into(mbroasted_express1);

                    holder.malert_itemname1.setText("" + insideMenuCategorieslist.get(position).getItemName_Ar());
                    holder.malert_itemdescription1.setText("" + insideMenuCategorieslist.get(position).getDescription_Ar());


                    for (int j = 0; j < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); j++) {
                        if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getSize() == 2) {
                            malert_price1.setText("" + decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices())));
                            itemprice = Float.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(j).getPrices());
                        }
                    }

                    mComment1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, Comment.class);
                            if (language.equalsIgnoreCase("En")) {
                                intent.putExtra("title", insideMenuCategorieslist.get(position).getItemName());
                            } else if (language.equalsIgnoreCase("Ar")) {
                                intent.putExtra("title", insideMenuCategorieslist.get(position).getItemName_Ar());
                            }
                            intent.putExtra("itemImage", insideMenuCategorieslist.get(position).getImage());
                            intent.putExtra("itemId", insideMenuCategorieslist.get(position).getItemId());
                            intent.putExtra("itemtype", insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize());
                            intent.putExtra("comment", mydatabase.getComment(insideMenuCategorieslist.get(position).getItemId(),
                                    Integer.toString(2)));
                            intent.putExtra("screen", "item");
                            context.startActivity(intent);
                        }
                    });

                    maler_spice1.setBackground(context.getResources().getDrawable(R.drawable.inside_menu_item_price1));
                    malert_choices1.setText(" حار  ");

                    malert_sub_icon1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (qty == 1) {
                                mydatabase.deleteItemFromOrder(insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));
                                malert_sub_icon1.setEnabled(false);
                            } else {

                                qty = qty - 1;
//                                mseekbar1.setProgress(qty - 1);
                                holder.malert_quantity1.setText(String.valueOf(qty));
                                malert_total_price.setText(decimalFormat.format((itemprice + orderprice) * qty));
                                Log.i("TAG", "item price sub " + itemprice);
//                                holder.minside_spice_count.setText(String.valueOf(qty));
//                                for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
//                                    if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 2) {
//                                        price1 = Integer.parseInt(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices()) * qty;
//                                    }
//                                }
//
//                                mydatabase.updateOrder(String.valueOf(qty), String.valueOf(price1), insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));

                                notifyDataSetChanged();
                            }
//                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//                            if (language.equalsIgnoreCase("En")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            }
                        }
                    });

                    malert_plus_icon1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            qty = qty + 1;
//                            mseekbar1.setProgress(qty - 1);
                            holder.malert_quantity1.setText(String.valueOf(qty));
                            malert_total_price.setText(decimalFormat.format((itemprice + orderprice) * qty));
                            Log.i("TAG", "item price plus " + itemprice);
//                            holder.minside_spice_count.setText(String.valueOf(qty));
//
//                            for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
//                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 2) {
//                                    price1 = Integer.parseInt(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices()) * qty;
//                                    notifyDataSetChanged();
//                                }
//                            }
//                            mydatabase.updateOrder(String.valueOf(qty), String.valueOf(price1), insideMenuCategorieslist.get(position).getItemId(), String.valueOf(insideMenuCategorieslist.get(position).getInsideMenuchild().get(0).getSize()));

//                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
//                            if (language.equalsIgnoreCase("En")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " SR");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            } else if (language.equalsIgnoreCase("Ar")) {
//                                InsideMenu.morder_price.setText("" + mydatabase.getTotalOrderPrice() + " ريال");
//                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
//                            }
                            notifyDataSetChanged();
                        }
                    });

                    mdelete1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.alertDialog1.dismiss();
                        }
                    });

//                    mseekbar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                        @Override
//                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                            int progresscount1 = progress;
//                            if (progresscount1 == 0) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            } else if (progresscount1 == 1) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            } else if (progresscount1 == 2) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            } else if (progresscount1 == 3) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            } else if (progresscount1 == 4) {
//                                progress++;
//                                qty = progress;
//                                holder.malert_quantity1.setText(String.valueOf(qty));
//                            }
//                        }
//
//                        @Override
//                        public void onStartTrackingTouch(SeekBar seekBar) {
//
//                        }
//
//                        @Override
//                        public void onStopTrackingTouch(SeekBar seekBar) {
//
//                        }
//                    });

                    holder.madd_to_basket1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.minside_spice_count.setText(String.valueOf(qty));
                            Log.e("QTY", String.valueOf(qty));

                            for (int i = 0; i < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); i++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getSize() == 2) {
//                    int regCnt = mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), "1");
                                    orderPrice = Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getPrices());

                                }
                            }

                            for (int k = 0; k < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); k++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(k).getSize() == 2) {
                                    totalAmt1 = (itemprice + orderprice) * qty;
                                    notifyDataSetChanged();
                                }
                            }
                            for (int l = 0; l < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); l++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getSize() == 2) {
                                    price1 = Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(l).getPrices());
                                    notifyDataSetChanged();
                                }
                            }
                            for (int m = 0; m < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); m++) {
                                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(m).getSize() == 2) {
                                    count1 = qty;
                                    notifyDataSetChanged();

                                }
                            }

                            String addl_ids = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "0";
                            if (saucepos != null && saucepos.size() > 0) {
                                for (int i = 0; i < saucepos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getImages();
                                        addl_price = addl_price + "," + SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty.size(); j++) {
                                            if (itemQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalsId();
                                        addl_name = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName();
                                        addl_name_ar = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalNameAr();
                                        addl_image = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getImages();
                                        addl_price = SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty.size(); j++) {
                                            if (itemQty.get(j).equals(SauceList.get(0).getChildItems().get(Integer.parseInt(saucepos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }


                            if (breadpos != null && breadpos.size() > 0) {
                                for (int i = 0; i < breadpos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getImages();
                                        addl_price = addl_price + "," + Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty1.size(); j++) {
                                            if (itemQty1.get(j).equals(Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalsId();
                                        addl_name = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName();
                                        addl_name_ar = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalNameAr();
                                        addl_image = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getImages();
                                        addl_price = Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty1.size(); j++) {
                                            if (itemQty1.get(j).equals(Breadlist.get(0).getChildItems().get(Integer.parseInt(breadpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }

                            if (drinkpos != null && drinkpos.size() > 0) {
                                for (int i = 0; i < drinkpos.size(); i++) {
                                    if (addl_ids != null && addl_ids.length() > 0) {

                                        addl_ids = addl_ids + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalsId();
                                        addl_name = addl_name + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName();
                                        addl_name_ar = addl_name_ar + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalNameAr();
                                        addl_image = addl_image + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getImages();
                                        addl_price = addl_price + "," + Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty2.size(); j++) {
                                            if (itemQty2.get(j).equals(Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = addl_qty + "," + Integer.toString(count);

                                    } else {
                                        addl_ids = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalsId();
                                        addl_name = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName();
                                        addl_name_ar = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalNameAr();
                                        addl_image = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getImages();
                                        addl_price = Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalPriceList().get(0).getPrice();
                                        int count = 0;
                                        for (int j = 0; j < itemQty2.size(); j++) {
                                            if (itemQty2.get(j).equals(Drinklist.get(0).getChildItems().get(Integer.parseInt(drinkpos.get(i))).getAdditionalName())) {
                                                count = count + 1;
                                            }
                                        }
                                        addl_qty = Integer.toString(count);
                                    }
                                }
                            }

                            if (mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), Integer.toString(2)) == 0) {

                                HashMap<String, String> values = new HashMap<>();
                                values.put("itemId", insideMenuCategorieslist.get(position).getItemId());
                                values.put("itemTypeId", Integer.toString(2));
                                values.put("subCategoryId", "");
                                values.put("additionals", addl_ids);
                                values.put("qty", String.valueOf(count1));
                                values.put("price", Float.toString(price1));
                                values.put("additionalsPrice", addl_price);
                                values.put("additionalsTypeId", addl_qty);
                                values.put("totalAmount", Float.toString(totalAmt1));
                                values.put("comment", Projecturl.COMMENTS);
                                values.put("status", "");
                                values.put("creationDate", "10/11/2016");
                                values.put("modifiedDate", "14/11/2016");
                                values.put("categoryId", insideMenuCategorieslist.get(position).getCatId());
                                values.put("itemName", insideMenuCategorieslist.get(position).getItemName());
                                values.put("itemNameAr", insideMenuCategorieslist.get(position).getItemName_Ar());
                                values.put("image", insideMenuCategorieslist.get(position).getImage());
                                values.put("nonDelivery", insideMenuCategorieslist.get(position).getIsDelivery());
                                values.put("AdditionalName", addl_name);
                                values.put("AdditionalNameAr", addl_name_ar);
                                values.put("Additionalimage", addl_image);
                                mydatabase.insertOrder(values);
                                notifyDataSetChanged();
                            } else {
                                int cnt = mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), Integer.toString(2));
                                count1 = qty + cnt;
                                finalPrice = count1 * price1;

                                mydatabase.updateOrderwithComment(String.valueOf(count1), String.valueOf(finalPrice), insideMenuCategorieslist.get(position).getItemId(), Integer.toString(2), Projecturl.COMMENTS);
                                notifyDataSetChanged();
                            }

                            InsideMenu.mcount_basket.setText("" + mydatabase.getTotalOrderQty());
                            if (language.equalsIgnoreCase("En")) {
                                InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
                            } else if (language.equalsIgnoreCase("Ar")) {
                                InsideMenu.morder_price.setText("" + decimalFormat.format(mydatabase.getTotalOrderPrice()));
                                InsideMenu.morder_quantity.setText("" + mydatabase.getTotalOrderQty());
                            }
                            holder.alertDialog1.dismiss();
                        }
                    });

                }
                holder.alertDialog1 = dialogBuilder.create();
                holder.alertDialog1.show();

                //Grab the window of the dialog, and change the width
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = holder.alertDialog1.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });

        if (language.equalsIgnoreCase("En")) {
            holder.minside_item_name.setText("" + insideMenuCategorieslist.get(position).getItemName());
        } else if (language.equalsIgnoreCase("Ar")) {
            holder.minside_item_name.setText("" + insideMenuCategorieslist.get(position).getItemName_Ar());
        }

        if (language.equalsIgnoreCase("En")) {
            holder.minside_description.setText("" + insideMenuCategorieslist.get(position).getDescription());
        } else if (language.equalsIgnoreCase("Ar")) {
            holder.minside_description.setText("" + insideMenuCategorieslist.get(position).getDescription_Ar());
        }

        Picasso.with(context).load("http://www.alrajhiksa.com/images/" + insideMenuCategorieslist.get(position).getImage()).into(holder.minside_image);

        if (insideMenuCategorieslist.get(position).getInsideMenuchild().size() == 1) {
            for (int i = 0; i < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); i++) {
                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getSize() == 1) {
//                    int regCnt = mydatabase.getItemOrderCountWithSize(insideMenuCategorieslist.get(position).getItemId(), "1");
                    String priceregular = decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getPrices()));
                    holder.minside_price_regular.setText("" + priceregular);
                    holder.mspice.setVisibility(GONE);
                    holder.mregular1.setVisibility(VISIBLE);
                }
            }
        } else {
            for (int i = 0; i < insideMenuCategorieslist.get(position).getInsideMenuchild().size(); i++) {
                holder.mspice.setVisibility(VISIBLE);
                holder.mregular1.setVisibility(VISIBLE);
                if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getSize() == 1) {
                    String priceregular = decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getPrices()));
                    holder.minside_price_regular.setText("" + priceregular);
                } else if (insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getSize() == 2) {
                    String pricespice = decimalFormat.format(Float.parseFloat(insideMenuCategorieslist.get(position).getInsideMenuchild().get(i).getPrices()));
                    holder.minside_price_spices.setText("" + pricespice);
                }
            }
        }
        return convertView;
    }

    public class GetAdditionals extends AsyncTask<String, Integer, String> {

        String response;
        String networkStatus;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            SauceList.clear();
            Breadlist.clear();
            Drinklist.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(context);
            dialog = ProgressDialog.show(context, "", "Loading items...");
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
//                    if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(context, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();
//                    }
//                    else{
//                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
//                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONObject mainobj = new JSONObject(result);
                            JSONArray ja = mainobj.getJSONArray("Success");
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo = ja.getJSONObject(i);

                                JSONArray key = jo.getJSONArray("Key");
                                Log.i("TAG", "key " + key.length());
                                Modifiers mod = new Modifiers();
                                ArrayList<Additionals> additionalsList = new ArrayList<>();
                                for (int j = 0; j < key.length(); j++) {


                                    JSONObject jo1 = key.getJSONObject(j);
                                    if (j == 0) {
                                        String modifierName = jo1.getString("ModifierName");
                                        String modifierName_Ar = jo1.getString("ModifierName_Ar");
                                        String modifierId = jo1.getString("ModifierId");
//                                        if(modifierId.equalsIgnoreCase("1")){
//                                            continue Outer;
//                                        }
                                        mod.setModifierName(modifierName);
                                        mod.setModifierNameAr(modifierName_Ar);
                                        mod.setModifierId(modifierId);
                                    } else {

//                                        JSONObject issueObj = jo.getJSONObject("Value");
                                        Iterator iterator = jo1.keys();
                                        while (iterator.hasNext()) {
                                            Additionals adis = new Additionals();
                                            String additonalId = (String) iterator.next();
                                            ArrayList<AdditionalPrices> additionalPrices = new ArrayList<>();
                                            JSONArray ja1 = jo1.getJSONArray(additonalId);

                                            for (int k = 0; k < ja1.length(); k++) {
                                                JSONArray additionals = ja1.getJSONArray(k);
                                                if (k == 0) {
                                                    JSONObject additionalDetails = additionals.getJSONObject(0);
                                                    String additionalName = additionalDetails.getString("AdditionalName");
                                                    String additionalName_Ar = additionalDetails.getString("AdditionalName_Ar");
                                                    String images = additionalDetails.getString("Images");
                                                    String additionalId = additionalDetails.getString("AdditionalId");
                                                    adis.setAdditionalName(additionalName);
                                                    adis.setAdditionalNameAr(additionalName_Ar);
                                                    adis.setAdditionalsId(additionalId);
                                                    adis.setImages(images);
                                                    adis.setModifierId(mod.getModifierId());
                                                } else {
                                                    for (int l = 0; l < additionals.length(); l++) {
                                                        AdditionalPrices ap = new AdditionalPrices();
                                                        JSONObject jo2 = additionals.getJSONObject(l);
                                                        ap.setItemType(jo2.getString("Type"));
                                                        ap.setPrice(jo2.getString("Price"));
                                                        additionalPrices.add(ap);
                                                    }
                                                    adis.setAdditionalPriceList(additionalPrices);
                                                }
                                            }
                                            additionalsList.add(adis);
                                        }
                                    }
                                }
                                mod.setChildItems(additionalsList);
//                                additional_list.add(mod);
                                if (mod.getModifierId().equals("1")) {
                                    SauceList.add(mod);
                                }
                                if (mod.getModifierId().equals("2")) {
                                    Breadlist.add(mod);
                                }
                                if (mod.getModifierId().equals("3")) {
                                    Drinklist.add(mod);
                                }
                            }
                            Log.i("TAG", "size " + SauceList);

                            if (SauceList.size() == 0) {
                                msauce_layout.setVisibility(View.GONE);
                            } else {
                                if (SauceList.size() > 0) {
                                    Log.i("TAG", "S size " + SauceList.get(0).getChildItems().size());
                                    msauce_layout.setVisibility(VISIBLE);
                                    msauce_gridview.setNumColumns(3);
                                    madapter = new AdditionallistAdpter(context, SauceList, language, InsideMenuAdapter.this);
                                    msauce_gridview.setAdapter(madapter);
//                                        notifyDataSetChanged();
                                }
                            }

                            if (Breadlist.size() == 0) {

                                mbread_layout.setVisibility(GONE);

                            } else {
                                if (Breadlist.size() > 0) {
                                    Log.i("TAG", "B size " + Breadlist.size());
                                    mbread_layout.setVisibility(VISIBLE);
                                    mbread_gridview.setNumColumns(3);

                                    mbreadadapter = new BreadListAdapter(context, Breadlist, language, InsideMenuAdapter.this);
                                    mbread_gridview.setAdapter(mbreadadapter);
                                    notifyDataSetChanged();
                                }

                            }

                            if (Drinklist.size() == 0) {

                                mdrink_layout.setVisibility(GONE);

                            } else {
                                if (Drinklist.size() > 0) {
                                    Log.i("TAG", "D size " + Drinklist.size());
                                    mdrink_layout.setVisibility(VISIBLE);
                                    mdrink_gridview.setNumColumns(3);

                                    mdrinkadapter = new DrinkListAdapter(context, Drinklist, language, InsideMenuAdapter.this);
                                    mdrink_gridview.setAdapter(mdrinkadapter);
                                    notifyDataSetChanged();
                                }

                            }
                            Log.i("TAG", "SauceList " + SauceList.size());
                            Log.i("TAG", "Breadlist " + Breadlist.size());
                            Log.i("TAG", "Drinklist " + Drinklist.size());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                Toast.makeText(context, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }
}