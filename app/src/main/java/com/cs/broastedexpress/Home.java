package com.cs.broastedexpress;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.broastedexpress.Activities.InsideMenu;
import com.cs.broastedexpress.Activities.Login;
import com.cs.broastedexpress.Activities.Notification;
import com.cs.broastedexpress.Fragments.HomeFragment;
import com.cs.broastedexpress.Fragments.MoreFragment;
import com.cs.broastedexpress.Fragments.OrderFragment;
import com.google.android.gms.maps.model.LatLng;

import static com.cs.broastedexpress.R.id.view;

public class Home extends AppCompatActivity {
    private static final int MESSAGE_REQUEST = 1;
    ImageView btn;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 3;
    Double lat, longi;

    private DrawerLayout mDrawerLayout;
    private DrawerLayout mDrawerLayout1;
    private ActionBarDrawerToggle mDrawerToggle;
    private ActionBarDrawerToggle mDrawerToggle1;
    DrawerListAdapter adapter;
    private String[] mSidemenuTitles, mSidemenuTitles1;
    private int[] mSidemenuIcons;
    private ListView mDrawerList;
    private ListView mDrawerList1;
    private TextView mTitleTV, mTitleTV1;
    View mview;
    //    ImageView langAr, langEng;
    TextView langAr, langEng,messagecount;
    private LinearLayout mDrawerLinear, mDrawerLinear1;
    private String mTitle, mAppTitle;
    FragmentManager fragmentManager = getSupportFragmentManager();
    boolean doubleBackToExitPressedOnce = false;
    boolean doubleBackToExitPressedOnce1 = false;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;
    ImageView mNotification, mNotification1, header_order;
    LinearLayout mdrawer_layoutbg;
    RelativeLayout morderbg, morderbg1;
    SharedPreferences userPrefs;
    String mLoginStatus;

    ImageView mmenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.drawer_layout);
        }else if (language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.drawer_layout_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        mLoginStatus = userPrefs.getString("login_status", "");
        Log.i("TAG","user id "+userPrefs.getString("userId", ""));

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            } else {
                getGPSCoordinates();
            }
        }else {
            getGPSCoordinates();
        }

        mAppTitle = (String) getTitle();
        mDrawerList = (ListView) findViewById(com.cs.broastedexpress.R.id.drawer_list);
        langAr = (TextView) findViewById(com.cs.broastedexpress.R.id.language_arabic);
        langEng = (TextView) findViewById(com.cs.broastedexpress.R.id.language_eng);
        messagecount= (TextView) findViewById(R.id.message_count);
        if (language.equalsIgnoreCase("En")) {
            mSidemenuTitles = new String[]{"Main", "Order", "Promotion", "More",
                    "Share Our App", "Rate Our App"};
            langAr.setVisibility(View.VISIBLE);
            langEng.setVisibility(View.GONE);
        } else if (language.equalsIgnoreCase("Ar")) {
            mSidemenuTitles = new String[]{"الرئيسية", "الطلب", "عروض", "المزيد",
                    "مشاركة طلبنا", "قيم تطبيقنا"};
            langAr.setVisibility(View.GONE);
            langEng.setVisibility(View.VISIBLE);
        }
        mSidemenuTitles1 = new String[]{"Main", "Order", "Promotion", "More",
                "Share Our App", "Rate Our App"};
        mSidemenuIcons = new int[]{com.cs.broastedexpress.R.drawable.menu_main, com.cs.broastedexpress.R.drawable.menu_order, com.cs.broastedexpress.R.drawable.menu_promotion, com.cs.broastedexpress.R.drawable.menu_more, com.cs.broastedexpress.R.drawable.menu_share, com.cs.broastedexpress.R.drawable.menu_rating};
        adapter = new DrawerListAdapter(this, com.cs.broastedexpress.R.layout.drawer_list_item,
                mSidemenuTitles, mSidemenuIcons, language);
        mDrawerLayout = (DrawerLayout) findViewById(com.cs.broastedexpress.R.id.drawer_layout);
        mDrawerLinear = (LinearLayout) findViewById(com.cs.broastedexpress.R.id.left_drawer);
        mdrawer_layoutbg = (LinearLayout) findViewById(com.cs.broastedexpress.R.id.drawer_layoutbg);

        morderbg1 = (RelativeLayout) findViewById(com.cs.broastedexpress.R.id.orderbg1);

        header_order = (ImageView) findViewById(com.cs.broastedexpress.R.id.header_title_order);

        mview = findViewById(view);

        mNotification = (ImageView) findViewById(com.cs.broastedexpress.R.id.notification);

//        mmenu = (ImageView) findViewById(R.id.menu);

        mNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLoginStatus.equalsIgnoreCase("loggedin")) {
                    Intent intent = new Intent(Home.this, Notification.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Home.this, Login.class);
                    startActivityForResult(intent, MESSAGE_REQUEST);
                }
            }
        });

        mTitleTV = (TextView) findViewById(com.cs.broastedexpress.R.id.header_title);

        configureToolbar();
        configureDrawer();
        mDrawerList.setAdapter(adapter);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        Fragment fragment = new HomeFragment();


        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(com.cs.broastedexpress.R.id.content_frame, fragment).commit();

        try {
            int start = getIntent().getIntExtra("startWith", -1);
            if (start != -1) {
                selectItem(start);
            }
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

//        if(language.equalsIgnoreCase("En")){
//            mTitleTV.setText("Oregano Pizzeria");
//        }else if(language.equalsIgnoreCase("Ar")) {
//            mTitleTV.setText("أوريجانو بيتزاريا");
//        }

        langAr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languagePrefsEditor.putString("language", "Ar");
                languagePrefsEditor.commit();
                Intent intent = new Intent(Home.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

        langEng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languagePrefsEditor.putString("language", "En");
                languagePrefsEditor.commit();
                Intent intent = new Intent(Home.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

//        mmenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                menuclick();
//
//            }
//        });


    }

    public void getGPSCoordinates(){
        gps = new GPSTracker(Home.this);
        if(gps != null){
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
//                new GetCurrentTime().execute();

                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(Home.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    getGPSCoordinates();
                }
                else {
                    Toast.makeText(Home.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(com.cs.broastedexpress.R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (language.equalsIgnoreCase("En")) {
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mDrawerLayout.closeDrawer(GravityCompat.START);

                    } else {
                        mDrawerLayout.openDrawer(GravityCompat.START);
                    }
                }else if (language.equalsIgnoreCase("Ar")){
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
                        mDrawerLayout.closeDrawer(GravityCompat.END);

                    } else {
                        mDrawerLayout.openDrawer(GravityCompat.END);
                    }
                }



//                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
//                    mDrawerLayout.closeDrawer(GravityCompat.START);
//
//                } else {
//                    mDrawerLayout.openDrawer(GravityCompat.START);
//                }

            }
        });

    }

    public void menuclick(){

        if (language.equalsIgnoreCase("En")) {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);

            } else {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        }else if (language.equalsIgnoreCase("Ar")){
            if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
                mDrawerLayout.closeDrawer(GravityCompat.END);

            } else {
                mDrawerLayout.openDrawer(GravityCompat.END);
            }
        }

    }

    public void configureDrawer() {
        // Configure drawer
        mDrawerLayout = (DrawerLayout) findViewById(com.cs.broastedexpress.R.id.drawer_layout);

        mDrawerToggle = new ActionBarDrawerToggle(Home.this, mDrawerLayout,
                com.cs.broastedexpress.R.string.drawer_open, com.cs.broastedexpress.R.string.drawer_closed) {

            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
//                mTitleTV.setText(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
//                mTitleTV.setText(mAppTitle);
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            view.setSelected(true);
            selectItem(position);
        }
    }

    public void selectItem(int position) {

        // update the main content by replacing fragments
        switch (position) {
            case 0:
                if (language.equalsIgnoreCase("En")) {
                    langAr.setVisibility(View.VISIBLE);
                    langEng.setVisibility(View.GONE);
                } else if (language.equalsIgnoreCase("Ar")) {
                    langAr.setVisibility(View.GONE);
                    langEng.setVisibility(View.VISIBLE);
                }
                mTitleTV.setVisibility(View.GONE);
                mTitle = mSidemenuTitles1[0];
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);
                Fragment unitsfragment = new HomeFragment();
//                mdrawer_layoutbg.setBackground(getResources().getDrawable(R.drawable.bg));
                morderbg1.setBackground(getResources().getDrawable(com.cs.broastedexpress.R.drawable.bg2));

                mview.setVisibility(View.VISIBLE);
                fragmentManager.beginTransaction().replace(com.cs.broastedexpress.R.id.content_frame, unitsfragment).commit();
                mNotification.setVisibility(View.VISIBLE);
                messagecount.setVisibility(View.VISIBLE);
                header_order.setVisibility(View.GONE);

                if(Projecturl.messageCount==0) {
                    messagecount.setVisibility(View.GONE);
                }else {
                    messagecount.setText("" + Projecturl.messageCount);
                }
//
//                AnalyticsManager.sendScreenView("Units Screen");
                break;
            case 1:
                langAr.setVisibility(View.GONE);
                langEng.setVisibility(View.GONE);
//                if(language.equalsIgnoreCase("En")){
//                    mTitleTV.setText("Order");
//                }else if(language.equalsIgnoreCase("Ar")) {
//                    mTitleTV.setText("الطلب");
//                }

                mTitleTV.setVisibility(View.GONE);

                header_order.setVisibility(View.VISIBLE);

                mTitle = mSidemenuTitles1[1];
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);
                Fragment orderFragment = new OrderFragment();
                morderbg1.setBackground(getResources().getDrawable(com.cs.broastedexpress.R.drawable.bg2));

                mview.setVisibility(View.GONE);
                fragmentManager.beginTransaction().replace(com.cs.broastedexpress.R.id.content_frame, orderFragment).commit();
                mNotification.setVisibility(View.GONE);
                messagecount.setVisibility(View.GONE);

//                AnalyticsManager.sendScreenView("Formulas Screen");
                break;

            case 2:
                langAr.setVisibility(View.GONE);
                langEng.setVisibility(View.GONE);
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);

                Intent a = new Intent(Home.this, InsideMenu.class);
                a.putExtra("catid", 9);
                startActivity(a);
                mview.setVisibility(View.VISIBLE);
                mNotification.setVisibility(View.GONE);
                messagecount.setVisibility(View.GONE);
                break;

            case 3:
                langAr.setVisibility(View.GONE);
                langEng.setVisibility(View.GONE);
                mTitleTV.setVisibility(View.VISIBLE);
                if (language.equalsIgnoreCase("En")) {
                    mTitleTV.setText("More");
                } else if (language.equalsIgnoreCase("Ar")) {
                    mTitleTV.setText("المزيد");
                }

                mTitle = mSidemenuTitles1[3];
                mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);
                Fragment colorFragment = new MoreFragment();
                morderbg1.setBackground(getResources().getDrawable(com.cs.broastedexpress.R.drawable.bg2));

                mview.setVisibility(View.VISIBLE);
                header_order.setVisibility(View.GONE);
                fragmentManager.beginTransaction().replace(com.cs.broastedexpress.R.id.content_frame, colorFragment).commit();
                mNotification.setVisibility(View.GONE);
                messagecount.setVisibility(View.GONE);

//                AnalyticsManager.sendScreenView("Color Coding Screen");
                break;

            case 4:

//                mTitle = mAppTitle;
                //mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);


                new Handler().postDelayed(new Runnable() {

                    // Using handler with postDelayed called runnable run method

                    @Override
                    public void run() {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT,
                                "https://play.google.com/store/apps/details?id=com.cs.boastedexpress&hl=en");
                        intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                                "Check out Oregano Pizza Delivery App !");
                        startActivity(Intent.createChooser(intent, "Share"));
                    }
                }, 200);
                break;
            default:
                break;

            case 5:
//                mTitle = mAppTitle;
                //mDrawerList.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerLinear);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cs.boastedexpress")));

                new Handler().postDelayed(new Runnable() {

                    // Using handler with postDelayed called runnable run method

                    @Override
                    public void run() {
//                        AppRater.app_launched(MainActivity.this);
                    }
                }, 200);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(com.cs.broastedexpress.R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_home, menu);
//        return true;
//  }

//    @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.

//        return super.onOptionsItemSelected(item);

    //noinspection SimplifiableIfStatement}

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLoginStatus = userPrefs.getString("login_status", "");
        if (requestCode == MESSAGE_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Intent intent = new Intent(Home.this, Notification.class);
            startActivity(intent);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Projecturl.messageCount==0) {
            messagecount.setVisibility(View.GONE);
        }else {
            messagecount.setText("" + Projecturl.messageCount);
        }
    }
}

