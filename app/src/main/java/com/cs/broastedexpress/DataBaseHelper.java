package com.cs.broastedexpress;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.cs.broastedexpress.Model.Orders;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 11/7/2016.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    private static String DB_PATH = "/data/data/com.cs.drcafe/databases/";

    private static String DB_NAME = "drCAFE.db";

    private SQLiteDatabase myDataBase;

    private final Context myContext;

    public DataBaseHelper(Context context) {
        super(context, "BroastedExpress.db", null, 6);
        this.myContext = context;
    }

    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if(dbExist){
            //do nothing - database already exist
        }else{

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try{
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

        }catch(SQLiteException e){

            //database does't exist yet.

        }


        if(checkDB != null){
            if(checkDB.getVersion() != 6){
                checkDB.close();
                return false;
            }else{
                checkDB.close();
            }


        }


        return checkDB != null ? true : false;
    }

    public void openDataBase() throws SQLException {

        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

    }

    @Override
    public synchronized void close() {

        if(myDataBase != null)
            myDataBase.close();

        super.close();

    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String qurey;
        qurey = "CREATE TABLE \"OrderTable\"(\"orderId\"INTEGER PRIMARY KEY  AUTOINCREMENT ,\"itemId\" INTEGER,\"itemTypeId\" INTEGER, \"subCategoryId\" INTEGER,\"additionals\" VARCHAR,\"qty\"VARCHAR,\"price\" INTEGER,\"additionalsPrice\" INTEGER,\"additionalsTypeId\" TEXT DEFAULT 0,\"totalAmount\" INTEGER,\"comment\" TEXT DEFAULT 0,\"status\" TEXT , \"creationDate\" TEXT DEFAULT 0, \"modifiedDate\" INTEGER DEFAULT 0,\"categoryId\" TEXT,\"itemName\" TEXT DEFAULT 0,\"itemNameAr\" TEXT,\"image\" VARCHAR,\"nonDelivery\" TEXT,\"AdditionalName\" TEXT DEFAULT 0,\"AdditionalNameAr\" TEXT,\"Additionalimage\" TEXT )";
        database.execSQL(qurey);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        String qurey;
        qurey = "DROP TABLE IF EXISTS OrderTable";
        database.execSQL(qurey);
        onCreate(database);
    }

    public void insertOrder(HashMap<String, String> qureyValue) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("itemId", qureyValue.get("itemId"));
        values.put("itemTypeId", qureyValue.get("itemTypeId"));
        values.put("subCategoryId", qureyValue.get("subCategoryId"));
        values.put("additionals", qureyValue.get("additionals"));
        values.put("qty", qureyValue.get("qty"));
        values.put("price", qureyValue.get("price"));
        values.put("additionalsPrice", qureyValue.get("additionalsPrice"));
        values.put("additionalsTypeId", qureyValue.get("additionalsTypeId"));
        values.put("totalAmount", qureyValue.get("totalAmount"));
        values.put("comment", qureyValue.get("comment"));
        values.put("status", qureyValue.get("status"));
        values.put("creationDate", qureyValue.get("creationDate"));
        values.put("modifiedDate", qureyValue.get("modifiedDate"));
        values.put("categoryId", qureyValue.get("categoryId"));
        values.put("itemName", qureyValue.get("itemName"));
        values.put("itemNameAr", qureyValue.get("itemNameAr"));
        values.put("image", qureyValue.get("image"));
        values.put("nonDelivery",qureyValue.get("nonDelivery"));
        values.put("AdditionalName", qureyValue.get("AdditionalName"));
        values.put("AdditionalNameAr", qureyValue.get("AdditionalNameAr"));
        values.put("Additionalimage", qureyValue.get("Additionalimage"));
        database.insert("OrderTable", null, values);
        database.close();
    }


    public ArrayList<Orders> getOrderInfo() {
        ArrayList<Orders> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Orders sc = new Orders();
                sc.setOrderId(cursor.getString(0));
                sc.setItemId(cursor.getString(1));
                sc.setItemTypeId(cursor.getString(2));
                sc.setSubCatId(cursor.getString(3));
                sc.setAdditionals(cursor.getString(4));
                sc.setQty(cursor.getString(5));
                sc.setPrice(cursor.getString(6));
                sc.setAdditionalsPrice(cursor.getString(7));
                sc.setAdditionalsTypeId(cursor.getString(8));
                sc.setTotalAmount(cursor.getString(9));
                sc.setComment(cursor.getString(10));
                sc.setStatus(cursor.getString(11));
                sc.setCreationDate(cursor.getString(12));
                sc.setModifiedDate(cursor.getString(13));
                sc.setCategoryId(cursor.getString(14));
                sc.setItemName(cursor.getString(15));
                sc.setItemNameAr(cursor.getString(16));
                sc.setImage(cursor.getString(17));
                sc.setNonDelivery(cursor.getString(18));
                sc.setAdditionalName(cursor.getColumnName(19));
                sc.setAdditionalNameAr(cursor.getColumnName(20));
                sc.setAdditionalimage(cursor.getColumnName(21));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public ArrayList<Orders> getNonDeliveryItems() {
        ArrayList<Orders> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where nonDelivery=?;";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, new String[] {"false"});
        if (cursor.moveToFirst()) {
            do {
                Orders sc = new Orders();
                sc.setOrderId(cursor.getString(0));
                sc.setItemId(cursor.getString(1));
                sc.setItemTypeId(cursor.getString(2));
                sc.setSubCatId(cursor.getString(3));
                sc.setAdditionals(cursor.getString(4));
                sc.setQty(cursor.getString(5));
                sc.setPrice(cursor.getString(6));
                sc.setAdditionalsPrice(cursor.getString(7));
                sc.setAdditionalsTypeId(cursor.getString(8));
                sc.setTotalAmount(cursor.getString(9));
                sc.setComment(cursor.getString(10));
                sc.setStatus(cursor.getString(11));
                sc.setCreationDate(cursor.getString(12));
                sc.setModifiedDate(cursor.getString(13));
                sc.setCategoryId(cursor.getString(14));
                sc.setItemName(cursor.getString(15));
                sc.setItemNameAr(cursor.getString(16));
                sc.setImage(cursor.getString(17));
                sc.setNonDelivery(cursor.getString(18));
                sc.setAdditionalName(cursor.getColumnName(19));
                sc.setAdditionalNameAr(cursor.getColumnName(20));
                sc.setAdditionalimage(cursor.getColumnName(21));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }


    public int getTotalOrderQty() {
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable", null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        } catch (SQLiteException sqle) {
            sqle.printStackTrace();
        }
        return qty;
    }

    public float getTotalOrderPrice() {
        float qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT SUM(totalAmount) FROM OrderTable", null);
        if (cur.moveToFirst()) {
            qty = cur.getFloat(0);
        }


        return qty;
    }

    public void updateOrder(String qty, String price, String orderId, String size) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET qty =" + qty + ",totalAmount =" + price + " where itemId =" + orderId + " AND itemTypeId =" + size;
        database.execSQL(updateQuery);
    }

    public void updateOrderwithComment(String qty, String price, String orderId, String size, String comment) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET qty =" + qty + ",totalAmount =" + price + ",comment = '" + comment + "' where itemId =" + orderId + " AND itemTypeId =" + size;
        database.execSQL(updateQuery);
    }

    public int getItemOrderCount(String itemId) {
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where itemId=" + itemId;
        Cursor c = database.rawQuery(deleteQuery, null);
        if (c.moveToFirst()) {
            cnt = c.getInt(0);
        }
        return cnt;
    }

    public String getComment(String itemId, String size) {
        String cnt = "";
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT comment FROM  OrderTable where itemId=" + itemId + " AND itemTypeId =" + size;
        Cursor c = database.rawQuery(deleteQuery, null);
        if (c.moveToFirst()) {
            cnt = c.getString(0);
        }
        return cnt;
    }

    public int getItemOrderCountWithSize(String itemId, String size) {
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where itemId=" + itemId + " AND itemTypeId =" + size;
        Cursor c = database.rawQuery(deleteQuery, null);
        try {
            if (c.moveToFirst()) {
                cnt = c.getInt(0);
            }
        } finally {
            c.close();
        }
        return cnt;
    }
//    public int getTotalOrderQty(){
//        int qty = 0;
//        String selectQuery = "SELECT itemId FROM  OrderTable";
//        SQLiteDatabase database = this.getWritableDatabase();
//        try {
//            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable", null);
//
//            if (cur.moveToFirst()) {
//                qty = cur.getInt(0);
//            }
//        }catch (SQLiteException sqle){
//            sqle.printStackTrace();
//        }
//        return qty;
//    }


    public int getCategoryQty(String catId) {
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable where categoryId = " + catId, null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        } catch (SQLiteException sqle) {
            sqle.printStackTrace();
        }
        return qty;
    }

//    public float getTotalOrderPrice(){
//        float qty = 0;
//        String selectQuery = "SELECT itemId FROM  OrderTable";
//        SQLiteDatabase database = this.getWritableDatabase();
//        Cursor cur = database.rawQuery("SELECT SUM(totalAmount) FROM OrderTable", null);
//        if(cur.moveToFirst())
//        {
//            qty  = cur.getFloat(0);
//        }
//
//        return qty;
//    }


    public void deleteItemFromOrder(String orderId, String size) {
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where itemId = " + orderId + " AND itemTypeId =" + size;
        database.execSQL(updateQuery);

    }

    public void deleteOrderTable() {
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable";
        database.execSQL(updateQuery);
    }

    public void updateComment(String orderId, String comment, String size) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET comment = '" + comment + "' where orderId =" + orderId + " AND itemTypeId =" + size;
        database.execSQL(updateQuery);
    }


    public ArrayList<Orders> getItemOrderInfo(String itemId) {
        ArrayList<Orders> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where itemId = " + itemId;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Orders sc = new Orders();
                sc.setOrderId(cursor.getString(0));
                sc.setItemId(cursor.getString(1));
                sc.setItemTypeId(cursor.getString(2));
                sc.setSubCatId(cursor.getString(3));
                sc.setAdditionals(cursor.getString(4));
                sc.setQty(cursor.getString(5));
                sc.setPrice(cursor.getString(6));
                sc.setAdditionalsPrice(cursor.getString(7));
                sc.setAdditionalsTypeId(cursor.getString(8));
                sc.setTotalAmount(cursor.getString(9));
                sc.setComment(cursor.getString(10));
                sc.setStatus(cursor.getString(11));
                sc.setCreationDate(cursor.getString(12));
                sc.setModifiedDate(cursor.getString(13));
                sc.setCategoryId(cursor.getString(14));
                sc.setItemName(cursor.getString(15));
                sc.setItemNameAr(cursor.getString(16));
                sc.setImage(cursor.getString(17));
                sc.setNonDelivery(cursor.getString(18));
                sc.setAdditionalName(cursor.getColumnName(19));
                sc.setAdditionalNameAr(cursor.getColumnName(20));
                sc.setAdditionalimage(cursor.getColumnName(21));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

}
