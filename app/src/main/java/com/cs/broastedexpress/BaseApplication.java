package com.cs.broastedexpress;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.cs.broastedexpress.Widgets.FontOverride;

/**
 * Created by CS on 12/14/2016.
 */

public class BaseApplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);


//        Fabric.with(this, new Crashlytics());
        FontOverride.setDefaultFont(this, "SERIF", "helveticaneue_regular.ttf");
    }
}
